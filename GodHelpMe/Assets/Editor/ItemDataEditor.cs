﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using TKFileManager;

#if UNITY_EDITOR
public class ItemDataEditor : EditorWindow
{

    public ItemData itemData = new ItemData();

    private string dataProjectFilePath = "Resources/Data/ToolItems";

    private string[] pricesName = new string[] { "0", "10", "20", "30", "50", "100" };

    private int[] prices = new int[] { 0, 10, 20, 30, 50, 100 };

    private string[] saleCountNames = new string[] { "0", "10", "20", "30", "50", "100" };

    private int[] saleCounts = new int[] { 0, 10, 20, 30, 50, 100 };

    private Sprite icon;
    
    [MenuItem("Window/Item Editor")]

    static void Init()
    {
        // EditorWindow.GetWindow (typeof (ItemDataEditor)).ShowPopup ();
        Rect windowRect = new Rect(Vector2.zero, new Vector2(400, 400));
        EditorWindow.GetWindowWithRect<ItemDataEditor>(windowRect, true, "Item Editor", true);
    }

    void OnGUI()
    {

        GUILayout.Space(10.0f);
        GUILayout.Label("Item Editor");
        GUILayout.Space(10.0f);        
        icon = (Sprite)EditorGUILayout.ObjectField("Choose Icon", icon, typeof(Sprite), true);
        if (icon)
        {
            string path = AssetDatabase.GetAssetPath(icon);
            itemData.iconPath = path.Replace("Assets/Resources/", "").Replace(".png", "");
        }
        
        GUILayout.Space(10.0f);
        itemData.name = EditorGUILayout.TextField("Name", itemData.name);
        GUILayout.Space(10.0f);
        itemData.price = EditorGUILayout.IntPopup("Price", itemData.price, pricesName, prices);
        GUILayout.Space(10.0f);
        itemData.count = EditorGUILayout.IntPopup("Sale Count", itemData.count, saleCountNames, saleCounts);
        GUILayout.Space(10.0f);
        itemData.info = EditorGUILayout.TextField("Introduce", itemData.info, GUILayout.MinHeight(100f));
        if (itemData != null)
        {
            // SerializedObject serializedObject = new SerializedObject (this);
            // SerializedProperty serializedProperty = serializedObject.FindProperty ("itemData");
            // EditorGUILayout.PropertyField (serializedProperty, true);
            // serializedObject.ApplyModifiedProperties ();
            if (GUILayout.Button("Save Item"))
            {
                SaveData();
            }
        }

        if (GUILayout.Button("Load Item"))
        {
            LoadData();
        }

    }

    private void SaveData()
    {

        string path = Application.dataPath + "/" + dataProjectFilePath + "/" + TKManager.GetUniqueID() + ".json";

        string dataAsJson = JsonUtility.ToJson(itemData);

        try
        {

            // Delete the file if it exists.
            if (File.Exists(path))
            {
                // Note that no lock is put on the
                // file and the possibility exists
                // that another process could do
                // something with it between
                // the calls to Exists and Delete.
                File.Delete(path);
            }

            // Create the file.
            using (FileStream fs = File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(dataAsJson);
                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }

            AssetDatabase.Refresh();

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        Debug.LogWarning("Save File Succeed. Please Note your file of fodler");

    }

    private void LoadData()
    {
        string path = Application.dataPath + "/" + dataProjectFilePath + "/" + itemData.name + ".json";

        try
        {

            // Delete the file if it exists.
            if (File.Exists(path))
            {
                // Open the stream and read it back.
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        itemData = JsonUtility.FromJson<ItemData>(s);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}
#endif