﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
public class DeveloperSystemManager : MonoBehaviour {
    [MenuItem ("Developer System Manager/PlayerPrefs/Clean")]
    static void CeleanPlayerPrefs () {
        PlayerPrefs.DeleteAll();
        Debug.Log ("Clean succeed all player prefs data.");
    }
}
#endif