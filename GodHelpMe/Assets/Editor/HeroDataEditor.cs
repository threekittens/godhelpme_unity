﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Text;
using TKFileManager;
using System.Linq;

/// <summary>
/// The partial is for defin parameters
/// </summary>
public partial class HeroDataEditor : EditorWindow
{
    private HeroData heroData = new HeroData();
    private Sprite cover;
    private Sprite picture;
    private GameObject heroPrefab;
    private Vector2 scrollPos;
    private int index;
    private int skillMaximum = 4;
    private List<HeroSkillData> skills = new List<HeroSkillData>();
    private List<Sprite> skillIcons = new List<Sprite>();
}


/// <summary>
/// The partial is for script start.
/// </summary>
public partial class HeroDataEditor : EditorWindow
{

    [MenuItem("Window/Hero Editor")]
    static void Init()
    {
        EditorWindow.GetWindow<HeroDataEditor>(true, "Hero Editor");
    }

    void OnGUI()
    {
        GUILayout.Space(10.0f);
        GUILayout.Label("Hero Editor");

        GUILayout.Space(10.0f);
        EditorGUILayout.BeginHorizontal();
        cover = (Sprite)EditorGUILayout.ObjectField("Cover of Hero", cover, typeof(Sprite), true);
        if (cover)
        {
            string path = AssetDatabase.GetAssetPath(cover);
            heroData.coverPath = path.Replace("Assets/Resources/", "").Replace(".png", "");
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(10.0f);

        picture = (Sprite)EditorGUILayout.ObjectField("Picture of Hero", picture, typeof(Sprite), true);
        if (picture)
        {
            string path = AssetDatabase.GetAssetPath(picture);
            heroData.picturePath = path.Replace("Assets/Resources/", "").Replace(".png", "");
        }

        GUILayout.Space(10.0f);
        heroPrefab = (GameObject)EditorGUILayout.ObjectField("Object of Hero", heroPrefab, typeof(GameObject), true);
        if (heroPrefab)
        {
            string path = AssetDatabase.GetAssetPath(heroPrefab);
            heroData.prefabPath = path;
            Debug.Log("prefabPath => " + heroData.prefabPath.Replace("Assets/Resources/", "").Replace(".prefab", ""));

            // if id is empty, get a unique id.
            if (heroData.id == default(string))
            {
                heroData.id = TKManager.GetUniqueID();
            }
        }

        GUILayout.Space(10.0f);
        heroData.id = EditorGUILayout.TextField("File ID.", heroData.id);

        GUILayout.Space(10.0f);
        heroData.price = EditorGUILayout.IntField("Price", heroData.price);

        GUILayout.Space(10.0f);
        heroData.name = EditorGUILayout.TextField("Hero Name", heroData.name);

        GUILayout.Space(10.0f);
        heroData.introduce = EditorGUILayout.TextField("Introduce", heroData.introduce, GUILayout.MinHeight(100f));

        GUILayout.Space(10.0f);
        EditorGUILayout.BeginHorizontal();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        if (skills != null)
        {
            if (skills.Count > 0)
            {
                for (int i = 0; i < skills.Count; i++)
                {
                    // if id is empty, get a unique id.
                    if (skills[i].id == default(string))
                    {
                        skills[i].id = TKManager.GetUniqueID();
                    }

                    GUILayout.Label("Skill " + (i + 1));

                    GUILayout.Space(10.0f);
                    skillIcons[i] = (Sprite)EditorGUILayout.ObjectField("Sprite", skillIcons[i], typeof(Sprite), true);

                    if (skillIcons[i])
                    {
                        string path = AssetDatabase.GetAssetPath(skillIcons[i]);
                        skills[i].iconPath = path.Replace("Assets/Resources/", "").Replace(".png", "");
                    }

                    GUILayout.Space(10.0f);
                    skills[i].id = EditorGUILayout.TextField("ID", skills[i].id);

                    GUILayout.Space(10.0f);
                    skills[i].name = EditorGUILayout.TextField("Name", skills[i].name);

                    GUILayout.Space(10.0f);
                    skills[i].info = EditorGUILayout.TextField("Introduce", skills[i].info, GUILayout.MinHeight(100f));

                }
            }

            heroData.skills = skills.ToArray();
        }
        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10.0f);
        if (GUILayout.Button("+", GUILayout.MaxHeight(50f)))
        {
            if (index >= skillMaximum)
            {
                Debug.LogWarning("NOTE! skill maximum is 4, you can't add more then 4");
                return;
            }

            HeroSkillData skill = new HeroSkillData();
            skills.Add(skill);
            skillIcons.Add(null);
            index++;
        }
        GUILayout.Space(10.0f);
        if (GUILayout.Button("-", GUILayout.MaxHeight(50f)))
        {
            if (index <= 0)
            {
                return;
            }
            skills.RemoveAt(index - 1);
            skillIcons.RemoveAt(index - 1);
            index--;
        }
        GUILayout.Space(10.0f);
        if (heroData != null)
        {
            if (GUILayout.Button("Save Item", GUILayout.MaxHeight(50f)))
            {
                SaveData();
            }
        }
        GUILayout.Space(10.0f);
        EditorGUILayout.EndHorizontal();
    }        
}

/// <summary>
/// The partial is for save of script.
/// </summary>
public partial class HeroDataEditor : EditorWindow
{
    private void SaveData()
    {
        string path = Application.dataPath + "/Resources/Data/Heros/" + heroData.id + ".json";
        Debug.Log(path);

        string dataAsJson = JsonUtility.ToJson(heroData);

        Debug.Log(dataAsJson);

        try
        {

            // Delete the file if it exists.
            if (File.Exists(path))
            {
                // Note that no lock is put on the
                // file and the possibility exists
                // that another process could do
                // something with it between
                // the calls to Exists and Delete.
                File.Delete(path);
            }

            // Create the file.
            using (FileStream fs = File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(dataAsJson);
                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }

            AssetDatabase.Refresh();

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        Debug.LogWarning("Save File Succeed. Please Note your file of fodler");

    }
}

/// <summary>
/// The partial is for load of script.
/// </summary>
public partial class HeroDataEditor : EditorWindow
{
    //private void LoadData()
    //{
    //    string path = Application.dataPath + "/" + dataProjectFilePath + "/" + itemData.name + ".json";

    //    try
    //    {

    //        // Delete the file if it exists.
    //        if (File.Exists(path))
    //        {
    //            // Open the stream and read it back.
    //            using (StreamReader sr = File.OpenText(path))
    //            {
    //                string s = "";
    //                while ((s = sr.ReadLine()) != null)
    //                {
    //                    itemData = JsonUtility.FromJson<ItemData>(s);
    //                }
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Console.WriteLine(ex.ToString());
    //    }
    //}
}

/// <summary>
/// The partial is for save of script.
/// </summary>
public partial class HeroDataEditor : EditorWindow
{
    
}