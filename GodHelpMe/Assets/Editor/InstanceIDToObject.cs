﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InstanceIDToObject : EditorWindow
{

    static int id;

    [MenuItem("Developer System Manager/Find Object")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        InstanceIDToObject window = (InstanceIDToObject)EditorWindow.GetWindow(typeof(InstanceIDToObject));
        window.Show();
    }

    void OnGUI()
    {
        id = EditorGUILayout.IntField("Instance ID:", id);
        if (GUILayout.Button("Find Object"))
        {
            Object obj = EditorUtility.InstanceIDToObject(id);

            if (!obj)
                Debug.LogError("No object could be found with instance id: " + id);
            else
                Debug.Log("Object's name: " + obj.name);
                Selection.activeObject = obj;


            //if (Help.HasHelpForObject(obj))
            //{
            //    Help.ShowHelpForObject(obj);
            //}

        }
    }

    void OnInspectorUpdate()
    {
        Repaint();
    }
}
