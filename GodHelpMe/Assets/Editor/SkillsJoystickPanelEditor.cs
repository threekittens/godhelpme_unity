﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;

#if UNITY_EDITOR
using UnityEditor;
#endif

public partial class SkillsJoystickPanelEditor : EditorWindow
{
    private HeroMultipleSkillDatas heroMultipleSkillDatas = new HeroMultipleSkillDatas();
    private TextAsset mHeroJSONFile;
    private GameObject mSkillJoystickPanel;
    private GameObject[] mSkillJoysticks = new GameObject[4];
    private GameObject[] mSkillJoystickImages = new GameObject[4];
    private SingleSkillData[] skillDatas = new SingleSkillData[4];
    private string[] mSkillAreaElementList = System.Enum.GetNames(typeof(SkillAreaType));
    private string[] mSkillTypeList = System.Enum.GetNames(typeof(SkillType));

    private enum SKillAreaAngleElement
    { 
        Sector60 = 60,        // 扇形
        Sector120 = 120,        // 扇形
    }

    private string[] mAngleNameType = System.Enum.GetNames(typeof(SKillAreaAngleElement));
    private int[] mAngleType = (int[])System.Enum.GetValues(typeof(SKillAreaAngleElement));
}

public partial class SkillsJoystickPanelEditor : EditorWindow
{

    [MenuItem("Window/Skills Joystick Panel Editor")]
    static void Init()
    {
        GetWindow<SkillsJoystickPanelEditor>(true, "Skills Joystick Panel Editor");
        
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        mHeroJSONFile = (TextAsset)EditorGUILayout.ObjectField("Hero JSON File", mHeroJSONFile, typeof(TextAsset), true);
        mSkillJoystickPanel = (GameObject)EditorGUILayout.ObjectField("Single Skill Joystick", mSkillJoystickPanel, typeof(GameObject), true);

        if (mSkillJoystickPanel && mHeroJSONFile)
        {
            string JSON = mHeroJSONFile.text;
            HeroData heroData = JsonUtility.FromJson<HeroData>(JSON);
            heroMultipleSkillDatas.heroID = heroData.id;

            for (int i = 0; i < mSkillJoystickPanel.transform.childCount; i++)
            {
                mSkillJoysticks[i] = mSkillJoystickPanel.transform.GetChild(i).gameObject;
                mSkillJoystickImages[i] = mSkillJoysticks[i].transform.GetChild(0).gameObject;
                SkillArea skillArea = mSkillJoystickImages[i].GetComponent<SkillArea>();
                DetectEnemy detectEnemy = mSkillJoystickImages[i].GetComponent<DetectEnemy>();

                SingleSkillData singleSkillData = new SingleSkillData();

                singleSkillData.areaType = (int)skillArea.areaType;                
                EditorGUILayout.Space();                
                GUILayout.Label(string.Format("{0}", detectEnemy.skillType));
                EditorGUILayout.BeginVertical();

                singleSkillData.id = EditorGUILayout.TextField("Skill ID", heroData.skills[i].id);

                singleSkillData.areaType = EditorGUILayout.Popup("Skill Area Type", singleSkillData.areaType, mSkillAreaElementList);

                skillArea.areaType = (SkillAreaType)singleSkillData.areaType;

                switch (skillArea.areaType)
                {
                    case SkillAreaType.OuterCircle:
                        singleSkillData.outerRadius = EditorGUILayout.FloatField("Outher Radius", skillArea.outerRadius);
                        singleSkillData.detectRange = skillArea.outerRadius;
                        singleSkillData.detectRange = EditorGUILayout.FloatField("Detect Range", singleSkillData.detectRange);
                        break;
                    case SkillAreaType.OuterCircle_InnerCircle:
                        singleSkillData.outerRadius = EditorGUILayout.FloatField("Outher Radius", skillArea.outerRadius);
                        singleSkillData.innerRadius = EditorGUILayout.FloatField("Inner Radius", skillArea.innerRadius);
                        singleSkillData.detectRange = skillArea.outerRadius;
                        singleSkillData.detectRange = EditorGUILayout.FloatField("Detect Range", singleSkillData.detectRange);
                        break;
                    case SkillAreaType.OuterCircle_InnerCube:
                        singleSkillData.outerRadius = EditorGUILayout.FloatField("Outher Radius", skillArea.outerRadius);
                        singleSkillData.cubeWidth = EditorGUILayout.FloatField("Inner Radius", skillArea.cubeWidth);
                        singleSkillData.detectRange = EditorGUILayout.FloatField("Detect Range", detectEnemy.range);
                        break;
                    case SkillAreaType.OuterCircle_InnerSector:
                        singleSkillData.outerRadius = EditorGUILayout.FloatField("Outher Radius", skillArea.outerRadius);
                        singleSkillData.innerRadius = EditorGUILayout.FloatField("Inner Radius", skillArea.innerRadius);
                        singleSkillData.angle = EditorGUILayout.IntPopup("Angle", skillArea.angle, mAngleNameType, mAngleType);
                        singleSkillData.detectRange = EditorGUILayout.FloatField("Detect Range", detectEnemy.range);
                        break;
                }
                                
                singleSkillData.maximumTargets = EditorGUILayout.IntField("Maximum Targets", detectEnemy.maxTargets);

                singleSkillData.skillType = (int)detectEnemy.skillType;
                singleSkillData.skillType = EditorGUILayout.Popup("Skill Type", singleSkillData.skillType, mSkillTypeList);               

                EditorGUILayout.EndVertical();
                EditorGUILayout.Space();

                skillDatas[i] = singleSkillData;
            }

            heroMultipleSkillDatas.skillDatas = skillDatas;

        }

        GUILayout.Space(10.0f);
        if (heroMultipleSkillDatas.skillDatas != null)
        {
            if (GUILayout.Button("Save Skill Datas", GUILayout.MaxHeight(50f)))
            {
                SaveData();
            }
        }
        GUILayout.Space(10.0f);

        EditorGUILayout.EndVertical();
    }
}



public partial class SkillsJoystickPanelEditor : EditorWindow
{

    private void SaveData()
    {
        string path = Application.dataPath + "/Resources/Data/Skills/" + heroMultipleSkillDatas.heroID + ".json";
        Debug.Log(path);

        string dataAsJson = JsonUtility.ToJson(heroMultipleSkillDatas);

        Debug.Log(dataAsJson);

        try
        {

            // Delete the file if it exists.
            if (File.Exists(path))
            {
                // Note that no lock is put on the
                // file and the possibility exists
                // that another process could do
                // something with it between
                // the calls to Exists and Delete.
                File.Delete(path);
            }

            // Create the file.
            using (FileStream fs = File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(dataAsJson);
                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }

            AssetDatabase.Refresh();

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        Debug.LogWarning("Save File Succeed. Please Note your file of fodler");

    }
}