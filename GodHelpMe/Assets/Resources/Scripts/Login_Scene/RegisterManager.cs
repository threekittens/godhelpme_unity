﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegisterManager : MonoBehaviour {
	
	public InputField accountIF;
	public InputField passwordIF;
	public Button registerBtn;

	private string email;
	private string password;

	private void Start()
	{
		accountIF.onEndEdit.AddListener (delegate { AccountInputEvent(accountIF); });
		passwordIF.onEndEdit.AddListener (delegate { PasswordInputEvent(passwordIF); });

		registerBtn.onClick.AddListener(RegisterEvent);
	}

	private void RegisterEvent()
	{
		print ("Here is RegisterEvent !");

		if (email != null && password != null)
			FirebaseManager.Singleton.SignUp (email, password);
	}

	private void AccountInputEvent(InputField input)
	{
		if (input.text.Length > 0) {			
			if (Validator.EmailIsValid(input.text)) 
			{
				email = input.text.ToString();
				print ("Account: " + input.text);
			} 
			else 
			{
				print ("Account not foramt of email");
			}
		} else if (input.text.Length == 0) {
			print ("Account Input Empty");
		}
	}

	private void PasswordInputEvent(InputField input)
	{
		if (input.text.Length > 0)
		{
			password = input.text.ToString();
			print("Password: " + input.text);
		}
		else if (input.text.Length == 0)
		{
			print("Password Input Empty");
		}
	}
}
