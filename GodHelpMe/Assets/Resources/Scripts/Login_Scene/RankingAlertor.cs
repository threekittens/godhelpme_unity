﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingAlertorEventArgs : System.EventArgs
{
    private string message;
    private string id;
    public string Message { get { return message; } set { message = value; } }
    public string Id { get { return id; } set { id = value; } }
    public RankingAlertorEventArgs() { }
    public RankingAlertorEventArgs(string _id, string _message) { Id = _id; Message = _message; }
}

public class RankingAlertor : MonoBehaviour
{
    public static event System.EventHandler OnCancel;
    public static event System.EventHandler OnClose;
    public static event System.EventHandler OnClickedMenu;

    private static GameObject alertor;
    private static TextAsset[] heroDatas;

    private static int displayRowCount = 100;

    private static Sprite[] pictureList;

    private static GameObject loading;

    private static Button perviousBtn;

    public static void Open()
    {
        InitializeAlertor();
    }

    private static void InitializeAlertor()
    {
        pictureList = Resources.LoadAll<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface() + "SpellBook");

        /// Initialize an instance of RankingAlerotr.
        GameObject instance = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterfaceLobyScene("RankingAlertor"));
        alertor = Instantiate(instance, GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform);
        alertor.SetActive(false);

        /// Initialize a cancel button.
        Button cancelButton = alertor.transform.Find("Background/CancelButton").GetComponent<Button>();
        cancelButton.onClick.AddListener(() => Cancel());

        loading = alertor.transform.Find("Background/HeroPlayerRankTable/Table/Loading").gameObject;

        /// Reset the RectTransform of the RankingAlertor.
        RectTransform rect = alertor.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        /// The method is for set hero selection menu.
        SetHeroSelectionMenu();

        /// The method is for set hero player rank table.
        SetHeroPlayerRankTable();

        alertor.SetActive(true);
    }

    private static void SetHeroSelectionMenu()
    {
        heroDatas = Resources.LoadAll<TextAsset>("Data/Heros");
        GameObject content = alertor.transform.Find("Background/HeroSelectionMenu/Content").gameObject;
        GameObject item = alertor.transform.Find("Background/HeroSelectionMenu/Content/Item").gameObject;
        item.SetActive(false);


        for (int i = 0; i < heroDatas.Length; i++)
        {
            string JSON = heroDatas[i].text;
            HeroData heroData = JsonUtility.FromJson<HeroData>(JSON);

            GameObject instance = Instantiate(item, item.transform.parent);

            Image image = instance.GetComponent<Image>();
            Sprite sprtie = Resources.Load<Sprite>(heroData.picturePath);
            image.sprite = sprtie;

            int index = i;
            Button btn = instance.GetComponent<Button>();
            btn.onClick.AddListener(() => HeroSelectionMenuEvent(btn, index));
            if (index == 0)
            {
                btn.gameObject.transform.Find("Frame").gameObject.SetActive(true);
                perviousBtn = btn;
            }

            instance.SetActive(true);
        }

        RectTransform contentRect = content.GetComponent<RectTransform>();
        GridLayoutGroup gridLayoutGroup = content.GetComponent<GridLayoutGroup>();
        float height =
            (gridLayoutGroup.cellSize.y * heroDatas.Length) +
            (gridLayoutGroup.spacing.y * heroDatas.Length - 1) +
            (gridLayoutGroup.padding.top * 2);
        contentRect.sizeDelta = new Vector2(contentRect.rect.width, height);
    }

    private static void HeroSelectionMenuEvent(Button btn, int i)
    {
        if (perviousBtn != null)
        {
            perviousBtn.gameObject.transform.Find("Frame").gameObject.SetActive(false);
        }

        btn.gameObject.transform.Find("Frame").gameObject.SetActive(true);
        perviousBtn = btn;

        loading.SetActive(!loading.activeSelf);

        string JSON = heroDatas[i].text;
        HeroData heroData = JsonUtility.FromJson<HeroData>(JSON);
        RankingAlertorEventArgs args = new RankingAlertorEventArgs();
        args.Id = heroData.id;
        if (OnClickedMenu != null) { OnClickedMenu(null, args); }
    }

    public static void SetDataToUpdateRankPanelBy(List<HeroAchievement> HAList, List<SignedPlayer> SPList)
    {
        Debug.Log("SetDataToUpdateRankPanelBy " + HAList.Count);

        for (int i = 0; i < displayRowCount; i++)
        {
            HeroAchievement HA = null;
            AssignTheHAData(SPList, HA, i);
        }

        for (int i = 0; i < HAList.Count; i++)
        {
            HeroAchievement HA = HAList[i];
            AssignTheHAData(SPList, HA, i);
        }

        SetPlayerOusideRow(HAList);

        loading.SetActive(!loading.activeSelf);
    }

    private static void AssignTheHAData(List<SignedPlayer> SPList, HeroAchievement HA, int i)
    {
        GameObject content = alertor.transform.Find("Background/HeroPlayerRankTable/Table/Content").gameObject;
        int children = content.transform.childCount;

        /// the first row is empty, so here i + 1 mean is the index of the row.
        GameObject row = content.transform.GetChild(i + 1).gameObject;
        //GameObject playerInfo = row.transform.Find("PlayerInfo").gameObject;

        if (FirebaseManager.Singleton)
        {
            Image picture = row.transform.Find("Picture").GetComponent<Image>();

            Debug.Log("SPList.Count  = " + SPList.Count);

            SignedPlayer SP = (HA != null && SPList.Exists(s => s.userid == HA.userid)) ? SPList.Find(s => s.userid == HA.userid) : null;
            picture.sprite = (SP != null) ? pictureList[SP.pictureindex] : Resources.Load<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface("picture"));

            GameObject playerInfo = row.transform.Find("PlayerInfo").gameObject;

            Text name = playerInfo.transform.Find("Name").GetComponent<Text>();
            name.text = (SP != null) ? SP.playername : "-";

            GameObject starPanel = playerInfo.transform.Find("StarPanel").gameObject;

            starPanel.SetActive((HA != null && HA.reincarnation > 0));

            GameObject star = starPanel.transform.Find("Image").gameObject;
            star.SetActive(false);

            if (starPanel.transform.childCount > 0)
            {
                for (int n = 0; n < starPanel.transform.childCount; n++)
                {
                    if (n == 0) { continue; }

                    if (starPanel.transform.GetChild(n))
                    {
                        Destroy(starPanel.transform.GetChild(n).gameObject);
                    }
                }
            }

            if ((HA != null))
            {
                if (HA.reincarnation > 0)
                {
                    for (int n = 0; n < HA.reincarnation; n++)
                    {
                        GameObject instance = Instantiate(star, starPanel.transform);
                        instance.SetActive(true);
                    }
                }
            }
        }

        Text level = row.transform.Find("Level").GetComponent<Text>();
        level.text = (HA != null) ? HA.level.ToString() : "-";

        Text checkpoint = row.transform.Find("CheckpointInfo/Image/Text").GetComponent<Text>();
        checkpoint.text = (HA != null) ? HA.checkpoint.ToString() : "-";

        Text score = row.transform.Find("TotalScoreInfo/Image/Text").GetComponent<Text>();
        score.text = (HA != null) ? SIPrefix.GetInfo(HA.score, 2).AmountWithPrefix : "-";
    }

    private static void SetHeroPlayerRankTable()
    {
        GameObject content = alertor.transform.Find("Background/HeroPlayerRankTable/Table/Content").gameObject;
        GameObject row = content.transform.Find("Row").gameObject;
        row.SetActive(false);

        for (int i = 0; i < displayRowCount; i++)
        {
            // // data change this.
            // int starCount = 0;

            GameObject _row = Instantiate(row, row.transform.parent);
            int n = i % 2;
            if (n == 0)
            {
                _row.GetComponent<Image>().color = Color.clear;
            }

            GameObject rankIcon = _row.transform.Find("RankIcon").gameObject;
            Image RI = rankIcon.GetComponent<Image>();
            RI.enabled = (i > 2) ? false : true;
            RI.sprite = (i > 2) ? null : Resources.Load<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface(string.Format("ranking_{0}", (i + 1))));

            GameObject text = rankIcon.transform.Find("Text").gameObject;
            int rank = i + 1;
            text.GetComponent<Text>().text = rank.ToString();
            text.SetActive((i > 2) ? true : false);

            GameObject playerInfo = _row.transform.Find("PlayerInfo").gameObject;
            GameObject starPanel = playerInfo.transform.Find("StarPanel").gameObject;

            // GameObject star = starPanel.transform.Find("Image").gameObject;
            // star.SetActive(false);

            // if (starCount > 0)
            // {
            //     for (int s = 0; s < starCount; s++)
            //     {
            //         GameObject instance = Instantiate(star, star.transform.parent);
            //         instance.SetActive(true);
            //     }
            // }
            // else
            // {
            //     starPanel.SetActive(false);
            // }

            starPanel.SetActive(false);

            _row.SetActive(true);
        }

        RectTransform contentRect = content.GetComponent<RectTransform>();
        GridLayoutGroup gridLayoutGroup = content.GetComponent<GridLayoutGroup>();
        float height =
            (gridLayoutGroup.cellSize.y * displayRowCount) +
            (gridLayoutGroup.spacing.y * displayRowCount - 1) +
            (gridLayoutGroup.padding.top * 2);
        contentRect.sizeDelta = new Vector2(contentRect.rect.width, height);
    }

    public static void SetPlayerOusideRow(List<HeroAchievement> HAList)
    {
        int rank = -1;
        HeroAchievement HA = null;

        if (HAList.Exists(ha => (ha.userid == FirebaseManager.Singleton.SignedPlayer.userid)))
        {
            HA = HAList.Find(ha => (ha.userid == FirebaseManager.Singleton.SignedPlayer.userid));
            rank = HAList.IndexOf(HA);
        }

        GameObject OR = alertor.transform.Find("Background/HeroPlayerRankTable/OutsideRow").gameObject;

        Image RI = OR.transform.Find("RankIcon").GetComponent<Image>();
        RI.enabled = (rank > 2 || rank <= -1) ? false : true;
        RI.sprite = (rank > 2 || rank <= -1) ? null : Resources.Load<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface(string.Format("ranking_{0}", (rank + 1))));

        Text RIT = RI.transform.Find("Text").GetComponent<Text>();
        RIT.text = (rank >= 0) ? (rank + 1).ToString() : "-";
        RIT.gameObject.SetActive(!RI.enabled);

        if (FirebaseManager.Singleton)
        {
            Image picture = OR.transform.Find("Picture").GetComponent<Image>();
            picture.sprite = pictureList[FirebaseManager.Singleton.SignedPlayer.pictureindex];

            GameObject playerInfo = OR.transform.Find("PlayerInfo").gameObject;

            Text name = playerInfo.transform.Find("Name").GetComponent<Text>();
            name.text = FirebaseManager.Singleton.SignedPlayer.playername;

            GameObject starPanel = playerInfo.transform.Find("StarPanel").gameObject;
            starPanel.SetActive((HA != null && HA.reincarnation > 0));

            GameObject star = starPanel.transform.Find("Image").gameObject;
            star.SetActive(false);

            if (starPanel.transform.childCount > 0)
            {
                for (int n = 0; n < starPanel.transform.childCount; n++)
                {
                    if (n == 0) { continue; }

                    if (starPanel.transform.GetChild(n))
                    {
                        Destroy(starPanel.transform.GetChild(n).gameObject);
                    }
                }
            }

            if ((HA != null))
            {
                if (HA.reincarnation > 0)
                {
                    for (int n = 0; n < HA.reincarnation; n++)
                    {
                        GameObject instance = Instantiate(star, starPanel.transform);
                        instance.SetActive(true);
                    }
                }
            }
        }

        Text level = OR.transform.Find("Level").GetComponent<Text>();
        level.text = (HA != null) ? HA.level.ToString() : "-";

        Text checkpoint = OR.transform.Find("CheckpointInfo/Image/Text").GetComponent<Text>();
        checkpoint.text = (HA != null) ? HA.checkpoint.ToString() : "-";

        Text score = OR.transform.Find("TotalScoreInfo/Image/Text").GetComponent<Text>();
        score.text = (HA != null) ? SIPrefix.GetInfo(HA.score, 2).AmountWithPrefix : "-";
    }

    public static void Close()
    {
        if (OnClose != null) { OnClose(null, System.EventArgs.Empty); }
        DestroyImmediate(alertor);
        Resources.UnloadUnusedAssets();
    }

    private static void Cancel()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
        if (OnCancel != null) { OnCancel(null, System.EventArgs.Empty); }

        Close();
    }
}