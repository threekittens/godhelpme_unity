﻿using Facebook.Unity;
using Google;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Linq;
using UnityEngine.UI;

public enum SignInType
{
    Email,
    Google,
    Facebook,
    Anonymously
}

public enum SignInErrorType
{
    Cancel,
    Fail,
}

public partial class FirebaseManager : MonoBehaviour
{
    [SerializeField]
    private string testUserId;
    private string GoogleWebClientId = "964748321101-3npb553ghi4m43oe4bmf0lgor2sdphos.apps.googleusercontent.com";

    private static FirebaseManager instance;
    public static FirebaseManager Singleton { get { return instance; } }

    private DatabaseReference DBReference;
    //private bool IsSignInFirst;

    public FirebaseAuth auth;
    //public FirebaseUser authedUser;

    public delegate void FirebaseManagerDelegate(bool finished);
    public event FirebaseManagerDelegate OnSignedPlayerUpdated;

    private SignedPlayer s = null;
    public SignedPlayer SignedPlayer
    {
        get
        {
            s = PlayerPrefsUtility.LoadValue<SignedPlayer>(ProjectKeywordList.kPPrefsForPlayerSignedInBefore);
            return s;
        }
        set
        {
            s = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerSignedInBefore, s));
            if (s == null) { return; }
            UpdatePlayerDataInFirebase(s, (complite) =>
            {
                if (OnSignedInFirebase != null) { OnSignedInFirebase(this.SignInType); }
                if (OnSignedPlayerUpdated != null) { OnSignedPlayerUpdated(complite); }
            });
        }
    }

    private SignInType signInType;
    public SignInType SignInType { get { return signInType; } set { signInType = value; } }

    public event EventHandler OutedSignIn;

    public delegate void SyncDataToFirebase(PlayerAttributeManager PAM, PAMType type);
    public SyncDataToFirebase OnSyncDataToFirebase;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    private void Start()
    {
        Debug.Log("Started the Firebase Manager.");
        InitializeFirebase();
    }

    // Handle initialization of the necessary firebase modules:
    private void InitializeFirebase()
    {
        //Debug.Log("Setting up Firebase Auth");
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://godsaveme-9e021.firebaseio.com");

        FirebaseApp.DefaultInstance.SetEditorP12FileName("GodSaveMe-e77099990b3b.p12");
        FirebaseApp.DefaultInstance.SetEditorServiceAccountEmail("firebase-adminsdk-8nexm@godsaveme-9e021.iam.gserviceaccount.com");
        FirebaseApp.DefaultInstance.SetEditorP12Password("notasecret");

        DBReference = FirebaseDatabase.DefaultInstance.RootReference;
        // Get the Auth class for your App.        
        auth = FirebaseAuth.GetAuth(FirebaseApp.DefaultInstance);

        OnSyncDataToFirebase = OnSyncDataToFirebaseProcress;

        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, EventArgs.Empty);

        auth.IdTokenChanged += AuthIdTokenChanged;
        AuthIdTokenChanged(this, EventArgs.Empty);

        //IsSignInFirst = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerSignInFirst);        
    }

    private void OnSyncDataToFirebaseProcress(PlayerAttributeManager PAM, PAMType type)
    {
        Debug.Log("OnSyncDataToFirebaseProcress");
        StartCoroutine(SyncDataToFirebaseProcress(PAM, type));
    }

    //Track state changes of the auth object.
    private void AuthStateChanged(object sender, EventArgs eventArgs)
    {
        //Debug.Log("AuthStateChanged");
        if (auth.CurrentUser != null)
        {
            Debug.Log(auth.CurrentUser.UserId);
        }

        //if (auth.CurrentUser != authedUser)
        //{
        //    bool signedIn = authedUser != auth.CurrentUser && auth.CurrentUser != null;

        //    if (!signedIn && authedUser != null)
        //    {
        //        Debug.Log("Signed out " + authedUser.UserId);
        //    }

        //    authedUser = auth.CurrentUser;
        //    if (signedIn)
        //    {
        //        Debug.Log("Signed in " + authedUser.UserId);
        //    }
        //}
    }

    private void AuthIdTokenChanged(object sender, EventArgs eventArgs)
    {
        //Debug.Log("AuthIdTokenChanged");
        if (auth.CurrentUser != null)
        {
            Debug.Log(auth.CurrentUser.UserId);
        }
    }

    private void OnDestroy()
    {
        //auth.StateChanged -= AuthStateChanged;
        //auth = null;
    }

    private void OnApplicationQuit()
    {
        //PlayerPrefsUtility.SaveValue<bool>(ProjectKeywordList.kPPrefsForPlayerSignInFirst, IsSignInFirst);
    }
}

/// <summary>
/// The partial is for send a proving mail to user Email.
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    public event EventHandler SendEmailSucceeded;

    public void SignUp(string email, string password)
    {
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                if (OnCreateAccountFail != null) { OnCreateAccountFail(SignInType.Email, SignInErrorType.Cancel); }
                return;
            }

            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                if (OnCreateAccountFail != null) { OnCreateAccountFail(SignInType.Email, SignInErrorType.Fail); }
                return;
            }

            // Firebase user has been created.            
            Debug.LogFormat("Firebase user created successfully: {0} ({1})", task.Result.DisplayName, task.Result.UserId);

            task.Result.SendEmailVerificationAsync().ContinueWith(sendEmailTask =>
            {
                if (sendEmailTask.IsCanceled)
                {
                    Debug.LogError("SendEmailVerificationAsync was canceled.");
                    return;
                }

                if (sendEmailTask.IsFaulted)
                {
                    Debug.LogError("SendEmailVerificationAsync encountered an error: " + sendEmailTask.Exception);
                    return;
                }

                if (this.SendEmailSucceeded != null)
                {
                    Debug.Log("Email sent successfully.");
                    this.SendEmailSucceeded(this, EventArgs.Empty);
                }
            });
        });
    }
}

/// <summary>
/// The Partial account sign in
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    public delegate void SignedInFirebaseDelegate(SignInType signInType);
    public delegate void SignInFirebaseFailDelegate(SignInType signInType, SignInErrorType signInErrorType);

    public event SignInFirebaseFailDelegate OnCreateAccountFail;
    public event SignInFirebaseFailDelegate OnSignInFirebaseFail;

    public event SignedInFirebaseDelegate OnSignInFirebaseStart;
    public event SignedInFirebaseDelegate OnSignedInFirebase;

    public void SignInAnonymouslyAsync(string testUserId)
    {
        this.testUserId = testUserId;
        // Request anonymous sign-in and wait until asynchronous call completes.
        if (OnSignInFirebaseStart != null) { OnSignInFirebaseStart(SignInType.Anonymously); }
        auth.SignInAnonymouslyAsync().ContinueWith(task => OnSignInWithCredentialAsync(task, SignInType.Anonymously));
    }

    public void SignIn(string email, string password)
    {
        Credential credential = EmailAuthProvider.GetCredential(email, password);
        if (OnSignInFirebaseStart != null) { OnSignInFirebaseStart(SignInType.Email); }
        auth.SignInWithCredentialAsync(credential).ContinueWith(task => OnSignInWithCredentialAsync(task, SignInType.Email, email, password));
    }

    public void SignOut()
    {
        auth.SignOut();
        OnFaceBookSignOut();
        OnGoogleSignOut();

        StartCoroutine(SignOutProcess());
    }

    IEnumerator SignOutProcess()
    {
        PlayerPrefs.DeleteAll();

        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerSignedInBefore) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerRewardDay) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerOwnDiamonds) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerRewardDayReset) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerIsGainReward) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerPreviousOpenApplicationTime) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerItemPackage) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerItemPanelList) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerUnlockToolButtonIndexList) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerGradeInfomation) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerInstalledTheGame) == false);
        yield return new WaitUntil(() => PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerFirstTimePlay) == false);

        if (SceneManager.GetActiveScene().name != ProjectKeywordList.kSceneForLogin)
        {
            yield return StartCoroutine(StartLoading(ProjectKeywordList.kSceneForLogin, GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform));
        }

        if (OutedSignIn != null) { OutedSignIn(this, EventArgs.Empty); }
        yield break;
    }

    private IEnumerator StartLoading(string scene, Transform T)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, transform);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }
}

/// <summary>
/// The part of below is for callback of SignInWithCredentialAsync
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    private void OnSignInWithCredentialAsync(Task<FirebaseUser> task, SignInType sit, string email = default(string), string password = default(string))
    {
        this.SignInType = sit;

        if (task.IsCanceled)
        {
            Debug.LogError("SignInWithCredentialAsync was canceled.");
            if (OnSignInFirebaseFail != null) { OnSignInFirebaseFail(this.SignInType, SignInErrorType.Cancel); }
            return;
        }
        if (task.IsFaulted)
        {
            Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
            if (OnSignInFirebaseFail != null) { OnSignInFirebaseFail(this.SignInType, SignInErrorType.Fail); }
            return;
        }

        FirebaseUser authedUser = task.Result;

        Debug.Log("OnSignInWithCredentialAsync");

        Debug.LogFormat("User signed in successfully: {0} ({1})", authedUser.DisplayName, authedUser.UserId);

        switch (this.SignInType)
        {
            case SignInType.Email:
                if (authedUser.IsEmailVerified == false)
                {
                    Debug.LogFormat("User Is Not EmailVerified");
                    SignOut();
                    return;
                }
                break;
            case SignInType.Google:
                break;
            case SignInType.Facebook:
                break;
            case SignInType.Anonymously:
                break;
        }

        if (PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerSignedInBefore) == false)
        {
            string userId;

            if (testUserId == "")
            {
                userId = authedUser.UserId;
            }
            else
            {
                userId = testUserId;
            }

            Debug.LogFormat("User signed in successfully: {0} ({1})", authedUser.DisplayName, userId);

            StartCoroutine(CheckFirebaseHasPlayerData(userId, (exists) =>
            {
                if (exists)
                {
                    Debug.Log("CheckFirebaseHasPlayerData is true");
                    SyncPlayerDataToLocalPrefs(userId, (complete) =>
                    {
                        if (complete)
                        {
                            if (OnSignedInFirebase != null)
                            {
                                OnSignedInFirebase(this.SignInType);
                            }
                        }
                    });
                }
                else
                {
                    Debug.Log("CheckFirebaseHasPlayerData is false");
                    SignedPlayer s = new SignedPlayer();
                    s.email = authedUser.Email;
                    DateTime d = DateTime.Now;
                    string dateTimeString = string.Format("{0:00}_{1:00}_{2:0000}_{3:00}:{4:00}:{5:00}", d.Month, d.Day, d.Year, d.Hour, d.Minute, d.Second);
                    s.registerdate = dateTimeString;
                    s.userid = authedUser.UserId;
                    s.authkey = TKFileManager.TKManager.GetUniqueID();
                    s.pictureindex = UnityEngine.Random.Range(0, 24);
                    s.gender = (int)CNPRooleGender.None;
                    s.birthdate = "";
                    s.playername = "";
                    this.SignedPlayer = s;
                }
            }));
        }
    }
}

/// <summary>
/// The part of below is for Googole Sign In.
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    private GoogleSignInConfiguration InitGoogleConfiguration()
    {
        // Produce a configuration.
        GoogleSignInConfiguration configuration = new GoogleSignInConfiguration
        {
            WebClientId = GoogleWebClientId,
            RequestIdToken = true
        };

        return configuration;
    }

    public void OnGoogleSignIn()
    {
        GoogleSignIn.Configuration = InitGoogleConfiguration();
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;

        if (OnSignInFirebaseStart != null) { OnSignInFirebaseStart(SignInType.Google); }

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    public void OnGoogleSignOut()
    {
#if UNITY_ANDROID || UNITY_IOS
        GoogleSignIn.DefaultInstance.SignOut();
        Debug.Log("Calling SignOut");
#endif
    }

    public void OnGoogleDisconnect()
    {
        Debug.Log("Calling Disconnect");
        GoogleSignIn.DefaultInstance.Disconnect();
    }

    private void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            using (IEnumerator<Exception> enumerator =
                    task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    GoogleSignIn.SignInException error =
                            (GoogleSignIn.SignInException)enumerator.Current;
                    Debug.LogError("Got Error: " + error.Status + " " + error.Message);
                }
                else
                {
                    Debug.LogWarning("Got Unexpected Exception?!?" + task.Exception);
                }
            }

            if (OnSignInFirebaseFail != null) { OnSignInFirebaseFail(SignInType.Google, SignInErrorType.Fail); }
            return;
        }
        else if (task.IsCanceled)
        {
            Debug.Log("Google Sign in Canceled");
            if (OnSignInFirebaseFail != null) { OnSignInFirebaseFail(SignInType.Google, SignInErrorType.Cancel); }
            return;
        }

        Debug.Log("Welcome: " + task.Result.DisplayName + "!");

        Credential credential = GoogleAuthProvider.GetCredential(task.Result.IdToken, null);
        auth.SignInWithCredentialAsync(credential).ContinueWith(authTask => OnSignInWithCredentialAsync(authTask, SignInType.Google));
    }
}

/// <summary>
/// The part of below is for Facebook SDK Login
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    public void OnFacebookSignIn()
    {
        StartCoroutine(InitFB());
    }

    IEnumerator InitFB()
    {
        yield return InitFBSDK();
        List<string> perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    IEnumerator InitFBSDK()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
            yield break;
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
            if (OnSignInFirebaseStart != null) { OnSignInFirebaseStart(SignInType.Facebook); }
            yield break;
        }
    }

    public void OnFaceBookSignOut()
    {
        if (FB.IsInitialized && FB.IsLoggedIn)
        {
            FB.LogOut();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            if (OnSignInFirebaseStart != null) { OnSignInFirebaseStart(SignInType.Facebook); }
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }

            Debug.Log(aToken.TokenString);

            if (OnSignInFirebaseStart != null) { OnSignInFirebaseStart(SignInType.Google); }

            Credential credential = FacebookAuthProvider.GetCredential(aToken.TokenString);
            auth.SignInWithCredentialAsync(credential).ContinueWith(task => OnSignInWithCredentialAsync(task, SignInType.Facebook));
        }
        else
        {
            if (OnSignInFirebaseFail != null) { OnSignInFirebaseFail(SignInType.Facebook, SignInErrorType.Cancel); }
        }
    }
}

/// <summary>
/// Ther part of below is for ActivityToLoadScene.
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    public void ActivityToLoadScene(ScenesElementManager scene)
    {
        StartCoroutine(LoadAsyncScene(scene));
    }

    private IEnumerator LoadAsyncScene(ScenesElementManager scene)
    {
        SceneManager.LoadSceneAsync(scene.ToString());
        yield return null;
    }
}

/// <summary>
/// Ther part of below is for check has local player prefas key.
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    private bool ChackHasLocalPlayerPrefsKey(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return true;
        }

        return false;
    }
}

/// <summary>
/// Ther part of below is for check has local player prefas key.
/// </summary>
public partial class FirebaseManager : MonoBehaviour
{
    public void ResetPassword(string email)
    {
        auth.SendPasswordResetEmailAsync(email).ContinueWith(OnSendPasswordResetEmail);
    }

    private void OnSendPasswordResetEmail(Task task)
    {
        if (task.IsFaulted)
        {
            return;
        }

        if (task.IsCanceled)
        {
            return;
        }

        if (this.SendEmailSucceeded != null)
        {
            Debug.Log("Email sent successfully.");
            this.SendEmailSucceeded(this, EventArgs.Empty);
        }
    }
}

/// <summary>
/// Part of below is for the Firebase Database other function.
/// </summary>    
public partial class FirebaseManager : MonoBehaviour
{
    //public void RecordSignedInUser(SignedPlayer s, Action<bool> callback)
    //{
    //    //DateTime d = DateTime.Now;
    //    //string dateTimeString = string.Format("{0:00}_{1:00}_{2:0000}_{3:00}:{4:00}:{5:00}", d.Month, d.Day, d.Year, d.Hour, d.Minute, d.Second);
    //    //signedUser.signInTime = dateTimeString;        

    //    InitializeNewPlayerOnFirebase(s, callback);
    //}

    public void GetRankDataByHeroID(string id, Action<string, List<HeroAchievement>, List<SignedPlayer>> action)
    {
        StartCoroutine(GetRankDataByHeroIDRelay(id, action));
    }

    IEnumerator GetRankDataByHeroIDRelay(string id, Action<string, List<HeroAchievement>, List<SignedPlayer>> action)
    {
        Task<DataSnapshot> getHATask = DBReference.Child("heroachievement").GetValueAsync();

        yield return new WaitUntil(() => getHATask.IsCompleted || getHATask.IsFaulted || getHATask.IsCanceled);

        List<HeroAchievement> haList = new List<HeroAchievement>();
        List<SignedPlayer> spList = new List<SignedPlayer>();

        if (getHATask.IsFaulted || getHATask.IsCanceled)
        {
            action("getHATask.IsFaulted || getHATask.IsCanceled", haList, spList);
            yield break;
        }

        foreach (DataSnapshot d in getHATask.Result.Children)
        {
            string _id = d.Child("uid").GetRawJsonValue();
            if (_id.Replace("\"", "") == id)
            {
                HeroAchievement ha = JsonUtility.FromJson<HeroAchievement>(d.GetRawJsonValue());
                haList.Add(ha);
                continue;
            }
        }

        /// Sort score.        
        haList.Sort((ha1, ha2) => ha1.score.CompareTo(ha2.score));

        /// Sort checkpoint.        
        haList.Sort((ha1, ha2) => ha1.checkpoint.CompareTo(ha2.checkpoint));

        /// Sort level.        
        haList.Sort((ha1, ha2) => ha1.level.CompareTo(ha2.level));

        /// Sort reincarnation.        
        haList.Sort((ha1, ha2) => ha1.reincarnation.CompareTo(ha2.reincarnation));

        Task<DataSnapshot> getSPTask = DBReference.Child("players").GetValueAsync();

        yield return new WaitUntil(() => getSPTask.IsCompleted || getSPTask.IsFaulted || getSPTask.IsCanceled);

        if (getSPTask.IsFaulted || getSPTask.IsCanceled)
        {
            action("getSPTask.IsFaulted || getSPTask.IsCanceled", haList, spList);
            yield break;
        }

        foreach (DataSnapshot d in getSPTask.Result.Children)
        {
            string _id = d.Child("userid").GetRawJsonValue();
            Debug.Log("_id => " + _id);
            if (haList.Exists(ha => (ha.userid == _id.Replace("\"", ""))))
            {
                SignedPlayer sp = JsonUtility.FromJson<SignedPlayer>(d.GetRawJsonValue());
                spList.Add(sp);
                continue;
            }
        }

        action("task.IsCompleted", haList, spList);

        yield break;
    }

    public void CheckPlayerNameIsAlerady(string newPlayerName, Action<bool> exist)
    {
        StartCoroutine(CheckPlayerNameInDatabase(newPlayerName, exist));
    }

    IEnumerator CheckPlayerNameInDatabase(string newPlayerName, Action<bool> exist)
    {
        Task<DataSnapshot> task = DBReference.Child("players").GetValueAsync();
        yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
        if (task.IsFaulted || task.IsCanceled) { yield break; }

        foreach (DataSnapshot d in task.Result.Children)
        {
            string name = d.Child("playername").GetRawJsonValue();
            if (newPlayerName == name.Replace("\"", ""))
            {
                exist(true);
                yield break;
            }
        }

        exist(false);
        yield break;
    }

    IEnumerator CheckIfHasChildForUnderTheDatabase(string child, Dictionary<string, object> childParameters)
    {
        Task<DataSnapshot> task = DBReference.Child(child).Child(this.SignedPlayer.authkey).GetValueAsync();
        yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
        if (task.IsFaulted || task.IsCanceled) { yield break; }

        if (task.Result.Exists == false)
        {
            foreach (KeyValuePair<string, object> pair in childParameters)
            {
                Task writeTask = DBReference.Child(child).Child(this.SignedPlayer.authkey).Child(pair.Key).SetValueAsync(pair.Value);
                yield return new WaitUntil(() => writeTask.IsCompleted || writeTask.IsFaulted || writeTask.IsCanceled);
                if (writeTask.IsFaulted || writeTask.IsCanceled) { yield break; }
            }
            yield break;
        }

        //string json = JsonUtility.ToJson(signedUser);
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        foreach (KeyValuePair<string, object> pair in childParameters)
        {
            childUpdates[string.Format("/{0}/{1}/{2}", child, this.SignedPlayer.authkey, pair.Key)] = pair.Value;
        }
        Task updateTask = DBReference.UpdateChildrenAsync(childUpdates);
        yield return new WaitUntil(() => updateTask.IsCompleted || updateTask.IsFaulted || updateTask.IsCanceled);
        if (updateTask.IsFaulted || updateTask.IsCanceled) { yield break; }
    }

    IEnumerator CheckFirebaseHasPlayerData(string UserId, Action<bool> callback)
    {
        Debug.Log("CheckFirebaseHasPlayerData");
        Task<DataSnapshot> task = DBReference.Child("players").Child(UserId).GetValueAsync();
        yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
        callback(task.Result.Exists);
    }

    private void SyncPlayerDataToLocalPrefs(string userId, Action<bool> complete)
    {
        StartCoroutine(SyncProcess(userId, complete));
    }

    IEnumerator SyncProcess(string userId, Action<bool> complete)
    {
        yield return StartCoroutine(LoadPlayerDataFromDatabaseToLoacl(userId));
        yield return StartCoroutine(LoadPlayerDayRewardStatusToLoacl());
        yield return StartCoroutine(LoadPlayerOwnDiamondsToLoacl());
        yield return StartCoroutine(LoadPlayerToolPackageToLoacl());
        yield return StartCoroutine(LoadPlayerOwnHerosListToLoacl());
        yield return StartCoroutine(LoadHeroHashtableFromDatabaseToLocal());
        complete(true);
    }

    IEnumerator SyncDataToFirebaseProcress(PlayerAttributeManager PAM, PAMType type)
    {
        switch (type)
        {
            case PAMType.SignedInBefore:
                {
                    break;
                }

            case PAMType.OwnDiamonds:
                {
                    Debug.Log("Started Sync OwnDiamonds data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary["count"] = PAM.PlayerOwnDiamonds;
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("owndiamonds", dictionary));
                    break;
                }

            case PAMType.RewardDay:
                {
                    Debug.Log("Started Sync RewardDay data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary["day"] = PAM.RewardDay;
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("dayrewardstatus", dictionary));
                    break;
                }

            case PAMType.RewardDayReset:
                {
                    Debug.Log("Started Sync RewardDayReset data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary["reset"] = PAM.RewardDayReset;
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("dayrewardstatus", dictionary));
                    break;
                }

            case PAMType.IsGainReward:
                {
                    Debug.Log("Started Sync IsGainReward data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary["isgain"] = PAM.IsGainReward;
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("dayrewardstatus", dictionary));
                    break;
                }

            case PAMType.PreviousOpenApplicationTime:
                {
                    Debug.Log("Started Sync PreviousOpenApplicationTime data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary["previous"] = PAM.PreviousOpenApplicationTime.ToString();
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("dayrewardstatus", dictionary));
                    break;
                }

            case PAMType.ItemPackage:
                {
                    Debug.Log("Started Sync ItemPackage data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    foreach (KeyValuePair<string, PlayerItem> tool in PAM.PlayerItemPackage)
                    {
                        dictionary[tool.Value.itemId] = tool.Value.itemCount;
                    }
                    //Debug.Log(dictionary.ToStringFull());
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("toolpackage", dictionary));
                    break;
                }

            case PAMType.ItemPanelList:
                {
                    Debug.Log("Started Sync ItemPanelList data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    for (int index = 0; index < PAM.PlayerItemPanelList.Count; index++)
                    {
                        PlayerItem item = PAM.PlayerItemPanelList[index];
                        dictionary[index.ToString()] = item.itemId;
                    }
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("toolpanellist", dictionary));
                    //yield return StartCoroutine(WritePlayerToolPanelList(signedPlayer, PAM.PlayerItemPanelList));
                    break;
                }

            case PAMType.UnlockToolButtonIndexList:
                {
                    Debug.Log("Started Sync UnlockToolButtonIndexList data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    for (int index = 0; index < PAM.UnLockToolButtonIndexList.Count; index++)
                    {
                        int cellIndex = PAM.UnLockToolButtonIndexList[index];
                        dictionary[index.ToString()] = cellIndex;
                    }
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("unlockcellindexlist", dictionary));
                    //yield return StartCoroutine(WriteUnlockToolCellIndexList(signedPlayer, PAM.UnLockToolButtonIndexList));
                    break;
                }

            case PAMType.GradeInfomation:
                {
                    Debug.Log("Started Sync GradeInfomation data to Firebase.");
                    DateTime d = DateTime.Now;
                    string dateTimeString = string.Format("{0:00}_{1:00}_{2:0000}_{3:00}:{4:00}:{5:00}", d.Month, d.Day, d.Year, d.Hour, d.Minute, d.Second);
                    string key = TKFileManager.TKManager.GetUniqueID();
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary[dateTimeString] = key;
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("gradehistory", dictionary));

                    string json = JsonUtility.ToJson(PAM.GradeInfomation);
                    Task task = DBReference.Child("gradeinfomation").Child(key).SetRawJsonValueAsync(json);
                    yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
                    if (task.IsFaulted || task.IsCanceled) { yield break; }
                    // is complete do something here.
                    break;
                }

            case PAMType.HeroAchievement:
                {
                    Debug.Log("Started Sync HeroAchievement data to Firebase.");

                    string herohashtable = "herohashtable";
                    string heroachievement = "heroachievement";

                    Task<DataSnapshot> checkUIDTask = DBReference.Child(herohashtable).Child(this.SignedPlayer.authkey).Child(PAM.HeroAchievement.uid).GetValueAsync();
                    yield return new WaitUntil(() => checkUIDTask.IsCompleted || checkUIDTask.IsFaulted || checkUIDTask.IsCanceled);

                    if (checkUIDTask.IsFaulted || checkUIDTask.IsCanceled)
                    {
                        Debug.Log("checkUIDTask.IsFaulted || checkUIDTask.IsCanceled.");
                        yield break;
                    }

                    if (checkUIDTask.IsCompleted)
                    {
                        Debug.Log("checkUIDTask.IsCompleted");

                        if (checkUIDTask.Result.Exists)
                        {
                            Debug.Log("checkUIDTask.Result.Exists");

                            string key = checkUIDTask.Result.GetValue(false) as string;
                            Debug.Log(key);

                            Task<DataSnapshot> getHATask = DBReference.Child(heroachievement).Child(key).GetValueAsync();
                            yield return new WaitUntil(() => getHATask.IsCompleted || getHATask.IsFaulted || getHATask.IsCanceled);

                            if (getHATask.IsFaulted || getHATask.IsCanceled)
                            {
                                Debug.Log("getHATask.IsFaulted || getHATask.IsCanceled.");
                                yield break;
                            }

                            if (getHATask.IsCompleted)
                            {
                                Debug.Log("getHATask.IsCompleted");

                                if (getHATask.Result.Exists)
                                {
                                    Debug.Log("getHATask.Result.Exists");

                                    Dictionary<string, object> childParameters = new Dictionary<string, object>();
                                    string dicKey = string.Format("/{0}/{1}/", heroachievement, key);

                                    childParameters[dicKey + "uid"] = PAM.HeroAchievement.uid;
                                    childParameters[dicKey + "level"] = PAM.HeroAchievement.level;
                                    childParameters[dicKey + "exp"] = PAM.HeroAchievement.exp;
                                    childParameters[dicKey + "reincarnation"] = PAM.HeroAchievement.reincarnation;
                                    childParameters[dicKey + "checkpoint"] = PAM.HeroAchievement.checkpoint;
                                    childParameters[dicKey + "score"] = PAM.HeroAchievement.score;
                                    childParameters[dicKey + "skill01"] = PAM.HeroAchievement.skill01;
                                    childParameters[dicKey + "skill02"] = PAM.HeroAchievement.skill02;
                                    childParameters[dicKey + "skill03"] = PAM.HeroAchievement.skill03;
                                    childParameters[dicKey + "skill04"] = PAM.HeroAchievement.skill04;
                                    childParameters[dicKey + "tcolevel"] = PAM.HeroAchievement.tcolevel;
                                    childParameters[dicKey + "tcoexp"] = PAM.HeroAchievement.tcoexp;
                                    childParameters[dicKey + "tno01level"] = PAM.HeroAchievement.tno01level;
                                    childParameters[dicKey + "tno01exp"] = PAM.HeroAchievement.tno01exp;
                                    childParameters[dicKey + "tno02level"] = PAM.HeroAchievement.tno02level;
                                    childParameters[dicKey + "tno02exp"] = PAM.HeroAchievement.tno02exp;
                                    childParameters[dicKey + "tno03level"] = PAM.HeroAchievement.tno03level;
                                    childParameters[dicKey + "tno03exp"] = PAM.HeroAchievement.tno03exp;
                                    childParameters[dicKey + "tno04level"] = PAM.HeroAchievement.tno04level;
                                    childParameters[dicKey + "tno04exp"] = PAM.HeroAchievement.tno04exp;
                                    childParameters[dicKey + "tno05level"] = PAM.HeroAchievement.tno05level;
                                    childParameters[dicKey + "tno05exp"] = PAM.HeroAchievement.tno05exp;

                                    // Dictionary<string, object> childUpdates = new Dictionary<string, object>();
                                    // foreach (KeyValuePair<string, object> pair in childParameters)
                                    // {
                                    //     childUpdates[string.Format("/{0}/{1}/{2}", heroachievement, key, pair.Key)] = pair.Value;
                                    //     // Debug.Log(string.Format("{0}, {1}", pair.Key, pair.Value));
                                    // }

                                    // foreach (KeyValuePair<string, object> pair in childUpdates)
                                    // {
                                    //     Debug.Log(string.Format("{0}, {1}", pair.Key, pair.Value));
                                    // }

                                    foreach (KeyValuePair<string, object> pair in childParameters)
                                    {
                                        Debug.Log(string.Format("{0}, {1}", pair.Key, pair.Value));
                                    }

                                    Task updateTask = DBReference.UpdateChildrenAsync(childParameters);
                                    yield return new WaitUntil(() => updateTask.IsCompleted || updateTask.IsFaulted || updateTask.IsCanceled);

                                    if (updateTask.IsFaulted || updateTask.IsCanceled)
                                    {
                                        Debug.Log("updateTask.IsFaulted || updateTask.IsCanceled.");
                                        yield break;
                                    }

                                    if (updateTask.IsCompleted)
                                    {
                                        Debug.Log("updateTask.IsCompleted");
                                        yield break;
                                    }
                                }
                            }
                        }
                    }

                    string uniqueID = TKFileManager.TKManager.GetUniqueID();

                    Task writeUIDTask = DBReference.Child(herohashtable).Child(this.SignedPlayer.authkey).Child(PAM.HeroAchievement.uid).SetValueAsync(uniqueID);
                    yield return new WaitUntil(() => writeUIDTask.IsCompleted || writeUIDTask.IsFaulted || writeUIDTask.IsCanceled);

                    if (writeUIDTask.IsFaulted || writeUIDTask.IsCanceled)
                    {
                        Debug.Log("writeUIDTask.IsFaulted || writeUIDTask.IsCanceled.");
                        yield break;
                    }

                    if (writeUIDTask.IsCompleted)
                    {
                        Debug.Log("writeUIDTask.IsCompleted");

                        string json = JsonUtility.ToJson(PAM.HeroAchievement);
                        Task writeHATask = DBReference.Child(heroachievement).Child(uniqueID).SetRawJsonValueAsync(json);
                        yield return new WaitUntil(() => writeHATask.IsCompleted || writeHATask.IsFaulted || writeHATask.IsCanceled);

                        if (writeHATask.IsFaulted || writeHATask.IsCanceled)
                        {
                            Debug.Log("writeHATask.IsFaulted || writeHATask.IsCanceled.");
                            yield break;
                        }

                        if (writeHATask.IsCompleted)
                        {
                            Debug.Log("writeHATask.IsCompleted");
                            yield break;
                        }
                    }

                    yield break;
                }

            case PAMType.OwnHeros:
                {
                    Debug.Log("Started Sync PlayerOwnHeros data to Firebase.");
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    for (int index = 0; index < PAM.PlayerOwnHeros.Count; index++)
                    {
                        string cellIndex = PAM.PlayerOwnHeros[index];
                        dictionary[index.ToString()] = cellIndex;
                    }
                    yield return StartCoroutine(CheckIfHasChildForUnderTheDatabase("ownheros", dictionary));
                    break;
                }
        }

        Debug.Log("Succeed for Sync data to Firebase.");
        yield break;
    }
}

/// <summary>
/// Part of below is for write data to Firebase Database.
/// </summary>    
public partial class FirebaseManager : MonoBehaviour
{
    //IEnumerator WriteNewUser(SignedPlayer s, Action<bool> callback)
    //{
    //    string json = JsonUtility.ToJson(s);
    //    Task task = DBReference.Child("players").Child(s.userid).SetRawJsonValueAsync(json);
    //    yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
    //    callback(task.IsCompleted);
    //}

    //IEnumerator UpdateUser(SignedPlayer signedUser, Action<bool> callback)
    //{
    //    string json = JsonUtility.ToJson(signedUser);
    //    Dictionary<string, object> childUpdates = new Dictionary<string, object>();
    //    childUpdates[string.Format("/players/{0}/{1}", signedUser.userid, "choosedrole")] = signedUser.choosedrole;
    //    childUpdates[string.Format("/players/{0}/{1}", signedUser.userid, "playername")] = signedUser.playername;
    //    childUpdates[string.Format("/players/{0}/{1}", signedUser.userid, "pictureindex")] = signedUser.pictureindex;
    //    Task task = DBReference.UpdateChildrenAsync(childUpdates);
    //    yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
    //    callback(task.IsCompleted);
    //}

    private void InitializeNewPlayerInFirebase(SignedPlayer s, Action<bool> callback)
    {
        StartCoroutine(CheckIfHasPlayerOrUnderTheDatabase(s, callback));
    }

    private void UpdatePlayerDataInFirebase(SignedPlayer s, Action<bool> callback)
    {
        StartCoroutine(CheckIfHasPlayerOrUnderTheDatabase(s, callback));
    }

    IEnumerator CheckIfHasPlayerOrUnderTheDatabase(SignedPlayer s, Action<bool> callback)
    {
        Task<DataSnapshot> task = DBReference.Child("players").Child(s.userid).GetValueAsync();
        yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);

        if (task.IsFaulted || task.IsCanceled) { yield break; }

        if (task.Result.Exists)
        {
            string json = JsonUtility.ToJson(s);
            Dictionary<string, object> childUpdates = new Dictionary<string, object>();

            childUpdates[string.Format("/players/{0}/{1}", s.userid, "userid")] = s.userid;
            // childUpdates[string.Format("/players/{0}/{1}", s.userid, "registerdate")] = s.registerdate;
            childUpdates[string.Format("/players/{0}/{1}", s.userid, "authkey")] = s.authkey;
            childUpdates[string.Format("/players/{0}/{1}", s.userid, "email")] = s.email;
            childUpdates[string.Format("/players/{0}/{1}", s.userid, "playername")] = s.playername;
            childUpdates[string.Format("/players/{0}/{1}", s.userid, "gender")] = s.gender;
            childUpdates[string.Format("/players/{0}/{1}", s.userid, "pictureindex")] = s.pictureindex;
            childUpdates[string.Format("/players/{0}/{1}", s.userid, "birthdate")] = s.birthdate;

            Task updateTask = DBReference.UpdateChildrenAsync(childUpdates);
            yield return new WaitUntil(() => updateTask.IsCompleted || updateTask.IsFaulted || updateTask.IsCanceled);
            callback(task.IsCompleted || task.IsFaulted || task.IsCanceled);
            yield break;
        }
        else
        {
            string json = JsonUtility.ToJson(s);
            DBReference.Child("players").Child(s.userid).SetRawJsonValueAsync(json);
            yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
            callback(task.IsCompleted || task.IsFaulted || task.IsCanceled);
            yield break;
        }
    }

    //IEnumerator WritePlayerOwnDiamonds(SignedPlayer signedUser, OwnDiamonds ownDiamonds)
    //{
    //    string json = JsonUtility.ToJson(ownDiamonds);
    //    //Task task = DBReference.Child("owndiamonds").Child(signedUser.authkey).SetRawJsonValueAsync(json);
    //    //yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
    //    DBReference.Child("owndiamonds").Child(signedUser.authkey).SetRawJsonValueAsync(json);
    //    yield break;
    //}

    //IEnumerator WritePlayerDayRewardStatus(SignedPlayer signedUser, DayRewardStatus dayRewardStatus)
    //{
    //    string json = JsonUtility.ToJson(dayRewardStatus);
    //    //Task task = DBReference.Child("dayrewardstatus").Child(signedUser.authkey).SetRawJsonValueAsync(json);
    //    //yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
    //    DBReference.Child("dayrewardstatus").Child(signedUser.authkey).SetRawJsonValueAsync(json);
    //    yield break;
    //}

    //IEnumerator WritePlayerToolPackage(SignedPlayer signedUser, Dictionary<string, PlayerItem> package)
    //{
    //    foreach (KeyValuePair<string, PlayerItem> tool in package)
    //    {
    //        //Task task = DBReference.Child("toolpackage").Child(signedUser.authkey).Child(tool.Value.itemId).SetValueAsync(tool.Value.itemCount);
    //        //yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
    //        DBReference.Child("toolpackage").Child(signedUser.authkey).Child(tool.Value.itemId).SetValueAsync(tool.Value.itemCount);
    //    }
    //    yield break;
    //}

    //IEnumerator WritePlayerToolPanelList(SignedPlayer signedUser, List<PlayerItem> list)
    //{
    //    Debug.Log(list.Count);
    //    for (int index = 0; index < list.Count; index++)
    //    {
    //        PlayerItem item = list[index];
    //        //Debug.Log(item);
    //        //Debug.Log(signedUser);
    //        //Debug.Log(DBReference);
    //        //Task task = DBReference.Child("toolpanellist").Child(signedUser.authkey).Child(index.ToString()).SetValueAsync(item.itemId);
    //        //yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
    //        DBReference.Child("toolpanellist").Child(signedUser.authkey).Child(index.ToString()).SetValueAsync(item.itemId);
    //    }
    //    yield break;
    //}

    //IEnumerator WriteUnlockToolCellIndexList(SignedPlayer signedUser, List<int> list)
    //{
    //    for (int index = 0; index < list.Count; index++)
    //    {
    //        int cellIndex = list[index];
    //        //Task task = DBReference.Child("unlockcellindexlist").Child(signedUser.authkey).Child(index.ToString()).SetValueAsync(cellIndex);
    //        //yield return new WaitUntil(() => task.IsCompleted || task.IsFaulted || task.IsCanceled);
    //        DBReference.Child("unlockcellindexlist").Child(signedUser.authkey).Child(index.ToString()).SetValueAsync(cellIndex);
    //    }
    //    yield break;
    //}

    //IEnumerator WriteGradeHistory(SignedPlayer signedUser, DateTime d, GradeInfomation gradeInfomation)
    //{
    //    string dateTimeString = string.Format("{0:00}_{1:00}_{2:0000}_{3:00}:{4:00}:{5:00}", d.Month, d.Day, d.Year, d.Hour, d.Minute, d.Second);
    //    string key = TKFileManager.TKManager.GetUniqueID();
    //    //Task task1 = DBReference.Child("gradehistory").Child(signedUser.authkey).Child(dateTimeString).SetValueAsync(key);
    //    //yield return new WaitUntil(() => task1.IsCompleted || task1.IsFaulted || task1.IsCanceled);
    //    DBReference.Child("gradehistory").Child(signedUser.authkey).Child(dateTimeString).SetValueAsync(key);

    //    string json = JsonUtility.ToJson(gradeInfomation);
    //    //Task task2 = DBReference.Child("gradeinfomation").Child(key).SetRawJsonValueAsync(json);
    //    //yield return new WaitUntil(() => task2.IsCompleted || task2.IsFaulted || task2.IsCanceled);
    //    DBReference.Child("gradeinfomation").Child(key).SetRawJsonValueAsync(json);

    //    yield break;
    //}
}

/// <summary>
/// Part of below is for load data from Firebase Database.
/// </summary>    
public partial class FirebaseManager : MonoBehaviour
{
    IEnumerator LoadPlayerDataFromDatabaseToLoacl(string userId)
    {
        Task syncTask = DBReference.Child("players").Child(userId).GetValueAsync().ContinueWith((task) =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("succeeded for loading player data from the database");
                SignedPlayer s = JsonUtility.FromJson<SignedPlayer>(task.Result.GetRawJsonValue());
                PlayerPrefsUtility.SaveValue<SignedPlayer>(ProjectKeywordList.kPPrefsForPlayerSignedInBefore, s);
                //PlayerPrefsUtility.SaveValue<int>(ProjectKeywordList.kPPrefsForPlayerCreatedRole, signedPlayer.choosedrole);
                return;
            }

            Debug.Log("fail for loading player data from the database");
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);

        Debug.Log("LoadPlayerDataFromDatabase syncTask IsCompleted");

        yield break;

    }

    IEnumerator LoadPlayerOwnDiamondsToLoacl()
    {
        Task syncTask = DBReference.Child("owndiamonds").Child(this.SignedPlayer.authkey).GetValueAsync().ContinueWith(task =>
        {
            Debug.Log(task.Result.GetRawJsonValue());
            Debug.Log(this.SignedPlayer.authkey);

            if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                OwnDiamonds ownDiamonds = JsonUtility.FromJson<OwnDiamonds>(snapshot.GetRawJsonValue());
                PlayerPrefsUtility.SaveValue<int>(ProjectKeywordList.kPPrefsForPlayerOwnDiamonds, ownDiamonds.count);
                // Do something with snapshot...
                return;
            }

            Debug.Log("LoadPlayerOwnDiamonds error");

        });

        yield return new WaitUntil(() => syncTask.IsCompleted);

        Debug.Log("LoadPlayerOwnDiamonds syncTask IsCompleted");

        yield break;
    }

    IEnumerator LoadPlayerDayRewardStatusToLoacl()
    {
        Task syncTask = DBReference.Child("dayrewardstatus").Child(this.SignedPlayer.authkey).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(snapshot.GetRawJsonValue());

                DayRewardStatus dayRewardStatus = JsonUtility.FromJson<DayRewardStatus>(snapshot.GetRawJsonValue());

                Debug.Log(dayRewardStatus.day);
                Debug.Log(dayRewardStatus.isgain);
                Debug.Log(dayRewardStatus.previous);
                Debug.Log(dayRewardStatus.reset);


                //int nextDay = dayRewardStatus.day;
                //if (nextDay == 7) { nextDay = 0; }

                PlayerPrefsUtility.SaveValue<int>(ProjectKeywordList.kPPrefsForPlayerRewardDay, dayRewardStatus.day);
                PlayerPrefsUtility.SaveValue<bool>(ProjectKeywordList.kPPrefsForPlayerRewardDayReset, dayRewardStatus.reset);
                PlayerPrefsUtility.SaveValue<bool>(ProjectKeywordList.kPPrefsForPlayerIsGainReward, dayRewardStatus.isgain);

                DateTime dateTime = DateTime.Parse(dayRewardStatus.previous);
                PlayerPrefsUtility.SaveValue<DateTime>(ProjectKeywordList.kPPrefsForPlayerPreviousOpenApplicationTime, dateTime);

                // Do something with snapshot...
                return;
            }
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);

        Debug.Log("LoadPlayerDayRewardStatus syncTask IsCompleted");

        yield break;
    }

    IEnumerator LoadPlayerToolPackageToLoacl()
    {
        Dictionary<string, PlayerItem> playerItemPackage = new Dictionary<string, PlayerItem>();

        Task syncTask = DBReference.Child("toolpackage").Child(this.SignedPlayer.authkey).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(snapshot.GetRawJsonValue());
                string jsonstring = snapshot.GetRawJsonValue().Replace("{", "").Replace("}", "");
                var array = jsonstring.Split(',');
                foreach (string member in array)
                {
                    var key = member.Split(':')[0];
                    key = key.Replace("\"", "");
                    var value = member.Split(':')[1];

                    int n;
                    var isNumeric = int.TryParse(value, out n);
                    if (isNumeric)
                    {
                        PlayerItem playerItem = new PlayerItem(key, n);
                        playerItemPackage.Add(key, playerItem);
                        //Debug.Log(playerItem.itemId);
                        //Debug.Log(playerItem.itemCount);
                    }
                }

                PlayerPrefsUtility.SaveDict<string, PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPackage, playerItemPackage);
                // Do something with snapshot...
                return;
            }
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);
        Debug.Log("LoadPlayerToolPackage syncTask IsCompleted");

        yield return StartCoroutine(LoadPlayerUnlockToolCellIndexListToLoacl());
        yield return StartCoroutine(LoadPlayerToolPanelListToLoacl(playerItemPackage));
        yield break;
    }

    IEnumerator LoadPlayerUnlockToolCellIndexListToLoacl()
    {
        Task syncTask = DBReference.Child("unlockcellindexlist").Child(this.SignedPlayer.authkey).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                string jsonstring = snapshot.GetRawJsonValue().Replace("[", "").Replace("]", "").Replace("\"", "");
                var array = jsonstring.Split(',');

                List<int> unLockToolButtonIndexList = new List<int>();
                foreach (string index in array)
                {
                    int n;
                    var isNumeric = int.TryParse(index, out n);
                    if (isNumeric)
                    {
                        unLockToolButtonIndexList.Add(n);
                    }
                }

                PlayerPrefsUtility.SaveList<int>(ProjectKeywordList.kPPrefsForPlayerUnlockToolButtonIndexList, unLockToolButtonIndexList);
                // Do something with snapshot...
                return;
            }
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);
        Debug.Log("LoadPlayerUnlockToolCellIndexList syncTask IsCompleted");

        yield break;
    }

    IEnumerator LoadPlayerToolPanelListToLoacl(Dictionary<string, PlayerItem> playerItemPackage)
    {
        Task syncTask = DBReference.Child("toolpanellist").Child(this.SignedPlayer.authkey).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(snapshot.GetRawJsonValue());

                string jsonstring = snapshot.GetRawJsonValue().Replace("[", "").Replace("]", "").Replace("\"", "");
                var array = jsonstring.Split(',');

                PlayerItem nullItem = new PlayerItem("null", default(int));
                List<PlayerItem> playerItemPanelList = Enumerable.Repeat<PlayerItem>(nullItem, array.Length).ToList();

                for (int index = 0; index < array.Length; index++)
                {
                    string id = array[index];

                    if (playerItemPackage.ContainsKey(id))
                    {
                        PlayerItem playerItem = playerItemPackage[id];
                        Debug.Log(playerItem.itemId);
                        Debug.Log(playerItem.itemCount);

                        playerItemPanelList[index] = playerItem;
                    }
                }

                PlayerPrefsUtility.SaveList<PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPanelList, playerItemPanelList);
                // Do something with snapshot...
                return;
            }
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);
        Debug.Log("LoadPlayerToolPanelList syncTask IsCompleted");

        yield break;
    }

    IEnumerator LoadPlayerOwnHerosListToLoacl()
    {
        Task syncTask = DBReference.Child("ownheros").Child(this.SignedPlayer.authkey).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(snapshot.GetRawJsonValue());

                string jsonstring = snapshot.GetRawJsonValue().Replace("[", "").Replace("]", "").Replace("\"", "");
                var array = jsonstring.Split(',');

                List<string> playerOwnHeroslList = Enumerable.Repeat<string>("null", array.Length).ToList();

                for (int index = 0; index < array.Length; index++)
                {
                    string id = array[index];
                    playerOwnHeroslList[index] = id;
                }

                PlayerPrefsUtility.SaveList<string>(ProjectKeywordList.kPPrefsForPlayerOwnHeros, playerOwnHeroslList);
                // Do something with snapshot...
                return;
            }
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);
        Debug.Log("LoadPlayerOwnHerosListToLoacl syncTask IsCompleted");
        yield break;
    }

    IEnumerator LoadHeroHashtableFromDatabaseToLocal()
    {
        Dictionary<string, string> heroHashtable = new Dictionary<string, string>();

        Task syncTask = DBReference.Child("herohashtable").Child(this.SignedPlayer.authkey).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(snapshot.GetRawJsonValue());
                string jsonstring = snapshot.GetRawJsonValue().Replace("{", "").Replace("}", "");
                var array = jsonstring.Split(',');
                foreach (string member in array)
                {
                    var key = member.Split(':')[0];
                    key = key.Replace("\"", "");
                    var value = member.Split(':')[1];

                    heroHashtable.Add(key, value);
                }

                PlayerPrefsUtility.SaveDict<string, string>(ProjectKeywordList.kPPrefsForPlayerHeroHashtable, heroHashtable);
                // Do something with snapshot...
                return;
            }
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);
        Debug.Log("LoadHeroHashtableFromDatabaseToLocal syncTask IsCompleted");

        List<string> list = PlayerPrefsUtility.LoadList<string>(ProjectKeywordList.kPPrefsForPlayerOwnHeros);

        int number = 0;
        for (int i = 0; i < list.Count; i++)
        {
            string id = list[i];
            if (heroHashtable.ContainsKey(id))
            {
                string hashID = heroHashtable[id];
                yield return StartCoroutine(LoadHeroAchievementFromDatabaseToLocal(hashID));
            }
            number++;
        }

        yield return new WaitUntil(() => number == list.Count);
        Debug.Log("LoadHeroAchievementFromDatabaseToLocal syncTask IsCompleted");

        yield break;
    }

    IEnumerator LoadHeroAchievementFromDatabaseToLocal(string id)
    {
        Dictionary<string, HeroAchievement> heroAchievementDic = PlayerPrefsUtility.LoadDict<string, HeroAchievement>(ProjectKeywordList.kPPrefsForPlayerHeroAchievementDic);

        if (heroAchievementDic == null)
        {
            heroAchievementDic = new Dictionary<string, HeroAchievement>();
        }

        Task syncTask = DBReference.Child("heroachievement").Child(id).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                HeroAchievement heroAchievement = JsonUtility.FromJson<HeroAchievement>(task.Result.GetRawJsonValue());
                heroAchievementDic.Add(heroAchievement.uid, heroAchievement);
                PlayerPrefsUtility.SaveDict<string, HeroAchievement>(ProjectKeywordList.kPPrefsForPlayerHeroAchievementDic, heroAchievementDic);
                return;
            }
        });

        yield return new WaitUntil(() => syncTask.IsCompleted);
        Debug.Log("LoadHeroAchievementFromDatabaseToLocal syncTask IsCompleted");
        yield break;
    }

    IEnumerator SaveValue<T>(string key, T value)
    {
        PlayerPrefsUtility.SaveValue(key, value);
        PlayerPrefs.Save();
        yield break;
    }

    IEnumerator SaveList<T>(string key, List<T> value)
    {
        PlayerPrefsUtility.SaveList(key, value);
        PlayerPrefs.Save();
        yield break;
    }

    IEnumerator SaveDict<Key, Value>(string key, Dictionary<Key, Value> value)
    {
        PlayerPrefsUtility.SaveDict(key, value);
        PlayerPrefs.Save();
        yield break;
    }
}