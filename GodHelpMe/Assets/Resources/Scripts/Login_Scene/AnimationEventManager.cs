﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationEventManager : MonoBehaviour
{
    public void AEM_PlayAudioClipOnce(AudioClip clip)
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayAudioClipOnce(clip);
        }        
    }
}
