﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using PathManager;
using UnityEngine.EventSystems;

public partial class LoginSceneUIManager : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private Text systemDetail;

    private string systemDetailString = default(string);

    private string email;
    private string resetEmail;
    private string password;

    [SerializeField] private GameObject loading;

    private bool touched = false;

    [SerializeField] private Animator LogoAnimator;
    [SerializeField] private Animator LoginViewBoardAnimator;
    [SerializeField] private Animator RegisterViewBackgroundAnimator;
    [SerializeField] private Animator ForgotPasswordViewBackgroundAnimator;

    private void Start()
    {
        WarnAlertor.OnClosed += OnClosed;

        FirebaseManager.Singleton.SendEmailSucceeded += OnSendEmailSucceeded;
        FirebaseManager.Singleton.OnCreateAccountFail += OnCreateAccountFail;
        FirebaseManager.Singleton.OnSignedInFirebase += OnSignedInFirebase;
        FirebaseManager.Singleton.OnSignInFirebaseStart += OnSignInFirebaseStart;
        FirebaseManager.Singleton.OnSignInFirebaseFail += OnSignInFirebaseFail;
    }

    IEnumerator ImmadiatlyEntryLobby()
    {
        if ((FirebaseManager.Singleton != null))
        {
            if (FirebaseManager.Singleton.SignedPlayer != null)
            {
                //yield return new WaitUntil(() => (FirebaseManager.Singleton != null));
                //yield return new WaitUntil(() => (FirebaseManager.Singleton.SignedPlayer != null));

                StartCoroutine(EntryNextScene(ProjectKeywordList.kSceneForLobby));
                yield break;
            }
        }

        yield break;
    }

    private void Update()
    {
        if (systemDetail)
        {
            systemDetail.text = systemDetailString;
        }
    }

    private void OnDestroy()
    {
        WarnAlertor.OnClosed -= OnClosed;

        FirebaseManager.Singleton.SendEmailSucceeded -= OnSendEmailSucceeded;
        FirebaseManager.Singleton.OnSignedInFirebase -= OnSignedInFirebase;
        FirebaseManager.Singleton.OnSignInFirebaseStart -= OnSignInFirebaseStart;
        FirebaseManager.Singleton.OnSignInFirebaseFail -= OnSignInFirebaseFail;

        Resources.UnloadUnusedAssets();
    }

    public void RegisterAccountEvent()
    {
        print("Here is RegisterAccountEvent !");

        if (email != null && password != null)
        {
            FirebaseManager.Singleton.SignUp(email, password);
        }
        else
        {
            string text = string.Format("Please check Email or password.");
            WarnAlertor.Show(text, transform);
        }
    }

    public void RegisterEvent()
    {
        StartCoroutine(CloseLoginViewBoard((isClose) => { if (isClose) { StartCoroutine(ShowRegisterBoard()); } }));
    }

    public void CancelRegister()
    {
        StartCoroutine(CloseRegisterBoard((isClose) => { if (isClose) { StartCoroutine(ShowLoginViewBoard()); } }));
    }

    IEnumerator ShowRegisterBoard(Action<bool> callback = null)
    {
        if (RegisterViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Default"))
        {
            RegisterViewBackgroundAnimator.SetBool("Callback", true);
            yield return new WaitUntil(() => RegisterViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Open"));
            if (callback != null) { callback(true); }
        }
        yield break;
    }

    IEnumerator CloseRegisterBoard(Action<bool> callback = null)
    {
        if (RegisterViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
        {
            RegisterViewBackgroundAnimator.SetBool("Callback", false);
            yield return new WaitUntil(() => RegisterViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Close"));
            if (callback != null) { callback(true); }
        }
        yield break;
    }

    public void LoginEvent()
    {
        if (email != null && password != null)
        {
            FirebaseManager.Singleton.SignIn(email, password);
        }
        else
        {
            string text = string.Format("Please check Email or password.");
            WarnAlertor.Show(text, transform);
            // For test.
            //StartCoroutine(EntryNextScene(delay, ProjectKeywordList.kSceneForCreateNewPlayer));
        }
    }

    private IEnumerator StartLoading(string scene, Transform T)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, T);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }

    IEnumerator EntryNextScene(string scene)
    {
        //Debug.Log("EntryNextScene");

        yield return StartCoroutine(CloseLoadingCallback((loading_complete) =>
        {
            if (loading_complete)
            {

                //SceneManager.LoadSceneAsync(scene);
                StartCoroutine(StartLoading(scene, transform));
            }
            else
            {
                string text = string.Format("Error please notice the service.");
                WarnAlertor.Show(text, transform);
            }
        }));

        yield break;
    }

    IEnumerator ShowLoginViewBoard(Action<bool> callback = null)
    {
        if (LoginViewBoardAnimator.GetCurrentAnimatorStateInfo(0).IsName("Close"))
        {
            LoginViewBoardAnimator.SetBool("Callback", false);
            yield return new WaitUntil(() => LoginViewBoardAnimator.GetCurrentAnimatorStateInfo(0).IsName("Default"));
            if (callback != null) { callback(true); }
        }

        yield break;
    }

    IEnumerator CloseLoginViewBoard(Action<bool> callback = null)
    {
        if (LoginViewBoardAnimator.GetCurrentAnimatorStateInfo(0).IsName("FadeInBoard"))
        {
            LoginViewBoardAnimator.SetBool("Callback", true);
            yield return new WaitUntil(() => LoginViewBoardAnimator.GetCurrentAnimatorStateInfo(0).IsName("Close"));
            if (callback != null) { callback(true); }
        }

        yield break;
    }

    IEnumerator ShowLoadingCallback(Action<bool> callback = null)
    {
        if (loading) { loading.SetActive(true); }
        yield return new WaitUntil(() => loading.activeInHierarchy == true);
        if (callback != null) { callback(true); }
        yield break;
    }

    IEnumerator CloseLoadingCallback(Action<bool> callback = null)
    {
        if (loading) { loading.SetActive(false); }
        yield return new WaitUntil(() => loading.activeInHierarchy == false);
        if (callback != null) { callback(true); }
        yield break;
    }

    public void ForgotPasswordEvent()
    {
        StartCoroutine(CloseLoginViewBoard((isClose) => { if (isClose) { StartCoroutine(ShowResetPasswordBoard()); } }));
    }

    public void CancelResetPassword()
    {
        StartCoroutine(CloseResetPasswordBoard((isClose) => { if (isClose) { StartCoroutine(ShowLoginViewBoard()); } }));
    }

    IEnumerator ShowResetPasswordBoard(Action<bool> callback = null)
    {
        if (ForgotPasswordViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Default"))
        {
            ForgotPasswordViewBackgroundAnimator.SetBool("Callback", true);
            yield return new WaitUntil(() => ForgotPasswordViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Open"));
            if (callback != null) { callback(true); }
            yield break;
        }
    }

    IEnumerator CloseResetPasswordBoard(Action<bool> callback = null)
    {
        if (ForgotPasswordViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
        {
            ForgotPasswordViewBackgroundAnimator.SetBool("Callback", false);
            yield return new WaitUntil(() => ForgotPasswordViewBackgroundAnimator.GetCurrentAnimatorStateInfo(0).IsName("Close"));
            if (callback != null) { callback(true); }
            yield break;
        }
    }

    public void ForgotPasswordEmailInput(InputField inputField)
    {
        if (Validator.EmailIsValid(inputField.text))
        {
            resetEmail = inputField.text;
            print("Email: " + inputField.text);
        }
        else
        {
            print("Text not foramt of email");
        }
    }

    public void ResetPassword()
    {
        if (resetEmail == null)
        {
            string text = string.Format("Please check Email.");
            WarnAlertor.Show(text, transform);
            return;
        }

        FirebaseManager.Singleton.ResetPassword(resetEmail);
    }

    public void SignInAnonymouslyEvent(string testUserId)
    {
        FirebaseManager.Singleton.SignInAnonymouslyAsync(testUserId);
    }

    public void FacebookAuthEvent()
    {
        print("Here is FacebookAuthEvent !");
        FirebaseManager.Singleton.OnFacebookSignIn();
        SEManager.Singleton.PlayDecideSE();
    }

    public void GoogleAuthEvent()
    {
        print("Here is GoogleAuthEvent !");
        FirebaseManager.Singleton.OnGoogleSignIn();
        SEManager.Singleton.PlayDecideSE();
    }

    public void AccountInputEvent(InputField input)
    {
        if (input.text.Length > 0)
        {
            if (Validator.EmailIsValid(input.text))
            {
                email = input.text.ToString();
                print("Account: " + input.text);
            }
            else
            {
                print("Account not foramt of email");
            }
        }
        else if (input.text.Length == 0)
        {
            print("Account Input Empty");
        }
    }

    public void PasswordInputEvent(InputField input)
    {
        if (input.text.Length > 0)
        {
            password = input.text.ToString();
            print("Password: " + input.text);
        }
        else if (input.text.Length == 0)
        {
            print("Password Input Empty");
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (touched) { return; }

        EntryLobbyEvent();

        touched = true;
    }

    public void EntryLobbyEvent()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayDecideSE();
        }


        /// The Player signed in before, so immediate enter next scene.
        if (CheckPlayerSignedInOrNot())
        {
            if (CheckPlayerCreatedRole())
            {
                //Debug.Log("123");
                StartCoroutine(ImmadiatlyEntryLobby());
            }
            else
            {
                StartCoroutine(EntryNextScene(ProjectKeywordList.kSceneForCreateNewPlayer));
            }
        }
        else
        {
            LogoAnimator.SetBool("FadeInBoardCallback", true);
            LoginViewBoardAnimator.SetBool("FadeInBoardCallback", true);
            Debug.Log("Touched!!!");
        }
    }
}

public partial class LoginSceneUIManager : MonoBehaviour
{
    // 发送验证 Email 后返回
    private void OnSendEmailSucceeded(object sender, EventArgs e)
    {
        string text = string.Format("請至您的信箱收取信件，並完成驗證後, 再次登入遊戲");
        WarnAlertor.Show(text, transform);
    }

    private void OnCreateAccountFail(SignInType signInType, SignInErrorType signInErrorType)
    {
        string text = string.Format("You create account get a error, please notice the game service.");
        WarnAlertor.Show(text, transform);
    }
}

/// <summary>
/// part of below is for sing in firebase call back.
/// </summary>
public partial class LoginSceneUIManager : MonoBehaviour
{
    private void OnSignInFirebaseStart(SignInType signInType)
    {
        Debug.Log(signInType.ToString());
        StartCoroutine(CloseLoginViewBoard((complete) =>
        {
            if (complete)
            {
                StartCoroutine(ShowLoadingCallback());
            }
        }));
    }

    private void OnSignedInFirebase(SignInType signInType)
    {
        switch (signInType)
        {
            case SignInType.Email:
                break;
            case SignInType.Google:
                break;
            case SignInType.Facebook:
                break;
            case SignInType.Anonymously:
                break;
        }

        if (CheckPlayerCreatedRole())
        {
            Debug.Log("456");
            StartCoroutine(EntryNextScene(ProjectKeywordList.kSceneForLobby));
        }
        else
        {
            StartCoroutine(EntryNextScene(ProjectKeywordList.kSceneForCreateNewPlayer));
        }
    }

    private void OnSignInFirebaseFail(SignInType signInType, SignInErrorType signInErrorType)
    {

        StartCoroutine(CloseLoadingCallback((complete) =>
        {
            if (complete)
            {
                StartCoroutine(ShowLoginViewBoard((isShow) =>
                {
                    if (isShow)
                    {
                        Debug.Log("OnSignInFirebaseFail");
                        string text = string.Format("Your {0} login get a error, please notice the game service.", signInType.ToString());
                        WarnAlertor.Show(text, transform);
                    }
                }));
            }
        }));
    }
}

/// <summary>
/// part of below is for get callback of closed WarnAlertor.
/// </summary>
public partial class LoginSceneUIManager : MonoBehaviour
{
    private void OnClosed(object o, EventArgs args)
    {
        FirebaseManager.Singleton.SignOut();
    }
}

/// <summary>
/// The part of below is for check player signed in or not.
/// check player created role or not.
/// </summary>
public partial class LoginSceneUIManager : MonoBehaviour
{
    private bool CheckPlayerSignedInOrNot()
    {
        if (PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerSignedInBefore)) { return true; }
        return false;
    }

    private bool CheckPlayerCreatedRole()
    {
        if (PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerSignedInBefore))
        {
            SignedPlayer s = PlayerPrefsUtility.LoadValue<SignedPlayer>(ProjectKeywordList.kPPrefsForPlayerSignedInBefore);
            if (s.gender != (int)CNPRooleGender.None) { return true; }
            return false;
        }
        return false;
    }
}

/// <summary>
/// part of below is for OnGUI.
/// </summary>
public partial class LoginSceneUIManager : MonoBehaviour
{
    public string stringToEdit = "";
    private void OnGUI()
    {
        stringToEdit = GUILayout.TextField(stringToEdit, 25);
        if (GUILayout.Button("Anonymously"))
        {
            SignInAnonymouslyEvent(stringToEdit);
        }
    }
}