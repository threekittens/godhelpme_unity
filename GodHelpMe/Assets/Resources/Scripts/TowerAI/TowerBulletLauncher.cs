﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public partial class TowerBulletLauncher : MonoBehaviour
{
    [SerializeField] private GameObject bullet;

    private GameObject bulletInstacne;

    [SerializeField] private float timeBetweenEachBullet;

    internal GameObject target;

    private PowerManager powerManager;

    [SerializeField] private int consumeMP;
    internal int ConsumeMP { get { return consumeMP; } set { consumeMP = value; } }

    private bool isStopShooting;

    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }
}

public partial class TowerBulletLauncher : MonoBehaviour
{
    private void Start()
    {
        powerManager = transform.parent.GetComponent<PowerManager>();
    }

    private void OnDestroy()
    {
        if (bulletInstacne != null)
        {
            Destroy(bulletInstacne);
        }
    }
}


public partial class TowerBulletLauncher : MonoBehaviour
{

    internal void Shooting()
    {
        //Debug.Log("Shooting Target is " + mTarget.name);
        //Debug.Log("Bullet Instance spawned.");
        InvokeRepeating("ProduceBullet", 0.0f, timeBetweenEachBullet);
    }

    internal void StopShoot()
    {
        //Debug.Log("Stoped Shoot");
        CancelInvoke();
    }

    private void ProduceBullet()
    {
        if (bullet)
        {
            if (powerManager != null)
            {
                if (powerManager.Power <= 0.0f) { return; }

                powerManager.Power -= ConsumeMP;
            }

            if (target)
            {
                bulletInstacne = Instantiate(bullet, transform.position, transform.rotation);
                Bullet b = bulletInstacne.GetComponent<Bullet>();
                b.Damage = Damage;
                b.Target = target.transform;
            }
        }
        else
        {
            Debug.LogError("Please install be shooting bullet prefab.");
        }
    }
}