﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class TowerScout : MonoBehaviour
{
    [SerializeField] private bool EditModel = false;
    [SerializeField] private int damage;
    internal int Damage
    {
        get { return damage; }
        set
        {
            damage = value;
            if (TBL)
            {
                TBL.Damage = damage;
            }
        }
    }

    [SerializeField] private int consumeMP;
    internal int ConsumeMP
    {
        get { return consumeMP; }
        set
        {
            consumeMP = value;
            if (TBL)
            {
                TBL.ConsumeMP = consumeMP;
            }
        }
    }

    [SerializeField] private float detectRange;
    [SerializeField] private bool showRangeSphere;
    private GameObject lockedTarget;
    private List<Collider> unknowTargetColliders = new List<Collider>();
    [SerializeField] private TowerBulletLauncher TBL;
}

public partial class TowerScout : MonoBehaviour
{
    private void FixedUpdate()
    {
        Event();
    }

    private void Event()
    {
        if (DistanceOfBetweenScoutAndLockedTarget() > detectRange + 1.0f)
        {
            TBL.StopShoot();
            lockedTarget = null;
            return;
        }
        else if (lockedTarget != null)
        {
            if (lockedTarget.GetComponent<HealthManager>().HealthFillAmount <= 0.0)
            {
                TBL.StopShoot();
            }
            return;
        }
        else
        {
            TBL.StopShoot();
            StartCoroutine(DetectingTheUnknowTargetEntered());
        }
    }
}

public partial class TowerScout : MonoBehaviour
{

}

public partial class TowerScout : MonoBehaviour
{
    IEnumerator DetectingTheUnknowTargetEntered()
    {
        unknowTargetColliders.Clear();
        unknowTargetColliders.AddRange(Physics.OverlapSphere(transform.position, detectRange, LayerMask.GetMask("ENEMY_ATTACKABLE")));
        yield return BeAttackedTargetDeterming(unknowTargetColliders);
    }
}

public partial class TowerScout : MonoBehaviour
{
    IEnumerator BeAttackedTargetDeterming(List<Collider> colliders)
    {
        yield return StartCoroutine(SearchEnemy(colliders, (complete, target) =>
        {
            if (complete && target != null)
            {
                //Debug.Log("target is " + target.name);
                lockedTarget = target;
                TBL.target = lockedTarget;
                TBL.Shooting();
            }
        }));
    }

    /// <summary>
    /// Search the lowest health enemy at nearby.
    /// </summary>
    /// <returns></returns>
    IEnumerator SearchEnemy(List<Collider> colliders, System.Action<bool, GameObject> callback)
    {
        List<HealthManager> targetsFiltrated = new List<HealthManager>();
        if (colliders.Count > 0)
        {
            foreach (Collider unknowTarget in colliders)
            {
                if (unknowTarget.gameObject.layer == 9)
                {
                    HealthManager healthManager = unknowTarget.gameObject.GetComponent<HealthManager>();
                    targetsFiltrated.Add(healthManager);
                }
            }

            targetsFiltrated.Sort(SortByScore);

            callback(true, targetsFiltrated[0].gameObject);
        }

        yield break;
    }

    static int SortByScore(HealthManager p1, HealthManager p2)
    {
        return p1.Health.CompareTo(p2.Health);
    }
}

public partial class TowerScout : MonoBehaviour
{
    private float DistanceOfBetweenScoutAndLockedTarget()
    {
        if (lockedTarget == null)
        {
            return default(float);
        }

        return Vector3.Distance(transform.position, lockedTarget.transform.position);
    }
}

public partial class TowerScout : MonoBehaviour
{
    /// <summary>
    /// The method draws a range of sphere.
    /// </summary>
    private void OnDrawGizmos()
    {
        if (showRangeSphere)
        {
            Color c = Color.green;
            c.a = .5f;
            Gizmos.color = c;
            Gizmos.DrawSphere(transform.position, detectRange);
        }
    }
}