﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TowerFunctionalityType
{
    HP = 0,
    MP = 1,
    Upgrade = 2
}

public partial class TowerFunctionalityController : MonoBehaviour
{

    [SerializeField]
    private bool EditModel = false;

    private GameObject panel;
    private GameObject relativeTower;

    [SerializeField]
    private float cureQuantity = 100.0f;

    [SerializeField]
    private ParticleSystem repairEffect;
    private ParticleSystem repairEffectInstance;

    private HealthManager towerHealthManager;
    private PowerManager towerPowerManager;

    private TowerProperty towerProperty;

    [SerializeField]
    private int cureMaxSecond = 5;
    private int curedSecond;

    private int index;
    private Transform player;

    [SerializeField]
    private TowerFunctionalityType type;

    private GameSceneUIManager gameSceneUIManager;

    [SerializeField]
    private int requestLifeBrachQuantity = 5;

    [SerializeField]
    private int requestFantasyCrystalQuantity = 5;

    private bool inRecovery = false;

    private float TowerPower;
}

public partial class TowerFunctionalityController : MonoBehaviour
{
    private void Start()
    {
        InitializeSolutionComponent();
    }

    private void Update()
    {
        if (player != null) { return; }

        if (Camera.main.GetComponent<ThirdCamera>() == null) { return; }

        if (Camera.main.GetComponent<ThirdCamera>().target) { player = Camera.main.GetComponent<ThirdCamera>().target; }
    }

    private void FixedUpdate()
    {
        switch (type)
        {
            case TowerFunctionalityType.HP:
                HealthPointRealTimeUpdate();
                break;
            case TowerFunctionalityType.MP:
                MagicPointRealTimeUpdate();
                break;
            case TowerFunctionalityType.Upgrade:
                UpgradeExpRealTimeUpdate();
                break;
        }
    }
}

public partial class TowerFunctionalityController : MonoBehaviour
{
    private void InitializeSolutionComponent()
    {
        gameSceneUIManager = GameObject.Find("USERINTERFACEMANAGER").GetComponent<GameSceneUIManager>();

        panel = transform.parent.gameObject;
        for (int i = 0; i < panel.transform.childCount; i++)
        {
            if (panel.transform.GetChild(i).name == transform.name)
            {
                index = i;
                break;
            }
        }

        string tag = string.Format("Tower0{0}", index + 1);
        relativeTower = GameObject.FindGameObjectWithTag(tag);

        if (relativeTower != null)
        {
            towerHealthManager = relativeTower.GetComponent<HealthManager>();
            towerPowerManager = relativeTower.GetComponent<PowerManager>();
            towerProperty = relativeTower.GetComponent<TowerProperty>();
        }
    }

    private void HealthPointRealTimeUpdate()
    {
        if (towerHealthManager == null) { return; }
        Button relativeButton = transform.GetComponent<Button>();
        Image inner = relativeButton.transform.GetChild(0).GetComponent<Image>();
        inner.fillAmount = towerHealthManager.HealthFillAmount;
    }

    private void MagicPointRealTimeUpdate()
    {
        if (towerPowerManager == null) { return; }
        Button relativeButton = transform.GetComponent<Button>();
        Image inner = relativeButton.transform.GetChild(0).GetComponent<Image>();
        TowerPower = towerPowerManager.PowerFillAmount;
        inner.fillAmount = TowerPower;
    }

    private void UpgradeExpRealTimeUpdate()
    {
        if (towerProperty == null) { return; }
        Button relativeButton = transform.GetComponent<Button>();
        Image inner = relativeButton.transform.GetChild(0).GetComponent<Image>();
        inner.fillAmount = towerProperty.ExpInnerAmount;
        if (relativeButton.transform.Find("Inner/Outward/Level") == null) { return; }
        Text level = relativeButton.transform.Find("Inner/Outward/Level").GetComponent<Text>();
        if (level == null) { return; }
        level.text = towerProperty.Level.ToString();
    }
}

public partial class TowerFunctionalityController : MonoBehaviour
{
    public void CuresHealthPointForTower()
    {
        //if (towerHealthManager.Health == towerHealthManager.MaxHealth) { return; }

        //if (gameSceneUIManager.LifeBranchQuantity < requestLifeBrachQuantity) { return; }

        //if (!inRecovery)
        //{
        //    inRecovery = true;
        //    gameSceneUIManager.LifeBranchQuantity -= requestLifeBrachQuantity;
        //    player.GetComponent<PlayerProperty>().OnReceiveTrackingEvent(PointsType.RepaireTower);
        //    repairEffectInstance = Instantiate(repairEffect, relativeTower.transform);
        //    repairEffectInstance.Stop();
        //    repairEffectInstance.Play();
        //    InvokeRepeating("CuresHealth", 0, 1);
        //}
        
        CuresHealthPointForTowerEvent();
    }
    
    private void CuresHealthPointForTowerEvent()
    {
        if (towerHealthManager.Health == towerHealthManager.MaxHealth) { return; }

        if (gameSceneUIManager.LifeBranchQuantity < requestLifeBrachQuantity) { return; }

        if (!inRecovery)
        {
            inRecovery = true;
            gameSceneUIManager.LifeBranchQuantity -= requestLifeBrachQuantity;
            player.GetComponent<PlayerProperty>().OnReceiveTrackingEvent(PointsType.RepaireTower);
            repairEffectInstance = Instantiate(repairEffect, relativeTower.transform);
            repairEffectInstance.Stop();
            repairEffectInstance.Play();
            InvokeRepeating("CuresHealth", 0, 1);
        }
    }

    private void CuresHealth()
    {
        if (towerHealthManager.Health == towerHealthManager.MaxHealth)
        {
            cureMaxSecond = default(int);
            CancelInvoke();
            repairEffectInstance.Stop();
            repairEffectInstance.Clear();
            DestroyImmediate(repairEffectInstance.gameObject);
            inRecovery = false;
            return;
        }

        if (curedSecond >= cureMaxSecond)
        {
            curedSecond = default(int);
            CancelInvoke();
            repairEffectInstance.Stop();
            repairEffectInstance.Clear();
            DestroyImmediate(repairEffectInstance.gameObject);
            inRecovery = false;
            return;
        }

        FloatingTextControl.Initialize();

        float quantity;

        if (towerHealthManager.Health + (cureQuantity * towerProperty.Level) > towerHealthManager.MaxHealth)
        {
            quantity = towerHealthManager.MaxHealth - towerHealthManager.MaxHealth;

        }
        else
        {
            quantity = (cureQuantity * towerProperty.Level);
        }

        towerHealthManager.Health += quantity;
        FloatingTextControl.CreateFloatingText(quantity.ToString(), relativeTower.transform.position, false);
        curedSecond += 1;
    }
}

public partial class TowerFunctionalityController : MonoBehaviour
{
    public void CuresMagicPointForTower()
    {
        //if (towerPowerManager.Power == towerPowerManager.MaxPower) { return; }

        //if (gameSceneUIManager.FantasyCrystalQuantity < requestFantasyCrystalQuantity) { return; }

        //if (!inRecovery)
        //{
        //    inRecovery = true;
        //    gameSceneUIManager.FantasyCrystalQuantity -= requestFantasyCrystalQuantity;
        //    player.GetComponent<PlayerProperty>().OnReceiveTrackingEvent(PointsType.RepaireTower);
        //    repairEffectInstance = Instantiate(repairEffect, relativeTower.transform);
        //    repairEffectInstance.Stop();
        //    repairEffectInstance.Play();
        //    InvokeRepeating("CuresMagic", 0, 1);
        //}
        
        CuresMagicPointForTowerEvent();
    }
    
    private void CuresMagicPointForTowerEvent()
    {
        if (towerPowerManager.Power == towerPowerManager.MaxPower) { return; }

        if (gameSceneUIManager.FantasyCrystalQuantity < requestFantasyCrystalQuantity) { return; }

        if (!inRecovery)
        {
            inRecovery = true;
            gameSceneUIManager.FantasyCrystalQuantity -= requestFantasyCrystalQuantity;
            player.GetComponent<PlayerProperty>().OnReceiveTrackingEvent(PointsType.RepaireTower);
            repairEffectInstance = Instantiate(repairEffect, relativeTower.transform);
            repairEffectInstance.Stop();
            repairEffectInstance.Play();
            InvokeRepeating("CuresMagic", 0, 1);
        }
    }

    private void CuresMagic()
    {
        if (towerPowerManager.Power == towerPowerManager.MaxPower)
        {
            cureMaxSecond = default(int);
            CancelInvoke();
            repairEffectInstance.Stop();
            repairEffectInstance.Clear();
            DestroyImmediate(repairEffectInstance.gameObject);
            inRecovery = false;
            return;
        }

        if (curedSecond >= cureMaxSecond)
        {
            curedSecond = default(int);
            CancelInvoke();
            repairEffectInstance.Stop();
            repairEffectInstance.Clear();
            DestroyImmediate(repairEffectInstance.gameObject);
            inRecovery = false;
            return;
        }

        FloatingTextControl.Initialize();

        float quantity;

        if (towerPowerManager.Power + (cureQuantity * towerProperty.Level) > towerPowerManager.MaxPower)
        {
            quantity = towerPowerManager.MaxPower - towerPowerManager.MaxPower;
        }
        else
        {
            quantity = (cureQuantity * towerProperty.Level);
        }

        towerPowerManager.Power += quantity;
        FloatingTextControl.CreateFloatingText(quantity.ToString(), relativeTower.transform.position, false);

        curedSecond += 1;
    }
}

public partial class TowerFunctionalityController : MonoBehaviour
{
    public void Upgrade()
    {
        ShowEffect();
        towerProperty.TowerUpgrade();
    }

    private void ShowEffect()
    {
        StartCoroutine(EffectPlay());
    }

    IEnumerator EffectPlay()
    {
        ParticleSystem instance = Instantiate(repairEffect, relativeTower.transform);
        instance.Stop();
        instance.Play();
        yield return new WaitUntil(() => instance.isEmitting == false);
        DestroyImmediate(instance.gameObject);
        yield break;
    }
}

public partial class TowerFunctionalityController : MonoBehaviour
{
    //public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.isWriting)
    //    {
    //        stream.SendNext(TowerPower);
    //    }
    //    else if (stream.isReading)
    //    {
    //        this.TowerPower = (float)stream.ReceiveNext();
    //    }
    //}
}
