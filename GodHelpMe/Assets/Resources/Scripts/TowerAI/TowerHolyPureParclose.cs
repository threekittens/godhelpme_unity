﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class TowerHolyPureParclose : MonoBehaviour
{
    private GameSceneUIManager gameSceneUIManager;
    private Image magicCircle;
    [SerializeField] private float parcloseRequireQuantity;
    [SerializeField] private float parcloseRequireMaxQuantity;
    [SerializeField] private float outputRequireEachTime = 1.0f;
    [SerializeField] private int parcloseKeepTime;
    private GameObject buffPanel;
    private GameObject buffPrefab;
    private bool isBuffOnPanel = false;
    [SerializeField] private TextAsset HolyPureParcloseJSON;
    [SerializeField] private ParticleSystem shieldEffect;
    [SerializeField] private ParticleSystem magicPillarBlastEffect;
    [SerializeField] private GameObject towerCoreScout;
    [SerializeField] private GameObject innerFireBrazier;
    [SerializeField] private GameObject otherFireBrazier;
    [SerializeField] private ParticleSystem areaParticleSystem;
    private List<GameObject> redFireEffectObjList = new List<GameObject>();
    private List<GameObject> blueFireEffectObjList = new List<GameObject>();
}

public partial class TowerHolyPureParclose : MonoBehaviour
{
    private void Start()
    {
        gameSceneUIManager = GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).GetComponent<GameSceneUIManager>();
        magicCircle = transform.Find("MagicCircleCanvas/Image").GetComponent<Image>();
        buffPanel = GameObject.Find("USERINTERFACEMANAGER/BuffPanel");
        buffPrefab = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceGameScene("Buff"));

        for (int i = 0; i < innerFireBrazier.transform.childCount; i++)
        {
            GameObject brazier = innerFireBrazier.transform.GetChild(i).gameObject;
            if (brazier.transform.childCount > 0)
            {
                GameObject redFirePS = brazier.transform.Find("FireAdditiveRed").gameObject;
                redFirePS.SetActive(false);
                redFireEffectObjList.Add(redFirePS);

                GameObject blueFirePS = brazier.transform.Find("FireAdditiveBlue").gameObject;
                blueFirePS.SetActive(false);
                blueFireEffectObjList.Add(blueFirePS);
            }
        }

        for (int i = 0; i < otherFireBrazier.transform.childCount; i++)
        {
            GameObject brazier = otherFireBrazier.transform.GetChild(i).gameObject;
            if (brazier.transform.childCount > 0)
            {
                GameObject redFirePS = brazier.transform.Find("FireAdditiveRed").gameObject;
                redFirePS.SetActive(false);
                redFireEffectObjList.Add(redFirePS);

                GameObject blueFirePS = brazier.transform.Find("FireAdditiveBlue").gameObject;
                blueFirePS.SetActive(false);
                blueFireEffectObjList.Add(blueFirePS);
            }
        }

        SetEffect(false);
    }
}

public partial class TowerHolyPureParclose : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isBuffOnPanel) { return; }
            if (towerCoreScout == null) { return; }
            StartOutputHolyPureStone();
        }
    }
}

public partial class TowerHolyPureParclose : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isBuffOnPanel) { return; }
            StopOutputHolyPureStone();
        }
    }
}

public partial class TowerHolyPureParclose : MonoBehaviour
{
    private void StartOutputHolyPureStone()
    {
        InvokeRepeating("OutputHolyPureStone", 0.0f, outputRequireEachTime);
        SetEffect(true);
    }

    private void StopOutputHolyPureStone()
    {
        CancelInvoke();
        SetEffect(false);
    }

    private void OutputHolyPureStone()
    {
        if (towerCoreScout == null) { CancelInvoke(); return; }
        if (IsFullOfRequiredStone())
        {
            StartTowerHolyPureParclose();
            shieldEffect.Play();
            magicPillarBlastEffect.Play();
            CancelInvoke();
            return;
        }

        if (gameSceneUIManager.HolyPureStoneQuantity == 0)
        {
            CancelInvoke();
            return;
        }

        gameSceneUIManager.HolyPureStoneQuantity -= (int)parcloseRequireQuantity;

        magicCircle.fillAmount += (parcloseRequireQuantity / parcloseRequireMaxQuantity);
    }

    private bool IsFullOfRequiredStone()
    {
        if (magicCircle.fillAmount == 1.0f)
        {
            return true;
        }
        return false;
    }

    private void OnBuffTimeOut()
    {
        StopTowerHolyPureParclose();
        shieldEffect.Stop();
        shieldEffect.Clear();
        magicPillarBlastEffect.Stop();
        magicPillarBlastEffect.Clear();
        SetEffect(false);
    }
}

public partial class TowerHolyPureParclose : MonoBehaviour
{
    private void StartTowerHolyPureParclose()
    {
        if (isBuffOnPanel) { return; }

        GameObject buffInstance = Instantiate(buffPrefab, buffPanel.transform);
        Buff buff = buffInstance.GetComponent<Buff>();
        buff.BuffJSON = HolyPureParcloseJSON;
        buff.OnBuffTimeOut += OnBuffTimeOut;
        buff.KeepSecond = parcloseKeepTime;
        buff.InitializeSolutionComponent();
        buff.TriggerBuff();

        towerCoreScout.SetActive(true);
        isBuffOnPanel = true;
    }

    private void StopTowerHolyPureParclose()
    {
        magicCircle.fillAmount = 0.0f;
        towerCoreScout.SetActive(false);
        isBuffOnPanel = false;
    }

    //private void CountdownKeepOfEffectTime()
    //{
    //    if (mKeepTime == mParcloseKeepTime)
    //    {
    //        mKeepTime = 0;
    //        StopCountdownKeepOfEffectTime();
    //        return;
    //    }
    //    mKeepTime += 1;
    //}
}

public partial class TowerHolyPureParclose : MonoBehaviour
{
    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.tag == "Player")
    //    {
    //        Debug.Log("OnTriggerStay");
    //        Debug.Log(other.gameObject.name);
    //    }
    //}

    private void SetEffect(bool status)
    {
        foreach (GameObject g in redFireEffectObjList) { g.SetActive(!status); }
        foreach (GameObject g in blueFireEffectObjList) { g.SetActive(status); }

        var main = areaParticleSystem.main;
        string red = "#FF00003C";
        string blue = "#00FFF53C";
        string htmlStirng = !status ? red : blue;
        Color color;
        if (ColorUtility.TryParseHtmlString(htmlStirng, out color))
        {
            main.startColor = color;
        }
        areaParticleSystem.Stop();
        areaParticleSystem.Clear();
        areaParticleSystem.Play();
    }
}