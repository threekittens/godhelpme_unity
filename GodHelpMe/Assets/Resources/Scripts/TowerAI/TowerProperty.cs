﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public partial class TowerProperty : MonoBehaviour
{
    [SerializeField] private TowerScout TS;
    private GameSceneUIManager GSUIManager;

    internal float ExpInnerAmount { get { return expInner.fillAmount; } }
    private int maxLevel = 1000;

    [SerializeField] private float recoverPowerTime;

    [SerializeField] private Image expInner;

    private Text levelText;

    private HealthManager healthManager;
    private PowerManager powerManager;
    private TowerBulletLauncher launcher;
    private Transform player;

    [SerializeField] private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }
    [SerializeField] private int _currentExp;
    internal int CurrentExp
    {
        get
        {
            if (PlayerAttributeManager.Singleton)
            {
                switch (towerType)
                {
                    case TowerType.Core:
                        {
                            _currentExp = PlayerAttributeManager.Singleton.HeroAchievement.tcoexp;
                            break;
                        }
                    case TowerType.No01:
                        {
                            _currentExp = PlayerAttributeManager.Singleton.HeroAchievement.tno01exp;
                            break;
                        }
                    case TowerType.No02:
                        {
                            _currentExp = PlayerAttributeManager.Singleton.HeroAchievement.tno02exp;
                            break;
                        }
                    case TowerType.No03:
                        {
                            _currentExp = PlayerAttributeManager.Singleton.HeroAchievement.tno03exp;
                            break;
                        }
                    case TowerType.No04:
                        {
                            _currentExp = PlayerAttributeManager.Singleton.HeroAchievement.tno04exp;
                            break;
                        }
                    case TowerType.No05:
                        {
                            _currentExp = PlayerAttributeManager.Singleton.HeroAchievement.tno05exp;
                            break;
                        }
                }
            }
            else
            {
                _currentExp = 0;
            }

            return _currentExp;
        }
        set
        {
            _currentExp = value;
            if (PlayerAttributeManager.Singleton)
            {
                HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievement;
                switch (towerType)
                {
                    case TowerType.Core:
                        {
                            h.tcoexp = _currentExp;
                            break;
                        }
                    case TowerType.No01:
                        {
                            h.tno01exp = _currentExp;
                            break;
                        }
                    case TowerType.No02:
                        {
                            h.tno02exp = _currentExp;
                            break;
                        }
                    case TowerType.No03:
                        {
                            h.tno03exp = _currentExp;
                            break;
                        }
                    case TowerType.No04:
                        {
                            h.tno04exp = _currentExp;
                            break;
                        }
                    case TowerType.No05:
                        {
                            h.tno05exp = _currentExp;
                            break;
                        }
                }
                PlayerAttributeManager.Singleton.HeroAchievement = h;

                if (expInner)
                {
                    float _c = _currentExp;
                    float _e = ExpRequired;
                    float fillAmount = _c / _e;
                    // Debug.Log("fillAmount " + fillAmount);
                    expInner.fillAmount = fillAmount;
                }
            }
        }
    }

    [SerializeField] private int consumeMP;
    internal int ConsumeMP { get { return consumeMP; } set { consumeMP = value; } }

    [SerializeField] private int fireStoneQuantity;
    private int FireStoneQuantity { get { return fireStoneQuantity; } set { fireStoneQuantity = value; } }

    [SerializeField] private int _expQuantity;
    private int ExpQuantity { get { return _expQuantity; } set { _expQuantity = value; } }

    [SerializeField] private int _expRequired;
    private int ExpRequired { get { return _expRequired; } set { _expRequired = value; } }

    private int _level = 1;
    internal int Level { get { return _level; } set { _level = value; levelText.text = _level.ToString(); } }

    private enum TowerType
    {
        Core = 0,
        No01 = 1,
        No02 = 2,
        No03 = 3,
        No04 = 4,
        No05 = 5
    }

    [SerializeField] private TowerType towerType;

    private struct TowerPropertyBasic
    {
        public int HP, MP, ConsumeMP, Damage, WeightHP, WeightMP, WeightDamage, Exp;

        TowerPropertyBasic(int HP, int MP, int ConsumeMP, int Damage, int WeightHP, int WeightMP, int WeightDamage, int Exp)
        {
            this.HP = HP;
            this.MP = MP;
            this.ConsumeMP = ConsumeMP;
            this.Damage = Damage;
            this.WeightHP = WeightHP;
            this.WeightMP = WeightMP;
            this.WeightDamage = WeightDamage;
            this.Exp = Exp;
        }
    }

    [SerializeField] private TowerPropertyBasic ppb;

    private TowerPropertyBasic PPB
    {
        get
        {
            switch (towerType)
            {
                case TowerType.Core:
                    {
                        ppb.HP = 25000;
                        ppb.MP = 1250;
                        ppb.ConsumeMP = 50;
                        ppb.Damage = 400;
                        ppb.Exp = 7500;
                        ppb.WeightHP = 225;
                        ppb.WeightMP = 50;
                        ppb.WeightDamage = 25;
                        break;
                    }
                default:
                    {
                        ppb.HP = 2500;
                        ppb.MP = 500;
                        ppb.ConsumeMP = 10;
                        ppb.Damage = 175;
                        ppb.Exp = 2500;
                        ppb.WeightHP = 150;
                        ppb.WeightMP = 25;
                        ppb.WeightDamage = 6;
                        break;
                    }
            }
            return ppb;
        }
    }
}

public partial class TowerProperty : MonoBehaviour
{
    private void Start()
    {
        GSUIManager = GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).GetComponent<GameSceneUIManager>();
        // launcher = transform.Find("TowerBulletLauncher").GetComponent<TowerBulletLauncher>();
        levelText = expInner.transform.Find("ExpOutward/Level").GetComponent<Text>();

        if (PlayerAttributeManager.Singleton)
        {
            switch (towerType)
            {
                case TowerType.Core:
                    {
                        Level = PlayerAttributeManager.Singleton.HeroAchievement.tcolevel;
                        break;
                    }
                case TowerType.No01:
                    {
                        Level = PlayerAttributeManager.Singleton.HeroAchievement.tno01level;
                        break;
                    }
                case TowerType.No02:
                    {
                        Level = PlayerAttributeManager.Singleton.HeroAchievement.tno02level;
                        break;
                    }
                case TowerType.No03:
                    {
                        Level = PlayerAttributeManager.Singleton.HeroAchievement.tno03level;
                        break;
                    }
                case TowerType.No04:
                    {
                        Level = PlayerAttributeManager.Singleton.HeroAchievement.tno04level;
                        break;
                    }
                case TowerType.No05:
                    {
                        Level = PlayerAttributeManager.Singleton.HeroAchievement.tno05level;
                        break;
                    }
            }

            float wHP = (towerType == TowerType.Core) ? 2.0f : 1.5f;
            healthManager = transform.GetComponent<HealthManager>();
            healthManager.Health = (PPB.HP + (PPB.HP * wHP * (Level - 1)));
            healthManager.DefaultHealth = healthManager.Health;
            healthManager.MaxHealth = healthManager.Health;

            float wMP = (towerType == TowerType.Core) ? 0.8f : 0.4f;
            powerManager = transform.GetComponent<PowerManager>();
            powerManager.Power = (PPB.MP + (PPB.MP * wMP * (Level - 1)));
            powerManager.DefaultPower = powerManager.Power;
            powerManager.MaxPower = powerManager.Power;

            powerManager.ActivityRecoverPower((towerType == TowerType.Core), 5.0f);

            float wDamage = 1000 * ((towerType == TowerType.Core) ? 0.25f : 0.06f) * (Level - 1);
            Damage = (PPB.Damage + Convert.ToInt32(wDamage));

            FireStoneQuantity = Convert.ToInt32(Mathf.Floor((Level / 10)) + 1);
            ExpQuantity = FireStoneQuantity * 500;
            ExpRequired = (PPB.Exp + (((towerType == TowerType.Core) ? 2500 : 300) * (Level - 1)));

            ConsumeMP = PPB.ConsumeMP + (((towerType == TowerType.Core) ? 2000 : 1000) * (1 / 100) * (Level - 1));

            float _c = CurrentExp;
            float _e = ExpRequired;
            float fillAmount = _c / _e;
            // Debug.Log("fillAmount " + fillAmount);
            expInner.fillAmount = fillAmount;

            TS.gameObject.SetActive((towerType != TowerType.Core));
        }
    }

    private void Update()
    {
        if (TS)
        {
            // Debug.Log("True");
            TS.Damage = Damage;
            TS.ConsumeMP = ConsumeMP;
        }
        if (player != null) { return; }
        if (Camera.main.GetComponent<ThirdCamera>() == null) { return; }
        if (Camera.main.GetComponent<ThirdCamera>().target) { player = Camera.main.GetComponent<ThirdCamera>().target; }
    }
}

public partial class TowerProperty : MonoBehaviour
{
    public void TowerUpgrade()
    {
        if (Level == maxLevel) { return; }
        if (GSUIManager.FireStoneQuantity < FireStoneQuantity) { return; }

        GSUIManager.FireStoneQuantity -= FireStoneQuantity;

        CurrentExp += ExpQuantity;

        if (CurrentExp >= ExpRequired)
        {
            UpLevel();
        }
    }
}

public partial class TowerProperty : MonoBehaviour
{
    private void UpLevel()
    {
        player.GetComponent<PlayerProperty>().OnReceiveTrackingEvent(PointsType.TowerUpLevel);

        Level += 1;
        if (Level == maxLevel)
        {
            Button button = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            if (button) { button.interactable = false; }
        }

        float wHP = (towerType == TowerType.Core) ? 2.0f : 1.5f;
        healthManager = transform.GetComponent<HealthManager>();
        healthManager.Health = (PPB.HP + (PPB.HP * wHP * (Level - 1)));
        healthManager.DefaultHealth = healthManager.Health;
        healthManager.MaxHealth = healthManager.Health;

        float wMP = (towerType == TowerType.Core) ? 0.8f : 0.4f;
        powerManager = transform.GetComponent<PowerManager>();
        powerManager.Power = (PPB.MP + (PPB.MP * wMP * (Level - 1)));
        powerManager.DefaultPower = powerManager.Power;
        powerManager.MaxPower = powerManager.Power;

        powerManager.ActivityRecoverPower((towerType == TowerType.Core), 5.0f);

        float wDamage = 1000 * ((towerType == TowerType.Core) ? 0.25f : 0.06f) * (Level - 1);
        Damage = (PPB.Damage + Convert.ToInt32(wDamage));

        FireStoneQuantity = Convert.ToInt32(Mathf.Floor((Level / 10)) + 1);
        ExpQuantity = FireStoneQuantity * 500;
        ExpRequired = (PPB.Exp + (((towerType == TowerType.Core) ? 2500 : 300) * (Level - 1)));

        ConsumeMP = PPB.ConsumeMP + (((towerType == TowerType.Core) ? 2500 : 1000) * (1 / 100) * (Level - 1));

        /// Reset the current exp to 0 or
        /// The CurrentExp minues the ExpRequired If the CurrentExp more than ExpRequired.
        if (CurrentExp >= ExpRequired)
        {
            CurrentExp -= ExpRequired;
        }
        else
        {
            CurrentExp = 0;
        }

        if (PlayerAttributeManager.Singleton)
        {
            HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievement;
            switch (towerType)
            {
                case TowerType.Core:
                    {
                        h.tcolevel = Level;
                        break;
                    }
                case TowerType.No01:
                    {
                        h.tno01level = Level;
                        break;
                    }
                case TowerType.No02:
                    {
                        h.tno02level = Level;
                        break;
                    }
                case TowerType.No03:
                    {
                        h.tno03level = Level;
                        break;
                    }
                case TowerType.No04:
                    {
                        h.tno04level = Level;
                        break;
                    }
                case TowerType.No05:
                    {
                        h.tno05level = Level;
                        break;
                    }
            }
            PlayerAttributeManager.Singleton.HeroAchievement = h;
        }
    }
}

public partial class TowerProperty : MonoBehaviour
{
    internal void GainExperienceFromEnemey(int exp)
    {
        Debug.Log("The Exp is " + exp);

        if (Level == maxLevel) { return; }

        CurrentExp += exp;

        if (CurrentExp >= ExpRequired)
        {
            UpLevel();
        }
    }
}
