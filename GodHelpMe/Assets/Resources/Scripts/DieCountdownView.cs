﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DieCountdownView : MonoBehaviour
{
    internal System.EventHandler OnDidCountdownFinished;

    public UnityEvent countdownFinishedEvent;

    [SerializeField]
    private int defaultTime = 60;
    private int currentTime;

    private Text time;

    private void Start()
    {
        time = transform.Find("Countdown/Time").GetComponent<Text>();
        currentTime = defaultTime;
    }

    private void OnEnable()
    {
        InvokeRepeating("CountdownTime", 0, 1);
    }

    private void OnDisable()
    {
        currentTime = defaultTime;
        CancelInvoke();
    }

    private void CountdownTime()
    {
        time.text = currentTime.ToString();

        if (currentTime == 0)
        {
            if (OnDidCountdownFinished != null) { OnDidCountdownFinished(this, System.EventArgs.Empty); }
            CancelInvoke();
            gameObject.SetActive(false);
            countdownFinishedEvent.Invoke();
            return;
        }
        else
        {
            currentTime -= 1;
        }
    }
}
