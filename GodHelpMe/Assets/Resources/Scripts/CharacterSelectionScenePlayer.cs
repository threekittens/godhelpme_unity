﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public partial class CharacterSelectionScenePlayer : MonoBehaviour
{
    internal int playerIndex = 0;
    internal int selectedHeroIndex = -1;
    private TextAsset[] heroDatas;
    private GameObject playerBoard;
    private GameObject characterButtonList;
    private int timeLimit = 30;
    private Text time;
    //private Slider loadingBar;
    //private Text loadingText;
    //private AsyncOperation async = null; // When assigned, load is in progress.
    private HeroData selectedHeroData;
    private Dictionary<int, string> selectedHeroIDs = new Dictionary<int, string>();
}

public partial class CharacterSelectionScenePlayer : MonoBehaviour
{
    private void Start()
    {
        heroDatas = Resources.LoadAll<TextAsset>("Data/Heros");
        playerBoard = GameObject.Find("PlayersBoard");
        characterButtonList = GameObject.Find("USERINTERFACEMANAGER/HeroListBackground/Mask/CharacterButtonList");
        time = GameObject.Find("USERINTERFACEMANAGER/Time/Text").GetComponent<Text>();

        InvokeRepeating("spawnCountdownTime", 0, 1);
    }
}

public partial class CharacterSelectionScenePlayer : MonoBehaviour
{
    //// Callback signature
    //public delegate void ChooseedCallback(int playerIndex, int selectedHeroIndex);

    //// Event declaration
    //public event ChooseedCallback OnSelectedHeroOnPlayerBoard;

    //internal void SelectedHeroOnPlayerBoard(int playerIndex, int selectedHeroIndex)
    //{

    //    this.playerIndex = playerIndex;

    //    if (this.selectedHeroIndex != -1)
    //    {
    //        Button privousButton = characterButtonList.transform.GetChild(this.selectedHeroIndex).GetComponent<Button>();
    //        privousButton.interactable = true;
    //        privousButton.image.color = new Color32(255, 255, 225, 255);
    //    }

    //    this.selectedHeroIndex = selectedHeroIndex;

    //    Button currentButton = characterButtonList.transform.GetChild(selectedHeroIndex).GetComponent<Button>();
    //    currentButton.interactable = false;
    //    currentButton.image.color = new Color32(255, 255, 225, 100);

    //    // The part is for here name.
    //    string JSON = heroDatas[selectedHeroIndex].text;
    //    selectedHeroData = JsonUtility.FromJson<HeroData>(JSON);

    //    // The part is for hero name.     
    //    Image playerSelectedHeroPic = playerBoard.transform.GetChild(playerIndex).transform.Find("PlayerPicture").GetComponent<Image>();
    //    Sprite heroSprite = Resources.Load<Sprite>(selectedHeroData.picturePath);
    //    playerSelectedHeroPic.sprite = heroSprite;

    //    addName(playerIndex, selectedHeroData.id);
    //}

    //private void addName(int playerIndex, string selectedHeroDataID)
    //{
    //    if (selectedHeroIDs.ContainsKey(playerIndex))
    //    {
    //        selectedHeroIDs[playerIndex] = selectedHeroDataID;
    //        return;
    //    }
    //    selectedHeroIDs.Add(playerIndex, selectedHeroDataID);
    //}
}


public partial class CharacterSelectionScenePlayer : MonoBehaviour
{

    private void spawnCountdownTime()
    {
        timeLimit--;

        countDownTime(timeLimit);

        if (timeLimit == 0)
        {
            Debug.LogWarning("Time stopped");
            CancelInvoke("spawnCountdownTime");
            PlayerAttributeManager.Singleton.SelectedHeroIDs = selectedHeroIDs;
            SceneManager.LoadSceneAsync(ProjectKeywordList.kSceneForGame);
        }
    }

    private void countDownTime(int timeLimit)
    {
        // 将 秒数 传入 并显示 成文字
        TimeSpan result = TimeSpan.FromSeconds(timeLimit);
        time.text = string.Format("{0:00}:{1:00}", result.Minutes, result.Seconds);
    }
}

public partial class CharacterSelectionScenePlayer : MonoBehaviour
{

}

public partial class CharacterSelectionScenePlayer : MonoBehaviour
{
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //do stuff        
        if (scene.name == ProjectKeywordList.kSceneForGame)
        {
            Debug.Log(scene.name);
        }
    }
}

public partial class CharacterSelectionScenePlayer : MonoBehaviour
{

}

public partial class CharacterSelectionScenePlayer : MonoBehaviour
{

}
