﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class AnimationClipOverrides : List<KeyValuePair<AnimationClip, AnimationClip>>
{
    public AnimationClipOverrides(int capacity) : base(capacity) { }

    public AnimationClip this[string name]
    {
        get { return this.Find(x => x.Key.name.Equals(name)).Value; }
        set
        {
            int index = this.FindIndex(x => x.Key.name.Equals(name));
            if (index != -1)
                this[index] = new KeyValuePair<AnimationClip, AnimationClip>(this[index].Key, value);
        }
    }
}

[RequireComponent(typeof(Animator))]
public class AnimatorRelay : MonoBehaviour
{
    private Thread thread;
    public List<AnimationClip> animationClips;
    internal AnimatorOverrideController animatorOverrideController;
    //public SerializedProperty animationClips;
    protected Animator animator;
    protected AnimationClipOverrides clipOverrides;

    public void Start()
    {
        animator = GetComponent<Animator>();
        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;

        clipOverrides = new AnimationClipOverrides(animatorOverrideController.overridesCount);
        animatorOverrideController.GetOverrides(clipOverrides);
        UpdateAnimationClips();
        //thread = new Thread(UpdateAnimationClips);
        //thread.Start();
    }

    private void UpdateAnimationClips()
    {
        if (animationClips.Count > 0)
        {
            foreach (AnimationClip clip in animationClips)
            {
                clipOverrides[clip.name] = clip;
                //animatorOverrideController[clip.name] = clip;
                //Debug.Log(clip.name);
            }

            animatorOverrideController.ApplyOverrides(clipOverrides);
        }
    }

    private void Update()
    {

    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(AnimatorRelay))]
public class AnimatorRelayEditor : Editor
{
    void OnEnable()
    {

    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUIUtility.labelWidth = 0;
        EditorGUIUtility.fieldWidth = 0;
        SerializedProperty clips = serializedObject.FindProperty("animationClips");
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(clips, true);
        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();
        EditorGUIUtility.labelWidth = 25;
        EditorGUIUtility.fieldWidth = 50;

        //for (int i = 0; i < clips.arraySize; i++)
        //{
        //    var prop = clips.GetArrayElementAtIndex(i);
        //    AnimationClip clip = (AnimationClip)prop.objectReferenceValue;
        //    animatorRelay.animatorOverrideController[clip.name] = clip;
        //}                                 
    }
}
#endif
