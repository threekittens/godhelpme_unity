﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicTriggerReceiverEventArgs : System.EventArgs
{
    private Collider other;
    private Collider2D collision;

    public Collider Other { get { return other; } set { other = value; } }
    public Collider2D Collision { get { return collision; } set { collision = value; } }

    public PhysicTriggerReceiverEventArgs() { }
}

public class PhysicTriggerReceiver : MonoBehaviour
{
    public event System.EventHandler OnReceiveTriggerEnter;
    public event System.EventHandler OnReceiveTriggerExit;
    public event System.EventHandler OnReceiveTriggerStay;

    public event System.EventHandler OnReceiveTriggerEnter2D;
    public event System.EventHandler OnReceiveTriggerExit2D;
    public event System.EventHandler OnReceiveTriggerStay2D;

    private void OnTriggerEnter(Collider other)
    {
        if (OnReceiveTriggerEnter != null)
        {
            PhysicTriggerReceiverEventArgs e = new PhysicTriggerReceiverEventArgs();
            e.Other = other;
            OnReceiveTriggerEnter(this, e);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (OnReceiveTriggerExit != null)
        {
            PhysicTriggerReceiverEventArgs e = new PhysicTriggerReceiverEventArgs();
            e.Other = other;
            OnReceiveTriggerExit(this, e);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (OnReceiveTriggerStay != null)
        {
            PhysicTriggerReceiverEventArgs e = new PhysicTriggerReceiverEventArgs();
            e.Other = other;
            OnReceiveTriggerStay(this, e);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (OnReceiveTriggerEnter2D != null)
        {
            PhysicTriggerReceiverEventArgs e = new PhysicTriggerReceiverEventArgs();
            e.Collision = collision;
            OnReceiveTriggerEnter2D(this, e);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (OnReceiveTriggerExit2D != null)
        {
            PhysicTriggerReceiverEventArgs e = new PhysicTriggerReceiverEventArgs();
            e.Collision = collision;
            OnReceiveTriggerExit2D(this, e);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (OnReceiveTriggerStay2D != null)
        {
            PhysicTriggerReceiverEventArgs e = new PhysicTriggerReceiverEventArgs();
            e.Collision = collision;
            OnReceiveTriggerStay2D(this, e);
        }
    }

}
