﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;
using System.Threading;
using UnityEngine.SceneManagement;

/// <summary>
/// Partial below is for define parameter.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    private Image network;
    private Text networkText;

    private Image battery;
    private Text batteryText;

    private Text timeText;

    [SerializeField]
    private Slider BGMSlider;
    [SerializeField]
    private Slider SESlider;

    [SerializeField]
    private Button BGMMuteHook;
    [SerializeField]
    private Button SEMuteHook;

    [SerializeField]
    private Sprite hookSprite;
    [SerializeField]
    private Sprite unHookSprite;

    [SerializeField]
    private Button sginoutButton;
}

/// <summary>
/// Partial below is for when the script running.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    private void Start()
    {
        EngraveThePhotonNetworkPingValueOnPanel();
        EngraveTheBatteryStatusOnPanel();
        EngraveTheRealTimeOnPanel();

        if (BGMManager.Singleton)
        {
            BGMSlider.value = BGMManager.Singleton.AudioSource.volume;
            BGMMuteHook.image.sprite = BGMManager.Singleton.AudioSource.mute ? unHookSprite : hookSprite;
        }

        if (SEManager.Singleton)
        {
            SESlider.value = SEManager.Singleton.AudioSource.volume;
            SEMuteHook.image.sprite = SEManager.Singleton.AudioSource.mute ? unHookSprite : hookSprite;
        }

        //Thread thr = new Thread(Func_NoArguments);
        //thr.Start();
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name == ProjectKeywordList.kSceneForGame)
        {
            sginoutButton.gameObject.SetActive(false);
        }
        else
        {
            sginoutButton.gameObject.SetActive(true);
        }

        StartCoroutine(UpdateRealTimeText());
        StartCoroutine(UpdateBatteryStatus());
        StartCoroutine(UpdatePhotonNetworkPineValue());
    }
}

/// <summary>
/// Partial below is for engrave the real time on panel.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    private void EngraveTheRealTimeOnPanel()
    {
        timeText = transform.Find("Background/SystemStatus/CurrentTime/Text").GetComponent<Text>();
    }
}

/// <summary>
/// Partial below is for...
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    IEnumerator UpdateRealTimeText()
    {
        timeText.text = String.Format("{0:T}", DateTime.Now);
        yield break;
    }
}

/// <summary>
/// Partial below is for engrave the battery status on panel.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    private void EngraveTheBatteryStatusOnPanel()
    {

        battery = transform.Find("Background/SystemStatus/Battery/BatteryOutside/BatteryInsid").GetComponent<Image>();

        battery.fillAmount = SystemInfo.batteryLevel;

        batteryText = transform.Find("Background/SystemStatus/Battery/Text").GetComponent<Text>();

        batteryText.text = String.Format("{0}%", SystemInfo.batteryLevel * 100f);

        //Debug.Log("Battery state: " + SystemInfo.batteryStatus);
        //Debug.Log("Battery level: " + SystemInfo.batteryLevel);
    }
}


/// <summary>
/// Partial below is for update battery status.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    IEnumerator UpdateBatteryStatus()
    {
        battery.fillAmount = SystemInfo.batteryLevel;
        batteryText.text = String.Format("{0}%", SystemInfo.batteryLevel * 100f);
        yield break;
    }
}

/// <summary>
/// Partial below is for engrave the network ping value on panel.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    private void EngraveThePhotonNetworkPingValueOnPanel()
    {
        //if (PUNManager.Singleton)
        //{
        //    network = transform.Find("Background/SystemStatus/Network/Image").GetComponent<Image>();
        //    networkText = transform.Find("Background/SystemStatus/Network/Text").GetComponent<Text>();
        //    networkText.text = String.Format("{0}ms", PUNManager.Singleton.GetPine());
        //}
    }
}

/// <summary>
/// Partial below is for update battery status.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    IEnumerator UpdatePhotonNetworkPineValue()
    {
        //if (PUNManager.Singleton)
        //{
        //    networkText.text = String.Format("{0}ms", PUNManager.Singleton.GetPine());
        //    //Debug.Log(PUNManager.Singleton.GetPine());
        //}
        yield break;
    }
}

/// <summary>
/// Partial below is for event of close button.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    public void Close()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }        
        DestroyImmediate(this.gameObject);
    }
}

/// <summary>
/// The partial is for call OnDisable and OnDestory.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    private void OnDisable()
    {
        //Debug.Log("OnDisable");
        Resources.UnloadUnusedAssets();
    }

    private void OnDestroy()
    {
        //Debug.Log("OnDestroy");
        Resources.UnloadUnusedAssets();
    }
}


/// <summary>
/// part of below is for Sign Out Button Event.
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{
    public void OnSignOutEvent()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayDecideSE();
        }            
        if (FirebaseManager.Singleton) { FirebaseManager.Singleton.SignOut(); }
    }

    public void BGMSliderControl(Slider slider)
    {
        if (BGMManager.Singleton)
        {
            AudioSource a = BGMManager.Singleton.AudioSource;
            a.volume = slider.value;
            BGMManager.Singleton.AudioSource = a;
        }
    }

    public void SESliderControl(Slider slider)
    {
        if (SEManager.Singleton)
        {
            AudioSource a = SEManager.Singleton.AudioSource;
            a.volume = slider.value;
            SEManager.Singleton.AudioSource = a;
        }

    }

    public void BGMMute(Button button)
    {
        if (BGMManager.Singleton)
        {
            AudioSource a = BGMManager.Singleton.AudioSource;
            a.mute = !a.mute;
            button.image.sprite = a.mute ? unHookSprite : hookSprite;
            BGMManager.Singleton.AudioSource = a;
        }
    }

    public void SEMute(Button button)
    {
        if (SEManager.Singleton)
        {
            AudioSource a = SEManager.Singleton.AudioSource;
            a.mute = !a.mute;
            button.image.sprite = a.mute ? unHookSprite : hookSprite;
            SEManager.Singleton.AudioSource = a;
        }
    }
}

/// <summary>
/// Partial below is for...
/// </summary>
public partial class SettingAlertor : MonoBehaviour
{

}
