﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HealthManager))]
public partial class EnemyProperty : MonoBehaviour
{
    [SerializeField]
    private ResourceType resourceType;
    internal ResourceType ResourceType { get { return resourceType; } }

    [SerializeField]
    private int rewardMaximumQuantity;
    internal int RewardMaximumQuantity { get { return RewardMaximumQuantity; } }

    private int cc;
    internal int CC
    {
        get
        {
            return cc;
        }
        set
        {
            cc = value;
            Debug.Log("How many the cc's value = " + cc);
        }
    }

    [SerializeField]
    private int exp;
    internal int Exp { get { return exp; } set { exp = value; } }

    [SerializeField]
    private long score;
    internal long Score { get { return score; } set { score = value; } }

    [SerializeField]
    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }

    private struct EnemyBasic
    {
        public int HP, Damage, Exp, WeightExp, WeightBossExp;
        public long Score, WeightBossScore;

        public EnemyBasic(int HP, int Damage, int Exp, int WeightExp, int WeightBossExp, long Score, long WeightBossScore)
        {
            this.HP = HP;
            this.Damage = Damage;

            this.Exp = Exp;
            this.WeightExp = WeightExp;
            this.WeightBossExp = WeightBossExp;

            this.Score = Score;
            this.WeightBossScore = WeightBossScore;
        }
    }

    [SerializeField]
    private EnemyBasic eb;
    private EnemyBasic EB
    {
        get
        {
            eb.HP = 125;
            eb.Damage = 10;
            eb.Exp = 11;
            eb.WeightExp = 3;
            eb.WeightBossExp = 10;
            eb.Score = 10;
            eb.WeightBossScore = 1000;
            return eb;
        }
    }

    private HealthManager healthManager;
    private GameSceneUIManager gameSceneUIManager;

    private enum MonsterType
    {
        Boss = 0,
        Enemy = 1
    }

    [SerializeField]
    private MonsterType monsterType;
}

public partial class EnemyProperty : MonoBehaviour
{
    private void Awake()
    {
        gameSceneUIManager = GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).GetComponent<GameSceneUIManager>();
    }

    private void Start()
    {
        Debug.Log("Started Initialize the EnemeyProperty...");
        Damage = EB.Damage * CC;
        Score = (CC % 5 == 0) ? ((EB.Score * CC) + EB.WeightBossScore) : (EB.Score * CC);
        Exp = (CC % 5 == 0) ? (EB.Exp + (EB.WeightExp * CC)) * EB.WeightBossExp : (EB.Exp + (EB.WeightExp * CC));
        healthManager = gameObject.GetComponent<HealthManager>();
        healthManager.Health = (CC % 5 == 0) ? ((EB.HP * (CC + 1)) + (500 * CC)) : (EB.HP * (CC + 1));
        healthManager.DefaultHealth = healthManager.Health;
        healthManager.MaxHealth = healthManager.Health;
        healthManager.OnHealthManagerDieNotice += OnHealthManagerDieNotice;

        MonoBehaviour[] c_array = transform.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour c in c_array) { c.enabled = true; }
    }

    private void OnDestroy()
    {
        healthManager.OnHealthManagerDieNotice -= OnHealthManagerDieNotice;
    }

    private void OnHealthManagerDieNotice(GameObject gobj)
    {
        if (gobj == gameObject)
        {
            if (PlayerAttributeManager.Singleton)
            {
                HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievement;
                h.score += Score;
                PlayerAttributeManager.Singleton.HeroAchievement = h;
            }

            if (gameSceneUIManager)
            {
                gameSceneUIManager.OnUpdateResourcePanelInformation(resourceType, rewardMaximumQuantity);
            }

            GameObject TC = GameObject.FindGameObjectWithTag(ProjectKeywordList.kTagForTowerCore);
            if (TC != null)
            {
                TowerProperty TP = TC.GetComponent<TowerProperty>();
                TP.GainExperienceFromEnemey(Exp);
            }

            GameObject a = healthManager.Attacker;
            if (a == null) { return; }

            if (a.tag == ProjectKeywordList.kTagForPlayer)
            {
                PlayerProperty PP = a.GetComponent<PlayerProperty>();
                PP.GainExperienceFromEnemey(Exp);

                switch (monsterType)
                {
                    case MonsterType.Boss:
                        {
                            PP.OnReceiveTrackingEvent(PointsType.Boss);
                            break;
                        }
                    case MonsterType.Enemy:
                        {
                            PP.OnReceiveTrackingEvent(PointsType.Enemy);
                            break;
                        }
                }
            }
        }
    }
}
