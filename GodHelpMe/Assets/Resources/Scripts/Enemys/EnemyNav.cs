﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyTarget { Player, TowerCore, Tower01, Tower02, Tower03, Tower04, Tower05 }

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(HealthManager))]
public class EnemyNav : MonoBehaviour
{
    internal delegate void NavStoppedAtTarget(bool status);
    internal NavStoppedAtTarget OnNavStoppedAtTarget;

    internal Transform Target { get { return target; } }

    internal float DefaultStopDis { get { return defaultStopDis; } }

    internal bool NavStopped
    {
        set
        {
            if (animator) { animator.SetBool(ProjectKeywordList.kAnimationForMove, !value); }

            //Debug.Log("NavStopped => " + value);

            if (stopForTheHealthEmpty)
            {
                if (OnNavStoppedAtTarget != null) { OnNavStoppedAtTarget(false); }
            }
            else
            {
                if (OnNavStoppedAtTarget != null) { OnNavStoppedAtTarget(value); }
            }
        }
    }
    internal NavMeshAgent agent;
    internal bool spawned = false;

    [SerializeField]
    private bool showDrawGizmos = false;
    [SerializeField]
    private float fixedDropSpeed = 5.0f;
    [SerializeField]
    private float fixedPosAxisY = 1.0f;
    [SerializeField]
    private float fixedStopDis;
    private float defaultStopDis;

    private Transform target;
    private Animator animator;
    private HealthManager healthManager;

    private bool isTheHealthEmpty = false;
    private bool stopForTheHealthEmpty = false;

    [SerializeField]
    private EnemyTarget defaultPriorityTarget;

    public bool AssignedTargetTag(EnemyTarget priorityTarget)
    {
        string tag = GetTagByEnemyTargets(priorityTarget);

        if (!GameObject.FindGameObjectWithTag(tag)) { return false; }

        defaultPriorityTarget = priorityTarget;
        target = GameObject.FindGameObjectWithTag(tag).transform;
        return true;
    }

    // Use this for initialization
    //void OnEnable()
    //{
    //    Initialize();
    //}

    private void Start()
    {
        Initialize();
        //Debug.Log("Initialize");
    }

    private void Initialize()
    {
        agent = transform.GetComponent<NavMeshAgent>();
        //Debug.Log(agent);
        animator = transform.GetComponent<Animator>();
        healthManager = transform.GetComponent<HealthManager>();
        healthManager.OnTheHealthEmpty += OnTheHealthEmpty;
        AssignedTargetTag(defaultPriorityTarget);
        spawned = true;
    }

    private void Update()
    {
        if (spawned == false) { return; }
        StartCoroutine(UpdateSearchTarget());
    }

    IEnumerator UpdateSearchTarget()
    {
        if (agent == null) { yield break; }
        if (target)
        {
            //Debug.Log("Finded target");
            transform.LookAt(target);
            transform.position = new Vector3(transform.position.x, fixedPosAxisY, transform.position.z);
            transform.rotation = Quaternion.Euler(0.0f, transform.rotation.eulerAngles.y, 0.0f);

            defaultStopDis = target.GetComponent<Collider>().bounds.size.x / 2.0f + fixedStopDis;
            //Debug.Log(defaultStopDis);

            float betweenDis = Vector3.Distance(transform.position, target.position);
            //Debug.Log(betweenDis);

            if (isTheHealthEmpty)
            {
                if (!stopForTheHealthEmpty)
                {
                    stopForTheHealthEmpty = true;

                    //Debug.Log("Stop for health <= 0.0");
                    agent.isStopped = true;
                    NavStopped = agent.isStopped;
                }

                yield break;
            }
            else if (betweenDis <= defaultStopDis)
            {
                if (!agent.isStopped)
                {
                    //Debug.Log("betweenDis <= defaultStopDis");
                    agent.isStopped = true;
                    NavStopped = agent.isStopped;
                }
                yield break;
            }
            else
            {
                //Debug.Log("Keep going");
                agent.isStopped = false;
                agent.SetDestination(target.position);
                NavStopped = agent.isStopped;
                yield break;
            }
        }
        else
        {
            //Debug.Log("Target disappear");
            //// If enemy doesn't finds any target nav to stop
            //if (AssignedTargetTag(defaultPriorityTarget))
            //{
            //    yield break;
            //}
            //else if (AssignedTargetTag(EnemyTarget.TowerCore))
            //{
            //    yield break;
            //}

            //nav.isStopped = true;
            //animator.SetBool(ProjectKeywordList.kAnimationForMove, !nav.isStopped);
        }

        yield break;
    }

    private void OnTheHealthEmpty(object o, System.EventArgs e)
    {
        isTheHealthEmpty = true;
        Vector3 newPos = new Vector3(transform.position.x, 0.0f, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, newPos, fixedDropSpeed * Time.deltaTime);
        fixedPosAxisY = transform.position.y;
    }

    private string GetTagByEnemyTargets(EnemyTarget target)
    {
        string rtn = default(string);
        switch (target)
        {
            case EnemyTarget.Player:
                rtn = "Player";
                break;
            case EnemyTarget.TowerCore:
                rtn = "TowerCore";
                break;
            case EnemyTarget.Tower01:
                rtn = "Tower01";
                break;
            case EnemyTarget.Tower02:
                rtn = "Tower02";
                break;
            case EnemyTarget.Tower03:
                rtn = "Tower03";
                break;
            case EnemyTarget.Tower04:
                rtn = "Tower04";
                break;
            case EnemyTarget.Tower05:
                rtn = "Tower05";
                break;
        }

        return rtn;
    }

    //Draw a sphere by range.
    private void OnDrawGizmos()
    {
        if (showDrawGizmos)
        {
            Color c = Color.red;
            c.a = 0.5f;
            Gizmos.color = c;
            Gizmos.DrawSphere(transform.position, defaultStopDis / 2.0f + fixedStopDis);
        }
    }
}