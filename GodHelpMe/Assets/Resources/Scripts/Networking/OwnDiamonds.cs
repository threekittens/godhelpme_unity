﻿using System;

[Serializable]
public class OwnDiamonds
{
    public int count;
    public OwnDiamonds() { }
    public OwnDiamonds(int count)
    {
        this.count = count;
    }
    public void UpdateOwnDiamonds(int count)
    {
        this.count = count;
    }
}