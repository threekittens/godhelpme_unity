﻿using System;

[Serializable]
public class GradeInfomation
{
    public int killboss = 0;
    public int killenemy = 0;
    public int repairtower = 0;
    public int uptower = 0;

    public GradeInfomation() { }

    public GradeInfomation(int killboss, int killenemy, int repairtower, int uptower)
    {
        this.killboss = killboss;
        this.killenemy = killenemy;
        this.repairtower = repairtower;
        this.uptower = uptower;
    }
}
