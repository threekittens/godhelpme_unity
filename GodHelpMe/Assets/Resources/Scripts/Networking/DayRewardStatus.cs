﻿using System;

[Serializable]
public class DayRewardStatus
{
    public int day;
    public bool reset;
    public bool isgain;
    public string previous;

    public DayRewardStatus() { }

    public DayRewardStatus(int day, bool reset, bool isgain, string previous)
    {
        this.day = day;
        this.reset = reset;
        this.isgain = isgain;
        this.previous = previous;
    }
}
