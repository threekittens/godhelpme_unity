﻿using System;

[Serializable]
public class SignedPlayer
{
    public string userid;
    public string registerdate;
    public string authkey;
    public string email;
    public string playername;
    public int gender;
    public int pictureindex;
    public string birthdate;

    public SignedPlayer() { }

    public SignedPlayer(string email)
    {
        this.email = email;
    }
}
