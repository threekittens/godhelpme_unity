﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HeroAchievement
{
    public string userid;
    public string uid;
    public int level = 1;
    public int exp = 0;
    public int reincarnation = 0;
    public int checkpoint = 0;
    public long score = 0;
    public int skill01 = 1;
    public int skill02 = 0;
    public int skill03 = 0;
    public int skill04 = 0;
    public int tcolevel = 1;
    public int tcoexp = 0;
    public int tno01level = 1;
    public int tno01exp = 0;
    public int tno02level = 1;
    public int tno02exp = 0;
    public int tno03level = 1;
    public int tno03exp = 0;
    public int tno04level = 1;
    public int tno04exp = 0;
    public int tno05level = 1;
    public int tno05exp = 0;

    public HeroAchievement()
    {

    }
}

