﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragButtonController : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [SerializeField] private GameObject beDragButton;
    [SerializeField] private BoxCollider2D boxCollider2D;
    private void Start()
    {
        boxCollider2D.size = new Vector2(Screen.width - 25f, Screen.height - 25f);
        beDragButton.transform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        //RectTransform invPanel = transform as RectTransform;
        //if (!RectTransformUtility.RectangleContainsScreenPoint(invPanel, eventData.position)) { return; }

        if (!boxCollider2D.OverlapPoint(eventData.position)) { return; }
        beDragButton.transform.SetPositionAndRotation(eventData.position, Quaternion.identity);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //RectTransform invPanel = transform as RectTransform;
        //if (!RectTransformUtility.RectangleContainsScreenPoint(invPanel, eventData.position)) { return; }

        if (!boxCollider2D.OverlapPoint(eventData.position)) { return; }
        beDragButton.transform.SetPositionAndRotation(eventData.position, Quaternion.identity);
    }
}
