﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public partial class WatchingAdsAlertor : MonoBehaviour
{

    public void Close()
    {
        DestroyImmediate(gameObject);
    }
}

public partial class WatchingAdsAlertor : MonoBehaviour
{

#if UNITY_IOS
    private string gameId = "1670050";
#elif UNITY_ANDROID
    private string gameId = "1486550";
#elif UNITY_EDITOR
    private string gameId = "1670050";
#elif UNITY_STANDALONE_WIN
    private string gameId = "1670050";
#endif

    Button m_Button;
    public string placementId = "rewardedVideo";

    private void Start()
    {
        m_Button = transform.Find("Background/Content/WatchingButton").GetComponent<Button>();
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize(gameId, true);
        }
    }

    private void Update()
    {
        if (m_Button)
        {
            m_Button.interactable = Advertisement.IsReady(placementId);
        }
    }

    public void ShowAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show(placementId, options);
    }

    private void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");

        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}