﻿using UnityEngine;
using PathManager;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public partial class RewardAlertor : MonoBehaviour
{

    [SerializeField]
    private Animator shinyAE;

    private GameObject rewardItem;
    private int randomItemCount;
    private TextAsset[] itemDatas;

    private void Start()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayRewardAlertSE();
        }        
        StartCoroutine(ProduceItemOfBoard());
    }

    IEnumerator ProduceItemOfBoard()
    {
        rewardItem = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceRewardView("RewardItem"));
        //files = Directory.GetFiles(Application.dataPath + "/Resources/Data/ToolItems", "*.json");

        itemDatas = Resources.LoadAll<TextAsset>("Data/ToolItems");

        // first is 1 not 0, becaus if is 0 doesn't has any Item.
        randomItemCount = Random.Range(1, 6);

        Debug.Log("有 " + randomItemCount + " 个");

        for (int i = 0; i < randomItemCount; i++)
        {
            rewardItem = Instantiate(rewardItem, transform.Find("RewardBoard").transform);

            int rewardNum = Random.Range(0, itemDatas.Length);

            TextAsset JSONFile = itemDatas[rewardNum];

            string id = JSONFile.name;

            string JSON = JSONFile.text;

            ItemData itemData = JsonUtility.FromJson<ItemData>(JSON);
            Debug.Log(rewardNum);

            Image image = rewardItem.transform.Find("Background/Image").GetComponent<Image>();
            image.sprite = Resources.Load<Sprite>(itemData.iconPath);

            Text count = rewardItem.transform.Find("Background/Count").GetComponent<Text>();
            count.text = itemData.count.ToString();

            Text name = rewardItem.transform.Find("Name").GetComponent<Text>();
            name.text = itemData.name;

            Text introduce = rewardItem.transform.Find("Introduce").GetComponent<Text>();
            introduce.text = itemData.info;

            PlayerItem playerItem = new PlayerItem(id, itemData.count);

            if (PlayerAttributeManager.Singleton)
            {
                Debug.Log("Into singleton.");

                if (id == "5A613148-AD6F0B-359EB4CF")
                {
                    Debug.Log("Combine diamond count to player own count.");
                    PlayerAttributeManager.Singleton.PlayerOwnDiamonds += itemData.count;
                    continue;
                }

                if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(id))
                {
                    Debug.Log("Combine item by id.");
                    Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                    PIP[id].itemCount += itemData.count;
                    PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;
                }
                else
                {
                    Debug.Log("Add Item.");
                    Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                    PIP.Add(id, playerItem);
                    PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;
                }
            }
        }

        //foreach (KeyValuePair<string, PlayerItem> item in PlayerAttributeManager.Singleton.PlayerItemPackage)
        //{
        //    Debug.Log(item.Key + " => " + item.Value);
        //}

        yield return new WaitForSeconds(shinyAE.GetCurrentAnimatorStateInfo(0).length + shinyAE.GetCurrentAnimatorStateInfo(0).normalizedTime);
        shinyAE.gameObject.SetActive(false);

        yield break;
    }

    public void Countinu()
    {
        DestroyImmediate(this.rewardItem);
        DestroyImmediate(this.gameObject);
    }

    private void OnDestroy()
    {
        Debug.Log("OnDestroy");
        Resources.UnloadUnusedAssets();
    }

    private void OnDisable()
    {
        Debug.Log("OnDisable");
        Resources.UnloadUnusedAssets();
    }
}
