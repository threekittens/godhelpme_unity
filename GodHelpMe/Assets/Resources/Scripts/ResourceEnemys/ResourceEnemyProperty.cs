﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ResourceType
{
    FireStone = 0,
    LifeBranch = 1,
    FantasyCrystal = 2,
    HolyPureStone = 3
}

public partial class ResourceEnemyProperty : MonoBehaviour
{
    [SerializeField]
    private ResourceType mResourceType;

    [SerializeField]
    private int mRewardMaximumQuantity;

    private HealthManager mHealthManager;

    private GameSceneUIManager mGameSceneUIManager;
}

public partial class ResourceEnemyProperty : MonoBehaviour
{
    private void Start()
    {
        InitializeSolutionComponent();        
    }

    private void OnDestroy()
    {
        mHealthManager.OnHealthManagerDieNotice -= OnHealthManagerDieNotice;
    }
}

public partial class ResourceEnemyProperty : MonoBehaviour
{
    private void InitializeSolutionComponent()
    {
        mGameSceneUIManager = GameObject.Find("USERINTERFACEMANAGER").GetComponent<GameSceneUIManager>();
        mHealthManager = gameObject.GetComponent<HealthManager>();
        mHealthManager.OnHealthManagerDieNotice += OnHealthManagerDieNotice;
    }
}

public partial class ResourceEnemyProperty : MonoBehaviour
{
    private int RandomRewardQuantity()
    {
        return Random.Range(1, mRewardMaximumQuantity);
    }
}

public partial class ResourceEnemyProperty : MonoBehaviour
{
    private void OnHealthManagerDieNotice(GameObject gobj)
    {
        if (gobj == gameObject)
        {            
            if (mGameSceneUIManager)
            {
                mGameSceneUIManager.OnUpdateResourcePanelInformation(mResourceType, RandomRewardQuantity());
            }
        }
    }
}

public partial class ResourceEnemyProperty : MonoBehaviour
{

}

public partial class ResourceEnemyProperty : MonoBehaviour
{

}

public partial class ResourceEnemyProperty : MonoBehaviour
{

}
