﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceSpawn : MonoBehaviour
{
    private List<GameObject> spawnedEnemys = new List<GameObject>();
    [SerializeField] private float startSpawnTime;
    [SerializeField] private float intervalSpawnTime;
    [SerializeField] private GameObject resourceEnemy;

    private void Start()
    {
        StartCoroutine(ReSpawnResource());
    }

    IEnumerator ReSpawnResource()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.layer == LayerMask.NameToLayer("Default"))
            {
                GameObject SP = transform.GetChild(i).gameObject;
                GameObject instance = Instantiate(resourceEnemy, SP.transform);
                spawnedEnemys.Add(instance);
            }
        }

        InvokeRepeating("ActiviteSpawn", startSpawnTime, intervalSpawnTime);

        yield break;
    }

    private void ActiviteSpawn()
    {
        spawnedEnemys.Remove(spawnedEnemys.Find(e => e == null));

        if (spawnedEnemys.Count >= transform.childCount) { return; }

        int index = Random.Range(0, transform.childCount - 1);
        Transform t = transform.GetChild(index).transform;
        GameObject instance = Instantiate(resourceEnemy, t);
        spawnedEnemys.Add(instance);
    }
}
