﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyNav))]
public class CloseFighter : MonoBehaviour
{
    [SerializeField]
    private int delayBetweenEachAttacking = 1;

    [SerializeField]
    private float attackSpeed = 1.0f;

    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }

    private Animator animator;

    [SerializeField]
    private GameObject rightWeapon;
    [SerializeField]
    private GameObject leftWeapon;

    private CapsuleCollider rightCapsuleCollider;
    private CapsuleCollider leftCapsuleCollider;


    [SerializeField]
    private GameObject weaponEffect;

    [SerializeField]
    private GameObject hitEffect;

    private List<Collider> targetColliders = new List<Collider>();

    private EnemyNav enemyNav;

    private void Start()
    {
        Debug.Log("Started Initialize the CloseFighter...");

        animator = transform.GetComponent<Animator>();
        enemyNav = transform.GetComponent<EnemyNav>();
        enemyNav.OnNavStoppedAtTarget += OnNavStoppedAtTarget;

        if (rightWeapon)
        {
            rightCapsuleCollider = rightWeapon.GetComponent<CapsuleCollider>();
            CloseFightingInductor closeFightingInductor = rightWeapon.GetComponent<CloseFightingInductor>();
            if (transform.tag == ProjectKeywordList.kTagForEnemy) { Damage = GetComponent<EnemyProperty>().Damage; }
            closeFightingInductor.Damage = Damage;
            closeFightingInductor.OnHitedGameObject += RightCloseFightingInductor_OnHitedGameObject;
            if (weaponEffect)
            {
                GameObject instance = Instantiate(weaponEffect, rightWeapon.transform);
                instance.transform.localPosition = rightCapsuleCollider.transform.localPosition;
                ScaleEffect(instance.GetComponent<ParticleSystem>());
            }
        }

        if (leftWeapon)
        {
            leftCapsuleCollider = leftWeapon.GetComponent<CapsuleCollider>();
            CloseFightingInductor closeFightingInductor = leftWeapon.GetComponent<CloseFightingInductor>();
            if (transform.tag == ProjectKeywordList.kTagForEnemy) { Damage = GetComponent<EnemyProperty>().Damage; }
            closeFightingInductor.Damage = Damage;
            closeFightingInductor.OnHitedGameObject += LeftCloseFightingInductor_OnHitedGameObject;
            if (weaponEffect)
            {
                GameObject instance = Instantiate(weaponEffect, leftWeapon.transform);
                instance.transform.localPosition = leftCapsuleCollider.transform.localPosition;
                ScaleEffect(instance.GetComponent<ParticleSystem>());
            }
        }
    }

    private void OnDestroy()
    {
        enemyNav.OnNavStoppedAtTarget -= OnNavStoppedAtTarget;
    }

    private void LeftCloseFightingInductor_OnHitedGameObject(int damage, GameObject g)
    {
        SetEffectAndHurt(damage, g, leftCapsuleCollider);
    }

    private void RightCloseFightingInductor_OnHitedGameObject(int damage, GameObject g)
    {
        SetEffectAndHurt(damage, g, rightCapsuleCollider);
    }

    private void SetEffectAndHurt(int damage, GameObject g, CapsuleCollider weapon)
    {
        if (g.layer != LayerMask.NameToLayer(ProjectKeywordList.kLayerForAttackable)) { return; }

        Transform target = g.transform;
        //Debug.Log(string.Format("target => {0}", target.name));

        HealthManager helethManager = target.GetComponent<HealthManager>();
        helethManager.Health -= damage;

        Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(weapon.transform.position);

        FloatingTextControl.Initialize();
        FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);

        if (hitEffect)
        {
            GameObject effectInstance = Instantiate(hitEffect);
            ParticleSystem effectInstancePS = effectInstance.GetComponent<ParticleSystem>();
            ScaleEffect(effectInstancePS);
            effectInstance.transform.localPosition = contactPoint;
            Destroy(effectInstance, effectInstancePS.main.duration);
        }
    }

    private void ScaleEffect(ParticleSystem effectInstance)
    {
        ParticleSystem[] ps_array = effectInstance.GetComponentsInChildren<ParticleSystem>(true);
        ps_array[0].Stop();

        foreach (ParticleSystem ps in ps_array)
        {
            ps.transform.localScale *= 2;
        }

        //Scale Lights' range
        Light[] lights = effectInstance.GetComponentsInChildren<Light>();
        foreach (Light _light in lights)
        {
            _light.range *= 2;
            _light.transform.localPosition *= 2;
        }

        ps_array[0].Play();
    }

    private void OnNavStoppedAtTarget(bool status)
    {
        StartCoroutine(Attacking(status));
    }

    IEnumerator Attacking(bool status)
    {
        animator.SetFloat(ProjectKeywordList.kAnimationForAttackSpeed, attackSpeed);
        SetWeaponCollider(status);

        switch (status)
        {
            case false:
                CancelInvoke();
                animator.SetBool(ProjectKeywordList.kAnimationForAttack, status);
                break;
            case true:
                InvokeRepeating("ContinuousAttacking", 0, delayBetweenEachAttacking);
                break;
        }

        yield break;
    }

    private void SetWeaponCollider(bool status)
    {
        if (leftCapsuleCollider) { leftCapsuleCollider.enabled = status; }
        if (rightCapsuleCollider) { rightCapsuleCollider.enabled = status; }
    }

    private void ContinuousAttacking()
    {
        animator.SetBool(ProjectKeywordList.kAnimationForAttack, true);
    }
}
