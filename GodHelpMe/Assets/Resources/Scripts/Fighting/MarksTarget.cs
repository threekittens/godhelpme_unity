﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarksTarget : MonoBehaviour
{
    private List<Renderer> mRs = new List<Renderer>();

    // Use this for initialization
    private void Start()
    {
        StartCoroutine(GetAllRendererInChildren());
    }

    IEnumerator GetAllRendererInChildren()
    {
        transform.GetComponentsInChildren(mRs);
        yield break;
    }

    internal void ExecutesMarks(bool inRange)
    {
        StartCoroutine(ExecutesMarksRelay(inRange));
    }

    IEnumerator ExecutesMarksRelay(bool inRange)
    {
        yield return StartCoroutine(DrawOutline(inRange));
    }

    IEnumerator DrawOutline(bool inRange)
    {

        foreach (Renderer r in mRs)
        {
            var color = inRange == false ? Color.black : Color.red;
            r.material.SetColor("_OutlineColor", color);
        }
        yield break;
    }
}
