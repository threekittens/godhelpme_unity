﻿
[System.Serializable]
public class HeroData {

    public string id;
    public string name;
    public string coverPath;
    public string picturePath;
    public string introduce;
    public string prefabPath;
    public HeroSkillData[] skills;
    public int price;
}


[System.Serializable]
public class HeroSkillData
{
    public string id;
    public string name;
    public string info;
    public string iconPath;
}
