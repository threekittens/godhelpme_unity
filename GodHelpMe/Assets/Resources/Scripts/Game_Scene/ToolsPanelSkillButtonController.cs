﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PathManager;


public partial class ToolsPanelSkillButtonController : MonoBehaviour
{
    internal PlayerItem playerItem { get { return mPlayerItem; } set { mPlayerItem = value; } }
    private PlayerItem mPlayerItem;
    private Button mButton;
    private List<PlayerItem> mPlayerItemPanelList;
    private Text mCountText;
    private Transform mPlayer;
    private GameObject mBuffPanel;
    private GameObject mBuffPrefab;
}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{
    private void Start()
    {
        if (PlayerAttributeManager.Singleton == null)
        {
            return;
        }

        mPlayerItemPanelList = PlayerAttributeManager.Singleton.PlayerItemPanelList;
        mButton = transform.GetChild(0).GetComponent<Button>();
        mButton.onClick.AddListener(() => OnButtonClicked());
        mCountText = transform.Find("Button/Count/Text").GetComponent<Text>();
        mBuffPanel = GameObject.Find("USERINTERFACEMANAGER/BuffPanel");
        mBuffPrefab = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceGameScene("Buff"));
    }

    private void Update()
    {
        if (mPlayer)
        {
            return;
        }

        mPlayer = Camera.main.GetComponent<ThirdCamera>().target;
    }

    private void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }
}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{
    private void OnButtonClicked()
    {
        if (PlayerAttributeManager.Singleton)
        {
            if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(mPlayerItem.itemId))
            {
                if (PlayerAttributeManager.Singleton.PlayerItemPackage[mPlayerItem.itemId].itemCount == 0)
                {
                    return;
                }

                Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                PIP[mPlayerItem.itemId].itemCount -= 1;
                PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;

                mCountText.text = PlayerAttributeManager.Singleton.PlayerItemPackage[mPlayerItem.itemId].itemCount.ToString();

                TriggerBuffByItemID(mPlayerItem.itemId);
            }
        }
    }
}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{
    private void TriggerBuffByItemID(string itemID)
    {
        mButton.interactable = false;
        Debug.Log(string.Format("used a itemID {0}", itemID));

        GameObject buffInstance = Instantiate(mBuffPrefab, mBuffPanel.transform);
        Buff buff = buffInstance.GetComponent<Buff>();
        buff.OnBuffTimeOut += OnBuffTimeOut;
        buff.Player = mPlayer;
        buff.KeepSecond = 30;
        buff.ItemID = itemID;
        buff.InitializeSolutionComponent();
        buff.TriggerBuff();
    }
}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{
    private void OnBuffTimeOut()
    {
        mButton.interactable = true;
        Debug.Log("Time Out!");
    }
}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{

}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{

}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{

}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{

}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{

}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{

}

public partial class ToolsPanelSkillButtonController : MonoBehaviour
{

}
