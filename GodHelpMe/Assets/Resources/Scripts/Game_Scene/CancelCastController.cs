﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CancelCastController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private ThirdCamera mThirdCamera;
    private Transform mPlayer;
    private SkillsJoystickPanel mSkillsJoystickPanel;

    private List<Renderer> mRs = new List<Renderer>();

    private void Start()
    {
        mThirdCamera = Camera.main.gameObject.GetComponent<ThirdCamera>();
        mPlayer = mThirdCamera.target;
        mSkillsJoystickPanel = GameObject.Find("USERINTERFACEMANAGER/SkillsJoystickPanel").GetComponent<SkillsJoystickPanel>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mSkillsJoystickPanel.OnPlayerCancelCastSkill(true);
        mPlayer.Find("SkillArea").GetComponentsInChildren(mRs);
        foreach (Renderer r in mRs)
        {
            var red_color = new Color32(255, 41, 41, 255);
            r.material.SetColor("_TintColor", red_color);
        }
        //Debug.Log("123");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mSkillsJoystickPanel.OnPlayerCancelCastSkill(false);
        mPlayer.Find("SkillArea").GetComponentsInChildren(mRs);
        foreach (Renderer r in mRs)
        {
            var blue_color = new Color32(58, 133, 255, 255);
            r.material.SetColor("_TintColor", blue_color);
        }
        //Debug.Log("456");
    }
}
