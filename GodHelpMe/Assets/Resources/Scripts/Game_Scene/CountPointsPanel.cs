﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CountPointsPanel : MonoBehaviour
{

    [SerializeField]
    private bool EditModel;

    internal bool IsVictory;

    [SerializeField]
    private GameObject VICTORY;
    [SerializeField]
    private GameObject DEFEAT;
    [SerializeField]
    private GameObject CountPointView;
    [SerializeField]
    private float WaitForSeconds;

    [SerializeField]
    private GameObject BottomPanel;
    private GameObject pointsPanelObj;

    private Dictionary<int, GradeInfomation> dictionary = new Dictionary<int, GradeInfomation>();

    private void OnEnable()
    {
        pointsPanelObj = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterfaceGameScene("PointsPanel"));

        StartCoroutine(ShowCountPointView());
    }

    IEnumerator ShowCountPointView()
    {
        if (IsVictory)
        {
            VICTORY.SetActive(true);
        }
        else
        {
            DEFEAT.SetActive(true);
        }

        yield return new WaitForSeconds(WaitForSeconds);

        int PointsTypeLength = Enum.GetValues(typeof(PointsType)).Length;

        //for (int index = 0; index < PointsTypeLength; index++)
        //{
        //    GameObject pointsPanelInstance = Instantiate(pointsPanelObj, BottomPanel.transform);

        //    PointsPanel pointsPanel = pointsPanelInstance.GetComponent<PointsPanel>();

        //    //Debug.Log(pointsPanel);

        //    pointsPanel.pointsType = (PointsType)index;

        //    int playerIndex = 0;
        //    pointsPanel.pointsPlayer = (PointsPlayer)playerIndex;

        //    switch (pointsPanel.pointsType)
        //    {
        //        case PointsType.Boss:
        //            pointsPanel.point = 5 * 250;
        //            break;
        //        case PointsType.Enemy:
        //            pointsPanel.point = 10 * 10;
        //            break;
        //        case PointsType.RepaireTower:
        //            pointsPanel.point = 15 * 30;
        //            break;
        //        case PointsType.TowerUpLevel:
        //            pointsPanel.point = 20 * 125;
        //            break;
        //    }

        //    pointsPanel.Set();
        //}

        //if (PlayerAttributeManager.Singleton)
        //{

        //}

        for (int index = 0; index < PointsTypeLength; index++)
        {
            GameObject pointsPanelInstance = Instantiate(pointsPanelObj, BottomPanel.transform);

            PointsPanel pointsPanel = pointsPanelInstance.GetComponent<PointsPanel>();
            pointsPanel.dictionary = dictionary;
            PointsType pointsType = (PointsType)index;
            pointsPanel.Set(pointsType);
        }

        CountPointView.SetActive(true);

        yield break;
    }

    public void OnClickedContinuButton()
    {
        Singleton_RooomLefted();
    }

    private void Singleton_RooomLefted()
    {
        SceneManager.LoadScene(ProjectKeywordList.kSceneForLobby);
    }
}
