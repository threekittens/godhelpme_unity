﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TKTools;

public enum Hero
{
    Iron,
    FreezingPioneer,
    Undertaker,
    Winsoul
}

/// <summary>
/// Partial below is for deifine parameter.
/// </summary>
public partial class GameSceneManager : MonoBehaviour
{
    [SerializeField] private bool oneByoneModel = true;
    [SerializeField] private bool EditModel = false;
    [SerializeField] private Hero heroType;
    [SerializeField] private bool isActivitySpawnEnemy;
    // Default countdaown start time.    
    [SerializeField] private int nextCoundwonSecond = 60;
    [SerializeField] private int playSecond = 60;
    // Limited spawn enemy count.
    //[SerializeField]
    //private int eachEnemysBornMaxCount = 5;
    [SerializeField] private Text levelText;
    [SerializeField] private Text infoText;
    [SerializeField] private Text countdownTimeText;
    [SerializeField] private GameObject towerCore;
    [SerializeField] private GameObject countPointsPanelInstance;
    [SerializeField] private GameObject heroSpawnPoint;
    [SerializeField] private Animator gameStartPanel;

    private int tipScreenIndex = 0;
    private int alertorIndex = 0;
    [SerializeField] private GameObject tipScreens;

    [SerializeField] private TipScreenControl tipScreenRecommand;
    [SerializeField] private Animator[] resourceMAs;
    [SerializeField] private Animator[] towerMAs;
    [SerializeField] private Animator mainCA;
    private List<GameObject> enemySpawnManagers = new List<GameObject>();
    private bool isLoadedNextCheckpoint = false;
    private bool isEntryNextCheckpointCountdownTime = false;
    private bool isSpawnStarted = false;
    private bool activityNextEnemiesStatus = false;
    private bool doesKilledAllEnemies = false;
    private string heroID;
    private bool isShowedCountPointsPanel = false;
    internal List<GameObject> TotalEnemiesList = new List<GameObject>();
    private int timeForSpawnCountdown;
    private int timeForPlayCountdown;
    private int maximumCheckpoint = int.MaxValue;
    private int checkpoint;
    private int Checkpoint
    {
        get
        {
            if (PlayerAttributeManager.Singleton)
            {
                checkpoint = PlayerAttributeManager.Singleton.HeroAchievement.checkpoint;
            }
            return checkpoint;
        }
        set
        {
            checkpoint = value;
            if (PlayerAttributeManager.Singleton)
            {
                HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievement;
                h.checkpoint = checkpoint;
                PlayerAttributeManager.Singleton.HeroAchievement = h;
            }
            Debug.Log("Update checkpoint");
        }
    }
}

public partial class GameSceneManager : MonoBehaviour
{
    private void Start()
    {
        SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
        //if (PlayerAttributeManager.Singleton)
        //{
        //    if (PlayerAttributeManager.Singleton.FirstTiemPlay == false)
        //    {
        //        TipScreenControl TSC = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
        //        TSC.OnPointEvent += Control_OnPointEvent;
        //        TSC.OnEnableActive += Control_OnEnableActive;
        //        TSC.gameObject.SetActive(true);
        //        return;
        //    }
        //}

        //if (EditModel)
        //{
        //    TipScreenControl TSC = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
        //    TSC.OnPointEvent += Control_OnPointEvent;
        //    TSC.OnEnableActive += Control_OnEnableActive;
        //    TSC.gameObject.SetActive(true);
        //    return;
        //}

        mainCA.enabled = false;
        
        TKAnimatiorEventManager TKAE = gameStartPanel.transform.GetComponent<TKAnimatiorEventManager>();
        TKAE.OnFinishedAnimationEvnet += OnFinishedAnimationEvnet;
        gameStartPanel.SetTrigger("Show");
    }

    private void Control_OnPointEvent(object sender, EventArgs e)
    {
        TipScreenControl privousTSC = sender as TipScreenControl;
        privousTSC.OnPointEvent -= Control_OnPointEvent;
        privousTSC.OnDisableActive += Control_OnDisableActive;
        privousTSC.gameObject.SetActive(false);

        if (tipScreenIndex == (tipScreens.transform.childCount - 1))
        {
            Debug.Log("Finished Tip");
            tipScreenIndex = 0;
            if (PlayerAttributeManager.Singleton)
                PlayerAttributeManager.Singleton.FirstTiemPlay = true;
            TKAnimatiorEventManager TKAEM = gameStartPanel.transform.GetComponent<TKAnimatiorEventManager>();
            TKAEM.OnFinishedAnimationEvnet += OnFinishedAnimationEvnet;
            gameStartPanel.SetTrigger("Show");
            return;
        }

        tipScreenIndex += 1;

        TipScreenControl nextTSC = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
        nextTSC.OnPointEvent += Control_OnPointEvent;
        nextTSC.OnEnableActive += Control_OnEnableActive;

        if (nextTSC.gameObject.name == "TipScreen_GameTutor_Introduce_CoreTower"
            || nextTSC.gameObject.name == "TipScreen_GameTutor_Introduce_Tower")
        {
            if (mainCA.GetCurrentAnimatorStateInfo(0).IsName("TutorMoveUp"))
            {
                nextTSC.gameObject.SetActive(true);
                return;
            }
            else
            {
                mainCA.SetBool("Move", true);
                TKAnimatiorEventManager TKAEM = mainCA.transform.GetComponent<TKAnimatiorEventManager>();
                TKAEM.OnFinishedAnimationEvnet += TKAEM_OnFinishedAnimationEvnet;
            }
        }
        else
        {
            if (mainCA.GetCurrentAnimatorStateInfo(0).IsName("TutorMoveUp"))
            {
                mainCA.SetBool("Move", false);
                TKAnimatiorEventManager TKAEM = mainCA.transform.GetComponent<TKAnimatiorEventManager>();
                TKAEM.OnFinishedAnimationEvnet += TKAEM_OnFinishedAnimationEvnet;
                mainCA.enabled = false;
                return;

            }
            nextTSC.gameObject.SetActive(true);
        }
    }

    private void TKAEM_OnFinishedAnimationEvnet()
    {
        TKAnimatiorEventManager TKAEM = mainCA.transform.GetComponent<TKAnimatiorEventManager>();
        TKAEM.OnFinishedAnimationEvnet -= TKAEM_OnFinishedAnimationEvnet;

        TipScreenControl nextTSC = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
        nextTSC.gameObject.SetActive(true);
    }

    private void Control_OnEnableActive(object sender, EventArgs e)
    {
        TipScreenControl TSC = sender as TipScreenControl;
        TSC.OnEnableActive -= Control_OnEnableActive;
        PlayAnimationWith(TSC);
    }

    private void Control_OnDisableActive(object sender, EventArgs e)
    {
        TipScreenControl TSC = sender as TipScreenControl;
        TSC.OnDisableActive -= Control_OnDisableActive;
        PlayAnimationWith(TSC);
    }

    private void PlayAnimationWith(TipScreenControl TSC)
    {
        switch (TSC.gameObject.name)
        {
            case "TipScreen_GameTutor_Resources_FS":
                {
                    alertorIndex = 0;
                    resourceMAs[alertorIndex].SetBool("Show", TSC.gameObject.activeSelf);
                    break;
                }
            case "TipScreen_GameTutor_Resources_LB":
                {
                    alertorIndex = 1;
                    resourceMAs[alertorIndex].SetBool("Show", TSC.gameObject.activeSelf);
                    break;
                }
            case "TipScreen_GameTutor_Resources_FC":
                {
                    alertorIndex = 2;
                    resourceMAs[alertorIndex].SetBool("Show", TSC.gameObject.activeSelf);
                    break;
                }
            case "TipScreen_GameTutor_Introduce_CoreTower":
                {
                    towerMAs[0].SetBool("Show", TSC.gameObject.activeSelf);
                    break;
                }
            case "TipScreen_GameTutor_Introduce_Tower":
                {
                    towerMAs[1].SetBool("Show", TSC.gameObject.activeSelf);
                    towerMAs[2].SetBool("Show", TSC.gameObject.activeSelf);
                    towerMAs[3].SetBool("Show", TSC.gameObject.activeSelf);
                    towerMAs[4].SetBool("Show", TSC.gameObject.activeSelf);
                    towerMAs[5].SetBool("Show", TSC.gameObject.activeSelf);
                    break;
                }
        }
    }

    private void OnFinishedAnimationEvnet()
    {
        TKAnimatiorEventManager TKAEM = gameStartPanel.transform.GetComponent<TKAnimatiorEventManager>();
        TKAEM.OnFinishedAnimationEvnet -= OnFinishedAnimationEvnet;
        StartCoroutine(StartEventRelay());
        StartCoroutine(ShowAlertorEvent());
    }

    IEnumerator ShowAlertorEvent()
    {
        tipScreenRecommand.gameObject.SetActive(true);
        tipScreenRecommand.OnPointEvent += TipAlerotr_OnPointEvent;

        resourceMAs[0].SetBool("Show", true);
        resourceMAs[1].SetBool("Show", true);
        resourceMAs[2].SetBool("Show", true);

        yield return new WaitUntil(() => tipScreenRecommand.gameObject.activeSelf == false);

        resourceMAs[0].SetBool("Show", false);
        resourceMAs[1].SetBool("Show", false);
        resourceMAs[2].SetBool("Show", false);

        yield break;
    }

    private void TipAlerotr_OnPointEvent(object sender, EventArgs e)
    {
        tipScreenRecommand.OnPointEvent -= TipAlerotr_OnPointEvent;
        tipScreenRecommand.gameObject.SetActive(false);
    }

    private void SceneManager_sceneUnloaded(Scene arg0)
    {
        SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;
        Debug.Log("Scene un loaded");
    }

    IEnumerator StartEventRelay()
    {
        yield return StartCoroutine(InitializtionTheSolutionComponent());
        yield return StartCoroutine(SetChoosedHero());

        if (isActivitySpawnEnemy)
        {
            if (oneByoneModel == false)
            {
                yield return StartCoroutine(ActivityCountdownTime());
            }
        }
    }

    private void Update()
    {
        if (oneByoneModel)
        {
            if (FindObjectsOfType<EnemyProperty>().Length == 0)
            {
                isSpawnStarted = false;

                if (!activityNextEnemiesStatus)
                {
                    activityNextEnemiesStatus = true;
                    doesKilledAllEnemies = true;
                    StartCoroutine(ActivityCountdownTime());
                }
            }
            else
            {
                isSpawnStarted = true;

                /// if not all of the enemies killed
                if (activityNextEnemiesStatus)
                {
                    activityNextEnemiesStatus = false;
                    doesKilledAllEnemies = false;
                }
            }
        }

        if (isShowedCountPointsPanel) { return; }

        if (TotalEnemiesList.Count == 0 && Checkpoint == maximumCheckpoint) { ShowCountPointPanel(true); }

        if (towerCore == null) { ShowCountPointPanel(false); }
    }

    private void ShowCountPointPanel(bool isVictory)
    {
        CountPointsPanel CountPointsPanel = countPointsPanelInstance.GetComponent<CountPointsPanel>();
        CountPointsPanel.IsVictory = isVictory;
        countPointsPanelInstance.SetActive(true);
        isShowedCountPointsPanel = true;
    }

    //private void OnApplicationQuit()
    //{
    //    Debug.Log("OnApplicationQuit");

    //    if (!doesKilledAllEnemies)
    //    {
    //        Debug.LogWarning("如果...沒有殺完所有敵人...");
    //        if (Checkpoint > 0)
    //        {
    //            /// Always loading past the checkpoint.                
    //            Debug.Log(" -=1 表示先加載上一波");
    //            Checkpoint -= 1;
    //        }
    //    }
    //    else if (!isSpawnStarted)
    //    {
    //        Debug.LogWarning("如果...還沒開始生成敵人...");
    //        if (Checkpoint > 0)
    //        {
    //            /// Always loading past the checkpoint.                
    //            Debug.Log(" -=1 表示先加載上一波");
    //            Checkpoint -= 1;
    //        }
    //    }
    //}

    private void OnDisable()
    {
        Debug.LogWarning("OnDisable");
        if (isEntryNextCheckpointCountdownTime && !isSpawnStarted)
        {
            Debug.LogWarning("如果...進入了下一關倒數計時...且...敵人還沒生成...");
            Debug.Log(Checkpoint);
            if (Checkpoint > 0)
            {
                /// Always loading past the checkpoint.                
                Debug.Log("Checkpoint = Checkpoint 表示下次進入加載當前一波");
                Checkpoint = Checkpoint;
            }
        }
        else if (!doesKilledAllEnemies)
        {
            Debug.LogWarning("如果...沒有殺完所有敵人...");
            Debug.Log(Checkpoint);
            if (Checkpoint > 0)
            {
                /// Always loading past the checkpoint.                
                Debug.Log(" -=1 表示先加載上一波");
                Checkpoint -= 1;
            }
        }
        else if (isLoadedNextCheckpoint && !isSpawnStarted)
        {
            Debug.LogWarning("如果..已經加載下一波...且...還沒開始生成敵人...");
            Debug.Log(Checkpoint);
            if (Checkpoint > 0)
            {
                /// Always loading past the checkpoint.                
                Debug.Log(" -=1 表示先加載上一波");
                Checkpoint -= 1;
            }
        }
    }

    private void OnDestroy()
    {
        Debug.LogWarning("On...Destroy...");
    }
}

/// <summary>
/// Partial below is for Initializes solution component.
/// </summary>
public partial class GameSceneManager : MonoBehaviour
{
    IEnumerator InitializtionTheSolutionComponent()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject ememySpawn = GameObject.Find(string.Format("Envionment/DevilGateGroup/Devil_Gate_0{0}/Enemy_Spawn", i));
            enemySpawnManagers.Add(ememySpawn);
        }

        yield break;
    }
}


public partial class GameSceneManager : MonoBehaviour
{
    public void SetAndResetChoosedHero()
    {
        StartCoroutine(SetChoosedHero());
    }

    IEnumerator SetChoosedHero()
    {
        if (EditModel)
        {
            switch (heroType)
            {
                case Hero.Iron:
                    heroID = "5A631C1A-112F6A0-34500C96";
                    break;
                case Hero.FreezingPioneer:
                    heroID = "5A631D54-15A2BCA-FC7267A";
                    break;
                case Hero.Undertaker:
                    heroID = "5A631E08-19B7EE0-114E1422";
                    break;
                case Hero.Winsoul:
                    heroID = "5A631EA5-1E2B40A-1C351B77";
                    break;
            }
        }
        else
        {
            heroID = PlayerAttributeManager.Singleton.ChoosedHeroID;
        }

        TextAsset heroData = Resources.Load<TextAsset>(string.Format("Data/Heros/{0}", heroID));

        string JSON = heroData.text;
        string prefabPath = JsonUtility.FromJson<HeroData>(JSON).prefabPath;

        GameObject prefab = Resources.Load<GameObject>(prefabPath);

        GameObject instance = Instantiate(prefab, heroSpawnPoint.transform.position, Quaternion.identity);

        Camera.main.GetComponent<ThirdCamera>().target = instance.transform;

        PlayerProperty p = instance.GetComponent<PlayerProperty>();
        p.HeroID = heroID;

        Motor motor = instance.GetComponent<Motor>();
        motor.moveSpeed = 10;

        MonoBehaviour[] c_array = instance.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour c in c_array) { c.enabled = true; }

        Debug.Log("Init Hero");

        instance.SetActive(true);

        PUNRPCEnabledHealthManager(instance.name);

        yield break;
    }

    private void PUNRPCEnabledHealthManager(string heroName)
    {
        Debug.Log("heroName " + heroName);
        StartCoroutine(PUNRPCEnabledHealthManagerRelay(heroName));
    }

    IEnumerator PUNRPCEnabledHealthManagerRelay(string heroName)
    {
        GameObject player = GameObject.Find(heroName);

        HealthManager healthManager = player.GetComponent<HealthManager>();
        healthManager.enabled = true;

        GameObject playerStatusBar = player.GetComponent<HealthManager>().mStatusBar.transform.parent.gameObject;
        playerStatusBar.SetActive(true);

        ParticleSystem flashCircleDownPS = player.transform.Find("FlashCircleDown").GetComponent<ParticleSystem>();
        flashCircleDownPS.gameObject.SetActive(true);
        yield return new WaitForSeconds((flashCircleDownPS.main.duration + flashCircleDownPS.main.startLifetime.constant));

        flashCircleDownPS.Stop();
        flashCircleDownPS.Clear();
        flashCircleDownPS.gameObject.SetActive(false);
        yield return new WaitUntil(() => (flashCircleDownPS.gameObject.activeInHierarchy == false));

        yield break;
    }

    /// About function of coundown time.    
    /// Only execute on PhotonNetwork Master Client.
    IEnumerator ActivityCountdownTime()
    {
        isLoadedNextCheckpoint = false;
        levelText.text = "Level " + (Checkpoint + 1);
        yield return StartCoroutine(ResetCountdownTime());
        yield return StartCoroutine(PlayAndReplayCountdownTime());
        isEntryNextCheckpointCountdownTime = true;
        yield break;
    }

    IEnumerator ResetCountdownTime()
    {
        timeForSpawnCountdown = nextCoundwonSecond;
        yield break;
    }

    IEnumerator PlayAndReplayCountdownTime()
    {
        InvokeRepeating("SpawnCountdownTime", 0, 1);
        yield break;
    }

    IEnumerator PauseAndStopCountdownTime()
    {
        CancelInvoke("SpawnCountdownTime");
        yield break;
    }

    private void SpawnCountdownTime()
    {
        // 将 秒数 传入 并显示 成文字
        string info = "距離魔物出現倒數";

        PUNRPCCountDownTime(info, timeForSpawnCountdown);

        if (timeForSpawnCountdown == 0)
        {
            isEntryNextCheckpointCountdownTime = false;
            Debug.LogWarning("Time stopped");
            StartCoroutine(BornEnemyLoop());
            StartCoroutine(PauseAndStopCountdownTime());
        }
        timeForSpawnCountdown--;
    }

    // ======================================
    IEnumerator BornEnemyLoop()
    {
        /// Started born the enemies.
        yield return StartCoroutine(ActivityEnemySpawns());

        /// Always excute if not enough the value maximaSpawnBornTimes.

        if (oneByoneModel == false)
        {
            yield return StartCoroutine(ActivityPlayCountdownTime());
        }
        yield break;
    }

    // About function of activity spawns
    // ========================================
    IEnumerator ActivityEnemySpawns()
    {
        if (enemySpawnManagers.Count > 0)
        {
            //Debug.LogWarning("spawn times => " + spawnTimes);
            if (Checkpoint == maximumCheckpoint)
            {
                //Debug.LogError("!!!!!!!!!!!!!!!!!!!!!!");
                foreach (GameObject enemySpawn in enemySpawnManagers)
                {
                    yield return StartCoroutine(CloseSpawn(enemySpawn));
                    yield return StartCoroutine(PauseAndStopCountdownTime());
                    yield return StartCoroutine(PauseAndStopPlayCountdownTime());
                }
                yield break;
            }
            else
            {
                levelText.text = "Level " + (Checkpoint + 1);

                foreach (GameObject enemySpawn in enemySpawnManagers)
                {
                    yield return StartCoroutine(StartAndRestartSpawn(enemySpawn, Checkpoint));
                }

                Debug.LogWarning("Start And Restart Spawn");

                // 每次都 + 1 表示加载下一波
                Checkpoint += 1;
                Debug.Log("每次都 + 1 表示加载下一波");
                isLoadedNextCheckpoint = true;

                // 显示 level 在 UI 上                
                Debug.LogWarning("Next Checkpoint => " + Checkpoint);
            }
        }
        else
        {
            Debug.LogError("enemySpawns is empty!!!");
        }

        yield break;
    }

    // Close the enemy spawn and then open to restart!
    IEnumerator StartAndRestartSpawn(GameObject enemySpawn, int _checkpoint)
    {
        // Get the script of sapwn manager.
        EnemySpawnManager manager = enemySpawn.GetComponent<EnemySpawnManager>();
        // Send the checkpoint to spawn manager.
        manager.Checkpoint = _checkpoint;
        // Close the spawn manager.
        manager.ActivitySpawen(false);
        // Open the spawn manager.
        manager.ActivitySpawen(true);
        yield break;
    }

    // Close the enemy spawn!
    IEnumerator CloseSpawn(GameObject enemySpawn)
    {
        // Get the script of sapwn manager.
        EnemySpawnManager manager = enemySpawn.GetComponent<EnemySpawnManager>();
        // Close the spawn manager.
        manager.ActivitySpawen(false);
        yield break;
    }


    IEnumerator ActivityPlayCountdownTime()
    {
        yield return StartCoroutine(ResetPlayCountdownTime());
        yield return StartCoroutine(PlayAndReplayPlayCountdownTime());
        yield break;
    }

    IEnumerator ResetPlayCountdownTime()
    {
        timeForPlayCountdown = playSecond;
        yield break;
    }

    IEnumerator PlayAndReplayPlayCountdownTime()
    {
        InvokeRepeating("PlayCountdownTime", 0, 1);
        yield break;
    }

    IEnumerator PauseAndStopPlayCountdownTime()
    {
        CancelInvoke("PlayCountdownTime");
        yield break;
    }

    private void PlayCountdownTime()
    {
        string info = "距離下次魔物來襲";

        PUNRPCCountDownTime(info, timeForPlayCountdown);

        if (timeForPlayCountdown == 0)
        {
            Debug.LogWarning("Play Time stopped");

            StartCoroutine(PauseAndStopPlayCountdownTime());

            if (Checkpoint != maximumCheckpoint)
            {
                //Debug.Log("Repeat Count down");
                StartCoroutine(ActivityCountdownTime());
            }
        }

        timeForPlayCountdown--;
    }

    private void PUNRPCCountDownTime(string info, int timeLimit)
    {
        infoText.text = info;
        TimeSpan result = TimeSpan.FromSeconds(timeLimit);
        countdownTimeText.text = string.Format("{0:00}:{1:00}", result.Minutes, result.Seconds);
    }

    private void PUNRPCLevelText(string info)
    {
        levelText.text = info;
    }
}


public partial class GameSceneManager : MonoBehaviour
{

}