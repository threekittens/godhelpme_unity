﻿using PathManager;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Partial below is for define parameter in the solution
/// </summary>
public partial class GameSceneUIManager : MonoBehaviour
{

    [SerializeField]
    private bool EditModel = false;

    [SerializeField]
    private Hero heroType;

    [SerializeField]
    private GameObject towerFunctionalityPanel;

    [SerializeField]
    private GameObject towerHealthPointPanel;

    [SerializeField]
    private GameObject towerMagicPointPanel;

    [SerializeField]
    private GameObject towerUpgradePanel;

    [SerializeField]
    private GameObject achievementInfoBoard;

    private TextAsset mHeroJSONFile;
    private string heroID;
    private HeroData mSelectedHeroData;

    private GameObject mSkillsJoystickPanel;
    private GameObject homeProgressBar;
    private GameObject homeProgressBarInstance;

    //private GameObject mHomeTransporter;
    //private GameObject mHomeTransporterInstance;
    //private GameObject mFlashUp;
    //private GameObject mFlashUpInstance;
    //private GameObject mFlashDown;
    //private GameObject mFlashDownInstance;
    private GameObject Player { get { return Camera.main.GetComponent<ThirdCamera>().target.gameObject; } }

    private string HeroName;

    private float mHomeLoadingSecond = 5.0f;

    private Text fireStoneQuantityText;
    private Text lifeBranchQuantityText;
    private Text fantasyCrystalQuantityText;
    private Text holyPureStoneQuantityText;

    [SerializeField]
    private GameObject settingAlertor;

    internal bool IsTriggerResourceBuff;

    internal int FireStoneQuantity
    {
        get { return fireStoneQuantity; }
        set
        {
            fireStoneQuantity = value;
            fireStoneQuantityText.text = ShowRewardText(fireStoneQuantity);
        }
    }
    internal int LifeBranchQuantity
    {
        get { return lifeBranchQuantity; }
        set
        {
            lifeBranchQuantity = value;
            lifeBranchQuantityText.text = ShowRewardText(lifeBranchQuantity);
        }
    }
    internal int FantasyCrystalQuantity
    {
        get { return fantasyCrystalQuantity; }
        set
        {
            fantasyCrystalQuantity = value;
            fantasyCrystalQuantityText.text = ShowRewardText(fantasyCrystalQuantity);
        }
    }
    internal int HolyPureStoneQuantity
    {
        get { return holyPureStoneQuantity; }
        set
        {
            holyPureStoneQuantity = value;
            holyPureStoneQuantityText.text = ShowRewardText(holyPureStoneQuantity);
        }
    }

    [SerializeField]
    private int fireStoneQuantity = 0;
    [SerializeField]
    private int lifeBranchQuantity = 0;
    [SerializeField]
    private int fantasyCrystalQuantity = 0;
    [SerializeField]
    private int holyPureStoneQuantity = 0;

    internal delegate void UpdateResourcePanelInformation(ResourceType type, int quantity);
    internal UpdateResourcePanelInformation OnUpdateResourcePanelInformation;
}

public partial class GameSceneUIManager : MonoBehaviour
{
    private void Awake()
    {
        LoadingFromResources();
    }

    private void Start()
    {
        OnUpdateResourcePanelInformation = OnUpdateResourcePanel;
        InitializtionTheSolutionComponent();
        SetSkillButtonsUIBy();
        SetToolsPanel();
    }

    private void Update()
    {

        if (PlayerAttributeManager.Singleton)
        {
            Text point = achievementInfoBoard.transform.Find("Point").GetComponent<Text>();
            point.text = SIPrefix.GetInfo(PlayerAttributeManager.Singleton.HeroAchievement.score, 2).AmountWithPrefix;
        }

        //if (Player)
        //{
        //    return;
        //}
        //else
        //{
        //    if (Camera.main.GetComponent<ThirdCamera>().target)
        //    {
        //        Player = Camera.main.GetComponent<ThirdCamera>().target.gameObject;
        //    }
        //}
    }

    private void OnDestroy()
    {
        OnUpdateResourcePanelInformation = null;
    }
}

/// <summary>
/// Partial below is for initialize.
/// </summary>
public partial class GameSceneUIManager : MonoBehaviour
{
    private void InitializtionTheSolutionComponent()
    {
        if (EditModel)
        {
            switch (heroType)
            {
                case Hero.Iron:
                    heroID = "5A631C1A-112F6A0-34500C96";
                    break;
                case Hero.FreezingPioneer:
                    heroID = "5A631D54-15A2BCA-FC7267A";
                    break;
                case Hero.Undertaker:
                    heroID = "5A631E08-19B7EE0-114E1422";
                    break;
                case Hero.Winsoul:
                    heroID = "5A631EA5-1E2B40A-1C351B77";
                    break;
            }
        }
        else
        {
            heroID = PlayerAttributeManager.Singleton.ChoosedHeroID;
        }

        mSkillsJoystickPanel = transform.Find("SkillsJoystickPanel").gameObject;
        SkillsJoystickPanel skillsJoystickPanel = mSkillsJoystickPanel.GetComponent<SkillsJoystickPanel>();
        skillsJoystickPanel.heroID = heroID;
        skillsJoystickPanel.InitializetionSolutionComponent();

        fireStoneQuantityText = transform.Find("ResourceInfoPanel/FireStonePanel/Text").GetComponent<Text>();
        fireStoneQuantityText.text = ShowRewardText(fireStoneQuantity);

        lifeBranchQuantityText = transform.Find("ResourceInfoPanel/LifeBranchPanel/Text").GetComponent<Text>();
        lifeBranchQuantityText.text = ShowRewardText(lifeBranchQuantity);

        fantasyCrystalQuantityText = transform.Find("ResourceInfoPanel/FantasyCrystalPanel/Text").GetComponent<Text>();
        fantasyCrystalQuantityText.text = ShowRewardText(fantasyCrystalQuantity);

        holyPureStoneQuantityText = transform.Find("ResourceInfoPanel/HolySpiritStonePanel/Text").GetComponent<Text>();
        holyPureStoneQuantityText.text = ShowRewardText(holyPureStoneQuantity);

        //mRepairTowerPanelAnimator = towerRepairPanel.GetComponent<Animator>();
        //mEnemySpawnInfoBoardAnimator = transform.Find("EnemySpawnInfoBoard").GetComponent<Animator>();
    }

    private void LoadingFromResources()
    {
        homeProgressBar = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterface("HomeProgressBar"));
        //mHomeTransporter = Resources.Load<GameObject>(LoadResourceFilePath.PrefabVFXEffects() + "HomeTransporter");
        //mFlashUp = Resources.Load<GameObject>(LoadResourceFilePath.PrefabVFXEffects() + "FlashCircleUp");
        //mFlashDown = Resources.Load<GameObject>(LoadResourceFilePath.PrefabVFXEffects() + "FlashCircleDown");
    }
}

/// <summary>
/// Partial below is for set skill buttons in USERINTERFACEMANAGER by a player who selected a hero's id.
/// </summary>
public partial class GameSceneUIManager : MonoBehaviour
{
    private void SetSkillButtonsUIBy()
    {
        if (heroID == default(string)) { return; }

        mHeroJSONFile = Resources.Load<TextAsset>(string.Format("Data/Heros/{0}", heroID));
        string JSON = mHeroJSONFile.text;
        mSelectedHeroData = JsonUtility.FromJson<HeroData>(JSON);

        // The part is for create introduce of skill.
        for (int i = 0; i < mSelectedHeroData.skills.Length; i++)
        {
            Image skillButton = mSkillsJoystickPanel.transform.GetChild(i).GetComponent<Image>();

            HeroSkillData skillData = mSelectedHeroData.skills[i];
            Sprite sprite = Resources.Load<Sprite>(skillData.iconPath);
            skillButton.sprite = sprite;
        }
    }
}

public partial class GameSceneUIManager : MonoBehaviour
{
    private void SetToolsPanel()
    {
        if (PlayerAttributeManager.Singleton)
        {
            if (PlayerAttributeManager.Singleton.PlayerItemPanelList.Count > 0)
            {
                Debug.Log(PlayerAttributeManager.Singleton.PlayerItemPanelList.Count);

                for (int i = 0; i < PlayerAttributeManager.Singleton.PlayerItemPanelList.Count; i++)
                {
                    PlayerItem playerItem = PlayerAttributeManager.Singleton.PlayerItemPanelList[i];

                    if (playerItem == null) { return; }

                    if (PlayerAttributeManager.Singleton.PlayerItemPackage.Count > 0)
                    {
                        string itemId = playerItem.itemId;
                        if (itemId == "null") { return; }
                        Debug.Log(itemId);
                        TextAsset JSONFile = Resources.Load<TextAsset>("Data/ToolItems/" + itemId);
                        string JSON = JSONFile.text;

                        GameObject ToolsPanelSkillButton = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceGameScene("ToolsPanelSkillButton"));
                        ToolsPanelSkillButton = Instantiate(ToolsPanelSkillButton, transform.Find("ToolsPanel/ToolsContent").transform);

                        ToolsPanelSkillButtonController toolsPanelSkillButtonController = ToolsPanelSkillButton.GetComponent<ToolsPanelSkillButtonController>();
                        toolsPanelSkillButtonController.playerItem = playerItem;

                        ItemData itemData = JsonUtility.FromJson<ItemData>(JSON);

                        Image buttonImage = ToolsPanelSkillButton.transform.Find("Button").GetComponent<Image>();
                        Sprite icon = Resources.Load<Sprite>(itemData.iconPath);
                        buttonImage.sprite = icon;

                        Text countText = ToolsPanelSkillButton.transform.Find("Button/Count/Text").GetComponent<Text>();
                        countText.text = playerItem.itemCount.ToString();

                        Text name = ToolsPanelSkillButton.transform.Find("Name").GetComponent<Text>();
                        name.text = itemData.name;
                    }
                    else
                    {
                        Debug.Log("Player doesn't have purchase any item in package.");
                    }
                }
            }
            else
            {
                Debug.Log("Player doesn't have choose any item on panel.");
            }
        }
    }
}

public partial class GameSceneUIManager : MonoBehaviour
{
    public void BackHome()
    {
        if (homeProgressBar)
        {
            //if (mHomeProgressBarInstance == null)
            //{
            //    mHomeProgressBarInstance = Instantiate(mHomeProgressBar);
            //    Vector2 screenPos = Camera.main.WorldToScreenPoint(mPlayer.transform.position);
            //    mHomeProgressBarInstance.transform.SetParent(transform, false);
            //    screenPos.y -= 50f;
            //    mHomeProgressBarInstance.transform.position = screenPos;

            //    HomeProgressBar homeProgressBar = mHomeProgressBarInstance.GetComponent<HomeProgressBar>();
            //    homeProgressBar.mLoadingSecond = mHomeLoadingSecond;
            //    homeProgressBar.OnHomeProgressBarDidStarted += OnHomeProgressBarDidStarted;
            //    homeProgressBar.OnHomeProgressBarDidStop += OnHomeProgressBarDidStop;
            //    homeProgressBar.OnHomeProgressBarOnDisable += OnHomeProgressBarOnDisable;
            //}

            if (homeProgressBarInstance != null) { DestroyImmediate(homeProgressBarInstance); }

            homeProgressBarInstance = Instantiate(homeProgressBar);
            Transform player = Camera.main.GetComponent<ThirdCamera>().target.gameObject.transform;
            UnityEngine.Vector2 screenPos = Camera.main.WorldToScreenPoint(player.position);
            homeProgressBarInstance.transform.SetParent(transform, false);
            screenPos.y -= 50f;
            homeProgressBarInstance.transform.position = screenPos;

            HomeProgressBar progressBar = homeProgressBarInstance.GetComponent<HomeProgressBar>();
            progressBar.mLoadingSecond = mHomeLoadingSecond;
            progressBar.OnHomeProgressBarDidStarted += OnHomeProgressBarDidStarted;
            progressBar.OnHomeProgressBarDidStop += OnHomeProgressBarDidStop;
            progressBar.OnHomeProgressBarOnDisable += OnHomeProgressBarOnDisable;

            homeProgressBarInstance.SetActive(true);
        }
        else
        {
            Debug.Log("The mHomeProgressBar is Null please to check.");
        }
    }

    private void OnHomeProgressBarDidStarted(HomeProgressBar homeProgressBar, float parogress)
    {
        homeProgressBar.OnHomeProgressBarDidStarted -= OnHomeProgressBarDidStarted;
        Debug.Log("I'm here HomeProgressBarDidStarted.");

        string heroName = Player.name;

        GameObject player = GameObject.Find(heroName);

        //if (mHomeTransporterInstance)
        //{
        //    DestroyImmediate(mHomeTransporterInstance);
        //}
        //mHomeTransporterInstance = Instantiate(mHomeTransporter.gameObject, player.transform);
        //mHomeTransporterInstance.transform.SetPositionAndRotation(player.transform.position + Vector3.up, Quaternion.identity);
        //ParticleSystem particleSystem = mHomeTransporterInstance.GetComponent<ParticleSystem>();
        //particleSystem.Stop();
        //var main = particleSystem.main;
        //main.duration = mHomeLoadingSecond;
        //mHomeTransporterInstance.SetActive(true);
        //particleSystem.Play();

        ParticleSystem homeTransporterPS = player.transform.Find("HomeTransporter").GetComponent<ParticleSystem>();
        var main = homeTransporterPS.main;
        main.duration = mHomeLoadingSecond;
        homeTransporterPS.gameObject.SetActive(true);
    }

    private void OnHomeProgressBarDidStop(HomeProgressBar homeProgressBar, float parogress)
    {
        homeProgressBar.OnHomeProgressBarDidStop -= OnHomeProgressBarDidStop;
        Debug.Log("I'm here HomeProgressBarDidStop.");

        string heroName = Player.name;
        StartCoroutine(TransportsThePlayer(heroName));
    }

    private void OnHomeProgressBarOnDisable(HomeProgressBar homeProgressBar)
    {
        homeProgressBar.OnHomeProgressBarOnDisable -= OnHomeProgressBarOnDisable;
        //DestroyImmediate(mHomeTransporterInstance);
    }

    IEnumerator TransportsThePlayer(string heroName)
    {
        GameObject player = GameObject.Find(heroName);

        ParticleSystem homeTransporterPS = player.transform.Find("HomeTransporter").GetComponent<ParticleSystem>();

        homeTransporterPS.gameObject.SetActive(false);
        yield return new WaitUntil(() => (homeTransporterPS.gameObject.activeInHierarchy == false));

        ParticleSystem flashCircleUpPS = player.transform.Find("FlashCircleUp").GetComponent<ParticleSystem>();
        flashCircleUpPS.transform.parent = null;
        yield return new WaitUntil(() => (flashCircleUpPS.transform.parent == null));

        flashCircleUpPS.gameObject.SetActive(true);

        player.SetActive(false);
        yield return new WaitUntil(() => (player.activeInHierarchy == false));

        ParticleSystem flashCircleDownPS = player.transform.Find("FlashCircleDown").GetComponent<ParticleSystem>();
        flashCircleDownPS.transform.parent = null;
        yield return new WaitUntil(() => (flashCircleDownPS.transform.parent == null));

        yield return new WaitForSeconds((flashCircleUpPS.main.duration + flashCircleUpPS.main.startLifetime.constant) / 1.5f);

        flashCircleUpPS.Stop();
        flashCircleUpPS.Clear();
        flashCircleUpPS.gameObject.SetActive(false);
        yield return new WaitUntil(() => (flashCircleUpPS.gameObject.activeInHierarchy == false));

        flashCircleUpPS.transform.parent = player.transform;
        yield return new WaitUntil(() => (flashCircleUpPS.transform.parent == player.transform));

        UnityEngine.Vector3 newPos = new UnityEngine.Vector3(0.0f, -0.1f, -15.0f);

        flashCircleDownPS.transform.SetPositionAndRotation(newPos, UnityEngine.Quaternion.identity);
        yield return new WaitUntil(() => (flashCircleDownPS.transform.position == newPos));

        player.transform.SetPositionAndRotation(newPos, UnityEngine.Quaternion.identity);
        yield return new WaitUntil(() => (player.transform.position == newPos));

        flashCircleDownPS.gameObject.SetActive(true);

        player.SetActive(true);
        yield return new WaitUntil(() => (player.activeInHierarchy == true));

        yield return new WaitForSeconds((flashCircleDownPS.main.duration + flashCircleDownPS.main.startLifetime.constant));

        flashCircleDownPS.Stop();
        flashCircleDownPS.Clear();
        flashCircleDownPS.gameObject.SetActive(false);
        yield return new WaitUntil(() => (flashCircleDownPS.gameObject.activeInHierarchy == false));

        flashCircleDownPS.transform.parent = player.transform;
        yield return new WaitUntil(() => (flashCircleDownPS.transform.parent == player.transform));

        flashCircleDownPS.transform.SetPositionAndRotation(UnityEngine.Vector3.zero, UnityEngine.Quaternion.identity);
        yield return new WaitUntil(() => (flashCircleDownPS.transform.position == UnityEngine.Vector3.zero));

        yield break;
    }
}


public partial class GameSceneUIManager : MonoBehaviour
{
    private void OnUpdateResourcePanel(ResourceType type, int quantity)
    {
        if (IsTriggerResourceBuff) { quantity *= 2; }

        switch (type)
        {
            case ResourceType.FireStone:
                fireStoneQuantity += quantity;
                fireStoneQuantityText.text = ShowRewardText(fireStoneQuantity);
                break;
            case ResourceType.LifeBranch:
                lifeBranchQuantity += quantity;
                lifeBranchQuantityText.text = ShowRewardText(lifeBranchQuantity);
                break;
            case ResourceType.FantasyCrystal:
                fantasyCrystalQuantity += quantity;
                fantasyCrystalQuantityText.text = ShowRewardText(fantasyCrystalQuantity);
                break;
            case ResourceType.HolyPureStone:
                holyPureStoneQuantity += quantity;
                holyPureStoneQuantityText.text = ShowRewardText(holyPureStoneQuantity);
                break;
        }
    }

    private string ShowRewardText(int quantity)
    {
        return string.Format("x{0}", quantity);
    }
}

public partial class GameSceneUIManager : MonoBehaviour
{
    public void OpenSettingMenuAlertor()
    {
        SettingMenuAlertor.OnTapedSettingMenu += SettingMenuAlertor_OnTapedSettingMenu;
        SettingMenuAlertor.OnTapedContinueGame += SettingMenuAlertor_OnTapedContinueGame;
        SettingMenuAlertor.OnTapedExitGame += SettingMenuAlertor_OnTapedExitGame;
        SettingMenuAlertor.OnClose += SettingMenuAlertor_OnClose;
        SettingMenuAlertor.ShowOn(this.transform);

        Time.timeScale = 0.0f;
    }

    private void SettingMenuAlertor_OnClose(object sender, System.EventArgs e)
    {
        SettingMenuAlertor.OnTapedSettingMenu -= SettingMenuAlertor_OnTapedSettingMenu;
        SettingMenuAlertor.OnTapedContinueGame -= SettingMenuAlertor_OnTapedContinueGame;
        SettingMenuAlertor.OnTapedExitGame -= SettingMenuAlertor_OnTapedExitGame;
        SettingMenuAlertor.OnClose -= SettingMenuAlertor_OnClose;
    }

    private void SettingMenuAlertor_OnTapedExitGame(object sender, System.EventArgs e)
    {
        if (PlayerAttributeManager.Singleton)
        {
            PlayerAttributeManager.Singleton.SyncHeroAchievement = true;
            PlayerAttributeManager.Singleton.SyncGradeInfomation = true;
            StartCoroutine(StartLoading(ProjectKeywordList.kSceneForLobby, this.transform));
        }
    }

    private IEnumerator StartLoading(string scene, Transform transform)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, transform);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;

        Time.timeScale = 1.0f;
        yield break;
    }

    private void SettingMenuAlertor_OnTapedContinueGame(object sender, System.EventArgs e)
    {
        Time.timeScale = 1.0f;
    }

    private void SettingMenuAlertor_OnTapedSettingMenu(object sender, System.EventArgs e)
    {
        OpenSettingAlertor();
    }

    private void OpenSettingAlertor()
    {
        GameObject instance = Instantiate(settingAlertor, this.transform);
        RectTransform rect = instance.GetComponent<RectTransform>();
        rect.offsetMin = new UnityEngine.Vector2(0, 0);
        rect.offsetMax = new UnityEngine.Vector2(0, 0);
    }
}

public partial class GameSceneUIManager : MonoBehaviour
{
    public void OpenTowerFunctionalityPanel()
    {
        Animator animator = towerFunctionalityPanel.GetComponent<Animator>();
        bool status = !animator.GetBool("Callout");
        //towerFunctionalityPanel.SetActive(status);
        animator.SetBool("Callout", status);

        CloseTowerHealthPointPanel();
        CloseTowerMagicPointPanel();
        CloseTowerUpgradePanel();
    }

    private void CloseTowerFunctionalityPanel()
    {
        Animator animator = towerFunctionalityPanel.GetComponent<Animator>();
        bool status = animator.GetBool("Callout");
        if (status == false) { return; }
        //towerFunctionalityPanel.SetActive(false);
        animator.SetBool("Callout", false);
    }

    public void OpenEnemySpawnInfoBoard()
    {
        Animator animator = achievementInfoBoard.GetComponent<Animator>();
        bool status = !animator.GetBool("Callout");
        animator.SetBool("Callout", status);
    }

    public void OpenTowerHealthPointPanel()
    {
        CloseTowerFunctionalityPanel();
        Animator animator = towerHealthPointPanel.GetComponent<Animator>();
        bool status = !animator.GetBool("Callout");
        //towerHealthPointPanel.SetActive(status);
        animator.SetBool("Callout", status);
    }

    private void CloseTowerHealthPointPanel()
    {
        Animator animator = towerHealthPointPanel.GetComponent<Animator>();
        bool status = animator.GetBool("Callout");
        if (status == false) { return; }
        //towerHealthPointPanel.SetActive(false);
        animator.SetBool("Callout", false);
    }

    public void OpenTowerMagicPointPanel()
    {
        CloseTowerFunctionalityPanel();
        Animator animator = towerMagicPointPanel.GetComponent<Animator>();
        bool status = !animator.GetBool("Callout");
        //towerMagicPointPanel.SetActive(status);
        animator.SetBool("Callout", status);
    }

    private void CloseTowerMagicPointPanel()
    {
        Animator animator = towerMagicPointPanel.GetComponent<Animator>();
        bool status = animator.GetBool("Callout");
        if (status == false) { return; }
        //towerMagicPointPanel.SetActive(false);
        animator.SetBool("Callout", false);
    }

    public void OpenTowerUpgradePanel()
    {
        CloseTowerFunctionalityPanel();
        Animator animator = towerUpgradePanel.GetComponent<Animator>();
        bool status = !animator.GetBool("Callout");
        //towerUpgradePanel.SetActive(status);
        animator.SetBool("Callout", status);
    }

    private void CloseTowerUpgradePanel()
    {
        Animator animator = towerUpgradePanel.GetComponent<Animator>();
        bool status = animator.GetBool("Callout");
        if (status == false) { return; }
        //towerUpgradePanel.SetActive(false);
        animator.SetBool("Callout", false);
    }
}

public partial class GameSceneUIManager : MonoBehaviour
{

}

public partial class GameSceneUIManager : MonoBehaviour
{
    //private void OnGUI()
    //{
    //    GUILayout.Space(50);
    //    GUILayout.Label(string.Format("player nick name is {0}", PhotonNetwork.player.NickName));
    //}
}
