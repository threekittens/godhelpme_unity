﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingMenuAlertor : MonoBehaviour {

    public static event System.EventHandler OnTapedSettingMenu;
    public static event System.EventHandler OnTapedContinueGame;
    public static event System.EventHandler OnTapedExitGame;
    public static event System.EventHandler OnClose;

    private static GameObject settingMenuAlertor;

    public static void ShowOn(Transform USERINTERFACEMANAGER)
    {
        GameObject instance = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterfaceGameScene("SettingMenuAlertor"));
        settingMenuAlertor = Instantiate(instance, USERINTERFACEMANAGER);

        Button settingButton = settingMenuAlertor.transform.Find("Background/Content/SettingButton").GetComponent<Button>();
        settingButton.onClick.AddListener(() => SettingMenu());

        Button continueGameButton = settingMenuAlertor.transform.Find("Background/Content/ContinueGameButton").GetComponent<Button>();
        continueGameButton.onClick.AddListener(() => ContinueGame());

        Button exitGameButton = settingMenuAlertor.transform.Find("Background/Content/ExitGameButton").GetComponent<Button>();
        exitGameButton.onClick.AddListener(() => ExitGame());        

        RectTransform rect = settingMenuAlertor.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        settingMenuAlertor.SetActive(true);
    }

    private static void SettingMenu()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCoinSE();
        }
        if (OnTapedSettingMenu != null) { OnTapedSettingMenu(null, System.EventArgs.Empty); }        
    }

    private static void ContinueGame()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
        if (OnTapedContinueGame != null) { OnTapedContinueGame(null, System.EventArgs.Empty); }
        Close();
    }

    private static void ExitGame()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
        if (OnTapedExitGame != null) { OnTapedExitGame(null, System.EventArgs.Empty); }
        Close();
    }

    private static void Close()
    {
        if (OnClose != null) { OnClose(null, System.EventArgs.Empty); }
        DestroyImmediate(settingMenuAlertor);
    }
}
