﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum CNPRooleGender
{
    None = 0,
    Male = 1000,
    Female = -1000
}

public class CNPSceneUIManager : MonoBehaviour
{
    private CNPRooleGender gender = CNPRooleGender.None;
    private string playerName = default(string);

    private void Start()
    {
        WarnAlertor.OnClosed += WarnAlertor_OnClosed;
    }

    private void WarnAlertor_OnClosed(object sender, System.EventArgs e)
    {

    }

    private void OnDestroy()
    {
        WarnAlertor.OnClosed -= WarnAlertor_OnClosed;
    }

    public void OnNameInputEvent(InputField input)
    {
        if (input.text.Length > 0)
        {
            playerName = input.text;
        }
    }

    public void OnManButtonEvent()
    {
        gender = CNPRooleGender.Male;
    }

    public void OnWomanButtonEvent()
    {
        gender = CNPRooleGender.Female;
    }

    public void OnCreateButtonEvent()
    {
        if (gender == CNPRooleGender.None) { return; }

        if (playerName == default(string)) { return; }

        if (FirebaseManager.Singleton)
        {
            FirebaseManager.Singleton.CheckPlayerNameIsAlerady(playerName, (exist) =>
            {
                if (exist)
                {
                    Debug.Log("exist" + playerName);
                    WarnAlertor.Show("名稱重複，請更換另一名稱", transform);
                }
                else
                {
                    FirebaseManager.Singleton.OnSignedPlayerUpdated += Singleton_OnSignedPlayerUpdated;

                    SignedPlayer s = FirebaseManager.Singleton.SignedPlayer;
                    s.playername = playerName;
                    s.gender = (int)gender;
                    FirebaseManager.Singleton.SignedPlayer = s;
                }
            });
        }
    }

    private void Singleton_OnSignedPlayerUpdated(bool finished)
    {
        FirebaseManager.Singleton.OnSignedPlayerUpdated -= Singleton_OnSignedPlayerUpdated;

        if (finished)
        {            
            StartCoroutine(StartLoading(ProjectKeywordList.kSceneForLobby, this.transform));
            Debug.Log("succeeded for Update player data.");
        }
        else
        {
            Debug.Log("fail for Update player data.");
        }
    }

    private IEnumerator StartLoading(string scene, Transform transform)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, transform);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }
}
