﻿using PathManager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    [SerializeField] private GameObject SKBPrefab;
    private GameObject SKB;

    [SerializeField] private LongRangeBulletLauncher iceThornLRBL;
    [SerializeField] private LongRangeBulletLauncher iceEagleLRBL;
    [SerializeField] private CapsuleCollider weaponCC;
    [SerializeField] private CloseFightingInductor CFI;

    [SerializeField] private ParticleSystem WWEPrefab;
    private ParticleSystem WWE;

    private Transform target;
    private Vector3 lookTargetDirection;
    [SerializeField] private Animator animator;
    [SerializeField] private PlayerProperty PP;
    [SerializeField] private PowerManager powerManager;
    [SerializeField] private int skill01Index = 0;
    private string[] skill01AnimationNames = new string[] { ProjectKeywordList.kAnimationForSkill01, ProjectKeywordList.kAnimationForSkill01_1, ProjectKeywordList.kAnimationForSkill01_2 };

    [SerializeField] private List<ParticleSystem> slashList;
    private int slashIndex;


    private bool onTriggerSkill01 = false;
    private bool onTriggerSkill02 = false;
    private bool onTriggerSkill03 = false;
    private bool onTriggerSkill04 = false;

    [System.Serializable]
    private struct SkillCDList
    {
        public int CD01, CD02, CD03, CD04;
        public int CMP01, CMP02, CMP03, CMP04;
        public SkillCDList(int _CD01, int _CD02, int _CD03, int _CD04, int _CMP01, int _CMP02, int _CMP03, int _CMP04)
        {
            this.CD01 = _CD01;
            this.CD02 = _CD02;
            this.CD03 = _CD03;
            this.CD04 = _CD04;
            this.CMP01 = _CMP01;
            this.CMP02 = _CMP02;
            this.CMP03 = _CMP03;
            this.CMP04 = _CMP04;
        }
    }

    [SerializeField] private SkillCDList _SKCDList;
    private SkillCDList SKCDList
    {
        get
        {
            //_SKCDList.CD01 = 0;
            //_SKCDList.CD02 = 34;
            //_SKCDList.CD03 = 34;
            //_SKCDList.CD04 = 54;

            //_SKCDList.CMP01 = 0;
            //_SKCDList.CMP02 = 40;
            //_SKCDList.CMP03 = 50;
            //_SKCDList.CMP04 = 75;

            return _SKCDList;
        }
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    private void Start()
    {
        InitializeSkillComponent();
    }

    private void InitializeSkillComponent()
    {

    }

    private void Update()
    {
        if (target)
        {
            if (lookTargetDirection.magnitude > 0.1f)
            {
                transform.parent.LookAt(target.position);
                transform.parent.rotation = Quaternion.Euler(0.0f, transform.parent.rotation.eulerAngles.y, 0.0f);
            }

            lookTargetDirection = Vector3.Lerp(lookTargetDirection, Vector3.zero, 5 * Time.deltaTime);
        }
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    public void CastSkill01(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[0].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD01;
            SJC.OnTriggerSkillCoolDown(CD);
        }
        if (onTriggerSkill01 == false) { StartCoroutine(Skill_01(HSP)); }
    }

    public void CastSkill02(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[1].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD02 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_02(HSP));
    }

    public void CastSkill03(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[2].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD03 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_03(HSP));
    }

    public void CastSkill04(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[3].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD04 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_04(HSP));
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    IEnumerator Skill_01(HeroSkillProperty HSP)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName(ProjectKeywordList.kAnimationForIdle) == false) { yield break; }

        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        target = HSP.targets[0].transform;

        weaponCC.enabled = true;

        yield return new WaitUntil(() => weaponCC.enabled);

        onTriggerSkill01 = true;

        animator.SetFloat(ProjectKeywordList.kAnimationForSkill01Speed, HSP.speed);

        slashIndex = skill01Index;

        yield return StartCoroutine(PalyingAnimation(skill01AnimationNames[skill01Index], true));
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName(skill01AnimationNames[skill01Index]));

        if (skill01Index < 2)
        {
            skill01Index += 1;
        }
        else
        {
            skill01Index = 0;
        }

        lookTargetDirection = HSP.forwardEndPos;

        float wD = (7.5f * (HSP.level - 1));
        int damage = PP.Damage * (((120 + Convert.ToInt32(wD))) / 100);
        CFI.Damage = damage;
        CFI.OnHitedGameObject += Skill01_OnHitedGameObject;

        int CMP = SKCDList.CMP01;
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void Skill01_OnHitedGameObject(int damage, GameObject gameObject)
    {
        CFI.OnHitedGameObject -= Skill01_OnHitedGameObject;

        if (gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            if (onTriggerSkill01 && weaponCC.enabled)
            {
                slashList[slashIndex].Play();

                Transform target = gameObject.transform;

                // if (target != this.target) { return; }

                HealthManager healthManager = target.GetComponent<HealthManager>();
                healthManager.Health -= (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
                healthManager.OnListeningAttacker(transform.parent.gameObject);

                Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.parent.position);

                FloatingTextControl.Initialize();
                FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);

                weaponCC.enabled = false;
                onTriggerSkill01 = false;
            }
        }
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    IEnumerator Skill_02(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill02, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        if (target != null) { transform.parent.LookAt(target.position); }

        int damage = PP.Damage * (((120 + (15 * (HSP.level - 1)))) / 100);
        iceThornLRBL.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        iceThornLRBL.launcherForward = new Vector3(transform.parent.forward.x, 0.0f, transform.parent.forward.z);
        iceThornLRBL.showMinimark = false;
        iceThornLRBL.bulletModel = LongRangeBulletModel.Genernal;
        iceThornLRBL.OnLongRangeBulletHitedTarget += IceThornLRBL_OnLongRangeBulletHitedTarget;
        iceThornLRBL.Launching();

        int CMP = SKCDList.CMP02 + ((1500 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void IceThornLRBL_OnLongRangeBulletHitedTarget(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
            iceThornLRBL.OnLongRangeBulletHitedTarget -= IceThornLRBL_OnLongRangeBulletHitedTarget;
        }
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    IEnumerator Skill_03(HeroSkillProperty HSP)
    {
        onTriggerSkill04 = true;

        weaponCC.enabled = true;

        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        WWE = Instantiate(WWEPrefab, transform.parent);
        WWE.Stop();
        WWE.Clear();
        WWE.Play();

        yield return new WaitUntil(() => WWE.isEmitting);

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill04Bool, true));

        // 获取 已锁定的 enemy 并取得 position 
        //target = skillProperty.targets[0].transform;
        //lookTargetDirection = skillProperty.forwardEndPos;

        int damage = PP.Damage * ((150 + (25 * (HSP.level - 1))) / 100);
        CFI.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        CFI.OnHitedGameObject += Skill03_OnHitedGameObject;

        int CMP = SKCDList.CMP03 + ((2000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        SKB = Instantiate(SKBPrefab);

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.parent.position);
        pos.y -= 50f;
        SKB.transform.SetParent(GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform, false);
        SKB.transform.position = pos;

        SkillProgressBar skillProgressBar = SKB.GetComponent<SkillProgressBar>();
        skillProgressBar.KeepTime = 10;
        skillProgressBar.OnSkillProgressBarDidStarted += OnSkillProgressBarDidStarted;
        skillProgressBar.OnSkillProgressBarDidStop += OnSkillProgressBarDidStop;
        skillProgressBar.OnSkillProgressBarOnDisable += OnSkillProgressBarOnDisable;

        SKB.SetActive(true);

        yield break;
    }

    private void Skill03_OnHitedGameObject(int damage, GameObject gameObject)
    {
        if (gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            if (onTriggerSkill04 && weaponCC.enabled)
            {
                Transform enemy = gameObject.transform;

                HealthManager healthManager = enemy.GetComponent<HealthManager>();
                healthManager.Health -= (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
                healthManager.OnListeningAttacker(transform.parent.gameObject);

                Vector3 contactPoint = enemy.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.parent.position);

                FloatingTextControl.Initialize();
                FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);
            }
        }
    }

    private void OnSkillProgressBarDidStarted(float parogress)
    {

    }

    private void OnSkillProgressBarDidStop(float parogress)
    {
        WWE.Stop();
        WWE.Clear();
        DestroyImmediate(WWE.gameObject);

        weaponCC.enabled = false;
        CFI.OnHitedGameObject -= Skill03_OnHitedGameObject;
        StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill04Bool, false));
    }

    private void OnSkillProgressBarOnDisable()
    {
        SkillProgressBar skillProgressBar = SKB.GetComponent<SkillProgressBar>();
        skillProgressBar.OnSkillProgressBarDidStarted -= OnSkillProgressBarDidStarted;
        skillProgressBar.OnSkillProgressBarDidStop -= OnSkillProgressBarDidStop;
        skillProgressBar.OnSkillProgressBarOnDisable -= OnSkillProgressBarOnDisable;
        Destroy(SKB);
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    IEnumerator Skill_04(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill03, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        if (target != null) { transform.parent.LookAt(target.position); }

        int damage = PP.Damage * (((270 + (30 * (HSP.level - 1)))) / 100);
        iceEagleLRBL.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        iceEagleLRBL.launcherForward = new Vector3(transform.parent.forward.x, 0.0f, transform.parent.forward.z);
        iceEagleLRBL.showMinimark = false;
        iceEagleLRBL.bulletModel = LongRangeBulletModel.Impacter;
        iceEagleLRBL.OnLongRangeBulletHitedTarget += IceEagleLRBL_OnLongRangeBulletHitedTarget;
        iceEagleLRBL.Launching();

        int CMP = SKCDList.CMP04 + ((2500 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void IceEagleLRBL_OnLongRangeBulletHitedTarget(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
            iceEagleLRBL.OnLongRangeBulletHitedTarget -= IceEagleLRBL_OnLongRangeBulletHitedTarget;
        }
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    private void DiscountPowerBySkillKind(float power)
    {
        powerManager.Power -= power;
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{
    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    IEnumerator PalyingAnimation(string name, bool isPlay)
    {
        if (animator)
        {
            animator.SetBool(name, isPlay);
        }
        yield break;
    }
}

public partial class Freezing_Pioneer_Skills : MonoBehaviour
{

}
