﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class IronCoreImpact : MonoBehaviour
{
    internal int level = 1;

    [SerializeField]
    private float damage = 150.0f;  // 最大损伤

    [SerializeField]
    private float serength = 10;

    private bool mIsImpact;

    private Vector3 mForwardEndPos;
    private Vector3 mImpact;

    private Motor mMotor;

    private SphereCollider mSphereCollider;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnHitedTarget;
}

public partial class IronCoreImpact : MonoBehaviour
{
    private void Start()
    {

    }

    private void OnEnable()
    {

        Destroy(gameObject, 1);

        mMotor = transform.parent.GetComponent<Motor>();
        //mSphereCollider = GetComponent<SphereCollider>();
    }

    private void FixedUpdate()
    {
        float step = serength * Time.deltaTime;

        if (mImpact.magnitude > 0.2)
        {
            mMotor.enabled = false;
            transform.parent.LookAt(mForwardEndPos);
            transform.parent.position = Vector3.Lerp(transform.parent.position, mForwardEndPos, step);
        }

        mImpact = Vector3.Lerp(mImpact, Vector3.zero, step);
        mMotor.enabled = true;
    }

    public void CastImptact(Vector3 forwardEndPos)
    {
        mForwardEndPos = forwardEndPos;
        mImpact = mForwardEndPos;
        //Debug.Log(mForwardEndPos.magnitude);
        //StartCoroutine(ActiveCollider());
    }

    IEnumerator ActiveCollider()
    {
        mSphereCollider.enabled = true;
        yield return new WaitForSeconds(0.1f);
        mSphereCollider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("other.gameObject => " + other.gameObject.name);
        //Debug.Log("other.gameObject.layer => " + other.gameObject.layer);
        if (other.gameObject.layer == LayerMask.NameToLayer("ENEMY_ATTACKABLE"))
        {
        
            FloatingTextControl.Initialize();
            Transform enemy = other.gameObject.transform;
            HealthManager helethManager = enemy.GetComponent<HealthManager>();
            helethManager.Health -= (damage *= level);
            Vector3 contactPoint = enemy.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);

            if (OnHitedTarget != null)
            {
                OnHitedTarget(other);
            }

        }
    }
}
