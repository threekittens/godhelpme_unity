﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Shielder : MonoBehaviour
{

    [SerializeField]
    private ParticleSystem shieldEffect;
    private ParticleSystem shieldEffectInstance;
    private int mitigation;
    internal int Mitigation { get { return mitigation; } set { mitigation = value; } }
    private int _keepTime = 10;
    internal int KeepTime
    {
        get
        {
            return _keepTime;
        }
        set
        {
            _keepTime = value;
        }
    }
}

public partial class Shielder : MonoBehaviour
{
    private void OnEnable()
    {
        if (shieldEffectInstance) { Destroy(shieldEffectInstance.gameObject); }
        shieldEffectInstance = Instantiate(shieldEffect, transform);
        StartCoroutine(CastShieldEffect());
    }

    IEnumerator CastShieldEffect()
    {
        yield return new WaitForSeconds(KeepTime);
        if (shieldEffectInstance) { Destroy(shieldEffectInstance.gameObject); }
        gameObject.SetActive(false);
    }
}
