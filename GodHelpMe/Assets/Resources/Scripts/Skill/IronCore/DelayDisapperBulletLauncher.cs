﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class DelayDisapperBulletLauncher : MonoBehaviour
{
    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnHitedTarget;
    
    internal delegate void StayInTarget(Transform target);
    internal event StayInTarget OnStayInTarget;

    internal delegate void AttackFinished(Transform target);
    internal event AttackFinished OnAttackFinished;

    [SerializeField]
    private GameObject bullet;
    private GameObject bulletInstance;

    [SerializeField]
    private ParticleSystem bulletEffect;

    [SerializeField]
    private ParticleSystem bulletHitEffect;

    [SerializeField]
    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }

    [SerializeField]
    private float bulletSpeed;

    [SerializeField]
    private float delayDisapper; // 延迟消失
    internal float DelayDisapper { get { return delayDisapper; } set { delayDisapper = value; } }

    internal bool tirrgerStrengthenDamage;
    internal Transform target;
    internal float maximumRange;

    private DelayDisapperBullet DDB;
}

public partial class DelayDisapperBulletLauncher : MonoBehaviour
{
    public void LauncinghBullet()
    {
        bulletInstance = Instantiate(bullet, transform.position, transform.rotation);

        if (bulletEffect)
        {
            ParticleSystem effect = Instantiate(bulletEffect, bulletInstance.transform);
            effect.Stop();
            effect.Play();
        }

        DDB = bulletInstance.GetComponent<DelayDisapperBullet>();
        DDB.target = target;
        DDB.maximumRange = maximumRange;
        DDB.tirrgerStrengthenDamage = tirrgerStrengthenDamage;
        DDB.speed = bulletSpeed;
        DDB.Damage = Damage;
        DDB.DelayDisapper = DelayDisapper;

        DDB.OnHitedTarget += DelayDisapperBullet_OnHitedTarget;
        DDB.OnStayInTarget += DelayDisapperBullet_OnStayInTarget;
        DDB.OnAttackFinished += DelayDisapperBullet_OnAttackFinished;
    }

    private void DelayDisapperBullet_OnStayInTarget(Transform target)
    {
        if (bulletHitEffect)
        {
            ParticleSystem instance = Instantiate(bulletHitEffect);
            instance.transform.position = target.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            ParticleSystem[] ps_array = instance.GetComponentsInChildren<ParticleSystem>(true);
            ps_array[0].Stop();

            CharacterController character = target.transform.GetComponent<CharacterController>();

            float effectScale = character.height / 5.0f;

            foreach (ParticleSystem ps in ps_array)
            {
                var main = ps.main;
                var mainSize = main.startSize;

                mainSize.constantMin *= effectScale * 2;
                mainSize.constantMax *= effectScale * 2;
                main.startSize = mainSize;
            }

            //Scale Lights' range
            Light[] lights = instance.GetComponentsInChildren<Light>();
            foreach (Light light in lights)
            {
                light.range *= effectScale * 2;
                light.transform.localPosition *= effectScale * 2;
            }

            instance.transform.parent = null;
            ps_array[0].Play();
            Destroy(instance.gameObject, 0.5f);
        }
    }

    private void DelayDisapperBullet_OnHitedTarget(Collider other)
    {
        GameObject target = other.gameObject;
        if (OnHitedTarget != null) { OnHitedTarget(other); }
    }

    private void DelayDisapperBullet_OnAttackFinished(Transform target)
    {
        DDB.OnHitedTarget -= DelayDisapperBullet_OnHitedTarget;
        DDB.OnStayInTarget -= DelayDisapperBullet_OnStayInTarget;
        DDB.OnAttackFinished -= DelayDisapperBullet_OnAttackFinished;

        if (OnAttackFinished != null) { OnAttackFinished(target); }

        if (bulletInstance != null) { Destroy(bulletInstance); }
    }
}

public partial class DelayDisapperBulletLauncher : MonoBehaviour
{


}

public partial class DelayDisapperBulletLauncher : MonoBehaviour
{


}