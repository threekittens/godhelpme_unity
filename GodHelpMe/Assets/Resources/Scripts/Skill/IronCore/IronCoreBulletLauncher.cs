﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathManager;

public partial class IronCoreBulletLauncher : MonoBehaviour {

    internal float BulletSpeed;

    internal int level;
    internal bool IsTirrgerStrengthenDamage;

    private GameObject mBulletObj;
    private GameObject mLaunchedBulletObj;    

    internal Transform mTarget;
    internal float mMaximumRange;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnBulletHitedTarget;
}

public partial class IronCoreBulletLauncher : MonoBehaviour
{
    private void OnEnable()
    {
        if (mBulletObj == null)
        {
            string bulletObjPath = LoadResourceFilePath.PrefabSkill() + "IronCoreBullet";
            mBulletObj = Resources.Load<GameObject>(bulletObjPath);
        }
    }

    private void OnDisable()
    {
        if (mLaunchedBulletObj != null)
        {
            Destroy(mLaunchedBulletObj);
        }
    }

    private void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }
}

public partial class IronCoreBulletLauncher : MonoBehaviour
{
    private void OnHitedTarget(Collider other)
    {
        gameObject.SetActive(false);

        if (OnBulletHitedTarget != null)
        {
            OnBulletHitedTarget(other);
        }
    }
}

public partial class IronCoreBulletLauncher : MonoBehaviour
{
    public void LauncinghBullet()
    {
        mLaunchedBulletObj = Instantiate(mBulletObj, transform.position, transform.rotation);
        IronCoreBullet bullet = mLaunchedBulletObj.GetComponent<IronCoreBullet>();
        bullet.IsTirrgerStrengthenDamage = IsTirrgerStrengthenDamage;
        bullet.mSpeed = BulletSpeed;
        bullet.level = level;
        bullet.mTarget = mTarget;
        bullet.mMaximumRange = mMaximumRange;
        bullet.OnHitedTarget += OnHitedTarget;
    }
}

public partial class IronCoreBulletLauncher : MonoBehaviour
{

}

public partial class IronCoreBulletLauncher : MonoBehaviour
{

}

public partial class IronCoreBulletLauncher : MonoBehaviour
{

}

public partial class IronCoreBulletLauncher : MonoBehaviour
{

}