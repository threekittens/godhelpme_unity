﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronCoreBullet : MonoBehaviour
{

    internal bool IsTirrgerStrengthenDamage;

    [SerializeField]
    private float damage = 100.0f;  // 最大损伤

    [SerializeField]
    internal float mSpeed;

    internal int level = 1;

    internal float mMaximumRange;
    internal Transform mTarget;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnHitedTarget;

    private void Update()
    {
        if (gameObject != null)
        {
            StartCoroutine(Moveing());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != mTarget.gameObject.layer)
        {
            //Debug.Log(other.name);
            return;
        }

        FloatingTextControl.Initialize();
        Transform enemy = other.gameObject.transform;
        HealthManager helethManager = enemy.GetComponent<HealthManager>();
        if (IsTirrgerStrengthenDamage)
        {
            damage *= 2;
        }
        helethManager.Health -= (damage *= level);
        Vector3 contactPoint = enemy.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
        FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);
        if (OnHitedTarget != null)
        {
            OnHitedTarget(other);
        }
        Destroy(gameObject);
    }

    IEnumerator Moveing()
    {
        if (mTarget == null)
        {
            Debug.Log("Target doesn't exist Destroy Bullet");
            DestroyImmediate(gameObject);
            yield break;
        }
        else
        {
            float dis = Vector3.Distance(mTarget.position, transform.position);

            if (dis > mMaximumRange)
            {
                //target = null;
            }
            else
            {
                Vector3 lookTargetDirection = mTarget.position;
                transform.LookAt(lookTargetDirection);
            }

            if (gameObject != null && mTarget != null)
            {
                //fire
                float step = mSpeed * Time.deltaTime;
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, mTarget.position + Vector3.up, step);
            }
        }
        yield break;
    }
}
