﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayDisapperBullet : MonoBehaviour
{
    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnHitedTarget;

    internal delegate void StayInTarget(Transform target);
    internal event StayInTarget OnStayInTarget;

    internal delegate void AttackFinished(Transform target);
    internal event AttackFinished OnAttackFinished;
    internal float speed;

    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }

    [SerializeField]
    private float _delayDisapper; // 延迟消失
    internal float DelayDisapper { get { return _delayDisapper; } set { _delayDisapper = value; } }
    internal bool tirrgerStrengthenDamage;

    internal Transform target;
    internal float maximumRange;
    private int countdownSecond = 0;

    private void Update()
    {
        if (gameObject != null)
        {
            StartCoroutine(Moveing());
        }
    }

    IEnumerator Moveing()
    {
        if (target == null)
        {
            Debug.Log("Target doesn't exist Destroy Bullet");
            DestroyImmediate(gameObject);
            yield break;
        }
        else
        {
            float dis = Vector3.Distance(target.position, transform.position);

            if (dis > maximumRange)
            {

            }
            else
            {
                Vector3 lookTargetDirection = target.position;
                transform.LookAt(lookTargetDirection);
            }

            if (target != null)
            {
                //fire
                float step = speed * Time.deltaTime;
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.transform.position + Vector3.up, step);
            }
        }
        yield break;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != target.gameObject.layer) { return; }
        countdownSecond = 0;
        InvokeRepeating("DiscountHealth", 0, 1);
        if (OnHitedTarget != null) { OnHitedTarget(other); }
        Destroy(gameObject, DelayDisapper);
    }

    private void DiscountHealth()
    {
        if (countdownSecond >= DelayDisapper)
        {
            countdownSecond = 0;
            if (OnAttackFinished != null) { OnAttackFinished(target); }
            CancelInvoke();
        }

        HealthManager helethManager = target.GetComponent<HealthManager>();
        helethManager.Health -= (tirrgerStrengthenDamage) ? (Damage * 2) : Damage;

        Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

        FloatingTextControl.Initialize();
        FloatingTextControl.CreateFloatingText(Damage.ToString(), contactPoint);

        if (OnStayInTarget != null) { OnStayInTarget(target); }

        countdownSecond++;
    }
}
