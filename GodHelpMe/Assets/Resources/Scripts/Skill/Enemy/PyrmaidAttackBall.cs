﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class PyrmaidAttackBall : MonoBehaviour
{
    internal ParticleSystem BallEffect {
        set {
            ParticleSystem instance = Instantiate(value, gameObject.transform);
            instance.Stop();
            instance.Play();
        }
    }
    internal ParticleSystem ballHitedEffect;

    internal float ballInflateSpeed = 1f;
    internal float ballRange = 1f;
    internal float ballDamage = 0f;    

    internal bool Activity { get { return activity; } set { activity = value; } }

    private bool activity = false;

    private void Update()
    {
        Debug.Log(ballInflateSpeed);
        Debug.Log(ballRange);

        if (gameObject.transform.localScale.magnitude >= ballRange)
        {
            DestroyImmediate(gameObject);
            return;
        }

        if (activity)
        {

            gameObject.transform.localScale += Vector3.one * ballInflateSpeed * Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {       
        if (other.gameObject.layer != LayerMask.NameToLayer(ProjectKeywordList.kLayerForAttackable)) { return; }

        Debug.Log("damage..." + ballDamage);
        Debug.Log("CloseFightingInductor_OnHitedGameObject...");

        Transform target = other.gameObject.transform;

        HealthManager helethManager = target.GetComponent<HealthManager>();
        helethManager.Health -= ballDamage;

        Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

        FloatingTextControl.Initialize();
        FloatingTextControl.CreateFloatingText(ballDamage.ToString(), contactPoint);

        if (ballHitedEffect)
        {
            ParticleSystem effectInstance = Instantiate(ballHitedEffect, target);
            effectInstance.transform.position = contactPoint;
            ScaleEffect(effectInstance);
            Destroy(effectInstance, effectInstance.main.duration);
        }
    }

    private void ScaleEffect(ParticleSystem effectInstance)
    {
        ParticleSystem[] ps_array = effectInstance.GetComponentsInChildren<ParticleSystem>(true);
        ps_array[0].Stop();

        foreach (ParticleSystem ps in ps_array)
        {
            ps.transform.localScale *= 2;
        }

        //Scale Lights' range
        Light[] lights = effectInstance.GetComponentsInChildren<Light>();
        foreach (Light light in lights)
        {
            light.range *= 2;
            light.transform.localPosition *= 2;
        }

        ps_array[0].Play();
    }
}
