﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(EnemyNav))]
[RequireComponent(typeof(Animator))]
public class PyrmaidAttack : MonoBehaviour
{

    [SerializeField]
    private ContinuityParticleSystem CPS;

    [SerializeField]
    private GameObject ballInstance;

    [SerializeField]
    private float ballInflateSpeed;
    [SerializeField]
    private float ballRange;
    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }

    [SerializeField]
    internal ParticleSystem ballEffect;
    [SerializeField]
    internal ParticleSystem ballHitedEffect;

    private EnemyNav enemyNav;
    private Animator animator;
    private bool isCPSPlayed = false;    
    // Use this for initialization
    private void Start()
    {
        Debug.Log("Started Initialize the PyrmaidAttack...");
        animator = transform.GetComponent<Animator>();
        enemyNav = transform.GetComponent<EnemyNav>();
        enemyNav.OnNavStoppedAtTarget += OnNavStoppedAtTarget;

        CPS.OnContinuityParticleSystemFinished += OnContinuityParticleSystemFinished;
    }

    private void OnDestroy()
    {
        enemyNav.OnNavStoppedAtTarget -= OnNavStoppedAtTarget;
        CPS.OnContinuityParticleSystemFinished -= OnContinuityParticleSystemFinished;
    }

    private void OnContinuityParticleSystemFinished(object sender, EventArgs e)
    {
        Debug.Log("attack !");
        isCPSPlayed = false;
        Attacking();
    }

    private void Attacking()
    {

        GameObject instance = Instantiate(ballEffect, transform).gameObject;
        instance.AddComponent<PyrmaidAttackBall>();

        SphereCollider sphereCollider = instance.GetComponent<SphereCollider>();
        sphereCollider.isTrigger = true;
        sphereCollider.radius = 2f;

        PyrmaidAttackBall pyrmaidAttackBall = instance.GetComponent<PyrmaidAttackBall>();        
        pyrmaidAttackBall.ballInflateSpeed = ballInflateSpeed;
        pyrmaidAttackBall.ballRange = ballRange;
        if (transform.tag == ProjectKeywordList.kTagForEnemy) { Damage = GetComponent<EnemyProperty>().Damage; }
        pyrmaidAttackBall.ballDamage = Damage;
        pyrmaidAttackBall.ballHitedEffect = ballHitedEffect;
        pyrmaidAttackBall.Activity = true;



        //GameObject instance = Instantiate(ballInstance, transform);

        //PyrmaidAttackBall pyrmaidAttackBall = instance.GetComponent<PyrmaidAttackBall>();
        //pyrmaidAttackBall.ballInflateSpeed = ballInflateSpeed;
        //pyrmaidAttackBall.ballRange = ballRange;
        //pyrmaidAttackBall.ballDamage = ballDamage;
        //pyrmaidAttackBall.BallEffect = ballEffect;
        //pyrmaidAttackBall.ballHitedEffect = ballHitedEffect;
        //pyrmaidAttackBall.Activity = true;
    }

    private void OnNavStoppedAtTarget(bool status)
    {
        if (status)
        {
            InvokeRepeating("PlayCPS", 0.0f, 0.5f);
        }
        else
        {
            if (CPS)
            {
                CPS.InterruptTheCPS();
            }
            CancelInvoke("PlayCPS");
            isCPSPlayed = false;
        }
    }

    private void PlayCPS()
    {
        if (!isCPSPlayed)
        {
            isCPSPlayed = true;

            if (CPS)
            {
                CPS.PlayTheCPS();
            }
        }
    }
}
