﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyNav))]
public class JetAttack : MonoBehaviour
{
    [SerializeField]
    private float delayToJet;
    [SerializeField]
    private float delayToBackoff;
    [SerializeField]
    private float impactStrength = 1.0f;
    [SerializeField]
    private float backoffStrength = 1.0f;
    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }
    [SerializeField]
    private CloseFightingInductor closeFightingInductor;
    [SerializeField]
    private EnemyNav enemyNav;
    [SerializeField]
    private GameObject hitEffect;
    [SerializeField]
    private ParticleSystem jetEffect;
    private ParticleSystem jetEffectInstance;

    private Animator animator;
    private bool startJet = false;
    private bool backOff = false;

    // Use this for initialization
    private void Start()
    {
        Debug.Log("Started Initialize the JetAttack...");
        //jetEffectInstance = Instantiate(jetEffect, jetEffect.transform.position, jetEffect.transform.rotation);
        animator = transform.GetComponent<Animator>();
        if (transform.tag == ProjectKeywordList.kTagForEnemy) { Damage = GetComponent<EnemyProperty>().Damage; }
        closeFightingInductor.Damage = Damage;
        closeFightingInductor.OnHitedGameObject += CloseFightingInductor_OnHitedGameObject;
        enemyNav.OnNavStoppedAtTarget += OnNavStoppedAtTarget;
    }

    private void CloseFightingInductor_OnHitedGameObject(int damage, GameObject gameObject)
    {
        if (gameObject.layer != LayerMask.NameToLayer(ProjectKeywordList.kLayerForAttackable)) { return; }

        if (gameObject.transform == enemyNav.Target)
        {
            //Debug.Log("damage..." + damage);
            //Debug.Log("CloseFightingInductor_OnHitedGameObject...");

            Transform target = gameObject.transform;

            HealthManager helethManager = target.GetComponent<HealthManager>();
            helethManager.Health -= damage;

            Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(closeFightingInductor.transform.position);

            FloatingTextControl.Initialize();
            FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);

            if (hitEffect)
            {
                GameObject effectInstance = Instantiate(hitEffect, target);
                effectInstance.transform.position = contactPoint;
                ParticleSystem effectInstancePS = effectInstance.GetComponent<ParticleSystem>();
                ScaleEffect(effectInstancePS);
                Destroy(effectInstance, effectInstancePS.main.duration);
            }

            StartCoroutine(DelayForSecondsEvent(delayToBackoff, () =>
            {
                startJet = false;
                backOff = true;
                StartJetEffect(startJet);
            }));
        }
    }

    private void OnDestroy()
    {
        closeFightingInductor.OnHitedGameObject -= CloseFightingInductor_OnHitedGameObject;
        enemyNav.OnNavStoppedAtTarget -= OnNavStoppedAtTarget;
    }

    private void OnNavStoppedAtTarget(bool status)
    {
        //Debug.Log(status);

        if (status)
        {
            animator.SetBool(ProjectKeywordList.kAnimationForMove, true);
            StartCoroutine(DelayForSecondsEvent(delayToJet, () =>
            {
                startJet = true;
                StartJetEffect(startJet);
                Debug.Log("Start to jet");
            }));
        }
    }

    IEnumerator DelayForSecondsEvent(float second, Action callback)
    {
        yield return new WaitForSeconds(second);
        callback();
        yield break;
    }

    // Update is called once per frame
    private void Update()
    {
        if (enemyNav.Target)
        {
            //Debug.Log(startJet);

            if (backOff)
            {
                float betweenDis = Vector3.Distance(transform.position, enemyNav.Target.position);
                if (betweenDis > enemyNav.DefaultStopDis + 5.0f)
                {
                    backOff = false;
                    return;
                }

                transform.position -= transform.forward * Time.deltaTime * backoffStrength;
            }
            else if (startJet)
            {
                transform.position += transform.forward * Time.deltaTime * impactStrength;
            }
        }
    }

    private void StartJetEffect(bool status)
    {
        if (jetEffect == null) { return; }

        if (status)
        {
            animator.SetFloat(ProjectKeywordList.kAnimationForMoveSpeed, 5.0f);
            ScaleEffect(jetEffect);
            jetEffect.Play();
        }
        else
        {
            animator.SetFloat(ProjectKeywordList.kAnimationForMoveSpeed, 1.0f);
            ReScaleEffect(jetEffect);
            jetEffect.Stop();
            jetEffect.Clear();
        }
    }

    private void ScaleEffect(ParticleSystem effectInstance)
    {
        //Debug.Log("scale effect");
        ParticleSystem[] ps_array = effectInstance.GetComponentsInChildren<ParticleSystem>(true);
        ps_array[0].Stop();

        foreach (ParticleSystem ps in ps_array)
        {
            ps.transform.localScale *= 2;
        }

        //Scale Lights' range
        Light[] lights = effectInstance.GetComponentsInChildren<Light>();
        foreach (Light light in lights)
        {
            light.range *= 2;
            light.transform.localPosition *= 2;
        }

        ps_array[0].Play();
    }

    private void ReScaleEffect(ParticleSystem effectInstance)
    {
        //Debug.Log("re scale effect");
        ParticleSystem[] ps_array = effectInstance.GetComponentsInChildren<ParticleSystem>(true);
        ps_array[0].Stop();

        foreach (ParticleSystem ps in ps_array)
        {
            ps.transform.localScale /= 2;
        }

        //Scale Lights' range
        Light[] lights = effectInstance.GetComponentsInChildren<Light>();
        foreach (Light light in lights)
        {
            light.range /= 2;
            light.transform.localPosition /= 2;
        }

        ps_array[0].Play();
    }
}
