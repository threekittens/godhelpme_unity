﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

//[ExecuteInEditMode]
public class ContinuityParticleSystem : MonoBehaviour
{

    [SerializeField]
    private float delayTimeBetweenPS;

    [SerializeField]
    private List<ParticleSystem> effectList;

    internal System.EventHandler OnContinuityParticleSystemFinished;

    private bool isCPSInterrupted;

    internal void PlayTheCPS()
    {
        StartCoroutine(ExecuteContinuiting());
    }

    internal void InterruptTheCPS()
    {        
        for (int i = 0; i < effectList.Count; i++)
        {
            ParticleSystem e = effectList[i];
            e.Stop();
            e.Clear();            
        }
        StopAllCoroutines();        
    }

    IEnumerator ExecuteContinuiting()
    {        
        for (int i = 0; i < effectList.Count; i++)
        {
            ParticleSystem e = effectList[i];
            e.Stop();
            e.Play();
            yield return new WaitUntil(() => e.isEmitting == false);
            e.Clear();
            yield return new WaitForSeconds(i * delayTimeBetweenPS);
        }

        FinishedCPS();
        yield break;
    }

    private void FinishedCPS()
    {
        if (OnContinuityParticleSystemFinished != null)
        {
            OnContinuityParticleSystemFinished(null, System.EventArgs.Empty);
        }
    }
}

//#if UNITY_EDITOR
//[CustomEditor(typeof(ContinuityParticleSystem))]
//public class ContinuityParticleSystemEditor : Editor
//{
//    string status;
//    ContinuityParticleSystem CPS;
//    void OnEnable()
//    {
//        CPS = (ContinuityParticleSystem)target;
//        status = CPS.play ? "Stop" : "Play";
//    }

//    public override void OnInspectorGUI()
//    {
//        serializedObject.Update();
//        EditorGUIUtility.labelWidth = 0;
//        EditorGUIUtility.fieldWidth = 0;
//        SerializedProperty effectList = serializedObject.FindProperty("effectList");
//        EditorGUI.BeginChangeCheck();
//        EditorGUILayout.PropertyField(effectList, true);
//        if (EditorGUI.EndChangeCheck())
//            serializedObject.ApplyModifiedProperties();
//        EditorGUIUtility.labelWidth = 25;
//        EditorGUIUtility.fieldWidth = 50;

//        if (GUILayout.Button(status))
//        {
//            CPS.play = true;
//            status = CPS.play ? "Stop" : "Play";
//            //Debug.Log(status);
//            //Debug.Log(CPS.play);
//        }
//    }
//}
//#endif
