﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : MonoBehaviour
{

    internal ParticleSystem bulletEffect
    {
        set
        {
            if (value)
            {
                if (gameObject)
                {
                    ParticleSystem effect = Instantiate(value, gameObject.transform);
                    effect.Stop();
                    effect.Play();
                }
            }
        }
    }

    internal ParticleSystem bulletHitEffect;

    // internal bool tirrgerStrengthenDamage;

    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }

    internal float speed;

    internal float maximumRange;
    internal Transform target;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnHitedTarget;

    private void Update()
    {
        if (gameObject != null)
        {
            StartCoroutine(Moveing());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == null) { return; }

        if (other.gameObject.layer != target.gameObject.layer) { return; }

        Transform enemy = other.gameObject.transform;

        HealthManager helethManager = enemy.GetComponent<HealthManager>();
        helethManager.Health -= Damage;

        Vector3 contactPoint = enemy.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

        FloatingTextControl.Initialize();
        FloatingTextControl.CreateFloatingText(Damage.ToString(), contactPoint);

        if (OnHitedTarget != null) { OnHitedTarget(other); }

        SetHitedEffect();

        Destroy(gameObject);
    }

    private void SetHitedEffect()
    {
        if (bulletHitEffect)
        {
            ParticleSystem instance = Instantiate(bulletHitEffect);
            instance.transform.position = target.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            ParticleSystem[] ps_array = instance.GetComponentsInChildren<ParticleSystem>(true);
            ps_array[0].Stop();

            CharacterController character = target.transform.GetComponent<CharacterController>();

            //float effectScale = 1.0f;
            //if (character) { effectScale = character.height / 5.0f; }

            foreach (ParticleSystem ps in ps_array)
            {
                //var main = ps.main;
                //var mainSize = main.startSize;

                //mainSize.constantMin *= effectScale * 2;
                //mainSize.constantMax *= effectScale * 2;
                //main.startSize = mainSize;

                ps.transform.localScale *= 2;
            }

            //Scale Lights' range
            Light[] lights = instance.GetComponentsInChildren<Light>();
            foreach (Light light in lights)
            {
                light.range *= 2;
                light.transform.localPosition *= 2;
            }

            instance.transform.parent = null;
            ps_array[0].Play();
            Destroy(instance.gameObject, 0.5f);
        }
    }

    IEnumerator Moveing()
    {
        if (target == null)
        {
            //Debug.Log("Target doesn't exist Destroy Bullet");
            DestroyImmediate(gameObject);
            yield break;
        }
        else
        {
            //float dis = Vector3.Distance(target.position, transform.position);

            //if (dis > maximumRange)
            //{
            //    //target = null;
            //}
            //else
            //{

            //}

            transform.LookAt(target.position);

            if (gameObject != null && target != null)
            {
                //fire
                float step = speed * Time.deltaTime;
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.position + Vector3.up, step);
            }
        }
        yield break;
    }
}
