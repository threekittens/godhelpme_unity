﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathManager;

public partial class SimpleBulletLauncher : MonoBehaviour
{
    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private ParticleSystem bulletEffect;

    [SerializeField]
    private ParticleSystem bulletHitEffect;

    [SerializeField]
    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }

    [SerializeField]
    private float bulletSpeed;
    internal float fixedBulletSpeed;

    internal Transform target;
    internal float maximumRange;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnSimpleBulletHitedTarget;
}

public partial class SimpleBulletLauncher : MonoBehaviour
{
    public void LauncinghBullet()
    {
        GameObject instance = Instantiate(bullet, transform.position, transform.rotation);

        SimpleBullet simpleBullet = instance.GetComponent<SimpleBullet>();
        simpleBullet.bulletEffect = bulletEffect;
        simpleBullet.bulletHitEffect = bulletHitEffect;
        simpleBullet.Damage = Damage;
        simpleBullet.speed = bulletSpeed * fixedBulletSpeed;
        simpleBullet.maximumRange = maximumRange;
        simpleBullet.target = target;
        simpleBullet.OnHitedTarget += OnHitedTarget;
    }
}

public partial class SimpleBulletLauncher : MonoBehaviour
{
    private void OnHitedTarget(Collider other)
    {
        GameObject target = other.gameObject;

        if (OnSimpleBulletHitedTarget != null)
        {
            OnSimpleBulletHitedTarget(other);
        }
    }
}

public partial class SimpleBulletLauncher : MonoBehaviour
{

}
