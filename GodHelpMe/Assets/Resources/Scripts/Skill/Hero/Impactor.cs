﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Impactor : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem impactorEffect;
    [SerializeField]
    private ParticleSystem impactorHitEffect;

    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }

    [SerializeField]
    private float serength = 10;

    private Motor motor;
    private Vector3 impact;
    private Vector3 forwardEndPos;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnImpactorHitedTarget;
}

public partial class Impactor : MonoBehaviour
{
    private void OnEnable()
    {
        motor = transform.parent.GetComponent<Motor>();
    }

    public void CastImptact(Vector3 targetPos)
    {
        StartCoroutine(ExcuteImpatct(targetPos));
    }

    IEnumerator ExcuteImpatct(Vector3 targetPos)
    {

        if (impactorEffect)
        {
            ParticleSystem effect = Instantiate(impactorEffect, transform.parent);
            effect.Stop();
            effect.Play();
            Destroy(effect.gameObject, 0.5f);
        }

        forwardEndPos = targetPos;
        impact = forwardEndPos;
        yield break;
    }

    private void FixedUpdate()
    {
        float step = serength * Time.deltaTime;

        if (impact.magnitude > 0.2)
        {
            motor.enabled = false;
            transform.parent.LookAt(forwardEndPos);
            transform.parent.position = Vector3.Lerp(transform.parent.position, forwardEndPos, step);
        }

        impact = Vector3.Lerp(impact, Vector3.zero, step);
        motor.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            Transform target = other.gameObject.transform;

            HealthManager helethManager = target.GetComponent<HealthManager>();
            helethManager.Health -= Damage;

            Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            FloatingTextControl.Initialize();
            FloatingTextControl.CreateFloatingText(Damage.ToString(), contactPoint);

            if (impactorHitEffect)
            {
                ParticleSystem effect = Instantiate(impactorHitEffect, transform.parent);
                effect.transform.position = target.GetComponent<Collider>().ClosestPointOnBounds(transform.parent.position);
                effect.transform.localScale *= 2;
                effect.Stop();
                effect.Play();
                Destroy(effect.gameObject, 0.5f);
            }

            if (OnImpactorHitedTarget != null)
            {
                OnImpactorHitedTarget(other);
            }
        }
    }
}
