﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SpinnerBullet : MonoBehaviour
{
    internal delegate void DestoryBulletEvent(int index);
    internal delegate void HitTargetEvent(int bulletIndex, Collider other);

    internal event DestoryBulletEvent OnDestoryBullet;
    internal event HitTargetEvent OnHitedTarget;
    internal ParticleSystem bulletHitEffect;
    [SerializeField] private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }
    internal float speed;
    internal int index;
    internal float maximumRange;
    internal Transform target;
    internal bool LaunchSwitch;
    internal bool IsLaunchingAll;
}

public partial class SpinnerBullet : MonoBehaviour
{
    private void OnDestroy()
    {
        //Debug.Log(string.Format("OnDestroy {0}", index));
        if (OnDestoryBullet != null) { OnDestoryBullet(index); }
    }

    private void Update()
    {
        if (LaunchSwitch)
        {
            if (gameObject != null)
            {
                StartCoroutine(Moveing());
            }
        }
    }

    IEnumerator Moveing()
    {
        if (target == null)
        {
            Debug.Log("Target doesn't exist Destroy Bullet");
            DestroyImmediate(gameObject);
            yield break;
        }
        else
        {
            float dis = Vector3.Distance(target.position, transform.position);

            if (dis > maximumRange)
            {
                //target = null;
            }
            else
            {
                Vector3 lookTargetDirection = target.position;
                transform.LookAt(lookTargetDirection);
            }

            if (gameObject != null && target != null)
            {
                //fire
                float step = speed * Time.deltaTime;
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.position + Vector3.up, step);
            }
        }
        yield break;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable)) { return; }

        Transform target = other.gameObject.transform;

        HealthManager helethManager = target.GetComponent<HealthManager>();
        helethManager.Health -= Damage;

        Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

        FloatingTextControl.Initialize();
        FloatingTextControl.CreateFloatingText(Damage.ToString(), contactPoint);

        if (bulletHitEffect)
        {
            ParticleSystem instance = Instantiate(bulletHitEffect);
            instance.transform.position = target.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            ParticleSystem[] ps_array = instance.GetComponentsInChildren<ParticleSystem>(true);
            ps_array[0].Stop();

            CharacterController character = target.transform.GetComponent<CharacterController>();

            float effectScale = character.height / 5.0f;

            foreach (ParticleSystem ps in ps_array)
            {
                var main = ps.main;
                var mainSize = main.startSize;

                mainSize.constantMin *= effectScale * 2;
                mainSize.constantMax *= effectScale * 2;
                main.startSize = mainSize;
            }

            //Scale Lights' range
            Light[] lights = instance.GetComponentsInChildren<Light>();
            foreach (Light light in lights)
            {
                light.range *= effectScale * 2;
                light.transform.localPosition *= effectScale * 2;
            }

            instance.transform.parent = null;
            ps_array[0].Play();
            Destroy(instance.gameObject, 0.5f);
        }

        if (OnHitedTarget != null) { OnHitedTarget(index, other); }

        LaunchSwitch = false;

        if (IsLaunchingAll) { return; }

        Destroy(gameObject);
    }
}

public partial class SpinnerBullet : MonoBehaviour
{

}

public partial class SpinnerBullet : MonoBehaviour
{

}

public partial class SpinnerBullet : MonoBehaviour
{

}
