﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathManager;
using System.Linq;

public partial class Iron_Core_Skills : MonoBehaviour
{

    [SerializeField] private SimpleBulletLauncher SBL;
    [SerializeField] private Impactor impactor;
    [SerializeField] private Shielder shielder;
    [SerializeField] private DelayDisapperBulletLauncher DDBL;

    private Transform target;
    private Vector3 lookTargetDirection;

    [SerializeField] private Animator animator;
    [SerializeField] private PlayerProperty PP;
    [SerializeField] private PowerManager powerManager;

    private bool onTriggerSkill01 = false;
    private bool onTriggerSkill02 = false;
    private bool onTriggerSkill03 = false;
    private bool onTriggerSkill04 = false;

    [System.Serializable]
    private struct SkillCDList
    {
        public int CD01, CD02, CD03, CD04;
        public int CMP01, CMP02, CMP03, CMP04;
        public SkillCDList(int _CD01, int _CD02, int _CD03, int _CD04, int _CMP01, int _CMP02, int _CMP03, int _CMP04)
        {
            this.CD01 = _CD01;
            this.CD02 = _CD02;
            this.CD03 = _CD03;
            this.CD04 = _CD04;
            this.CMP01 = _CMP01;
            this.CMP02 = _CMP02;
            this.CMP03 = _CMP03;
            this.CMP04 = _CMP04;
        }
    }

    [SerializeField] private SkillCDList _SKCDList;
    private SkillCDList SKCDList
    {
        get
        {
            //_SKCDList.CD01 = 0;
            //_SKCDList.CD02 = 34;
            //_SKCDList.CD03 = 34;
            //_SKCDList.CD04 = 54;

            //_SKCDList.CMP01 = 0;
            //_SKCDList.CMP02 = 40;
            //_SKCDList.CMP03 = 50;
            //_SKCDList.CMP04 = 75;

            return _SKCDList;
        }
    }

}

public partial class Iron_Core_Skills : MonoBehaviour
{
    private void Start()
    {
        InitializeSkillComponent();
    }

    private void InitializeSkillComponent()
    {
        // animator = transform.parent.GetComponent<Animator>();
        // PP = transform.parent.GetComponent<PlayerProperty>();
        // powerManager = transform.parent.GetComponent<PowerManager>();
    }

    private void Update()
    {
        if (target)
        {
            if (lookTargetDirection.magnitude > 0.1f)
            {
                transform.parent.LookAt(target.position);
                transform.parent.rotation = Quaternion.Euler(0.0f, transform.parent.rotation.eulerAngles.y, 0.0f);
            }

            lookTargetDirection = Vector3.Lerp(lookTargetDirection, Vector3.zero, 5 * Time.deltaTime);
        }
    }
}

public partial class Iron_Core_Skills : MonoBehaviour
{
    public void CastSkill01(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[0].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD01;
            SJC.OnTriggerSkillCoolDown(CD);
        }
        if (!onTriggerSkill01) { StartCoroutine(Skill_01(HSP)); }
    }

    public void CastSkill02(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[1].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD02 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_02(HSP));
    }

    public void CastSkill03(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[2].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD03 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_03(HSP));
    }

    public void CastSkill04(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[3].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD04 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_04(HSP));
    }
}

public partial class Iron_Core_Skills : MonoBehaviour
{
    IEnumerator Skill_01(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        onTriggerSkill01 = true;

        animator.SetFloat(ProjectKeywordList.kAnimationForSkill01Speed, HSP.speed);
        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill01, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        HealthManager healthManager = target.GetComponent<HealthManager>();
        healthManager.OnListeningAttacker(transform.parent.gameObject);


        int damage = PP.Damage * ((175 + (15 * (HSP.level - 1))) / 100);
        SBL.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        SBL.target = target;
        SBL.maximumRange = HSP.range;
        SBL.fixedBulletSpeed = HSP.speed;
        SBL.OnSimpleBulletHitedTarget += SBL_OnSimpleBulletHitedTarget;
        SBL.LauncinghBullet();

        int CMP = SKCDList.CMP01;
        DiscountPowerBySkillKind(CMP);
        yield break;
    }

    private void SBL_OnSimpleBulletHitedTarget(Collider other)
    {
        SBL.OnSimpleBulletHitedTarget -= SBL_OnSimpleBulletHitedTarget;
        onTriggerSkill01 = false;
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
        }
    }
}

public partial class Iron_Core_Skills : MonoBehaviour
{
    IEnumerator Skill_02(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill02, true));

        // 获取 已锁定的 enemy 并取得 position 
        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        int damage = PP.Damage * ((150 + (25 * (HSP.level - 1))) / 100);
        impactor.Damage = damage;
        impactor.OnImpactorHitedTarget += Impactor_OnImpactorHitedTarget;

        impactor.gameObject.SetActive(true);

        impactor.CastImptact(HSP.forwardEndPos);

        int CMP = SKCDList.CMP02 + ((1000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void Impactor_OnImpactorHitedTarget(Collider other)
    {
        impactor.OnImpactorHitedTarget -= Impactor_OnImpactorHitedTarget;
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            impactor.gameObject.SetActive(false);

            ImpactReceiver impactReceiver = other.gameObject.GetComponent<ImpactReceiver>();
            if (impactReceiver) impactReceiver.AddImpact(transform.parent.forward, 10);
            lookTargetDirection = Vector3.zero;

            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);

            Debug.Log(string.Format("OnHitedTarget"));
        }
    }
}

public partial class Iron_Core_Skills : MonoBehaviour
{
    IEnumerator Skill_03(HeroSkillProperty HSP)
    {
        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill03, true));

        int mitigation = -15 - ((1300 / 100) * (HSP.level - 1));
        shielder.Mitigation = mitigation;

        int keepTime = 15;
        shielder.KeepTime = keepTime;

        shielder.gameObject.SetActive(true);

        int CMP = SKCDList.CMP03 + ((1000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }
}

public partial class Iron_Core_Skills : MonoBehaviour
{
    IEnumerator Skill_04(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill04, true));

        List<GameObject> oderlist = HSP.targets;
        oderlist.OrderBy(g => Vector3.Distance(transform.position, g.transform.position));

        target = oderlist[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        DDBL.target = target;
        DDBL.Damage = PP.Damage * ((130 + (20 * (HSP.level - 1))) / 100);
        DDBL.maximumRange = HSP.range;
        DDBL.tirrgerStrengthenDamage = PP.IsTirrgerStrengthenDamage;
        DDBL.DelayDisapper = 4 + ((25 / 100) * (HSP.level - 1));
        DDBL.OnAttackFinished += DDBL_OnAttackFinished;
        DDBL.LauncinghBullet();

        int CMP = SKCDList.CMP04 + ((1000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void DDBL_OnAttackFinished(Transform target)
    {
        DDBL.OnAttackFinished -= DDBL_OnAttackFinished;
        if (target.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = target.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
        }
    }
}

public partial class Iron_Core_Skills : MonoBehaviour
{
    private void DiscountPowerBySkillKind(float power)
    {
        powerManager.Power -= power;
    }
}

public partial class Iron_Core_Skills : MonoBehaviour
{
    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    IEnumerator PalyingAnimation(string name, bool isPlay)
    {
        if (animator) { animator.SetBool(name, isPlay); }
        yield break;
    }
}