﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathManager;

public partial class SpinnerBulletLauncher : MonoBehaviour
{
    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnBulletHitedTarget;

    internal delegate void PanelBangOnTarget(Transform target);
    internal event PanelBangOnTarget OnPanelBangOnTarget;

    private Vector3[] positions = new Vector3[]
    {
            new Vector3(0.0f, 0.0f, 2.0f),
            new Vector3(-1.75f, 0.0f, -1.0f),
            new Vector3(1.75f, 0.0f, -1.0f),
    };

    private List<GameObject> bullets = new List<GameObject>();

    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private ParticleSystem bulletEffect;

    [SerializeField]
    private ParticleSystem bulletHitEffect;

    [SerializeField]
    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }

    [SerializeField]
    private int bangDamage;
    internal int BangDamage { get { return bangDamage; } set { bangDamage = value; } }

    [SerializeField]
    private float bulletSpeed;

    internal float fixedBulletSpeed = 1.0f;

    [SerializeField]
    private float rotateSpeed;

    private float defaultRotateSpeed;

    internal Transform target;
    internal float maximumRange;

    private bool canBeLaunch;

    private GameObject rotatePanelOnTarget;

    private bool IsLaunchingAll;

    private int CountdownForBangTime;

}

public partial class SpinnerBulletLauncher : MonoBehaviour
{
    private void Start()
    {
        InitializeSolutionComponent();
    }

    private void InitializeSolutionComponent()
    {
        defaultRotateSpeed = rotateSpeed;

        for (int i = 0; i < positions.Length; i++)
        {
            InitializeBullet(i);
        }
    }

    private void InitializeBullet(int index)
    {
        Vector3 v3 = positions[index];
        GameObject instance = Instantiate(bullet, transform);
        if (bulletEffect)
        {
            ParticleSystem effect = Instantiate(bulletEffect, instance.transform);
            effect.Stop();
            effect.Play();
        }
        instance.transform.localPosition = v3;
        SpinnerBullet spinnerBullet = instance.GetComponent<SpinnerBullet>();
        spinnerBullet.bulletHitEffect = bulletHitEffect;
        spinnerBullet.index = index;
        spinnerBullet.speed = bulletSpeed * fixedBulletSpeed;
        bullets.Add(instance);
        Debug.Log("Totoal count " + bullets.Count);
    }

    private void FixedUpdate()
    {
        RotationLauncher();
        RotationRotatePanelOnTarget();
    }
}

public partial class SpinnerBulletLauncher : MonoBehaviour
{
    private void RotationLauncher()
    {
        if (transform.gameObject.activeInHierarchy == false) { return; }
        transform.Rotate(new Vector3(0.0f, -10 * Time.deltaTime * rotateSpeed, 0.0f));
    }

    private void RotationRotatePanelOnTarget()
    {
        if (rotatePanelOnTarget == null) { return; }

        if (rotatePanelOnTarget.gameObject.activeInHierarchy == false) { return; }

        if (rotatePanelOnTarget.transform.childCount < 3) { return; }

        rotatePanelOnTarget.transform.GetChild(0).Rotate(new Vector3(-10 * Time.deltaTime * rotateSpeed * 1.1f, -10 * Time.deltaTime * rotateSpeed * 1.1f, 0.0f));
        rotatePanelOnTarget.transform.GetChild(2).Rotate(new Vector3(-10 * Time.deltaTime * rotateSpeed * 1.2f, 10 * Time.deltaTime * rotateSpeed * 1.2f, 0.0f));
        rotatePanelOnTarget.transform.GetChild(1).Rotate(new Vector3(-10 * Time.deltaTime * rotateSpeed * 1.3f, 0.0f, 0.0f));
    }
}

public partial class SpinnerBulletLauncher : MonoBehaviour
{
    internal void LaunchingSingle()
    {
        IsLaunchingAll = false;

        if (bullets.Count == 0) { return; }

        if (!canBeLaunch)
        {
            canBeLaunch = true;

            //0 ,1 ,2
            GameObject bullet = bullets[0];
            bullet.GetComponent<SphereCollider>().enabled = true;
            bullet.transform.SetParent(null);

            SpinnerBullet spinnerBullet = bullet.GetComponent<SpinnerBullet>();
            spinnerBullet.Damage = Damage;
            spinnerBullet.target = target;
            spinnerBullet.maximumRange = maximumRange;
            spinnerBullet.OnHitedTarget += OnHitedTarget;
            spinnerBullet.OnDestoryBullet += OnDestoryBullet;
            spinnerBullet.IsLaunchingAll = IsLaunchingAll;
            spinnerBullet.LaunchSwitch = true;
        }
    }

    internal void LaunchingAll()
    {
        StartCoroutine(LaunchingAllEvent());
    }

    IEnumerator LaunchingAllEvent()
    {
        if (transform.childCount != 3) { yield break; }

        IsLaunchingAll = true;

        rotatePanelOnTarget = new GameObject("RotatePanelOnTarget");
        rotatePanelOnTarget.transform.SetParent(target);
        rotatePanelOnTarget.transform.localPosition = Vector3.zero;

        yield return new WaitUntil(() => rotatePanelOnTarget.activeInHierarchy);

        for (int i = 0; i < bullets.Count; i++)
        {
            Debug.Log(" i " + i);
            yield return new WaitForSeconds(0.25f * (i + 1));
            yield return new WaitUntil(() => canBeLaunch == false);
            if (!canBeLaunch)
            {
                canBeLaunch = true;

                GameObject bullet = bullets[i];

                bullet.transform.SetParent(null);
                bullet.GetComponent<SphereCollider>().enabled = true;

                SpinnerBullet spinnerBullet = bullet.GetComponent<SpinnerBullet>();
                spinnerBullet.Damage = Damage;
                spinnerBullet.target = target;
                spinnerBullet.maximumRange = maximumRange;
                spinnerBullet.OnHitedTarget += OnHitedTarget;
                spinnerBullet.OnDestoryBullet += OnDestoryBullet;
                spinnerBullet.IsLaunchingAll = IsLaunchingAll;
                spinnerBullet.LaunchSwitch = true;
            }
        }

        yield break;
    }
}

public partial class SpinnerBulletLauncher : MonoBehaviour
{
    private void OnDestoryBullet(int index)
    {
        if (IsLaunchingAll) { return; }

        StartCoroutine(ReBuildBullet(index));
    }

    IEnumerator ReBuildBullet(int index)
    {
        if (bullets.Count == 0) { yield break; }

        GameObject spinnerBullet = bullets.Find(bullet => bullet.GetComponent<SpinnerBullet>().index == index);
        spinnerBullet.GetComponent<SpinnerBullet>().OnHitedTarget -= OnHitedTarget;
        spinnerBullet.GetComponent<SpinnerBullet>().OnDestoryBullet -= OnDestoryBullet;
        bullets.Remove(spinnerBullet);

        if (bullets.Count == 3 || transform.childCount == 3) { yield break; }

        InitializeBullet(index);

        yield break;
    }

    private void OnHitedTarget(int bulletIndex, Collider collider)
    {
        Debug.Log("bullet hit to target " + bulletIndex);

        canBeLaunch = false;

        if (OnBulletHitedTarget != null) { OnBulletHitedTarget(collider); }

        if (IsLaunchingAll == false || rotatePanelOnTarget == null) { return; }

        //Debug.Log("Start Countdown For Bang!!!");

        StartCoroutine(PanelBangOnTargetEvent(bulletIndex));
    }

    IEnumerator PanelBangOnTargetEvent(int bulletIndex)
    {
        GameObject bullet = bullets.Find(b => b.GetComponent<SpinnerBullet>().index == bulletIndex);
        //GameObject bullet = bullets[bulletIndex];

        SpinnerBullet spinnerBullet = bullet.GetComponent<SpinnerBullet>();
        spinnerBullet.OnHitedTarget -= OnHitedTarget;
        spinnerBullet.OnDestoryBullet -= OnDestoryBullet;

        GameObject panelChild = new GameObject("PanelChild");
        panelChild.transform.SetParent(rotatePanelOnTarget.transform);
        panelChild.transform.localPosition = Vector3.zero;

        yield return new WaitUntil(() => panelChild != null);

        bullet.transform.SetParent(panelChild.transform);
        bullet.transform.localPosition = target.GetComponent<CharacterController>().center;

        yield return new WaitUntil(() => bullet.transform.parent == panelChild.transform);

        if (rotatePanelOnTarget.transform.childCount < 3) { yield break; }

        InvokeRepeating("CountdownForBang", 0, 1);

        yield break;
    }
}

public partial class SpinnerBulletLauncher : MonoBehaviour
{
    private void CountdownForBang()
    {
        if (CountdownForBangTime == 5)
        {
            for (int i = 0; i < positions.Length; i++)
            {
                GameObject bullet = bullets[i];
                bullet.GetComponent<SphereCollider>().enabled = false;
                bullet.transform.SetParent(transform);
                bullet.transform.localPosition = positions[i];
            }

            rotatePanelOnTarget.layer = LayerMask.NameToLayer(ProjectKeywordList.kLayerForAttackable);
            rotatePanelOnTarget.transform.SetParent(null);

            SpinnerBulletRotatePanelOnTarget spinnerBulletRotatePanelOnTarget = rotatePanelOnTarget.AddComponent<SpinnerBulletRotatePanelOnTarget>();
            spinnerBulletRotatePanelOnTarget.OnPanelBangHitedTarget += OnPanelBangHitedTarget;

            SphereCollider sphereCollider = rotatePanelOnTarget.AddComponent<SphereCollider>();
            sphereCollider.radius = target.GetComponent<CharacterController>().center.y;
            sphereCollider.isTrigger = true;
            sphereCollider.enabled = true;

            rotateSpeed = defaultRotateSpeed;
            CountdownForBangTime = 0;
            CancelInvoke();
            return;
        }

        rotateSpeed *= 1.5f;
        CountdownForBangTime++;
    }

    private void OnPanelBangHitedTarget(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable)) { return; }

        Transform target = other.gameObject.transform;

        HealthManager helethManager = target.GetComponent<HealthManager>();
        helethManager.Health -= BangDamage;

        Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

        FloatingTextControl.Initialize();
        FloatingTextControl.CreateFloatingText(BangDamage.ToString(), contactPoint);

        Destroy(rotatePanelOnTarget, 0.1f);

        if (OnPanelBangOnTarget != null) { OnPanelBangOnTarget(target); }
    }
}

public partial class SpinnerBulletLauncher : MonoBehaviour
{

}
