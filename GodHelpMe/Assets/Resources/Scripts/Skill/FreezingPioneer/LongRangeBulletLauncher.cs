﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class LongRangeBulletLauncher : MonoBehaviour
{

    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private ParticleSystem bulletEffect;

    [SerializeField]
    private ParticleSystem bulletHitEffect;

    [SerializeField]
    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }

    [SerializeField]
    private float bulletSpeed;

    [SerializeField]
    private float bulletDelayDisappear;

    internal bool showMinimark = true;
    
    // internal bool tirrgerStrengthenDamage;

    internal Vector3 launcherForward;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnLongRangeBulletHitedTarget;

    internal LongRangeBulletModel bulletModel = LongRangeBulletModel.Genernal; // Default model is Genernal.
}

public partial class LongRangeBulletLauncher : MonoBehaviour
{
    public void Launching()
    {
        GameObject instance = Instantiate(bullet, transform.position, transform.rotation);

        if (bulletEffect)
        {
            ParticleSystem effect = Instantiate(bulletEffect, instance.transform);
            effect.Stop();
            effect.Play();
        }

        LongRangeBullet longRangeBullet = instance.GetComponent<LongRangeBullet>();
        longRangeBullet.showMinimark = showMinimark;
        longRangeBullet.speed = bulletSpeed;
        longRangeBullet.Damage = Damage;
        longRangeBullet.launcherForward = launcherForward;
        longRangeBullet.bulletModel = bulletModel;
        longRangeBullet.OnHitedTarget += OnHitedTarget;

        Destroy(instance, bulletDelayDisappear);
    }
}

public partial class LongRangeBulletLauncher : MonoBehaviour
{
    private void OnHitedTarget(Collider other)
    {
        GameObject target = other.gameObject;

        if (bulletHitEffect)
        {
            ParticleSystem instance = Instantiate(bulletHitEffect);
            instance.transform.position = target.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            ParticleSystem[] ps_array = instance.GetComponentsInChildren<ParticleSystem>(true);
            ps_array[0].Stop();

            CharacterController character = target.transform.GetComponent<CharacterController>();

            float effectScale = character.height / 5.0f;

            foreach (ParticleSystem ps in ps_array)
            {
                var main = ps.main;
                var mainSize = main.startSize;

                mainSize.constantMin *= effectScale * 2;
                mainSize.constantMax *= effectScale * 2;
                main.startSize = mainSize;
            }

            //Scale Lights' range
            Light[] lights = instance.GetComponentsInChildren<Light>();
            foreach (Light light in lights)
            {
                light.range *= effectScale * 2;
                light.transform.localPosition *= effectScale * 2;
            }

            instance.transform.parent = null;
            ps_array[0].Play();
            Destroy(instance.gameObject, 0.5f);
        }

        if (OnLongRangeBulletHitedTarget != null)
        {
            OnLongRangeBulletHitedTarget(other);
        }
    }
}

public partial class LongRangeBulletLauncher : MonoBehaviour
{

}

public partial class LongRangeBulletLauncher : MonoBehaviour
{

}
