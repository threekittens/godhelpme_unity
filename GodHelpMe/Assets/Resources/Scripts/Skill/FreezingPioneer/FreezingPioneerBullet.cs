﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FreezingPioneerBulletModel
{
    Genernal = 0,
    Impacter = 1
}

public partial class FreezingPioneerBullet : MonoBehaviour
{
    internal int level = 1;
    internal bool IsTirrgerStrengthenDamage;

    internal float Speed = 10.0f;

    [SerializeField]
    internal int damage = 75;  // 最大损伤

    [SerializeField]
    internal float delayDisapper; // 延迟消失

    [SerializeField]
    internal float mSpeed;

    internal float mMaximumRange;
    internal Transform mTarget;
    private int mCountdownSecond = 0;

    internal delegate void HitTargetEvent(Collider other);
    internal delegate void AttackFinished();
    internal event HitTargetEvent OnHitedTarget;
    internal event AttackFinished OnAttackFinished;

    internal GameObject Launcher;
    private GameObject mLauncher;

    internal Vector3 LauncherForward;

    internal FreezingPioneerBulletModel BulletModel = FreezingPioneerBulletModel.Genernal; // Default model is Genernal.
}

public partial class FreezingPioneerBullet : MonoBehaviour
{

    private void OnEnable()
    {
        //if (Launcher)
        //{
        //    Debug.Log("Bullet On Enable");
        //    mLauncher = Launcher;
        //}
    }

    void FixedUpdate()
    {       
        //Debug.Log("02" + mLauncher);
        transform.GetComponent<Rigidbody>().velocity = LauncherForward * Speed;
        //mBulletInstance.GetComponent<Rigidbody>().AddForce(mLauncher.transform.forward * 20);
    }
}

public partial class FreezingPioneerBullet : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            FloatingTextControl.Initialize();
            Transform target = other.gameObject.transform;
            HealthManager helethManager = target.GetComponent<HealthManager>();

            if (IsTirrgerStrengthenDamage)
            {
                damage *= 2;
            }

            helethManager.Health -= (damage *= (level));
            Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);

            if (OnHitedTarget != null)
            {
                OnHitedTarget(other);
            }

            if (BulletModel == FreezingPioneerBulletModel.Impacter)
            {
                ImpactReceiver impactReceiver = other.gameObject.GetComponent<ImpactReceiver>();
                if (impactReceiver) impactReceiver.AddImpact(transform.forward, 3f);
            }            

            //Debug.Log(string.Format("gameobject => {0}", gameObject.name));               
        }
    }
}

public partial class FreezingPioneerBullet : MonoBehaviour
{

}

public partial class FreezingPioneerBullet : MonoBehaviour
{

}

public partial class FreezingPioneerBullet : MonoBehaviour
{

}

public partial class FreezingPioneerBullet : MonoBehaviour
{

}
