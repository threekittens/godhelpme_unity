﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LongRangeBulletModel
{
    Genernal = 0,
    Impacter = 1
}

public partial class LongRangeBullet : MonoBehaviour
{
    internal bool showMinimark = true;
    
    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }
    internal float speed;

    // internal bool tirrgerStrengthenDamage;

    internal Vector3 launcherForward;

    internal LongRangeBulletModel bulletModel = LongRangeBulletModel.Genernal; // Default model is Genernal.

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnHitedTarget;
}

public partial class LongRangeBullet : MonoBehaviour
{

    private void FixedUpdate()
    {
        transform.GetComponent<Rigidbody>().velocity = launcherForward * speed;
        Transform mark = transform.Find("MinimapPlayerMark/Image");
        if (showMinimark == false)
        {
            mark.gameObject.SetActive(false);
            return;
        }

        float zaxis = 135 - transform.rotation.eulerAngles.y;
        //Debug.Log(transform.rotation.eulerAngles.y);
        //Debug.Log(zaxis);
        mark.localRotation = Quaternion.Euler(0.0f, 0.0f, zaxis);
        //Debug.Log("rect " + mark.localRotation.eulerAngles);
    }
}

public partial class LongRangeBullet : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            Transform target = other.gameObject.transform;

            HealthManager helethManager = target.GetComponent<HealthManager>();
            helethManager.Health -= Damage;

            Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            FloatingTextControl.Initialize();
            FloatingTextControl.CreateFloatingText(Damage.ToString(), contactPoint);

            if (OnHitedTarget != null) { OnHitedTarget(other); }

            if (bulletModel == LongRangeBulletModel.Impacter)
            {
                ImpactReceiver impactReceiver = other.gameObject.GetComponent<ImpactReceiver>();
                if (impactReceiver) impactReceiver.AddImpact(transform.forward, 3f);
            }
        }
    }
}

public partial class LongRangeBullet : MonoBehaviour
{

}