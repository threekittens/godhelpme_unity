﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FreezingPioneerBulletLauncher : MonoBehaviour
{
    internal bool IsTirrgerStrengthenDamage;
    internal int level;
    internal float Speed;

    private GameObject mBullet;
    private GameObject mBulletInstance;

    internal Transform mTarget;
    internal float mMaximumRange;

    internal Vector3 LauncherForward;

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnBulletHitedTarget;

    internal FreezingPioneerBulletModel BulletModel = FreezingPioneerBulletModel.Genernal; // Default model is Genernal.
}

public partial class FreezingPioneerBulletLauncher : MonoBehaviour
{
    private void OnEnable()
    {
        if (mBullet == null)
        {
            //Debug.Log("FreezingPioneerBulletLauncher OnEnable.");
            string bulletPath = LoadResourceFilePath.PrefabSkill() + "FreezingPioneerBullet";
            mBullet = Resources.Load<GameObject>(bulletPath);
        }
    }
}

public partial class FreezingPioneerBulletLauncher : MonoBehaviour
{
    //public void LauncinghBullet()
    //{
    //    mBulletInstance = Instantiate(mBullet, transform.position, transform.rotation);
    //}

    public void Launching()
    {
        mBulletInstance = Instantiate(mBullet, transform.position, transform.rotation);
        FreezingPioneerBullet freezingPioneerBullet = mBulletInstance.GetComponent<FreezingPioneerBullet>();
        freezingPioneerBullet.Speed = Speed;
        freezingPioneerBullet.level = level;
        freezingPioneerBullet.mTarget = mTarget;
        freezingPioneerBullet.Launcher = gameObject;
        freezingPioneerBullet.IsTirrgerStrengthenDamage = IsTirrgerStrengthenDamage;
        freezingPioneerBullet.LauncherForward = LauncherForward;
        freezingPioneerBullet.mMaximumRange = mMaximumRange;
        freezingPioneerBullet.BulletModel = BulletModel;
        freezingPioneerBullet.OnHitedTarget += OnHitedTarget;
        freezingPioneerBullet.OnAttackFinished += OnAttackFinished;
        // Add velocity to the bullet
        // Destroy the bullet after 2 seconds
        //mBulletInstance.GetComponent<Rigidbody>().velocity = mBulletInstance.transform.forward * 100;
        Destroy(mBulletInstance, 1.0f);
    }
}

public partial class FreezingPioneerBulletLauncher : MonoBehaviour
{
    private void OnHitedTarget(Collider other)
    {
        gameObject.SetActive(false);

        if (OnBulletHitedTarget != null)
        {
            OnBulletHitedTarget(other);
        }
    }

    private void OnAttackFinished()
    {
        if (mBulletInstance != null)
        {
            Destroy(mBulletInstance);
        }
    }
}

public partial class FreezingPioneerBulletLauncher : MonoBehaviour
{

}

public partial class FreezingPioneerBulletLauncher : MonoBehaviour
{

}
