﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillCoolDown : MonoBehaviour {

    private int cdSecond;
    internal int CDSecond { get { return cdSecond; } set { cdSecond = value; } }
    private int resetCDSecond;
    private Text CDTimeText;
    private void Start()
    {
        CDTimeText = transform.GetChild(0).GetComponent<Text>();
        CDTimeText.text = CDSecond.ToString();        
    }

    private void OnEnable()
    {
        resetCDSecond = CDSecond;
        InvokeRepeating("CoolDown", 0, 1);
    }

    private void CoolDown()
    {
        if (CDSecond == 0)
        {
            gameObject.SetActive(false);
            ResetSecond();
            CancelInvoke();
            return;
        }
        CDSecond--;
        CDTimeText.text = CDSecond.ToString();
    }
    
    private void ResetSecond()
    {
        CDSecond = resetCDSecond;
    }
}
