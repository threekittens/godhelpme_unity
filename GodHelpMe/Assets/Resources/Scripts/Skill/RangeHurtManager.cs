﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class RangeHurtManager : MonoBehaviour
{

    [SerializeField]
    private float detectTargetRange;

    [SerializeField]
    public bool isDrawGizmos;

    [SerializeField]
    [Range(0, 100)]
    public int maxDamage = 75;  // 最大损伤

    [SerializeField]
    public RangeHurtManagerType managerType;

    [SerializeField]
    public int targetLayer;

    [SerializeField]
    public string targetTag;

    [SerializeField]
    public ParticleSystem attackEffect;

    [SerializeField]
    public Animation attackAnimation;

    [SerializeField]
    public string attackAnimationName;

    private ParticleSystem effect;

    public enum RangeHurtManagerType
    {
        Devil,
        Tower,
        Enemy,
        Player,
    }

    public void ExecuteRangeAttack()
    {
        InvokeRepeating("repeatAttack", 0, 1);
    }

    private void repeatAttack()
    {
        StartCoroutine(activateDetectedTarget());
    }

    IEnumerator activateDetectedTarget()
    {
        GameObject target = null;

        Collider[] colliders = Physics.OverlapSphere(transform.position, detectTargetRange, targetLayer);

        Debug.Log("colliders count " + colliders.Count());
        
        if (colliders.Length > 0)
        {

            attackAnimation.Play(attackAnimationName, PlayMode.StopAll);

            effect = Instantiate(attackEffect, transform.position, transform.rotation);
            effect.Play();

            Destroy(effect.gameObject, effect.main.duration + effect.main.startLifetime.constant);

            foreach (Collider collider in colliders)
            {
                if (collider.gameObject.tag == targetTag)
                {
                    try
                    {
                        // Do something that can throw an exception

                        //FloatingTextControl.Initialize();

                        target = collider.gameObject;

                        Debug.Log("target => " + target.name);

                        HealthManager helethManager = target.GetComponent<HealthManager>();
                        var damage = maxDamage;

                        helethManager.Health -= damage;

                        //FloatingTextControl.CreateFloatingText(damage.ToString(), enemy);
                        Debug.Log("Hit!!! Target!!");
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e, this);
                    }
                }
            }

        }
        else
        {
            if (attackAnimation.isPlaying)
            {
                attackAnimation.Stop(attackAnimationName);
            }           
            CancelInvoke("repeatAttack");            
        }

        if (target == null)
        {
            if (attackAnimation.isPlaying)
            {
                attackAnimation.Stop(attackAnimationName);
            }            
            CancelInvoke("repeatAttack");
        }

        yield return null;
    }

    //畫出範圍用的
    private void OnDrawGizmos()
    {
        if (isDrawGizmos)
        {
            Color c = Color.yellow;
            c.a = 0.5f;
            Gizmos.color = c;
            Gizmos.DrawSphere(transform.position, detectTargetRange);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(RangeHurtManager))]
public class RangeHurtManagerEditor : Editor
{
    RangeHurtManager manager;
    void OnEnable()
    {
        manager = (RangeHurtManager)target;
    }

    public override void OnInspectorGUI()
    {
        manager.attackAnimation = (Animation)EditorGUILayout.ObjectField("Animation", manager.attackAnimation, typeof(Animation), true);

        manager.attackAnimationName = EditorGUILayout.TextField("Animation Name", manager.attackAnimationName);

        manager.isDrawGizmos = EditorGUILayout.Toggle("Is Draw Gizmos", manager.isDrawGizmos);

        manager.attackEffect = (ParticleSystem)EditorGUILayout.ObjectField("Attack Effect", manager.attackEffect, typeof(ParticleSystem), true);

        manager.managerType = (RangeHurtManager.RangeHurtManagerType)EditorGUILayout.EnumPopup("RangeHurtManagerType", manager.managerType);
        switch (manager.managerType)
        {
            case RangeHurtManager.RangeHurtManagerType.Devil:
                {
                    manager.maxDamage = EditorGUILayout.IntSlider("manager.maxDamage", manager.maxDamage, 0, 10000);
                    manager.targetLayer = LayerMask.GetMask("ATTACKABLE");
                    manager.targetTag = "Player";
                    break;
                }
            case RangeHurtManager.RangeHurtManagerType.Enemy:
                {
                    manager.maxDamage = EditorGUILayout.IntSlider("manager.maxDamage", manager.maxDamage, 0, 100);
                    manager.targetLayer = LayerMask.GetMask("ATTACKABLE");
                    manager.targetTag = "Player";
                    break;
                }
            case RangeHurtManager.RangeHurtManagerType.Tower:
                {
                    manager.maxDamage = EditorGUILayout.IntSlider("manager.maxDamage", manager.maxDamage, 0, 1000);
                    manager.targetLayer = LayerMask.GetMask("ENEMY_ATTACKABLE");
                    manager.targetTag = "Enemy";
                    break;
                }
            case RangeHurtManager.RangeHurtManagerType.Player:
                {
                    manager.maxDamage = EditorGUILayout.IntSlider("manager.maxDamage", manager.maxDamage, 0, 100);
                    manager.targetLayer = LayerMask.GetMask("ENEMY_ATTACKABLE");
                    manager.targetTag = "Enemy";
                    break;
                }
        }
    }
}
#endif