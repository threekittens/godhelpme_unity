﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteAttack : MonoBehaviour
{

    [SerializeField]
    private float bulletSpeed = 10;

    [SerializeField]
    private Transform bulletSpawn;

    [SerializeField]
    private GameObject bulletPrefab;

    private GameObject bullet;

    public Transform target;

    public bool isShooting = false;

    private void OnDestroy()
    {
        if (bullet != null)
        {
            Destroy(bullet);
        }
    }

    private void Update()
    {

        if (isShooting == true)
        {
            Debug.Log("remote attack runing");
            if (bullet == null)
            {
                Debug.Log("Spawn Bullet");
                bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
            }
        }

        StartCoroutine(activateDetectedTarget());
    }

    IEnumerator activateDetectedTarget()
    {

        if (target == null)
        {
            if (bullet != null)
            {
                Debug.Log("Target doesn't exist Destroy Bullet");
                Destroy(bullet);
            }
        }

        yield return null;

        if (target != null)
        {

            float dis = Vector3.Distance(target.position, transform.position);

            if (dis > 6)
            {
                target = null;
                transform.LookAt(target);
            }
            else
            {
                Vector3 lookTargetDirection = target.position;
                transform.LookAt(lookTargetDirection);

                Transform healthBar = transform.GetChild(1).transform;
                healthBar.LookAt(healthBar.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
            }

            if (bullet != null && target != null)
            {
                //fire
                float step = bulletSpeed * Time.deltaTime;
                bullet.transform.position = Vector3.MoveTowards(bullet.transform.position, target.position, step);
            }

            yield return null;
        }
    }
}
