﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class Undertaker_Skills : MonoBehaviour
{
    [SerializeField] private ParticleSystem rangeHitEffect;

    [SerializeField] private ParticleSystem rangeDevelHandEffect;
    private ParticleSystem rangeDevelHandEffectInstance;

    [SerializeField] private GameObject bigGravestone;
    private GameObject bigGravestoneInstance;

    [SerializeField] private float gravestoneDropSpeed;

    private List<GameObject> targetListFor03;
    private List<GameObject> targetListFor04;
    private Transform target;
    private Vector3 lookTargetDirection;
    [SerializeField] private Animator animator;
    [SerializeField] private PlayerProperty PP;
    [SerializeField] private PowerManager powerManager;
    [SerializeField] private SpinnerBulletLauncher SBL;
    private const float keepTimeUnit = 0.25f;
    private float keepTime;
    private float keepMaxTime;
    private int rangeDamage;
    private int RangeDamage { get { return rangeDamage; } set { rangeDamage = value; } }
    private bool onTriggerSkill01 = false;
    private bool onTriggerSkill02 = false;
    private bool onTriggerSkill03 = false;
    private bool onTriggerSkill04 = false;

    [System.Serializable]
    private struct SkillCDList
    {
        public int CD01, CD02, CD03, CD04;
        public int CMP01, CMP02, CMP03, CMP04;
        public SkillCDList(int _CD01, int _CD02, int _CD03, int _CD04, int _CMP01, int _CMP02, int _CMP03, int _CMP04)
        {
            this.CD01 = _CD01;
            this.CD02 = _CD02;
            this.CD03 = _CD03;
            this.CD04 = _CD04;
            this.CMP01 = _CMP01;
            this.CMP02 = _CMP02;
            this.CMP03 = _CMP03;
            this.CMP04 = _CMP04;
        }
    }

    [SerializeField] private SkillCDList _SKCDList;
    private SkillCDList SKCDList
    {
        get
        {
            //_SKCDList.CD01 = 0;
            //_SKCDList.CD02 = 34;
            //_SKCDList.CD03 = 34;
            //_SKCDList.CD04 = 54;

            //_SKCDList.CMP01 = 0;
            //_SKCDList.CMP02 = 40;
            //_SKCDList.CMP03 = 50;
            //_SKCDList.CMP04 = 75;

            return _SKCDList;
        }
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    private void Start()
    {
        InitializeSkillComponent();
    }

    private void InitializeSkillComponent()
    {

    }

    private void Update()
    {

        if (target)
        {
            if (lookTargetDirection.magnitude > 0.1f)
            {
                transform.parent.LookAt(target.position);
                transform.parent.rotation = Quaternion.Euler(0.0f, transform.parent.rotation.eulerAngles.y, 0.0f);
            }

            lookTargetDirection = Vector3.Lerp(lookTargetDirection, Vector3.zero, 5 * Time.deltaTime);
        }

        if (bigGravestoneInstance == null) { return; }

        if (bigGravestoneInstance.transform.position.y <= -5.0)
        {
            DestroyImmediate(bigGravestoneInstance);
            return;
        }

        bigGravestoneInstance.transform.position += Vector3.down * Time.deltaTime * gravestoneDropSpeed;
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    public void CastSkill01(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[0].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD01;
            SJC.OnTriggerSkillCoolDown(CD);
        }
        if (!onTriggerSkill01) { StartCoroutine(Skill_01(HSP)); }
    }

    public void CastSkill02(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[1].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD02 - ((500 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_02(HSP));
    }

    public void CastSkill03(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[2].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD03 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_03(HSP));
    }

    public void CastSkill04(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[3].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD04 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_04(HSP));
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    IEnumerator Skill_01(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        //onTriggerSkill01 = true;

        animator.SetFloat(ProjectKeywordList.kAnimationForSkill01Speed, HSP.speed);
        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill01, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        int damage = PP.Damage * ((105 + (10 * (HSP.level - 1))) / 100);
        SBL.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        SBL.target = target;
        SBL.maximumRange = HSP.range;
        SBL.fixedBulletSpeed = HSP.speed;
        SBL.OnBulletHitedTarget += SBL_OnBulletHitedTargetFor01;
        SBL.LaunchingSingle();

        int CMP = SKCDList.CMP01;
        DiscountPowerBySkillKind(CMP);
    }

    private void SBL_OnBulletHitedTargetFor01(Collider other)
    {
        SBL.OnBulletHitedTarget -= SBL_OnBulletHitedTargetFor01;
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
        }
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    IEnumerator Skill_02(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill02, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        int damage = PP.Damage * ((35 + (5 * (HSP.level - 1))) / 100);
        SBL.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        int bangDamage = PP.Damage * ((130 + (20 * (HSP.level - 1))) / 100);
        SBL.BangDamage = (PP.IsTirrgerStrengthenDamage) ? (bangDamage * 2) : bangDamage;
        SBL.target = target;
        SBL.maximumRange = HSP.range;
        SBL.fixedBulletSpeed = HSP.speed;
        SBL.OnPanelBangOnTarget += SBL_OnPanelBangOnTargetFor02;
        SBL.LaunchingAll();

        int CMP = SKCDList.CMP02 + ((3000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void SBL_OnPanelBangOnTargetFor02(Transform _target)
    {
        SBL.OnPanelBangOnTarget -= SBL_OnPanelBangOnTargetFor02;
        if (_target.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = _target.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
        }
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    IEnumerator Skill_03(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill03, true));

        List<GameObject> oderlist = HSP.targets;
        oderlist.OrderBy(g => Vector3.Distance(transform.position, g.transform.position));

        target = oderlist[oderlist.Count / 2].transform;

        if (oderlist.Count % 2 == 1)
        {
            // if length is odd, return middle numer
            target = oderlist[oderlist.Count / 2].transform;
        }
        else
        {
            // if length is even return the first of the two middle numbers
            target = oderlist[oderlist.Count / 2 - 1].transform;
        }

        lookTargetDirection = HSP.forwardEndPos;

        if (rangeDevelHandEffect)
        {
            rangeDevelHandEffectInstance = Instantiate(rangeDevelHandEffect);
            rangeDevelHandEffectInstance.transform.localPosition = target.position;

            ParticleSystem[] ps_array = rangeDevelHandEffectInstance.GetComponentsInChildren<ParticleSystem>(true);
            ps_array[0].Stop();

            CharacterController character = target.transform.GetComponent<CharacterController>();

            foreach (ParticleSystem ps in ps_array)
            {
                var main = ps.main;
                var mainSize = main.startSize;

                mainSize.constantMin *= 2;
                mainSize.constantMax *= 2;
                main.startSize = mainSize;

                var shape = ps.shape;
                shape.radius *= 2;
            }

            //Scale Lights' range
            Light[] lights = rangeDevelHandEffectInstance.GetComponentsInChildren<Light>();
            foreach (Light light in lights)
            {
                light.range *= 2;
                light.transform.localPosition *= 2;
            }

            rangeDevelHandEffectInstance.transform.parent = null;
            ps_array[0].Play();

        }

        targetListFor03 = HSP.targets;

        int rd = PP.Damage * ((140 + (15 * (HSP.level - 1))) / 100);
        RangeDamage = (PP.IsTirrgerStrengthenDamage) ? (rd * 2) : rd;

        float kt = 4 + ((25 / 100) * (HSP.level - 1));
        keepMaxTime = kt;

        InvokeRepeating("Skill03RangeAttack", 0.0f, keepTimeUnit);

        int CMP = SKCDList.CMP03 + ((4000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void Skill03RangeAttack()
    {
        if (targetListFor03.Count > 0)
        {
            // 获取 已锁定的 enemy 并取得 position 
            foreach (GameObject target in targetListFor03)
            {
                HealthManager healthManager = target.GetComponent<HealthManager>();
                healthManager.OnListeningAttacker(transform.parent.gameObject);
                healthManager.Health -= RangeDamage;

                Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.parent.position);

                FloatingTextControl.Initialize();
                FloatingTextControl.CreateFloatingText(RangeDamage.ToString(), contactPoint);

                if (rangeHitEffect)
                {
                    ParticleSystem instance = Instantiate(rangeHitEffect);
                    instance.transform.position = target.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

                    ParticleSystem[] ps_array = instance.GetComponentsInChildren<ParticleSystem>(true);
                    ps_array[0].Stop();

                    CharacterController character = target.transform.GetComponent<CharacterController>();

                    float effectScale = character.height / 5.0f;

                    foreach (ParticleSystem ps in ps_array)
                    {
                        var main = ps.main;
                        var mainSize = main.startSize;

                        mainSize.constantMin *= effectScale * 2;
                        mainSize.constantMax *= effectScale * 2;
                        main.startSize = mainSize;
                    }

                    //Scale Lights' range
                    Light[] lights = instance.GetComponentsInChildren<Light>();
                    foreach (Light light in lights)
                    {
                        light.range *= effectScale * 2;
                        light.transform.localPosition *= effectScale * 2;
                    }

                    instance.transform.parent = null;
                    ps_array[0].Play();
                    Destroy(instance.gameObject, 0.5f);
                }
            }
        }

        if (keepTime == keepMaxTime)
        {
            CancelInvoke();
            keepTime = 0.0f;
            targetListFor03.Clear();
            rangeDevelHandEffectInstance.Stop();
            rangeDevelHandEffectInstance.Clear();
            DestroyImmediate(rangeDevelHandEffectInstance.gameObject);
        }

        keepTime += keepTimeUnit;
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    IEnumerator Skill_04(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill04, true));

        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName(ProjectKeywordList.kAnimationForSkill04));

        GameObject gravestone = transform.parent.Find("006_UnderTaker").transform.Find("Gravestone").gameObject;

        gravestone.SetActive(false);

        yield return new WaitUntil(() => gravestone.activeInHierarchy == false);

        List<GameObject> oderlist = HSP.targets;
        oderlist.OrderBy(g => Vector3.Distance(transform.position, g.transform.position));

        target = oderlist[oderlist.Count / 2].transform;

        if (oderlist.Count % 2 == 1)
        {
            // if length is odd, return middle numer
            target = oderlist[oderlist.Count / 2].transform;
        }
        else
        {
            // if length is even return the first of the two middle numbers
            target = oderlist[oderlist.Count / 2 - 1].transform;
        }

        if (bigGravestone)
        {
            bigGravestoneInstance = Instantiate(bigGravestone);
            bigGravestoneInstance.transform.localPosition = target.position + Vector3.up * 10.0f;
        }

        lookTargetDirection = HSP.forwardEndPos;

        yield return new WaitUntil(() => bigGravestoneInstance.transform.position.y <= -5.0);

        targetListFor04 = HSP.targets;

        int rd = PP.Damage * ((290 + (40 * (HSP.level - 1))) / 100);
        int damage = (PP.IsTirrgerStrengthenDamage) ? (rd * 2) : rd;

        Skill04RangeAttack(damage);

        int CMP = SKCDList.CMP04 + ((5000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        gravestone.SetActive(true);

        yield break;
    }

    private void Skill04RangeAttack(int damage)
    {
        if (targetListFor04.Count > 0)
        {
            // 获取 已锁定的 enemy 并取得 position 
            foreach (GameObject target in targetListFor04)
            {
                HealthManager healthManager = target.GetComponent<HealthManager>();
                healthManager.OnListeningAttacker(transform.parent.gameObject);
                healthManager.Health -= damage;

                Vector3 contactPoint = target.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.parent.position);

                FloatingTextControl.Initialize();
                FloatingTextControl.CreateFloatingText(damage.ToString(), contactPoint);

                if (rangeHitEffect)
                {
                    ParticleSystem instance = Instantiate(rangeHitEffect);
                    instance.transform.position = target.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

                    ParticleSystem[] ps_array = instance.GetComponentsInChildren<ParticleSystem>(true);
                    ps_array[0].Stop();

                    CharacterController character = target.transform.GetComponent<CharacterController>();

                    float effectScale = character.height / 5.0f;

                    foreach (ParticleSystem ps in ps_array)
                    {
                        var main = ps.main;
                        var mainSize = main.startSize;

                        mainSize.constantMin *= effectScale * 2;
                        mainSize.constantMax *= effectScale * 2;
                        main.startSize = mainSize;
                    }

                    //Scale Lights' range
                    Light[] lights = instance.GetComponentsInChildren<Light>();
                    foreach (Light light in lights)
                    {
                        light.range *= effectScale * 2;
                        light.transform.localPosition *= effectScale * 2;
                    }

                    instance.transform.parent = null;
                    ps_array[0].Play();
                    Destroy(instance.gameObject, 0.5f);
                }
            }

            targetListFor04.Clear();
        }
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    private void DiscountPowerBySkillKind(int power)
    {
        powerManager.Power -= power;
    }
}

public partial class Undertaker_Skills : MonoBehaviour
{
    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    IEnumerator PalyingAnimation(string name, bool isPlay)
    {
        if (animator)
        {
            animator.SetBool(name, isPlay);
        }
        yield break;
    }
}
