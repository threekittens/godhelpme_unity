﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class HeroSkillProperty
{
    public int level = 1;
    public float speed = 1.0f;
    // public float power;
    public List<GameObject> targets;
    public float range;
    public Vector3 forwardEndPos;
    public HeroSkillProperty() { }
}

[System.Serializable]
public class HeroSkillEvent : UnityEvent<HeroSkillProperty, object> { }

public partial class SkillController : MonoBehaviour
{
    [SerializeField] private int skill01Level = 1;
    [SerializeField] [Range(1.0f, 10.0f)] private float skill01Speed = 1.0f;
    public HeroSkillEvent skill01;

    [SerializeField] private int skill02Level = 1;
    [SerializeField] [Range(1.0f, 10.0f)] private float skill02Speed = 1.0f;
    public HeroSkillEvent skill02;

    [SerializeField] private int skill03Level = 1;
    [SerializeField] [Range(1.0f, 10.0f)] private float skill03Speed = 1.0f;
    public HeroSkillEvent skill03;

    [SerializeField] private int skill04Level = 1;
    [SerializeField] [Range(1.0f, 10.0f)] private float skill04Speed = 1.0f;
    public HeroSkillEvent skill04;

    private GameObject skillJoystickPanel;
    private GameObject[] skillJoysticks = new GameObject[4];
    internal GameObject[] SkillJoysticks { get { return skillJoysticks; } }
    private GameObject[] skillJoystickImages = new GameObject[4];
}

public partial class SkillController : MonoBehaviour
{
    private void Start()
    {
        InitializeSkillComponent();
    }

    private void InitializeSkillComponent()
    {
        skillJoystickPanel = GameObject.Find("USERINTERFACEMANAGER/SkillsJoystickPanel");
        for (int i = 0; i < skillJoystickPanel.transform.childCount; i++)
        {
            skillJoysticks[i] = skillJoystickPanel.transform.GetChild(i).gameObject;
            SkillJoystickController skillJoystickController = skillJoysticks[i].GetComponent<SkillJoystickController>();
            skillJoystickController.OnTriggerUpSkillLevel += OnTriggerUpSkillLevel;

            skillJoystickImages[i] = skillJoysticks[i].transform.GetChild(0).gameObject;
            DetectEnemy detectEnemy = skillJoystickImages[i].GetComponent<DetectEnemy>();
            detectEnemy.OnDetectedEnemy += OnDetectEnemy;
        }
    }

    private void OnDestroy()
    {
        if (skillJoystickPanel == null) { return; }
        for (int i = 0; i < skillJoystickPanel.transform.childCount; i++)
        {
            skillJoysticks[i] = skillJoystickPanel.transform.GetChild(i).gameObject;
            SkillJoystickController skillJoystickController = skillJoysticks[i].GetComponent<SkillJoystickController>();
            skillJoystickController.OnTriggerUpSkillLevel -= OnTriggerUpSkillLevel;

            skillJoystickImages[i] = skillJoysticks[i].transform.GetChild(0).gameObject;
            DetectEnemy detectEnemy = skillJoystickImages[i].GetComponent<DetectEnemy>();
            detectEnemy.OnDetectedEnemy -= OnDetectEnemy;
        }
    }

    private void OnTriggerUpSkillLevel(SkillKind skillKind, int level)
    {
        Debug.Log(string.Format("{0}, {1}", skillKind, level));
        switch (skillKind)
        {
            case SkillKind.Skill01:
                skill01Level = level;
                break;
            case SkillKind.Skill02:
                skill02Level = level;
                break;
            case SkillKind.Skill03:
                skill03Level = level;
                break;
            case SkillKind.Skill04:
                skill04Level = level;
                break;
        }
    }

    private void OnDetectEnemy(DetectEnenmyProperty detectEnenmyProperty)
    {
        switch (detectEnenmyProperty.skillType)
        {
            case SkillType.skill01:
                StartCoroutine(Skill_01(detectEnenmyProperty));
                break;
            case SkillType.skill02:
                StartCoroutine(Skill_02(detectEnenmyProperty));
                break;
            case SkillType.skill03:
                StartCoroutine(Skill_03(detectEnenmyProperty));
                break;
            case SkillType.skill04:
                StartCoroutine(Skill_04(detectEnenmyProperty));
                break;
        }

        // int index = (int)detectEnenmyProperty.skillType;
        // skillJoysticks[index] = skillJoystickPanel.transform.GetChild(index).gameObject;
        // SkillJoystickController skillJoystickController = skillJoysticks[index].GetComponent<SkillJoystickController>();
        // skillJoystickController.OnTriggerSkillCoolDown(index);
    }

    IEnumerator Skill_01(DetectEnenmyProperty detectEnenmyProperty)
    {
        HeroSkillProperty skillProperty = new HeroSkillProperty();
        skillProperty.level = skill01Level;
        skillProperty.speed = skill01Speed;
        skillProperty.targets = detectEnenmyProperty.targets;
        skillProperty.range = detectEnenmyProperty.range;
        skillProperty.forwardEndPos = detectEnenmyProperty.forwardEndPos;

        skill01.Invoke(skillProperty, this);
        yield break;
    }

    IEnumerator Skill_02(DetectEnenmyProperty detectEnenmyProperty)
    {
        HeroSkillProperty skillProperty = new HeroSkillProperty();
        skillProperty.level = skill02Level;
        skillProperty.speed = skill02Speed;
        skillProperty.targets = detectEnenmyProperty.targets;
        skillProperty.range = detectEnenmyProperty.range;
        skillProperty.forwardEndPos = detectEnenmyProperty.forwardEndPos;

        skill02.Invoke(skillProperty, this);
        yield break;
    }

    IEnumerator Skill_03(DetectEnenmyProperty detectEnenmyProperty)
    {
        HeroSkillProperty skillProperty = new HeroSkillProperty();
        skillProperty.level = skill03Level;
        skillProperty.speed = skill03Speed;
        skillProperty.targets = detectEnenmyProperty.targets;
        skillProperty.range = detectEnenmyProperty.range;
        skillProperty.forwardEndPos = detectEnenmyProperty.forwardEndPos;

        skill03.Invoke(skillProperty, this);
        yield break;
    }

    IEnumerator Skill_04(DetectEnenmyProperty detectEnenmyProperty)
    {
        HeroSkillProperty skillProperty = new HeroSkillProperty();
        skillProperty.level = skill04Level;
        skillProperty.speed = skill04Speed;
        skillProperty.targets = detectEnenmyProperty.targets;
        skillProperty.range = detectEnenmyProperty.range;
        skillProperty.forwardEndPos = detectEnenmyProperty.forwardEndPos;

        skill04.Invoke(skillProperty, this);
        yield break;
    }
}

public partial class SkillController : MonoBehaviour
{

}

public partial class SkillController : MonoBehaviour
{

}
