﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerBulletRotatePanelOnTarget : MonoBehaviour
{

    internal delegate void HitTargetEvent(Collider other);
    internal event HitTargetEvent OnPanelBangHitedTarget;

    //internal int level;
    //internal bool IsTirrgerStrengthenDamage;
    //internal float damage = 100.0f;  // 最大损伤

    private void OnTriggerEnter(Collider other)
    {

        if (OnPanelBangHitedTarget != null)
        {
            OnPanelBangHitedTarget(other);
        }
            
    }
}
