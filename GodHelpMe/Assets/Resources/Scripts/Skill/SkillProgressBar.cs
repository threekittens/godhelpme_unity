﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillProgressBar : MonoBehaviour
{

    private GameObject outward;
    private Image inner;
    private float progress = 100.0f;
    private float unit = 100.0f;

    private float keepTime;
    internal float KeepTime { get { return keepTime; } set { keepTime = value; } }

    internal delegate void SkillProgressBarDidStarted(float parogress);
    internal delegate void SkillProgressBarDidStop(float parogress);
    internal delegate void SkillProgressBarOnDisable();

    internal SkillProgressBarDidStarted OnSkillProgressBarDidStarted;
    internal SkillProgressBarDidStop OnSkillProgressBarDidStop;
    internal SkillProgressBarOnDisable OnSkillProgressBarOnDisable;

    private void OnEnable()
    {
        outward = transform.GetChild(0).gameObject;
        inner = outward.transform.GetChild(0).gameObject.GetComponent<Image>();
        inner.type = Image.Type.Filled;
        inner.fillMethod = Image.FillMethod.Horizontal;
        inner.fillOrigin = (int)Image.OriginHorizontal.Left;
        InvokeRepeating("Progressing", 0.0f, (1.0f / unit) * KeepTime);
    }

    private void OnDisable()
    {
        if (OnSkillProgressBarOnDisable != null)
        {
            OnSkillProgressBarOnDisable();
        }

        CancelInvoke();
    }

    private void Update()
    {

    }

    private void Progressing()
    {
        if (inner.fillAmount == 0.0f)
        {
            StopBarProgressing();
        }

        float currentFillAmount = (progress / unit);
        inner.fillAmount = currentFillAmount;
        if (OnSkillProgressBarDidStarted != null)
        {
            OnSkillProgressBarDidStarted(currentFillAmount);
        }
        progress--;
    }

    private void StopBarProgressing()
    {
        //Debug.Log("StopBarProgressing");
        float currentFillAmount = inner.fillAmount;
        if (OnSkillProgressBarDidStop != null)
        {
            OnSkillProgressBarDidStop(currentFillAmount);
        }
        progress = 1.0f;
        gameObject.SetActive(false);
    }
}