﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Win_Soul_Skills : MonoBehaviour
{
    [SerializeField] private SimpleBulletLauncher SBL;
    [SerializeField] private LongRangeBulletLauncher LRBL;
    [SerializeField] private Impactor impactor;
    [SerializeField] private ParticleSystem moveEffect;

    private Transform target;
    [SerializeField] private Animator animator;
    private float maximumRange;

    private Vector3 lookTargetDirection;
    [SerializeField] private PlayerProperty PP;

    [SerializeField] private PowerManager powerManager;

    private bool onTriggerSkill01 = false;
    private bool onTriggerSkill02 = false;
    private bool onTriggerSkill03 = false;
    private bool onTriggerSkill04 = false;

    [System.Serializable]
    private struct SkillCDList
    {
        public int CD01, CD02, CD03, CD04;
        public int CMP01, CMP02, CMP03, CMP04;
        public SkillCDList(int _CD01, int _CD02, int _CD03, int _CD04, int _CMP01, int _CMP02, int _CMP03, int _CMP04)
        {
            this.CD01 = _CD01;
            this.CD02 = _CD02;
            this.CD03 = _CD03;
            this.CD04 = _CD04;
            this.CMP01 = _CMP01;
            this.CMP02 = _CMP02;
            this.CMP03 = _CMP03;
            this.CMP04 = _CMP04;
        }
    }

    [SerializeField] private SkillCDList _SKCDList;
    private SkillCDList SKCDList
    {
        get
        {
            //_SKCDList.CD01 = 0;
            //_SKCDList.CD02 = 34;
            //_SKCDList.CD03 = 34;
            //_SKCDList.CD04 = 54;

            //_SKCDList.CMP01 = 0;
            //_SKCDList.CMP02 = 40;
            //_SKCDList.CMP03 = 50;
            //_SKCDList.CMP04 = 75;

            return _SKCDList;
        }
    }

}

public partial class Win_Soul_Skills : MonoBehaviour
{
    private void Start()
    {
        InitializeSkillComponent();
    }

    private void InitializeSkillComponent()
    {

    }

    private void Update()
    {
        if (target)
        {
            if (lookTargetDirection.magnitude > 0.1f)
            {
                transform.parent.LookAt(target.position);
                transform.parent.rotation = Quaternion.Euler(0.0f, transform.parent.rotation.eulerAngles.y, 0.0f);
            }

            lookTargetDirection = Vector3.Lerp(lookTargetDirection, Vector3.zero, 5 * Time.deltaTime);
        }
        else
        {
            onTriggerSkill01 = false;
        }
    }
}

public partial class Win_Soul_Skills : MonoBehaviour
{
    public void CastSkill01(HeroSkillProperty skillProperty, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[0].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD01;
            SJC.OnTriggerSkillCoolDown(CD);
        }
        if (!onTriggerSkill01) { StartCoroutine(Skill_01(skillProperty)); }
    }

    public void CastSkill02(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[1].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD02 - ((500 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_02(HSP));
    }

    public void CastSkill03(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[2].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD03 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_03(HSP));
    }

    public void CastSkill04(HeroSkillProperty HSP, object obj)
    {
        if (obj as SkillController)
        {
            SkillController SC = (SkillController)obj;
            SkillJoystickController SJC = SC.SkillJoysticks[3].GetComponent<SkillJoystickController>();
            int CD = SKCDList.CD04 - ((100 / 100) * (HSP.level - 1));
            SJC.OnTriggerSkillCoolDown(CD);
        }
        StartCoroutine(Skill_04(HSP));
    }
}

public partial class Win_Soul_Skills : MonoBehaviour
{
    IEnumerator Skill_01(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        onTriggerSkill01 = true;

        animator.SetFloat(ProjectKeywordList.kAnimationForSkill01Speed, HSP.speed);
        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill01, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        int damage = PP.Damage * ((125 + (10 * (HSP.level - 1))) / 100);
        SBL.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        SBL.target = target;
        SBL.maximumRange = HSP.range;
        SBL.fixedBulletSpeed = HSP.speed;
        SBL.OnSimpleBulletHitedTarget += SBL_OnSimpleBulletHitedTarget;
        SBL.LauncinghBullet();

        int CMP = SKCDList.CMP01;
        DiscountPowerBySkillKind(CMP);
        yield break;
    }

    private void SBL_OnSimpleBulletHitedTarget(Collider other)
    {
        SBL.OnSimpleBulletHitedTarget -= SBL_OnSimpleBulletHitedTarget;
        onTriggerSkill01 = false;
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
        }
    }
}

public partial class Win_Soul_Skills : MonoBehaviour
{
    IEnumerator Skill_02(HeroSkillProperty HSP)
    {
        ParticleSystem effect = null;
        if (moveEffect)
        {
            effect = Instantiate(moveEffect, transform);
            effect.transform.localScale *= 2;
            effect.Stop();
            effect.Play();
        }

        animator.SetFloat(ProjectKeywordList.kAnimationForMoveSpeed, 2);
        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill03, true));

        lookTargetDirection = HSP.forwardEndPos;

        Motor motor = transform.parent.GetComponent<Motor>();

        float speed = ((140 + (2.5f * (HSP.level - 1))) / 100);
        motor.moveSpeed *= speed;

        int CMP = SKCDList.CMP03 + ((2500 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        int keepTime = 6 + (600 / 100) * (HSP.level - 1);
        yield return new WaitForSeconds(keepTime);

        effect.Stop();
        DestroyImmediate(effect.gameObject);
        animator.SetFloat(ProjectKeywordList.kAnimationForMoveSpeed, 1);
        motor.moveSpeed /= speed;

        yield break;
    }
}

public partial class Win_Soul_Skills : MonoBehaviour
{
    IEnumerator Skill_03(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill02, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        int damage = PP.Damage * ((150 + (25 * (HSP.level - 1))) / 100);
        impactor.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        impactor.OnImpactorHitedTarget += Impactor_OnImpactorHitedTarget;

        impactor.gameObject.SetActive(true);

        impactor.CastImptact(HSP.forwardEndPos);

        int CMP = SKCDList.CMP02 + ((2000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void Impactor_OnImpactorHitedTarget(Collider other)
    {
        impactor.OnImpactorHitedTarget -= Impactor_OnImpactorHitedTarget;
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            impactor.gameObject.SetActive(false);

            ImpactReceiver impactReceiver = other.gameObject.GetComponent<ImpactReceiver>();
            if (impactReceiver) impactReceiver.AddImpact(transform.parent.forward, 10);
            lookTargetDirection = Vector3.zero;

            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);

            Debug.Log(string.Format("OnHitedTarget"));
        }
    }
}

public partial class Win_Soul_Skills : MonoBehaviour
{
    IEnumerator Skill_04(HeroSkillProperty HSP)
    {
        if (HSP.targets == null) { yield break; }

        if (HSP.targets.Count <= 0) { yield break; }

        yield return StartCoroutine(PalyingAnimation(ProjectKeywordList.kAnimationForSkill04, true));

        target = HSP.targets[0].transform;
        lookTargetDirection = HSP.forwardEndPos;

        if (target != null)
        {
            transform.parent.LookAt(target.position);
        }


        int damage = PP.Damage * ((340 + (40 * (HSP.level - 1))) / 100);
        LRBL.Damage = (PP.IsTirrgerStrengthenDamage) ? (damage * 2) : damage;
        LRBL.launcherForward = new Vector3(transform.parent.forward.x, 0.0f, transform.parent.forward.z);
        LRBL.bulletModel = LongRangeBulletModel.Genernal;
        LRBL.OnLongRangeBulletHitedTarget += LRBL_OnLongRangeBulletHitedTarget;
        LRBL.Launching();

        int CMP = SKCDList.CMP04 + ((3000 / 100) * (HSP.level - 1));
        DiscountPowerBySkillKind(CMP);

        yield break;
    }

    private void LRBL_OnLongRangeBulletHitedTarget(Collider other)
    {
        LRBL.OnLongRangeBulletHitedTarget -= LRBL_OnLongRangeBulletHitedTarget;
        if (other.gameObject.layer == LayerMask.NameToLayer(ProjectKeywordList.kLayerForEnemyAttackable))
        {
            HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
            healthManager.OnListeningAttacker(transform.parent.gameObject);
        }
    }
}

public partial class Win_Soul_Skills : MonoBehaviour
{
    private void DiscountPowerBySkillKind(int power)
    {
        powerManager.Power -= power;
    }
}

public partial class Win_Soul_Skills : MonoBehaviour
{
    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    IEnumerator PalyingAnimation(string name, bool isPlay)
    {
        if (animator) { animator.SetBool(name, isPlay); }
        yield break;
    }
}
