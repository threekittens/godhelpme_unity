﻿
using UnityEngine;

[System.Serializable]
public class HeroMultipleSkillDatas
{
    public string heroID;
    public SingleSkillData[] skillDatas;
}

[System.Serializable]
public class SingleSkillData
{
    public string id;
    public int skillType;

    public int areaType;

    public float outerRadius;
    public float innerRadius;
    public float cubeWidth;

    public int angle;

    public float detectRange;
    public int maximumTargets;
}
