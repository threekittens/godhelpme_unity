﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PathManager;
using UnityEngine.SceneManagement;


// The partial is define parameter.
public partial class HeroIntroduceSceneUIManager : MonoBehaviour
{
    private float buttonBoardWidth;
    private float betweenSizeOfButton = 7.0f;
    private TextAsset[] heroDatas;
}


// The partial is for Script start.
public partial class HeroIntroduceSceneUIManager : MonoBehaviour
{
    private void Start()
    {
        BuildHeroButtonOnBoard();
    }

    private void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }
}


public partial class HeroIntroduceSceneUIManager : MonoBehaviour
{
    private void BuildHeroButtonOnBoard()
    {
        GameObject buttonBoard = transform.Find("HeroList/ButtonBoard").gameObject;
        GameObject obj = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceHeroIntroduceScene("HeroIntroduceButton"));

        heroDatas = Resources.LoadAll<TextAsset>("Data/Heros");

        foreach (TextAsset JSONFile in heroDatas)
        {
            string JSON = JSONFile.text;
            HeroData heroData = JsonUtility.FromJson<HeroData>(JSON);

            GameObject heroIntroduceButton = Instantiate(obj, buttonBoard.transform);

            Button button = heroIntroduceButton.GetComponent<Button>();
            button.onClick.AddListener(() => TouchHeroButtonEvnet(heroData.id));

            Sprite heroSprite = Resources.Load<Sprite>(heroData.coverPath);
            button.image.sprite = heroSprite;

            Text heroName = heroIntroduceButton.transform.Find("Text").GetComponent<Text>();
            heroName.text = heroData.name;

            RectTransform rectTransform = heroIntroduceButton.GetComponent<RectTransform>();
            buttonBoardWidth += rectTransform.rect.width;

            //总宽加缝隙宽度
            buttonBoardWidth += betweenSizeOfButton;
        }

        //缝隙总数 = 按钮总数 - 1
        buttonBoardWidth -= betweenSizeOfButton;

        RectTransform buttonBoardRectTransform = buttonBoard.GetComponent<RectTransform>();
        buttonBoardRectTransform.sizeDelta = new Vector2(buttonBoardWidth, buttonBoardRectTransform.rect.height);
    }
}

public partial class HeroIntroduceSceneUIManager : MonoBehaviour
{
    private void TouchHeroButtonEvnet(string heroID)
    {
        SEManager.Singleton.PlayOpenBookSE();
        Debug.Log("hero ID => " + heroID);
        if (PlayerAttributeManager.Singleton) { PlayerAttributeManager.Singleton.ChoosedHeroID = heroID; }
        StartCoroutine(LoadAsyncScene(ProjectKeywordList.kSceneForHeroSkillIntroduce));
    }
}


public partial class HeroIntroduceSceneUIManager : MonoBehaviour
{
    public void UnLoadingScene()
    {
        SEManager.Singleton.PlayCancelSE();
        StartCoroutine(LoadAsyncScene(PlayerAttributeManager.Singleton.previousSceneName));
    }
}


public partial class HeroIntroduceSceneUIManager : MonoBehaviour
{
    private IEnumerator LoadAsyncScene(string name)
    {
        SceneManager.LoadSceneAsync(name);
        yield return null;
    }
}