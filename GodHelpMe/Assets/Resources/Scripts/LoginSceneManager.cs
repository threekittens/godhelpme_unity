﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public partial class LoginSceneManager : MonoBehaviour
{
    private void Start()
    {
        
    }

    // 加入Master后返回
    private void OnMasterConnected(object sender, System.EventArgs e)
    {
        Debug.Log("We are connected to Master");
    }

    private void Destroy()
    {
        
    }
}

public partial class LoginSceneManager : MonoBehaviour
{
    private void ActivityToLoadScene(int index)
    {
        StartCoroutine(LoadAsyncScene(index));
    }

    private IEnumerator LoadAsyncScene(int index)
    {
        SceneManager.LoadSceneAsync(index);
        yield return null;
    }
}

public partial class LoginSceneManager : MonoBehaviour
{

}