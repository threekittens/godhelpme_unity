﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseReincarnationToolAlertor : MonoBehaviour
{

    public static event System.EventHandler OnDetermined;
    public static event System.EventHandler OnCanceled;
    public static event System.EventHandler OnClose;

    private static GameObject useReincarnationToolAlertor;

    public static void ShowOn(Transform USERINTERFACEMANAGER)
    {
        GameObject instance = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterfaceCharacterSelectionScene("UseReincarnationToolAlertor"));
        useReincarnationToolAlertor = Instantiate(instance, USERINTERFACEMANAGER);

        Button determineButton = useReincarnationToolAlertor.transform.Find("Background/Content/DetermineButton").GetComponent<Button>();
        determineButton.onClick.AddListener(() => Determine());

        Button cancelButton = useReincarnationToolAlertor.transform.Find("Background/Content/CancelButton").GetComponent<Button>();
        cancelButton.onClick.AddListener(() => Cancel());                

        RectTransform rect = useReincarnationToolAlertor.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        useReincarnationToolAlertor.SetActive(true);
    }

    private static void Determine()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayCoinSE();
        if (OnDetermined != null) { OnDetermined(null, System.EventArgs.Empty); }
        Close();
    }

    private static void Cancel()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayCancelSE();
        if (OnCanceled != null) { OnCanceled(null, System.EventArgs.Empty); }
        Close();
    }

    private static void Close()
    {
        if (OnClose != null) { OnClose(null, System.EventArgs.Empty); }
        DestroyImmediate(useReincarnationToolAlertor);
    }
}
