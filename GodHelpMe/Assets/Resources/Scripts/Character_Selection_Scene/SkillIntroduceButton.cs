﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillIntroduceButton : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    internal int index;
    public float durationThreshold = 1.0f;
    public event System.EventHandler OnLongPressed;
    public event System.EventHandler OnPressedUp;
    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;

    private void Update()
    {
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted > durationThreshold)
            {
                longPressTriggered = true;
                if (OnLongPressed != null)
                {
                    OnLongPressed(this, System.EventArgs.Empty);
                }
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        timePressStarted = Time.time;
        isPointerDown = true;
        longPressTriggered = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDown = false;
        if (OnPressedUp != null)
        {
            OnPressedUp(this, System.EventArgs.Empty);
        }
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerDown = false;
    }
}
