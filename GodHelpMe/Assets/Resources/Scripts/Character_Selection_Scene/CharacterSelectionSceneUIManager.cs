﻿using PathManager;
using System;
using System.Collections;
using System.Collections.Generic;
using TKTools;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


/// <summary>
/// Partial below is for define parameter.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    [SerializeField] private int timeLimit = 90;
    [SerializeField] private GameObject characterPlatform;
    [SerializeField] private GameObject goalForHero;
    [SerializeField] private GameObject characterButtonList;
    [SerializeField] private GameObject skillIntroducePanel;
    [SerializeField] private GameObject heroSkillsBar;
    [SerializeField] private GameObject playersBoard;
    [SerializeField] private GameObject starCountPanel;
    [SerializeField] private GameObject star;
    [SerializeField] private Text level;
    [SerializeField] private Text checkpoint;
    [SerializeField] private Text score;
    [SerializeField] private int reincarnationLimitedLevel;
    [SerializeField] private Text reincarnationItemCount;
    [SerializeField] private TextAsset reincarnationItemData;
    [SerializeField] private Button usingLockedBtn;
    [SerializeField] private GameObject bottomPanel;

    private bool usingLocked;
    private bool UsingLocked
    {
        get { return usingLocked; }
        set
        {
            usingLocked = value;
            usingLockedBtn.gameObject.SetActive(usingLocked);
            bottomPanel.SetActive(!usingLocked);
        }
    }

    private GameObject skillIntroducePanelInstance;
    private int selectedHeroIndex = -1;
    private int SelectedHeroIndex
    {
        get
        {
            selectedHeroIndex = PlayerPrefsUtility.LoadValue<int>(ProjectKeywordList.kPPrefsForPlayerSelectedHeroIndex);
            return selectedHeroIndex;
        }
        set
        {
            selectedHeroIndex = value;
            PlayerPrefsUtility.SaveValue<int>(ProjectKeywordList.kPPrefsForPlayerSelectedHeroIndex, selectedHeroIndex);
        }
    }
    //private int playerIndex = 0;
    [SerializeField] private GameObject decisionLogo;

    [SerializeField] private Button lockButton;

    private Button[] chooseButtonList;
    private TextAsset[] heroDatas;

    [SerializeField] private Text heroNameText;
    [SerializeField] private Text timeText;

    private HeroData selectedHeroData;

    //[SerializeField]
    //private Button lockHeroButton;

    /// <summary>
    /// The parameter is for check is locked player who selects hero. 
    /// Default value is false.
    /// </summary>
    private bool lockedHeroStatus = false;
    private bool LockedHeroStatus
    {
        get
        {
            lockedHeroStatus = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerDecisionWarStatus);
            return lockedHeroStatus;
        }
        set
        {
            lockedHeroStatus = value;
            PlayerPrefsUtility.SaveValue<bool>(ProjectKeywordList.kPPrefsForPlayerDecisionWarStatus, lockedHeroStatus);

            decisionLogo.SetActive(lockedHeroStatus);
            Text buttonText = lockButton.transform.Find("Text").GetComponent<Text>();
            buttonText.text = lockedHeroStatus ? "取消出戰" : "出戰";

            if (decisionLogo.activeInHierarchy)
            {
                if (SEManager.Singleton)
                {
                    ///TODO:Play sound effect;
                }
            }
            else
            {
                if (SEManager.Singleton)
                {
                    ///TODO:Play sound effect;
                }
            }
        }
    }

    /// <summary>
    /// The parameter is for check player who whether selected hero or not.
    /// Default value is false.
    /// </summary>
    private bool selectedHeroStatus = false;

    private bool heroWalkToTarget = true;
    private bool HeroWalkToTarget { get { return heroWalkToTarget; } set { heroWalkToTarget = value; } }

    private GameObject choosedHero;
    private GameObject ChoosedHero
    {
        get { return choosedHero; }
        set
        {
            choosedHero = value;
            StartCoroutine(ChoosedHeroRelay(choosedHero));
        }
    }

    private IEnumerator ChoosedHeroRelay(GameObject hero)
    {
        HeroWalkToTarget = false;
        hero.transform.localPosition = Vector3.zero + (Vector3.down * 2);
        hero.transform.Find("MinimapPlayerMark").gameObject.SetActive(false);
        MonoBehaviour[] c_array = hero.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour c in c_array) { c.enabled = false; }

        ParticleSystem ps = hero.transform.Find("FlashCircleDown").GetComponent<ParticleSystem>();
        ps.gameObject.transform.parent = null;
        // Why is to set the position because the parent is null;
        //ps.gameObject.transform.position += Vector3.down;
        //ps.gameObject.transform.rotation = Quaternion.identity;
        yield return new WaitUntil(() => (ps.gameObject.transform.parent == null));

        ps.gameObject.SetActive(true);
        yield return new WaitForSeconds((ps.main.duration + ps.main.startLifetime.constant) / 1.5f);

        ps.gameObject.transform.parent = hero.transform;
        ps.gameObject.transform.localPosition = Vector3.zero;
        ps.gameObject.transform.localRotation = Quaternion.identity;
        yield return new WaitUntil(() => (ps.gameObject.transform.parent == hero.transform));

        ps.Stop();
        ps.Clear();
        ps.gameObject.SetActive(false);
        yield return new WaitUntil(() => (ps.gameObject.activeInHierarchy == false));

        hero.SetActive(true);
    }

    private PhysicTriggerReceiver receiver;

    private float moveSpeed = 20.0f;

    private List<HeroData> HDList = new List<HeroData>();

    [SerializeField] private Sprite usingLockedBtnDefaultSpirte;
    [SerializeField] private ParticleSystem usingLockedPS;
}



/// <summary>
/// Partial below is for script to start
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        InitializtionScenePlayer();
        InitializtionTheSolutionComponent();
        InitializtionHeros();
        //StartCountdownTime();        
    }

    private void Update()
    {
        characterPlatform.transform.LookAt(goalForHero.transform);
        if (!HeroWalkToTarget)
        {
            if (ChoosedHero.activeSelf == true)
            {
                ChoosedHero.GetComponent<Animator>().SetBool(ProjectKeywordList.kAnimationForMove, !HeroWalkToTarget);
                CharacterController cc = ChoosedHero.GetComponent<CharacterController>();
                Debug.Log(goalForHero.transform.position);
                Vector3 goal = new Vector3(ChoosedHero.transform.forward.x * moveSpeed, 1.0f, ChoosedHero.transform.forward.z * moveSpeed);
                cc.SimpleMove(goal);
                Debug.Log("Go to Target...");
            }
        }
    }

    private void OnDestroy()
    {
        Debug.Log("CharacterSelectionSceneUIManager Destory");
        receiver.OnReceiveTriggerEnter -= CharacterSelectionSceneUIManager_OnReceiveTriggerEnter;
    }
}


/// <summary>
/// Partial below is for some componet to initialize.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    private void InitializtionScenePlayer()
    {
        //if (PlayerAttributeManager.Singleton && PUNManager.Singleton.ConnectStatus)
        //{
        //    foreach (PhotonPlayer player in PUNManager.Singleton.CurrentRoomPlayerList)
        //    {
        //        PlayerAttributeManager.Singleton.PlayerList.Add(player);
        //    }

        //    PlayerAttributeManager.Singleton.PlayerList.Sort();
        //    PlayerAttributeManager.Singleton.PlayerList = PlayerAttributeManager.Singleton.PlayerList;
        //    PlayerAttributeManager.Singleton.PlayerIndex = PlayerAttributeManager.Singleton.PlayerList.IndexOf(PhotonNetwork.player);
        //    //playerIndex = PlayerAttributeManager.Singleton.PlayerList.IndexOf(PhotonNetwork.player);
        //}        
    }
}




/// <summary>
/// Partial below is for some componet to initialize.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    private void InitializtionTheSolutionComponent()
    {
        //heroNameText = transform.Find("HeroName").GetComponent<Text>();
        //characterButtonList = transform.Find("HeroListBackground/Mask/CharacterButtonList").gameObject;
        //heroSkillsBar = transform.Find("HeroSkillsBar").gameObject;
        //skillIntroducePanel = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceCharacterSelectionScene("SkillIntroducePanel"));
        skillIntroducePanelInstance = Instantiate(skillIntroducePanel, transform);
        //timeText = transform.Find("Time/Text").GetComponent<Text>();
        //lockButton = transform.Find("LockButton").gameObject.GetComponent<Button>();        
        lockButton.onClick.AddListener(() => this.LockButtonEvent());

        receiver = goalForHero.GetComponent<PhysicTriggerReceiver>();
        receiver.OnReceiveTriggerEnter += CharacterSelectionSceneUIManager_OnReceiveTriggerEnter;
    }

    private void CharacterSelectionSceneUIManager_OnReceiveTriggerEnter(object sender, EventArgs e)
    {
        HeroWalkToTarget = true;
        if (ChoosedHero)
        {
            ChoosedHero.GetComponent<Animator>().SetBool(ProjectKeywordList.kAnimationForMove, !HeroWalkToTarget);
        }
    }
}



/// <summary>
/// Partial below is for set heros.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    private void InitializtionHeros()
    {
        /// This below is for set choose buttons.        
        Button buttonObj = Resources.Load<Button>("Prefab/UserInterface/Character_Selection_Scene/CharacterChooseButton");

        heroDatas = Resources.LoadAll<TextAsset>("Data/Heros");

        chooseButtonList = new Button[heroDatas.Length];

        HDList.Clear();

        for (int i = 0; i < heroDatas.Length; i++)
        {
            string JSON = heroDatas[i].text;
            HeroData heroData = JsonUtility.FromJson<HeroData>(JSON);
            HDList.Add(heroData);

            GameObject prefab = Resources.Load<GameObject>(heroData.prefabPath);
            GameObject instance = Instantiate(prefab, characterPlatform.transform);
            instance.transform.localPosition = Vector3.zero;
            instance.SetActive(false);

            /// Partial below is for set choose buttons.
            Button chooseButton = Instantiate(buttonObj, characterButtonList.transform);
            Sprite buttonSprite = Resources.Load<Sprite>(heroData.picturePath);
            chooseButton.image.sprite = buttonSprite;
            chooseButton.onClick.AddListener(() => ChooseHeroButtonEvent(chooseButton));
            chooseButtonList[i] = chooseButton;
        }

        usingLockedBtn.onClick.AddListener(() => UsingLockedBtnEvent());

        // if (PlayerAttributeManager.Singleton)
        // {
        //     for (int i = 0; i < PlayerAttributeManager.Singleton.PlayerOwnHeros.Count; i++)
        //     {
        //         string id = PlayerAttributeManager.Singleton.PlayerOwnHeros[i];
        //         if (HDList.Exists(D => (id == D.id)))
        //         {
        //             int index = HDList.FindIndex(D => (id == D.id));
        //             Button chooseButton = chooseButtonList[index];
        //             GameObject lockImage = chooseButton.transform.Find("lock").gameObject;
        //             lockImage.SetActive(true);
        //         }
        //     }
        // }

        ///Part of below is for initializing status of locked hero already.

        decisionLogo.SetActive(LockedHeroStatus);
        Text btnText = lockButton.transform.Find("Text").GetComponent<Text>();
        btnText.text = LockedHeroStatus ? "取消出戰" : "出戰";

        int HIndex = PlayerPrefsUtility.LoadValue<int>(ProjectKeywordList.kPPrefsForPlayerSelectedHeroIndex);
        SelectCharacterBy(HIndex);
    }
}


/// <summary>
/// Partial below is for start countdown time when player selecting who want's plays hero.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    //private void StartCountdownTime()
    //{
    //    InvokeRepeating("SpawnCountdownTime", 0, 1);
    //}

    //private void SpawnCountdownTime()
    //{
    //    // 将 秒数 传入 并显示 成文字
    //    TimeSpan result = TimeSpan.FromSeconds(timeLimit);
    //    timeText.text = string.Format("{0:00}:{1:00}", result.Minutes, result.Seconds);

    //    if (timeLimit == 0)
    //    {
    //        Debug.LogWarning("Time stopped");

    //        StartCoroutine(EventRelay_00());
    //    }

    //    timeLimit--;
    //}

    //IEnumerator EventRelay_00()
    //{
    //    CancelInvoke("SpawnCountdownTime");

    //    if (selectedHeroStatus == false)
    //    {
    //        StartCoroutine(EventRelay_02());
    //    }

    //    /// The parameter "second" means is player list total count plus 1.
    //    /// That means is for a time of waiting for the system chooses a hero for a player.
    //    //int second = PhotonNetwork.playerList.Length + 1;
    //    //yield return new WaitForSeconds(second / 2);

    //    SceneManager.LoadScene(ProjectKeywordList.kSceneForGame);

    //    yield break;
    //}

    //IEnumerator EventRelay_02()
    //{
    //    int second = PlayerAttributeManager.Singleton.PlayerIndex;
    //    yield return new WaitForSeconds(second / 2);
    //    List<int> unSelectedHeroIndexList = new List<int>();

    //    for (int i = 0; i < characterButtonList.transform.childCount; i++)
    //    {
    //        Button currentButton = characterButtonList.transform.GetChild(i).GetComponent<Button>();
    //        if (currentButton.interactable == true)
    //        {
    //            unSelectedHeroIndexList.Add(i);
    //        }
    //    }

    //    int randomIndex = UnityEngine.Random.Range(0, unSelectedHeroIndexList.Count);
    //    int heroIndex = unSelectedHeroIndexList[randomIndex];
    //    SelectCharacterBy(heroIndex);
    //    yield break;
    //}
}

/// <summary>
/// Partial below is for selecet character by index
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    private void SelectCharacterBy(int index)
    {
        // Always be the original rotation.
        //characterPlatform.transform.SetPositionAndRotation(new Vector3(0, 0, 0), Quaternion.Euler(new Vector3(0, 180, 0)));

        characterPlatform.transform.Rotate(new Vector3(0.0f, 180.0f, 0.0f));

        if (SelectedHeroIndex != -1)
        {

            HeroWalkToTarget = true;

            // The part is privous selected.
            GameObject pastHero = characterPlatform.transform.GetChild(SelectedHeroIndex).gameObject;
            pastHero.SetActive(false);
            chooseButtonList[SelectedHeroIndex].transform.Find("HighlightImage").gameObject.SetActive(false);
            // RPC player who privous selects hero index to All            

            Button privousButton = characterButtonList.transform.GetChild(SelectedHeroIndex).GetComponent<Button>();
            privousButton.interactable = true;
            privousButton.image.color = new Color32(255, 255, 225, 255);
        }

        // The part when assig new index, will be current selected.
        SelectedHeroIndex = index;

        chooseButtonList[SelectedHeroIndex].transform.Find("HighlightImage").gameObject.SetActive(true);

        // The part is for here name.
        string JSON = heroDatas[SelectedHeroIndex].text;
        selectedHeroData = JsonUtility.FromJson<HeroData>(JSON);
        heroNameText.text = selectedHeroData.name;

        // The part is for runing hero animation "idle".
        ChoosedHero = characterPlatform.transform.GetChild(SelectedHeroIndex).gameObject;

        //Vector3 position = Vector3.zero;
        //Vector3 euler = Vector3.zero;

        //switch (selectedHeroData.id)
        //{
        //    case "5A631C1A-112F6A0-34500C96":
        //        position = new Vector3(0f, -2.5f, -3f);
        //        break;
        //    case "5A631D54-15A2BCA-FC7267A":
        //        position = new Vector3(0f, -2.5f, -3f);
        //        euler = new Vector3(12.5f, 0, 0);
        //        break;
        //    case "5A631E08-19B7EE0-114E1422":
        //        position = new Vector3(0f, -3f, -3f);
        //        break;
        //    case "5A631EA5-1E2B40A-1C351B77":
        //        position = new Vector3(0f, -1f, -3f);
        //        break;
        //}

        //hero.transform.localPosition = Vector3.zero;
        //characterPlatform.transform.position += position;
        //characterPlatform.transform.Rotate(euler);

        //ChoosedHero.SetActive(true);

        if (PlayerAttributeManager.Singleton)
        {

            Debug.Log("PlayerAttributeManager.Singleton.PlayerOwnHeros.Count " + PlayerAttributeManager.Singleton.PlayerOwnHeros.Count);

            // for (int i = 0; i < PlayerAttributeManager.Singleton.PlayerOwnHeros.Count; i++)
            // {
            //     string id = PlayerAttributeManager.Singleton.PlayerOwnHeros[i];
            //     UsingLocked = !(selectedHeroData.id == id);
            // }

            UsingLocked = !PlayerAttributeManager.Singleton.PlayerOwnHeros.Contains(selectedHeroData.id);

            // Animator a = usingLockedBtn.transform.GetComponent<Animator>();
            // a.Play("UnlockHero_Animation", -1, 0.0f);

            SelectedHeroByPlayer(PlayerAttributeManager.Singleton.PlayerIndex, SelectedHeroIndex);
        }

        // The part is for create introduce of skill.
        for (int i = 0; i < selectedHeroData.skills.Length; i++)
        {
            Button skillButton = heroSkillsBar.transform.GetChild(i).GetComponent<Button>();
            skillButton.gameObject.SetActive(true);

            SkillIntroduceButton skillButtonScript = skillButton.GetComponent<SkillIntroduceButton>();
            skillButtonScript.index = i;

            HeroSkillData skillData = selectedHeroData.skills[skillButtonScript.index];
            Sprite sprite = Resources.Load<Sprite>(skillData.iconPath);

            skillButton.image.sprite = sprite;
            skillButtonScript.OnLongPressed += SkillIntroduceButtonOnLongPressed;
            skillButtonScript.OnPressedUp += SkillIntroduceButtonOnLongPressedUp;
        }

        if (lockButton.interactable == false)
        {
            lockButton.interactable = true;
            //selectedHeroStatus = true;
        }

        if (index == SelectedHeroIndex) { return; }

        var characterListCount = characterPlatform.transform.childCount;

        if (index < 0 || index >= characterListCount) { return; }
    }

    private void SelectedHeroByPlayer(int playerIndex, int heroIndex)
    {
        Button currentButton = characterButtonList.transform.GetChild(heroIndex).GetComponent<Button>();
        currentButton.interactable = false;
        currentButton.image.color = new Color32(255, 255, 225, 100);

        // The part is for here name.
        string JSON = heroDatas[heroIndex].text;
        selectedHeroData = JsonUtility.FromJson<HeroData>(JSON);

        if (PlayerAttributeManager.Singleton)
        {
            PlayerAttributeManager.Singleton.ChoosedHeroID = selectedHeroData.id;
            SetHeroPropertyBy(PlayerAttributeManager.Singleton.ChoosedHeroID);
        }

        // The part is for hero name.     
        Image playerSelectedHeroPic = playersBoard.transform.GetChild(playerIndex).transform.Find("PlayerPicture").GetComponent<Image>();
        Sprite heroSprite = Resources.Load<Sprite>(selectedHeroData.picturePath);
        playerSelectedHeroPic.sprite = heroSprite;
    }

    private void SetHeroPropertyBy(string heroID)
    {
        //Debug.Log("PlayerAttributeManager.Singleton.HeroAchievementDic.Count " + PlayerAttributeManager.Singleton.HeroAchievementDic.Count);
        //foreach (KeyValuePair<string, HeroAchievement> pair in PlayerAttributeManager.Singleton.HeroAchievementDic)
        //{
        //    Debug.Log("Check it " + pair.Key);
        //}

        if (PlayerAttributeManager.Singleton.HeroAchievementDic.ContainsKey(heroID))
        {
            HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievementDic[heroID];
            Debug.Log("SetHeroPropertyBy");
            SetHeroAchievementInfoBy(h);
            SetStarCountPanelBy(h);
        }
    }

    private void SetHeroAchievementInfoBy(HeroAchievement h)
    {
        level.text = string.Format("等級 {0}", h.level);

        if (PlayerAttributeManager.Singleton)
        {
            string id = reincarnationItemData.name;
            string JSON = reincarnationItemData.text;
            //ItemData itemData = JsonUtility.FromJson<ItemData>(JSON);
            if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(id))
            {
                PlayerItem p = PlayerAttributeManager.Singleton.PlayerItemPackage[id];
                reincarnationItemCount.text = string.Format("擁有 {0}", p.itemCount);
            }
            else
            {
                reincarnationItemCount.text = string.Format("擁有 {0}", 0);
            }

            checkpoint.text = string.Format("關卡數 {0}", h.checkpoint);
            Debug.Log("checkpoint.text " + checkpoint.text);

            score.text = string.Format("得分 {0}", h.score);
            Debug.Log("point.text " + score.text);
        }
    }

    private void SetStarCountPanelBy(HeroAchievement h)
    {
        for (int i = 0; i < h.reincarnation; i++)
        {
            Instantiate(star, starCountPanel.transform);
        }
    }
}



/// <summary>
/// Partial below is for choose hero.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    private void ChooseHeroButtonEvent(Button button)
    {
        if (HeroWalkToTarget == false) { return; }

        int index = Array.IndexOf(chooseButtonList, button);

        if (LockedHeroStatus == false)
        {
            if (SEManager.Singleton)
            {
                SEManager.Singleton.PlayChooseSE();
            }

            SelectCharacterBy(index);
        }
        else
        {
            WarnAlertor.OnClosed += WA_OnClosed;
            WarnAlertor.Show("請先取消原出戰的英雄", transform);
        }
    }

    private void WA_OnClosed(object sender, EventArgs e)
    {
        WarnAlertor.OnClosed -= WA_OnClosed;
    }

    private void UsingLockedBtnEvent()
    {
        Debug.Log("The Hero Locked");
        ToolItemPurchaseAlertor.OnPurchased += TIPA_OnPurchased;
        ToolItemPurchaseAlertor.OnClose += TIPA_OnClose;
        ToolItemPurchaseAlertor.OnCancel += TIPA_OnCancel;
        ToolItemPurchaseAlertor.Show(selectedHeroData.price, transform, "請問你要解鎖此英雄嗎?");
    }

    private void TIPA_OnPurchased(object sender, EventArgs e)
    {
        if (PlayerAttributeManager.Singleton.PlayerOwnDiamonds >= selectedHeroData.price)
        {
            PlayerAttributeManager.Singleton.PlayerOwnDiamonds -= selectedHeroData.price;

            // These are syncing to Game Scene.
            if (PlayerAttributeManager.Singleton)
            {
                List<string> OH = PlayerAttributeManager.Singleton.PlayerOwnHeros;
                if (OH.Contains(selectedHeroData.id)) { OH.Remove(selectedHeroData.id); }
                OH.Add(selectedHeroData.id);
                PlayerAttributeManager.Singleton.PlayerOwnHeros = OH;

                Animator a = usingLockedBtn.transform.GetComponent<Animator>();
                TKAnimatiorEventManager TKAEM = usingLockedBtn.transform.GetComponent<TKAnimatiorEventManager>();
                TKAEM.OnFinishedAnimationEvnet += OnFinishedAnimationEvnet;
                a.SetTrigger("Unlock");

            }

            return;
        }
        else
        {
            ToolShopFullDiamondAlertor.OnGoDiamondShop += ToolShopFullDiamondAlertor_OnGoDiamondShop;
            ToolShopFullDiamondAlertor.OnCancel += ToolShopFullDiamondAlertor_OnCancel;
            ToolShopFullDiamondAlertor.OnClose += ToolShopFullDiamondAlertor_OnClose;
            Transform USERINTERFACEMANAGER = GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform;
            ToolShopFullDiamondAlertor.Show(USERINTERFACEMANAGER);
        }
    }

    IEnumerator UnlockHeroSymbolAnimationFinished()
    {
        // Animator a = usingLockedBtn.transform.GetComponent<Animator>();
        // yield return new WaitUntil(() => AnimatorIsPlaying(a, "Default"));        
        UsingLocked = false;
        usingLockedBtn.transform.GetComponent<Image>().sprite = usingLockedBtnDefaultSpirte;
        usingLockedPS.Play();
        yield return new WaitForSeconds((usingLockedPS.main.duration + usingLockedPS.main.startLifetime.constant));
        usingLockedPS.Stop();
        usingLockedPS.Clear();
        yield break;
    }

    private void OnFinishedAnimationEvnet()
    {
        TKAnimatiorEventManager TKAEM = usingLockedBtn.transform.GetComponent<TKAnimatiorEventManager>();
        TKAEM.OnFinishedAnimationEvnet -= OnFinishedAnimationEvnet;
        // Animator a = usingLockedBtn.transform.GetComponent<Animator>();
        // a.Play("UnlockHero_Animation", -1, 0f);
        StartCoroutine(UnlockHeroSymbolAnimationFinished());
    }

    private void TIPA_OnClose(object sender, EventArgs e)
    {
        ToolItemPurchaseAlertor.OnPurchased -= TIPA_OnPurchased;
        ToolItemPurchaseAlertor.OnClose -= TIPA_OnClose;
        ToolItemPurchaseAlertor.OnCancel -= TIPA_OnCancel;
    }

    private void TIPA_OnCancel(object sender, EventArgs e)
    {

    }

}



/// <summary>
/// Partial below is for SkillIntroduceButton Event call back.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    private void SkillIntroduceButtonOnLongPressed(object obj, EventArgs args)
    {
        SetSkillInfomationOnPanel((SkillIntroduceButton)obj);
        skillIntroducePanel.SetActive(true);
    }

    private void SkillIntroduceButtonOnLongPressedUp(object obj, EventArgs args)
    {
        skillIntroducePanel.SetActive(false);
    }
}


public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    public void LockButtonEvent()
    {
        if (LockedHeroStatus)
        {
            if (SEManager.Singleton)
            {
                SEManager.Singleton.PlayToolItemUnlockSE();
            }
            LockedHeroStatus = false;
        }
        else
        {
            if (SEManager.Singleton)
            {
                SEManager.Singleton.PlaySelectHeroLockSE();
            }
            LockedHeroStatus = true;
        }

        //LockSelelctedStatus(PlayerAttributeManager.Singleton.PlayerIndex, lockButtonStatus);
    }

    private void LockSelelctedStatus(int index, bool lockStatus)
    {
        playersBoard.transform.GetChild(index).Find("PlayerPicture/HighlightImage").gameObject.SetActive(lockStatus);
    }
}


/// <summary>
/// The partial is for set skill infomation on panel.
/// </summary>
public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    private void SetSkillInfomationOnPanel(SkillIntroduceButton skillButton)
    {
        HeroSkillData skillData = selectedHeroData.skills[skillButton.index];
        string n = skillData.name;
        string introduce = skillData.info;
        Text skillName = skillIntroducePanel.transform.Find("Name").GetComponent<Text>();
        skillName.text = n;
        Text skillIntroduce = skillIntroducePanel.transform.Find("Introduce").GetComponent<Text>();
        skillIntroduce.text = introduce;
    }
}

public partial class CharacterSelectionSceneUIManager : MonoBehaviour
{
    public void UnLoadingScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
        StartCoroutine(StartLoading(ProjectKeywordList.kSceneForLobby, this.transform));
    }

    public void ToHeroIntroductScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }
        Debug.Log("hero ID => " + selectedHeroData.id);
        if (PlayerAttributeManager.Singleton) { PlayerAttributeManager.Singleton.ChoosedHeroID = selectedHeroData.id; }
        StartCoroutine(StartLoading(ProjectKeywordList.kSceneForHeroSkillIntroduce, this.transform));
    }

    private IEnumerator StartLoading(string scene, Transform t)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, t);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }

    public void ToReincarnation()
    {
        Debug.Log("ToReincarnation");

        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }

        ///TODO
        if (PlayerAttributeManager.Singleton)
        {
            if (PlayerAttributeManager.Singleton.HeroAchievement.level < reincarnationLimitedLevel)
            {
                WarnAlertor.OnClosed += WarnAlertor_OnClosed;
                string m = string.Format("該英雄等級尚未達到轉生限制{0}，請繼續加油哦！", reincarnationLimitedLevel);
                WarnAlertor.Show(m, this.transform);
                return;
            }

            string id = reincarnationItemData.name;
            string JSON = reincarnationItemData.text;
            ItemData item = JsonUtility.FromJson<ItemData>(JSON);

            if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(id))
            {
                if (PlayerAttributeManager.Singleton.PlayerItemPackage[id].itemCount > 0)
                {
                    UseReincarnationToolAlertor.OnDetermined += UseReincarnationToolAlertor_OnDetermined;
                    UseReincarnationToolAlertor.OnCanceled += UseReincarnationToolAlertor_OnCanceled;
                    UseReincarnationToolAlertor.OnClose += UseReincarnationToolAlertor_OnClose;
                    UseReincarnationToolAlertor.ShowOn(this.transform);
                    return;
                }
            }

            ToolItemPurchaseAlertor.OnPurchased += ToolItemPurchaseAlertor_OnPurchased;
            ToolItemPurchaseAlertor.OnCancel += ToolItemPurchaseAlertor_OnCancel;
            ToolItemPurchaseAlertor.OnClose += ToolItemPurchaseAlertor_OnClose;
            ToolItemPurchaseAlertor.Show(item.price, this.transform);
        }
    }

    private void WarnAlertor_OnClosed(object sender, EventArgs e)
    {
        WarnAlertor.OnClosed -= WarnAlertor_OnClosed;
    }

    private void ToolItemPurchaseAlertor_OnClose(object sender, EventArgs e)
    {
        ToolItemPurchaseAlertor.OnPurchased -= ToolItemPurchaseAlertor_OnPurchased;
        ToolItemPurchaseAlertor.OnCancel -= ToolItemPurchaseAlertor_OnCancel;
        ToolItemPurchaseAlertor.OnClose -= ToolItemPurchaseAlertor_OnClose;
    }

    private void ToolItemPurchaseAlertor_OnCancel(object sender, EventArgs e)
    {

    }

    private void ToolItemPurchaseAlertor_OnPurchased(object sender, EventArgs e)
    {
        string id = reincarnationItemData.name;
        string JSON = reincarnationItemData.text;
        ItemData item = JsonUtility.FromJson<ItemData>(JSON);
        int playerOwnCount = 0;
        int eachItemMaxmin = 10;
        if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(id))
        {
            playerOwnCount = PlayerAttributeManager.Singleton.PlayerItemPackage[id].itemCount;
        }
        Debug.Log("The Item Price is " + item.price);

        if (PlayerAttributeManager.Singleton)
        {
            if ((playerOwnCount + item.count) > eachItemMaxmin)
            {
                //Debug.Log("Sorry!, You can't purchase the Item, because the Item maxmin is 500");
                //GameObject warnAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterface("WarnAlertor"));
                //warnAlertor = Instantiate(warnAlertor, GameObject.Find("USERINTERFACEMANAGER").transform);
                //Text tip = warnAlertor.transform.Find("Background/Image/Tip").GetComponent<Text>();

                //tip.text = "Note! You Can't Buy more then " + eachItemMaxmin;

                //var rect = warnAlertor.GetComponent<RectTransform>();
                //rect.offsetMin = new Vector2(0, 0);
                //rect.offsetMax = new Vector2(0, 0);

                WarnAlertor.OnClosed += WarnAlertor_OnClosed;
                string m = string.Format("注意！您不可以購買超過{0}個哦！", eachItemMaxmin);
                WarnAlertor.Show(m, this.transform);

                return;
            }

            if (PlayerAttributeManager.Singleton.PlayerOwnDiamonds >= item.price)
            {
                PlayerAttributeManager.Singleton.PlayerOwnDiamonds = PlayerAttributeManager.Singleton.PlayerOwnDiamonds - item.price;
                playerOwnCount = playerOwnCount + item.count;
                reincarnationItemCount.text = string.Format("擁有 {0}", playerOwnCount);
                Debug.Log("You got " + playerOwnCount + " already.");

                // These are syncing to Game Scene.
                if (PlayerAttributeManager.Singleton)
                {
                    PlayerItem playerItem = new PlayerItem(id, playerOwnCount);

                    if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(id) == false)
                    {
                        Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                        PIP.Add(id, playerItem);
                        PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;
                    }
                    else
                    {
                        Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                        PIP[id] = playerItem;
                        PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;
                    }

                    //foreach (KeyValuePair<string, PlayerItem> item in PlayerAttributeManager.Singleton.PlayerItemPackage)
                    //{
                    //    Debug.Log(item.Key + " => " + item.Value);
                    //}

                    //PlayerPrefsUtility.SaveDict<string, PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPackage, PlayerAttributeManager.Singleton.PlayerItemPackage);
                }

                return;
            }
            else
            {
                //Debug.LogWarning("You Can't on Panel this item, because you hanv not enugh dimaonds");
                //GameObject toolShopFullDiamondAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceToolsShopScene("ToolShopFullDiamondAlertor"));
                //toolShopFullDiamondAlertor = Instantiate(toolShopFullDiamondAlertor, GameObject.Find("USERINTERFACEMANAGER").transform);

                //var rect = toolShopFullDiamondAlertor.GetComponent<RectTransform>();
                //rect.offsetMin = new Vector2(0, 0);
                //rect.offsetMax = new Vector2(0, 0);
                ToolShopFullDiamondAlertor.OnGoDiamondShop += ToolShopFullDiamondAlertor_OnGoDiamondShop;
                ToolShopFullDiamondAlertor.OnCancel += ToolShopFullDiamondAlertor_OnCancel;
                ToolShopFullDiamondAlertor.OnClose += ToolShopFullDiamondAlertor_OnClose;
                Transform USERINTERFACEMANAGER = GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform;
                ToolShopFullDiamondAlertor.Show(USERINTERFACEMANAGER);
            }
        }
    }

    private void ToolShopFullDiamondAlertor_OnGoDiamondShop(object sender, System.EventArgs e)
    {
        if (PlayerAttributeManager.Singleton)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            PlayerAttributeManager.Singleton.previousSceneToDiamondShopName = currentScene.name;
            StartLoading(ProjectKeywordList.kSceneForDiamondShop, this.transform);
        }
    }

    private void ToolShopFullDiamondAlertor_OnCancel(object sender, System.EventArgs e)
    {

    }

    private void ToolShopFullDiamondAlertor_OnClose(object sender, System.EventArgs e)
    {
        ToolShopFullDiamondAlertor.OnGoDiamondShop -= ToolShopFullDiamondAlertor_OnGoDiamondShop;
        ToolShopFullDiamondAlertor.OnCancel -= ToolShopFullDiamondAlertor_OnCancel;
        ToolShopFullDiamondAlertor.OnClose -= ToolShopFullDiamondAlertor_OnClose;
    }

    private void UseReincarnationToolAlertor_OnClose(object sender, EventArgs e)
    {
        UseReincarnationToolAlertor.OnDetermined -= UseReincarnationToolAlertor_OnDetermined;
        UseReincarnationToolAlertor.OnCanceled -= UseReincarnationToolAlertor_OnCanceled;
        UseReincarnationToolAlertor.OnClose -= UseReincarnationToolAlertor_OnClose;
    }

    private void UseReincarnationToolAlertor_OnCanceled(object sender, EventArgs e)
    {

    }

    private void UseReincarnationToolAlertor_OnDetermined(object sender, EventArgs e)
    {
        string id = reincarnationItemData.name;
        string JSON = reincarnationItemData.text;
        ItemData item = JsonUtility.FromJson<ItemData>(JSON);

        if (PlayerAttributeManager.Singleton)
        {
            if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(id))
            {
                if (PlayerAttributeManager.Singleton.PlayerItemPackage[id].itemCount == 0)
                {
                    return;
                }

                Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                PIP[id].itemCount -= 1;
                PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;

                int playerOwnCount = PlayerAttributeManager.Singleton.PlayerItemPackage[id].itemCount;
                reincarnationItemCount.text = string.Format("擁有 {0}", playerOwnCount);

                StartReincarnation();

            }
        }
    }

    private void StartReincarnation()
    {
        if (PlayerAttributeManager.Singleton)
        {
            HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievement;
            h.level = 1;
            h.reincarnation += 1;
            PlayerAttributeManager.Singleton.HeroAchievement = h;
        }
    }
}


public partial class CharacterSelectionSceneUIManager
{
    private bool AnimatorIsPlaying(Animator a)
    {
        return a.GetCurrentAnimatorStateInfo(0).length >
               a.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    private bool AnimatorIsPlaying(Animator a, string stateName)
    {
        return AnimatorIsPlaying(a) && a.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
}