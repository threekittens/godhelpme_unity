﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathManager;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Open setting alertor.
/// </summary>
public partial class SystemTopBar : MonoBehaviour
{
    private Text diamondsCount;
    private GameObject alertor;

    [SerializeField] private bool CreateShop = false;

    private void Start()
    {
        diamondsCount = transform.Find("Diamond_Background/Text").GetComponent<Text>();
        alertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterface("SettingAlertor"));
    }

    private void Update()
    {
        diamondsCount.text = PlayerAttributeManager.Singleton.PlayerOwnDiamonds.ToString();
    }
}

/// <summary>
/// Open setting alertor.
/// </summary>
public partial class SystemTopBar : MonoBehaviour
{
    public void OpenSettingAlertor()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }

        if (alertor == null) { return; }

        GameObject instance = Instantiate(alertor, GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform);
        RectTransform rect = instance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }
}

/// <summary>
/// Loding to Diamond Shop Scene.
/// </summary>
public partial class SystemTopBar : MonoBehaviour
{
    public void LoadingDiamondShopScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayDecideSE();
        }

        if (CreateShop)
        {
            StartCoroutine(StartLoading(ProjectKeywordList.kSceneForDiamondShop, GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform));
            return;
        }

        WarnAlertor.OnClosed += WA_OnClosed;
        WarnAlertor.Show("目前尚未開放寶石商店，敬請期待", GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform);
    }

    private void WA_OnClosed(object sender, System.EventArgs e)
    {
        WarnAlertor.OnClosed -= WA_OnClosed;
    }
}

/// <summary>
/// Do load async to assign scene.
/// </summary>
public partial class SystemTopBar : MonoBehaviour
{
    //private IEnumerator LoadAsyncScene(string name)
    //{
    //Scene currentScene = SceneManager.GetActiveScene();
    //PlayerAttributeManager.Singleton.previousSceneToDiamondShopName = currentScene.name;
    //    SceneManager.LoadSceneAsync(name);
    //    yield return null;
    //}

    private IEnumerator StartLoading(string scene, Transform T)
    {

        if (PlayerAttributeManager.Singleton)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            PlayerAttributeManager.Singleton.previousSceneToDiamondShopName = currentScene.name;
        }

        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, T);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }
}