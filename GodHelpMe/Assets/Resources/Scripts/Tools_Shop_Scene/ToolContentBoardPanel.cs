﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolContentBoardPanel : MonoBehaviour
{
    private TextAsset[] itemDatas;

    private void Start()
    {
        produceToolContentBoardInControlBoard();
    }

    private void produceToolContentBoardInControlBoard()
    {

        GameObject contentBoardPanel = transform.gameObject;

        itemDatas = Resources.LoadAll<TextAsset>("Data/ToolItems");

        for (int i = 0; i < itemDatas.Length; i++)
        {
            TextAsset JSONFile = itemDatas[i];
            
            if (JSONFile.name == "5A613148-AD6F0B-359EB4CF" || JSONFile.name == "5AF92E15-112A3DC-2C0AEE79") { return; }

            string JSON = itemDatas[i].text;

            ItemData itemData = JsonUtility.FromJson<ItemData>(JSON);

            GameObject obj = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceToolsShopScene("ToolContentBoard"));
            ToolContentBoard toolContentBoard = Instantiate(obj, contentBoardPanel.transform).GetComponent<ToolContentBoard>();

            Sprite icon = Resources.Load<Sprite>(itemData.iconPath);

            ToolItem item = new ToolItem();

            item.id = JSONFile.name;
            item.icon = icon;
            item.name = itemData.name;
            item.info = itemData.info;
            item.price = itemData.price;
            item.purchaseCount = itemData.count;

            toolContentBoard.SetToolContentBoard(item);
        }
    }
}
