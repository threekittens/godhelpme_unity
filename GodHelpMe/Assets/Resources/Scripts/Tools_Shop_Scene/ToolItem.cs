﻿
using System.Collections.Generic;
using UnityEngine;

public class ToolItem {

    public string id;
    public Sprite icon;
    public string name;
    public string info;
    public int price;
    public int purchaseCount;

    public Dictionary<string, object> data;

    public ToolItem()
    {

    }

    public ToolItem(Dictionary<string, object> newData)
    {
        this.data = newData;
        this.icon = (Sprite)newData["icon"];
        this.name = (string)newData["name"];
        this.info = (string)newData["info"];
        this.price = (int)newData["price"];
        this.purchaseCount = (int)newData["purchaseCount"];
    }    
}
