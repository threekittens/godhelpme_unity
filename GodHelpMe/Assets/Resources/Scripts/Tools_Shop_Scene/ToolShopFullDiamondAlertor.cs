﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class ToolShopFullDiamondAlertor : MonoBehaviour
{
    public static event System.EventHandler OnGoDiamondShop;
    public static event System.EventHandler OnCancel;
    public static event System.EventHandler OnClose;

    private static GameObject toolShopFullDiamondAlertor;

    public static void Show(Transform USERINTERFACEMANAGER)
    {
        GameObject instance = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterfaceToolsShopScene("ToolShopFullDiamondAlertor"));
        toolShopFullDiamondAlertor = Instantiate(instance, USERINTERFACEMANAGER);

        Button goDiamondShopButton = toolShopFullDiamondAlertor.transform.Find("Background/Content/GoDiamondShopButton").GetComponent<Button>();
        goDiamondShopButton.onClick.AddListener(() => GoDiamondShop());

        Button cancelButton = toolShopFullDiamondAlertor.transform.Find("Background/Content/CancelButton").GetComponent<Button>();
        cancelButton.onClick.AddListener(() => Cancel());

        RectTransform rect = toolShopFullDiamondAlertor.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        toolShopFullDiamondAlertor.SetActive(true);
    }

    private static void GoDiamondShop()
    {
        SEManager.Singleton.PlayOpenBookSE();
        if (OnGoDiamondShop != null) { OnGoDiamondShop(null, System.EventArgs.Empty); }
        Close();
    }

    private static void Cancel()
    {
        SEManager.Singleton.PlayCancelSE();
        if (OnCancel != null) { OnCancel(null, System.EventArgs.Empty); }
        Close();
    }

    private static void Close()
    {
        if (OnClose != null) { OnClose(null, System.EventArgs.Empty); }
        DestroyImmediate(toolShopFullDiamondAlertor);
    }
}