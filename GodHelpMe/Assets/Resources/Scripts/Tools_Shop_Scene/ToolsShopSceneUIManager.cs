﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PathManager;

public partial class ToolsShopSceneUIManager : MonoBehaviour
{
    private GameObject settingAlertor;

    private void Awake()
    {
        LoadResources();
    }

    private void LoadResources()
    {
        settingAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterface("SettingAlertor"));
    }
}

/// <summary>
/// Open setting alertor.
/// </summary>
public partial class ToolsShopSceneUIManager : MonoBehaviour
{
    public void OpenSettingAlertor()
    {
        if (settingAlertor == null) { return; }

        GameObject instance = Instantiate(settingAlertor, this.transform);
        var rect = instance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }
}

public partial class ToolsShopSceneUIManager : MonoBehaviour
{
    public void UnLoadingScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }        
        StartCoroutine(StartLoading(ProjectKeywordList.kSceneForLobby, this.transform));
    }
}


public partial class ToolsShopSceneUIManager : MonoBehaviour
{
    private IEnumerator StartLoading(string scene, Transform transform)
    {
        if (PlayerAttributeManager.Singleton)
        {            
            PlayerAttributeManager.Singleton.previousSceneName = null;
        }

        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, transform);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }
}
