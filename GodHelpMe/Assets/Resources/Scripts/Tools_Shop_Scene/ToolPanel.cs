﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PathManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
public class ToolButtonStatus
{
    public bool isCellLocked;
    public bool isItemOnPanel;
    public string iconName;
}

public partial class ToolPanel : MonoBehaviour
{

    //=======================================================//

    private int maxminButtonCount = 5;
    private float totalHeight;

    private int spacing = 5;

    private int upspacing = 15;
    private int downspacing = 15;

    private List<ToolButton> toolButtonList;
    public List<int> unLockToolButtonIndexList = new List<int>();
    private int queuAddIndex;

    //private List<ToolButton> lockToolButtons;
    //public List<ToolButton> unLockToolButtons;
    //private List<ToolButtonStatus> unlockValuesListForSave;
    //private List<int> canceledIndexList;


    private GameObject unlockToolCellAlertorObj;

    private GameObject toolButtonObj;
    private TextAsset[] toolItemsData;

    //=======================================================//

    private void Awake()
    {
        LoadResources();
    }

    private void Start()
    {
        StartCoroutine(SetToolButtonPanel());
    }

    private void LoadResources()
    {
        unlockToolCellAlertorObj = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceToolsShopScene("UnlockToolCellAlertor"));
        toolButtonObj = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceToolsShopScene("ToolButton"));
        toolItemsData = Resources.LoadAll<TextAsset>("Data/ToolItems");
    }

    IEnumerator SetToolButtonPanel()
    {
        // if Load list is null, will init new one.
        //lockToolButtons = new List<ToolButton>();
        //unLockToolButtons = new List<ToolButton>();

        //queuAddIndex = PlayerPrefsUtility.LoadValue<int>(ProjectKeywordList.kPPrefsForPlayerQueuAddIndex);
        //canceledIndexList = PlayerPrefsUtility.LoadList<int>(ProjectKeywordList.kPPrefsForPlayerCanceledIndexList);
        //unlockValuesListForSave = PlayerPrefsUtility.LoadList<ToolButtonStatus>(ProjectKeywordList.kPPrefsForPlayerUnlockValuesListForSave);

        // These are syncing to Game Scene.
        //if (PlayerAttributeManager.Singleton)
        //{
        //    if (unlockValuesListForSave.Count >= 0 && PlayerAttributeManager.Singleton.playerItemPanelList.Count == 0)
        //    {
        //        PlayerAttributeManager.Singleton.playerItemPanelList = Enumerable.Repeat<PlayerItem>(null, maxminButtonCount).ToList();
        //        Debug.Log("item panel list Count is " + PlayerAttributeManager.Singleton.playerItemPanelList.Count);
        //    }
        //}

        yield return StartCoroutine(BuildCellOnToolItemPanel());
        yield return StartCoroutine(SyncUnlockCellOnPanelList());
        yield return StartCoroutine(SyncItemOnPanelList());
    }
}

public partial class ToolPanel : MonoBehaviour
{
    IEnumerator BuildCellOnToolItemPanel()
    {
        ToolsShopSceneUIManager USERINTERFACEMANAGER = GameObject.Find("USERINTERFACEMANAGER").GetComponent<ToolsShopSceneUIManager>();
        toolButtonList = new List<ToolButton>();

        for (int index = 0; index < maxminButtonCount; index++)
        {
            GameObject toolButtonInstance = Instantiate(toolButtonObj, transform);
            ToolButton toolButton = toolButtonInstance.GetComponent<ToolButton>();

            if (index <= 1)
            {
                toolButton.isCellLocked = false;
                Debug.Log("toolButton isCellLocked is false");
            }

            toolButton.OnDisOnPanelArgs += OnDisOnPanelCallBack;
            toolButton.onClick.AddListener(() => OpenUnlockToolCellAlertor(toolButton));

            toolButtonList.Add(toolButton);

            RectTransform rect = toolButton.gameObject.transform.GetComponent<RectTransform>();
            totalHeight += (rect.rect.height + spacing);
        }

        RectTransform parentRect = transform.GetComponent<RectTransform>();
        parentRect.sizeDelta = new Vector2(parentRect.rect.width, totalHeight + (upspacing + downspacing));

        yield return new WaitUntil(() => toolButtonList.Count == maxminButtonCount);
        Debug.Log("BuildCellOnToolItemPanel is done.");
        yield break;
    }

    IEnumerator SyncUnlockCellOnPanelList()
    {
        if (PlayerAttributeManager.Singleton == null)
        {
            Debug.LogError("PlayerAttributeManager.Singleton is null");
            yield break;
        }

        if (PlayerAttributeManager.Singleton.UnLockToolButtonIndexList.Count == 0)
        {
            Debug.Log("unLockToolButtonIndexList count is zero.");
            yield break;
        }

        foreach (int index in PlayerAttributeManager.Singleton.UnLockToolButtonIndexList)
        {
            ToolButton toolButton = toolButtonList[index];
            toolButton.isCellLocked = false;
        }

        Debug.Log("SyncUnlockCellOnPanelList is done.");
        yield break;

    }

    IEnumerator SyncItemOnPanelList()
    {
        if (PlayerAttributeManager.Singleton == null)
        {
            Debug.LogError("PlayerAttributeManager.Singleton is null");
            yield break;
        }

        if (PlayerAttributeManager.Singleton.PlayerItemPanelList.Count == 0)
        {
            PlayerItem nullItem = new PlayerItem("null", default(int));
            PlayerAttributeManager.Singleton.PlayerItemPanelList = Enumerable.Repeat<PlayerItem>(nullItem, maxminButtonCount).ToList();
            Debug.Log("item panel list Count is " + PlayerAttributeManager.Singleton.PlayerItemPanelList.Count);
            yield break;
        }
        else
        {
            for (int index = 0; index < PlayerAttributeManager.Singleton.PlayerItemPanelList.Count; index++)
            {
                PlayerItem item = PlayerAttributeManager.Singleton.PlayerItemPanelList[index];
                Debug.Log(item);

                if (PlayerAttributeManager.Singleton.PlayerItemPackage.Count > 0)
                {
                    if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(item.itemId))
                    {
                        foreach (TextAsset TA in toolItemsData)
                        {
                            if (TA.name == item.itemId)
                            {
                                string JSON = TA.text;
                                ItemData itemData = JsonUtility.FromJson<ItemData>(JSON);
                                Sprite icon = Resources.Load<Sprite>(itemData.iconPath);
                                ToolButton toolButton = toolButtonList[index];
                                toolButton.image.sprite = icon;
                                toolButton.isItemOnPanel = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        Debug.Log("SyncItemOnPanelList is done.");
        yield break;

        //if (PlayerAttributeManager.Singleton.playerItemPanelList.Count != maxminButtonCount)
        //{
        //    for (int i = 0; i < maxminButtonCount; i++)
        //    {
        //        if (PlayerAttributeManager.Singleton.playerItemPanelList.ElementAtOrDefault(i) == null)
        //        {
        //            PlayerItem nullItem = new PlayerItem("null", default(int));
        //            PlayerAttributeManager.Singleton.playerItemPanelList.Add(nullItem);
        //        }
        //    }
        //    Debug.Log("item panel list Count is " + PlayerAttributeManager.Singleton.playerItemPanelList.Count);
        //}        
    }
}

/// <summary>
/// The partial is for add item on panel.
/// </summary>
public partial class ToolPanel : MonoBehaviour
{
    public void AddItemOnPanelCallBack(ToolItem item)
    {
        if (toolButtonList.Count != maxminButtonCount)
        {
            return;
        }

        if (PlayerAttributeManager.Singleton == null)
        {
            Debug.LogError("PlayerAttributeManager.Singleton is null");
            return;
        }

        if (PlayerAttributeManager.Singleton.PlayerItemPanelList.Count != maxminButtonCount)
        {
            Debug.LogError("PlayerAttributeManager.Singleton.playerItemPanelList.Count is not equals maxminButtonCount");
            return;
        }

        if (PlayerAttributeManager.Singleton.PlayerItemPackage.Count == 0)
        {
            Debug.LogError("PlayerAttributeManager.Singleton.playerItemPackage is empty");
            return;
        }

        if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(item.id) == false)
        {
            Debug.LogError("PlayerAttributeManager.Singleton.playerItemPackage doesn't include the item");
            return;
        }

        PlayerItem playerItem = PlayerAttributeManager.Singleton.PlayerItemPackage[item.id];

        if (PlayerAttributeManager.Singleton.PlayerItemPanelList.Any(PI => PI != null && PI.itemId == playerItem.itemId))
        {
            Debug.Log("The item already on panel.");
            return;
        }

        foreach (ToolButton toolButton in toolButtonList)
        {
            if (toolButton.isCellLocked == false && toolButton.isItemOnPanel == false)
            {
                int index = toolButtonList.IndexOf(toolButton);
                toolButton.image.sprite = item.icon;
                toolButton.isItemOnPanel = true;

                Debug.Log("index " + index);
                List<PlayerItem> list = PlayerAttributeManager.Singleton.PlayerItemPanelList;
                list[index] = playerItem;
                PlayerAttributeManager.Singleton.PlayerItemPanelList = list;
                break;
            }
        }
    }
}

/// <summary>
/// The part of below is for player dismiss a tool item on panel.
/// </summary>
public partial class ToolPanel : MonoBehaviour
{
    private void OnDisOnPanelCallBack(object obj, EventArgs eventArgs)
    {
        if (PlayerAttributeManager.Singleton == null)
        {
            Debug.LogError("PlayerAttributeManager.Singleton is null");
            return;
        }

        if (PlayerAttributeManager.Singleton.PlayerItemPanelList.Count != maxminButtonCount)
        {
            Debug.LogError("PlayerAttributeManager.Singleton.playerItemPanelList.Count is not equals maxminButtonCount");
            return;
        }

        ToolButton toolButton = (obj as ToolButton);
        toolButton.isItemOnPanel = false;

        int index = toolButtonList.IndexOf(toolButton);

        PlayerItem nullItem = new PlayerItem("null", default(int));
        List<PlayerItem> list = PlayerAttributeManager.Singleton.PlayerItemPanelList;
        list[index] = nullItem;
        PlayerAttributeManager.Singleton.PlayerItemPanelList = list;
    }
}

/// <summary>
/// Open Unlock Tool Cell Alertor.
/// </summary>
public partial class ToolPanel : MonoBehaviour
{
    private ToolButton beUnLockToolCell = null;

    private void OpenUnlockToolCellAlertor(ToolButton toolButton)
    {
        if (toolButton.isCellLocked)
        {
            if (unlockToolCellAlertorObj == null) { return; }

            beUnLockToolCell = toolButton;

            Transform USERINTERFACEMANAGER = GameObject.Find("USERINTERFACEMANAGER").transform;
            GameObject instance = Instantiate(unlockToolCellAlertorObj, USERINTERFACEMANAGER);
            UnlockToolCellAlertor unlockToolCellAlertor = instance.GetComponent<UnlockToolCellAlertor>();
            unlockToolCellAlertor.UnLockComfirm += UnLockComfirm;
            var rect = instance.GetComponent<RectTransform>();
            rect.offsetMin = new Vector2(0, 0);
            rect.offsetMax = new Vector2(0, 0);
        }
    }

    private void UnLockComfirm(object obj, EventArgs e)
    {
        if (PlayerAttributeManager.Singleton == null) { return; }
        UnlockToolCellAlertor alert = obj as UnlockToolCellAlertor;
        if (PlayerAttributeManager.Singleton.PlayerOwnDiamonds >= alert.UnLockPrice)
        {
            PlayerAttributeManager.Singleton.PlayerOwnDiamonds -= alert.UnLockPrice;
            if (SEManager.Singleton)
            {
                SEManager.Singleton.PlayToolItemUnlockSE();
            }
            UnlockToolCellAlertor unlockToolCellAlertor = (obj as UnlockToolCellAlertor);
            StartCoroutine(UnlockToolCell(unlockToolCellAlertor));
        }
        else
        {
            ToolShopFullDiamondAlertor.OnGoDiamondShop += ToolShopFullDiamondAlertor_OnGoDiamondShop;
            ToolShopFullDiamondAlertor.OnCancel += ToolShopFullDiamondAlertor_OnCancel;
            ToolShopFullDiamondAlertor.OnClose += ToolShopFullDiamondAlertor_OnClose;
            Transform USERINTERFACEMANAGER = GameObject.Find("USERINTERFACEMANAGER").transform;
            ToolShopFullDiamondAlertor.Show(USERINTERFACEMANAGER);
        }
    }

    IEnumerator UnlockToolCell(UnlockToolCellAlertor unlockToolCellAlertor)
    {
        yield return StartCoroutine(ResetUnlockToolCellAlertor(unlockToolCellAlertor));
        if (beUnLockToolCell)
        {
            if (beUnLockToolCell.isCellLocked)
            {
                beUnLockToolCell.isCellLocked = false;
            }
            int index = toolButtonList.IndexOf(beUnLockToolCell);

            if (PlayerAttributeManager.Singleton)
            {
                List<int> list = PlayerAttributeManager.Singleton.UnLockToolButtonIndexList;
                list.Add(index);
                PlayerAttributeManager.Singleton.UnLockToolButtonIndexList = list;
            }
        }
        beUnLockToolCell = null;
        yield break;
    }

    IEnumerator ResetUnlockToolCellAlertor(UnlockToolCellAlertor unlockToolCellAlertor)
    {
        unlockToolCellAlertor.UnLockComfirm -= UnLockComfirm;
        DestroyImmediate(unlockToolCellAlertor.gameObject);
        yield break;
    }

    private void ToolShopFullDiamondAlertor_OnGoDiamondShop(object sender, System.EventArgs e)
    {
        if (PlayerAttributeManager.Singleton)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            PlayerAttributeManager.Singleton.previousSceneToDiamondShopName = currentScene.name;
            SceneManager.LoadSceneAsync(ProjectKeywordList.kSceneForDiamondShop);
        }
    }

    private void ToolShopFullDiamondAlertor_OnCancel(object sender, System.EventArgs e)
    {

    }

    private void ToolShopFullDiamondAlertor_OnClose(object sender, System.EventArgs e)
    {
        ToolShopFullDiamondAlertor.OnGoDiamondShop -= ToolShopFullDiamondAlertor_OnGoDiamondShop;
        ToolShopFullDiamondAlertor.OnCancel -= ToolShopFullDiamondAlertor_OnCancel;
        ToolShopFullDiamondAlertor.OnClose -= ToolShopFullDiamondAlertor_OnClose;
    }
}

/// <summary>
/// The partial is for unlock a cell.
/// </summary>
public partial class ToolPanel : MonoBehaviour
{

}