﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathManager;
using UnityEngine;
using UnityEngine.UI;

public partial class ToolButton : Button
{
    private Sprite spriteLock;
    private Sprite spriteUnLock;

    private GameObject cancelBtn;

    protected override void Awake()
    {
        Debug.Log("Button Awake");
        spriteLock = Resources.Load<Sprite>(LoadResourceFilePath.TextureUserInterface("lock"));
        spriteUnLock = Resources.Load<Sprite>(LoadResourceFilePath.TextureUserInterface("unlock"));
        image = transform.GetComponent<Image>();
        image.sprite = (cellLocaked) ? spriteLock : spriteUnLock;
        cancelBtn = transform.Find("CancelButton").gameObject;
        cancelBtn.SetActive(itemOnPanel);
    }

    public string iconName;

    // Default is true.
    private bool cellLocaked = true;
    public bool isCellLocked
    {
        get
        {
            return cellLocaked;
        }
        set
        {
            cellLocaked = value;
            image.sprite = (cellLocaked) ? spriteLock : spriteUnLock;
        }
    }

    // Default is false.    
    private bool itemOnPanel = false;
    public bool isItemOnPanel
    {
        get
        {
            return itemOnPanel;
        }
        set
        {
            itemOnPanel = value;
            if (itemOnPanel == false && isCellLocked == false) { image.sprite = spriteUnLock; }
            cancelBtn.SetActive(itemOnPanel);
        }
    }
}

public partial class ToolButton : Button
{
    public event EventHandler OnDisOnPanelArgs;

    public void DisOnPanel()
    {
        SEManager.Singleton.PlayToolItemUnInstallSE();

        isItemOnPanel = false;
        Debug.Log("DisOnPanel");

        if (OnDisOnPanelArgs != null)
        {
            OnDisOnPanelArgs(this, EventArgs.Empty);
        }
    }
}

//public class ToolButtonEventArgs : System.EventArgs
//{
//    public int index;

//    public ToolButtonEventArgs(int index)
//    {
//        this.index = index;
//    }
//    // ...省略額外參數
//}

//public partial class ToolButton : Button
//{
//    public event EventHandler<ToolButtonEventArgs> buttonSelected;    

//    // 此函數假設由 UI 按鈕呼叫
//    public void OnSelected(int index)
//    {
//        if (this.buttonSelected != null)
//        {
//            this.buttonSelected(this, new ToolButtonEventArgs(index));
//        }
//    }

//    public void DisOnPanel()
//    {
//        Debug.Log("DisOnPanel");
//    }
//}