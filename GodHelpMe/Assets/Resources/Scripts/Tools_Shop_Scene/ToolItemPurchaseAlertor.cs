﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolItemPurchaseAlertor : MonoBehaviour
{
    public static event System.EventHandler OnPurchased;
    public static event System.EventHandler OnCancel;
    public static event System.EventHandler OnClose;

    private static GameObject toolItemPurchaseAlertor;

    public static void Show(int price, Transform USERINTERFACEMANAGER, string tipM = default(string))
    {
        GameObject instance = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterfaceToolsShopScene("ToolItemPurchaseAlertor"));
        toolItemPurchaseAlertor = Instantiate(instance, USERINTERFACEMANAGER);

        Button purchaseButton = toolItemPurchaseAlertor.transform.Find("Background/Content/PurchaseButton").GetComponent<Button>();
        purchaseButton.onClick.AddListener(() => Purchase());

        Button cancelButton = toolItemPurchaseAlertor.transform.Find("Background/Content/CancelButton").GetComponent<Button>();
        cancelButton.onClick.AddListener(() => Cancel());

        Text tip = toolItemPurchaseAlertor.transform.Find("Background/Content/Tip").GetComponent<Text>();
        if (tipM != default(string)) { tip.text = tipM; }

        Text priceText = purchaseButton.transform.Find("Price").GetComponent<Text>();
        priceText.text = price.ToString();

        RectTransform rect = toolItemPurchaseAlertor.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        toolItemPurchaseAlertor.SetActive(true);
    }

    private static void Purchase()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCoinSE();
        }
        if (OnPurchased != null) { OnPurchased(null, System.EventArgs.Empty); }
        Close();
    }

    private static void Cancel()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
        if (OnCancel != null) { OnCancel(null, System.EventArgs.Empty); }
        Close();
    }

    private static void Close()
    {
        if (OnClose != null) { OnClose(null, System.EventArgs.Empty); }
        DestroyImmediate(toolItemPurchaseAlertor);
    }
}
