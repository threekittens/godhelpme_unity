﻿using System.Collections;
using System.Collections.Generic;
using PathManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// The partial is for init.
/// </summary>
public partial class ToolContentBoard : MonoBehaviour
{

    private ToolItem item;

    private int playerOwnCount = 0;

    private Text countText;

    [SerializeField]
    private int eachItemMaxmin = 500;

    public void SetToolContentBoard(ToolItem item)
    {
        this.item = item;

        Image icon = transform.Find("Icon").GetComponent<Image>();
        icon.sprite = item.icon;

        Text name = transform.Find("Name").GetComponent<Text>();
        name.text = item.name;

        Text info = transform.Find("Introduce").GetComponent<Text>();
        info.text = item.info;

        countText = transform.Find("UseButtonAndCountPanel/Count").GetComponent<Text>();

        if (PlayerAttributeManager.Singleton)
        {
            if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(item.id))
            {
                playerOwnCount = PlayerAttributeManager.Singleton.PlayerItemPackage[item.id].itemCount;
            }
        }

        countText.text = playerOwnCount.ToString();

        Text price = transform.Find("Purchase/Price").GetComponent<Text>();
        price.text = item.price.ToString();

        Text purchaseCount = transform.Find("Icon/PurchaseCount").GetComponent<Text>();
        purchaseCount.text = item.purchaseCount.ToString();

        GameObject self = transform.gameObject;
    }
}

/// <summary>
/// The partial is for Touch On Panel.
/// </summary>
public partial class ToolContentBoard : MonoBehaviour
{

    public void OnPanel()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayToolItemInstallSE();
        }

        if (this.playerOwnCount > 0)
        {
            ToolPanel toolPanel = GameObject.Find("UseToolOutlinePanel/UseToolMask/ToolPanel").GetComponent<ToolPanel>();
            toolPanel.AddItemOnPanelCallBack(item);
            Debug.Log("You want on Panel is " + this.item.name);
        }
        else
        {
            Debug.Log("You Can't on Panel this Item" + this.item.name);
        }
    }
}

/// <summary>
/// The part of below is for touched the purchase button, and the delegate of ToolItemPurchaseAlertor
/// </summary>
public partial class ToolContentBoard : MonoBehaviour
{
    public void OnPurchase()
    {
        Transform USERINTERFACEMANAGER = GameObject.Find("USERINTERFACEMANAGER").transform;
        ToolItemPurchaseAlertor.OnPurchased += ToolItemPurchaseAlertor_OnPurchased;
        ToolItemPurchaseAlertor.OnCancel += ToolItemPurchaseAlertor_OnCancel;
        ToolItemPurchaseAlertor.OnClose += ToolItemPurchaseAlertor_OnClose;
        ToolItemPurchaseAlertor.Show(this.item.price, USERINTERFACEMANAGER);
    }

    private void ToolItemPurchaseAlertor_OnPurchased(object sender, System.EventArgs e)
    {
        Debug.Log("The Item Price is " + this.item.price);

        if (PlayerAttributeManager.Singleton)
        {
            if ((playerOwnCount + item.purchaseCount) > eachItemMaxmin)
            {
                //Debug.Log("Sorry!, You can't purchase the Item, because the Item maxmin is 500");
                //GameObject warnAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterface("WarnAlertor"));
                //warnAlertor = Instantiate(warnAlertor, GameObject.Find("USERINTERFACEMANAGER").transform);
                //Text tip = warnAlertor.transform.Find("Background/Image/Tip").GetComponent<Text>();

                //tip.text = "Note! You Can't Buy more then " + eachItemMaxmin;

                //var rect = warnAlertor.GetComponent<RectTransform>();
                //rect.offsetMin = new Vector2(0, 0);
                //rect.offsetMax = new Vector2(0, 0);

                WarnAlertor.OnClosed += WarnAlertor_OnClosed;
                string m = string.Format("注意！您不可以購買超過{0}個哦！", eachItemMaxmin);
                WarnAlertor.Show(m, this.transform);

                return;
            }

            if (PlayerAttributeManager.Singleton.PlayerOwnDiamonds >= item.price)
            {
                PlayerAttributeManager.Singleton.PlayerOwnDiamonds = PlayerAttributeManager.Singleton.PlayerOwnDiamonds - item.price;
                playerOwnCount = playerOwnCount + item.purchaseCount;
                countText.text = playerOwnCount.ToString();
                Debug.Log("You got " + playerOwnCount + " already.");

                // These are syncing to Game Scene.
                if (PlayerAttributeManager.Singleton)
                {
                    PlayerItem playerItem = new PlayerItem(item.id, playerOwnCount);

                    if (PlayerAttributeManager.Singleton.PlayerItemPackage.ContainsKey(item.id) == false)
                    {
                        Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                        PIP.Add(item.id, playerItem);
                        PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;
                    }
                    else
                    {
                        Dictionary<string, PlayerItem> PIP = PlayerAttributeManager.Singleton.PlayerItemPackage;
                        PIP[item.id] = playerItem;
                        PlayerAttributeManager.Singleton.PlayerItemPackage = PIP;
                    }

                    //foreach (KeyValuePair<string, PlayerItem> item in PlayerAttributeManager.Singleton.PlayerItemPackage)
                    //{
                    //    Debug.Log(item.Key + " => " + item.Value);
                    //}

                    //PlayerPrefsUtility.SaveDict<string, PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPackage, PlayerAttributeManager.Singleton.PlayerItemPackage);
                }

                return;
            }
            else
            {
                //Debug.LogWarning("You Can't on Panel this item, because you hanv not enugh dimaonds");
                //GameObject toolShopFullDiamondAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceToolsShopScene("ToolShopFullDiamondAlertor"));
                //toolShopFullDiamondAlertor = Instantiate(toolShopFullDiamondAlertor, GameObject.Find("USERINTERFACEMANAGER").transform);

                //var rect = toolShopFullDiamondAlertor.GetComponent<RectTransform>();
                //rect.offsetMin = new Vector2(0, 0);
                //rect.offsetMax = new Vector2(0, 0);
                ToolShopFullDiamondAlertor.OnGoDiamondShop += ToolShopFullDiamondAlertor_OnGoDiamondShop;
                ToolShopFullDiamondAlertor.OnCancel += ToolShopFullDiamondAlertor_OnCancel;
                ToolShopFullDiamondAlertor.OnClose += ToolShopFullDiamondAlertor_OnClose;
                Transform USERINTERFACEMANAGER = GameObject.Find("USERINTERFACEMANAGER").transform;
                ToolShopFullDiamondAlertor.Show(USERINTERFACEMANAGER);
            }
        }
    }

    private void WarnAlertor_OnClosed(object sender, System.EventArgs e)
    {
        WarnAlertor.OnClosed -= WarnAlertor_OnClosed;
    }

    private void ToolItemPurchaseAlertor_OnCancel(object sender, System.EventArgs e)
    {

    }

    private void ToolItemPurchaseAlertor_OnClose(object sender, System.EventArgs e)
    {
        ToolItemPurchaseAlertor.OnPurchased -= ToolItemPurchaseAlertor_OnPurchased;
        ToolItemPurchaseAlertor.OnCancel -= ToolItemPurchaseAlertor_OnCancel;
        ToolItemPurchaseAlertor.OnClose -= ToolItemPurchaseAlertor_OnClose;
    }
}

/// <summary>
/// The part of below is for the delegate of ToolShopFullDiamondAlertor
/// </summary>
public partial class ToolContentBoard : MonoBehaviour
{
    private void ToolShopFullDiamondAlertor_OnGoDiamondShop(object sender, System.EventArgs e)
    {
        if (PlayerAttributeManager.Singleton)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            PlayerAttributeManager.Singleton.previousSceneToDiamondShopName = currentScene.name;
            StartLoading(ProjectKeywordList.kSceneForDiamondShop, GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER).transform);
        }
    }

    private void ToolShopFullDiamondAlertor_OnCancel(object sender, System.EventArgs e)
    {

    }

    private void ToolShopFullDiamondAlertor_OnClose(object sender, System.EventArgs e)
    {
        ToolShopFullDiamondAlertor.OnGoDiamondShop -= ToolShopFullDiamondAlertor_OnGoDiamondShop;
        ToolShopFullDiamondAlertor.OnCancel -= ToolShopFullDiamondAlertor_OnCancel;
        ToolShopFullDiamondAlertor.OnClose -= ToolShopFullDiamondAlertor_OnClose;
    }

    private IEnumerator StartLoading(string scene, Transform transform)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, transform);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }
}