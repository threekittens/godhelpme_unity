﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class UnlockToolCellAlertor : MonoBehaviour
{
    [SerializeField]
    private Text price;

    [SerializeField]
    private int unLockPrice;
    public int UnLockPrice { get { return unLockPrice; } }
    public event System.EventHandler UnLockComfirm;

    private void Start()
    {
        price.text = UnLockPrice.ToString();
    }

    private void OnEnable()
    {

    }
}

public partial class UnlockToolCellAlertor : MonoBehaviour
{
    public void Close()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
        DestroyImmediate(this.gameObject);
    }
}

public partial class UnlockToolCellAlertor : MonoBehaviour
{
    public void UnLock()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCoinSE();
        }

        if (UnLockComfirm != null)
        {
            UnLockComfirm(this, System.EventArgs.Empty);
        }
    }
}