﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PathManager;
using UnityEngine.UI;

/// <summary>
/// The partial is for define parameter.
/// </summary>
public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{
    [SerializeField]
    internal string heroID;
    [SerializeField]
    internal GameObject characterPlatform;
}


/// <summary>
/// The partial is for Script start.
/// </summary>
public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{
    private void Start()
    {
        BuildHeroDataByID();
    }

    private void Update()
    {
        RotationCharacterPlatform();
    }
}

/// <summary>
/// The partial is for build hero by id.
/// </summary>
public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{

    private void BuildHeroDataByID()
    {
        if (PlayerAttributeManager.Singleton) { heroID = PlayerAttributeManager.Singleton.ChoosedHeroID; }

        if (heroID == default(string))
        {
            Debug.LogWarning("heroID = " + default(string));
            return;
        }

        TextAsset JSONFile = Resources.Load<TextAsset>(string.Format("Data/Heros/{0}", heroID));
        string JSON = JSONFile.text;
        HeroData heroData = JsonUtility.FromJson<HeroData>(JSON);

        if (heroData.id == heroID)
        {
            Text title = transform.Find("TitlePanel/Title").GetComponent<Text>();
            title.text = heroData.name;

            GameObject skillButtonBar = transform.Find("SkillIntroducePanel/SkillButtonBar").gameObject;
            GameObject skillButtonObj = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceHeroSkillIntroduceScene("SkillButton"));

            for (int i = 0; i < heroData.skills.Length; i++)
            {
                HeroSkillData skillData = heroData.skills[i];
                Button skillButton = Instantiate(skillButtonObj, skillButtonBar.transform).GetComponent<Button>();

                Sprite sprite = Resources.Load<Sprite>(skillData.iconPath);
                skillButton.image.sprite = sprite;

                string name = skillData.name;
                string introduce = skillData.info;

                if (i == 0)
                {
                    Text skillName = transform.Find("SkillIntroducePanel/SkillName").GetComponent<Text>();
                    skillName.text = name;

                    Text skillIntroduce = transform.Find("SkillIntroducePanel/SkillIntroduce").GetComponent<Text>();
                    skillIntroduce.text = introduce;
                }

                skillButton.onClick.AddListener(() => skillButtonEvent(name, introduce));
            }

            GameObject heroObj = Resources.Load<GameObject>(heroData.prefabPath);
            GameObject hero = Instantiate(heroObj, characterPlatform.transform);
            hero.transform.Find("MinimapPlayerMark").gameObject.SetActive(false);
            MonoBehaviour[] c_array = hero.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour c in c_array) { c.enabled = false; }

            characterPlatform.transform.SetPositionAndRotation(new Vector3(0, 0, 0), Quaternion.Euler(new Vector3(0, 180, 0)));

            Vector3 position = Vector3.zero;
            Vector3 euler = Vector3.zero;

            switch (heroData.id)
            {
                case "5A631C1A-112F6A0-34500C96":
                    position = new Vector3(-3f, -2.5f, -3f);
                    break;
                case "5A631D54-15A2BCA-FC7267A":
                    position = new Vector3(-3f, -2.5f, -3f);
                    euler = new Vector3(12.5f, 0, 0);
                    break;
                case "5A631E08-19B7EE0-114E1422":
                    position = new Vector3(-3f, -3f, -3f);
                    break;
                case "5A631EA5-1E2B40A-1C351B77":
                    position = new Vector3(-3f, -1f, -3f);
                    break;
            }

            hero.transform.localPosition = Vector3.zero;
            characterPlatform.transform.position += position;
            characterPlatform.transform.Rotate(euler);
        }
    }
}

public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{
    private void RotationCharacterPlatform()
    {
        if (characterPlatform)
        {
            characterPlatform.transform.Rotate(new Vector3(0.0f, -(Input.GetAxis(ProjectKeywordList.kAxisForHorizontal)) * 10, 0.0f));
        }
    }
}

/// <summary>
/// The partial is for skill button event.
/// </summary>
public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{

    private void skillButtonEvent(string name, string introduce)
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }

        Text skillName = transform.Find("SkillIntroducePanel/SkillName").GetComponent<Text>();
        skillName.text = name;

        Text skillIntroduce = transform.Find("SkillIntroducePanel/SkillIntroduce").GetComponent<Text>();
        skillIntroduce.text = introduce;
    }
}


/// <summary>
/// The partial is for call OnDisable and OnDestory.
/// </summary>
public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{

    private void OnDisable()
    {
        Debug.Log("OnDisable");
        Resources.UnloadUnusedAssets();
    }

    private void OnDestroy()
    {
        Debug.Log("OnDestroy");
        Resources.UnloadUnusedAssets();
    }
}

/// <summary>
/// The partial is for Close.
/// </summary>
public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{
    private void UnLoadingScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
        StartCoroutine(StartLoading(ProjectKeywordList.kSceneForCharacterSelection, this.transform));
    }
}

public partial class HeroSkillIntroduceSceneUIManager : MonoBehaviour
{
    private IEnumerator StartLoading(string scene, Transform transform)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, transform);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }
}
