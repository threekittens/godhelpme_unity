﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{
    private VituralJoystick mMoveJoystick;
    private CharacterController mController;
    private Animator mAnimator;

    private Vector3 dir = Vector3.zero;

    [SerializeField]
    internal float mGravity = 1;

    [SerializeField]
    internal float moveSpeed = 3.0F;

    [SerializeField]
    private float animSpeed = 3.0F;

    private int staySecond = 0;
    private int stayLimmitSecond = 30;
    private bool isStayContdownStatus = false;

    private TimeSpan time;

    private void Start()
    {
        mMoveJoystick = GameObject.Find("Joystick").GetComponent<VituralJoystick>();
        mAnimator = transform.GetComponent<Animator>();
        mController = GetComponent<CharacterController>();
        InvokeRepeating("CountdownSeconds", 0, 1);
    }

    private void FixedUpdate()
    {
        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");

        if (dir.magnitude > 1)
        {
            dir.Normalize();
        }

        if (mMoveJoystick.InputDirection != Vector3.zero)
        {
            dir = mMoveJoystick.InputDirection;
        }

        if (Mathf.Abs(dir.x) > 0.1f || Mathf.Abs(dir.z) > 0.1f)
        {
            Vector3 controllDirection = new Vector3(Mathf.Round(dir.x * moveSpeed), 0.0f, Mathf.Round(dir.z * moveSpeed));
            controllDirection.y -= mGravity * Time.deltaTime;
            gameObject.transform.LookAt(controllDirection + gameObject.transform.position);
            mController.SimpleMove(controllDirection);

            isStayContdownStatus = false;
            staySecond = 0;

            GameObject skillProgressBarInstance = GameObject.FindGameObjectWithTag("SkillProgressBar");
            if (skillProgressBarInstance)
            {
                return;
            }

            // 播放 动画      
            StartCoroutine(PlayingMoveAnimation(true));

            GameObject homeProgressBarInstance = GameObject.FindGameObjectWithTag("HomeProgressBar");
            if (homeProgressBarInstance)
            {
                transform.Find("HomeTransporter").gameObject.SetActive(false);
                homeProgressBarInstance.SetActive(false);
            }

        }
        else
        {
            isStayContdownStatus = true;
            StartCoroutine(PlayingMoveAnimation(false));
        }
    }

    bool AnimatorIsPlaying()
    {
        return mAnimator.GetCurrentAnimatorStateInfo(0).length >
               mAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    private void CountdownSeconds()
    {
        if (staySecond > stayLimmitSecond)
        {
            mAnimator.SetBool(ProjectKeywordList.kAnimationForEffect, true);
            staySecond = 0;
        }
        staySecond++;
    }

    IEnumerator PlayingMoveAnimation(bool isPlay)
    {
        if (mAnimator)
        {
            //Debug.Log(mAnimator.layerCount);
            mAnimator.SetBool(ProjectKeywordList.kAnimationForMove, isPlay);
        }
        yield break;
    }

    IEnumerator DelayedFiveScond()
    {
        TimeSpan now = TimeSpan.FromTicks(DateTime.Now.Ticks);

        yield return new WaitForSeconds(5);

        TimeSpan after = TimeSpan.FromTicks(DateTime.Now.Ticks);

        TimeSpan diff = after - now;
        double seconds = diff.TotalSeconds;
    }
}
