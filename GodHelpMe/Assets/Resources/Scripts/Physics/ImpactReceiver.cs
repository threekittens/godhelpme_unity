﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactReceiver : MonoBehaviour
{
    [SerializeField]
    private float serength = 0.5f;
    float mass = 3.0F; // defines the character mass
    Vector3 impact = Vector3.zero;
    private Vector3 forwardEndPos;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        Debug.Log("Started Initialize the ImpactReceiver...");
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void FixedUpdate()
    {
        float step = serength * Time.deltaTime;

        // apply the impact force:
        if (impact.magnitude > 0.2f)
        {
            transform.LookAt(forwardEndPos);
            transform.position = Vector3.Lerp(transform.position, forwardEndPos, step);
        }

        // consumes the impact energy each cycle:
        impact = Vector3.Lerp(impact, Vector3.zero, step);
    }

    // call this function to add an impact force:
    public void AddImpact(Vector3 dir, float force)
    {
        //dir.Normalize();
        //if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        //impact += dir.normalized * force / mass;
        forwardEndPos = transform.position + dir * force;
        impact = forwardEndPos;
    }
}
