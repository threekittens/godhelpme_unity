﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Part of below is for define parameter.
/// </summary>
public partial class SkillsJoystickPanel : MonoBehaviour
{
    internal delegate void PlayerUpdatedLevel(int currentLevel);
    internal PlayerUpdatedLevel OnPlayerUpdateLevel;

    internal delegate void PlayerCancelCastSkill(bool IsCast);
    internal PlayerCancelCastSkill OnPlayerCancelCastSkill;

    internal int SkillPoint { get { return mSkillPoint; } set { mSkillPoint = value; } }
    private int mSkillPoint = 0;

    internal bool IsCancelCast { get { return mIsCancelCast; } set { mIsCancelCast = value; } }
    private bool mIsCancelCast;

    internal string heroID;
    private GameObject[] mSkillJoysticks = new GameObject[4];
    private GameObject[] mSkillJoystickImages = new GameObject[4];
    private GameObject[] mSkillUpLevelButtons = new GameObject[3];
    //private TextAsset skillDataJSONFile;
    private HeroMultipleSkillDatas mHeroMultipleSkillDatas;
}

public partial class SkillsJoystickPanel : MonoBehaviour
{
    private void Start()
    {
        //mHeroID = "5A631C1A-112F6A0-34500C96";
        //InitializetionSolutionComponent();
        OnPlayerUpdateLevel = OnPlayerUpdatedLevel;
        OnPlayerCancelCastSkill = OnPlayerCanceledCastSkill;
    }

    private void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }
}

public partial class SkillsJoystickPanel : MonoBehaviour
{

    internal void InitializetionSolutionComponent()
    {

        TextAsset skillDataJSONFile = Resources.Load<TextAsset>(string.Format("Data/Skills/{0}", heroID));
        string JSON = skillDataJSONFile.text;
        mHeroMultipleSkillDatas = JsonUtility.FromJson<HeroMultipleSkillDatas>(JSON);

        for (int i = 0; i < transform.childCount; i++)
        {            
            mSkillJoysticks[i] = transform.GetChild(i).gameObject;
            mSkillJoystickImages[i] = mSkillJoysticks[i].transform.GetChild(0).gameObject;
            SingleSkillData singleSkillData = mHeroMultipleSkillDatas.skillDatas[i];

            SkillArea skillArea = mSkillJoystickImages[i].GetComponent<SkillArea>();
            skillArea.areaType = (SkillAreaType)singleSkillData.areaType;
            skillArea.outerRadius = singleSkillData.outerRadius;
            skillArea.innerRadius = singleSkillData.innerRadius;
            skillArea.cubeWidth = singleSkillData.cubeWidth;
            skillArea.angle = singleSkillData.angle;

            DetectEnemy detectEnemy = mSkillJoystickImages[i].GetComponent<DetectEnemy>();
            detectEnemy.range = singleSkillData.detectRange;
            detectEnemy.maxTargets = singleSkillData.maximumTargets;
            detectEnemy.skillType = (SkillType)singleSkillData.skillType;

            if (i > 0)
            {
                //Debug.Log(i);
                int x = i - 1;
                mSkillUpLevelButtons[x] = mSkillJoysticks[i].transform.Find("SkillUpLevelButton").gameObject;
            }
        }
    }
}

public partial class SkillsJoystickPanel : MonoBehaviour
{
    private void OnPlayerUpdatedLevel(int currentLevel)
    {
        mSkillPoint = mSkillPoint + 1;

        mSkillUpLevelButtons[0].SetActive(true);

        if ((currentLevel % 2) == 0)
        {
            mSkillUpLevelButtons[1].SetActive(true);
        }

        if ((currentLevel % 3) == 0)
        {
            mSkillUpLevelButtons[2].SetActive(true);
        }
    }
}

public partial class SkillsJoystickPanel : MonoBehaviour
{

    private void OnPlayerCanceledCastSkill(bool IsCast)
    {
        mIsCancelCast = IsCast;
        //Debug.Log("mIsCancelCast " + mIsCancelCast);
        //Debug.Log("IsCancelCast " + IsCancelCast);
    }
}

public partial class SkillsJoystickPanel : MonoBehaviour
{

}

public partial class SkillsJoystickPanel : MonoBehaviour
{

}

public partial class SkillsJoystickPanel : MonoBehaviour
{

}


