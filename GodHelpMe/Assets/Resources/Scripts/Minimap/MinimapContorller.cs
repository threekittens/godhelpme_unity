﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MinimapContorller : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler
{
    //private Camera mMiniCamera;
    //private Vector3 portalExitSize;
    //private Vector3 localCursor;

    private Transform mPoint;
    private Vector2 mPointOffset;
    private Camera mMainCamera;
    private Vector3 mMainCameraOriginallyOffset;
    private PolygonCollider2D mPolygonCollider2D;
    private ThirdCamera mThirdCamera;
    private Vector3 mPointOringlellyPosition;

    [SerializeField]
    private Vector3 mMapSize;

    [SerializeField]
    private Vector2 mMinimapSize;

    private void Start()
    {
        //mMiniCamera = GameObject.Find("MinimapCamera").GetComponent<Camera>();
        //portalExitSize = new Vector3(mMiniCamera.targetTexture.width, mMiniCamera.targetTexture.height, 0);

        mMinimapSize = transform.GetComponent<RectTransform>().sizeDelta;

        //Debug.Log(string.Format("mMinimapSize {0}", mMinimapSize));

        mPoint = transform.Find("MinimapBackground/MinimapOutline/MinimapRawImage/Point");
        mPolygonCollider2D = transform.Find("MinimapBackground").GetComponent<PolygonCollider2D>();

        mMainCamera = Camera.main;
        mThirdCamera = mMainCamera.GetComponent<ThirdCamera>();
        mMainCameraOriginallyOffset = mMainCamera.transform.position;
        ScanPointPosition();
        InitPointPosition();
    }

    private void ScanPointPosition()
    {
        mPointOringlellyPosition = Vector3.zero;

        RaycastHit hit;
        Vector2 v = new Vector2(Screen.width / 2, Screen.height / 2);
        if (Physics.Raycast(Camera.main.ScreenPointToRay(v), out hit))
        {
            mPointOringlellyPosition = hit.point;
        }
    }

    private void InitPointPosition()
    {
        Vector2 targetPosition = new Vector2(mPointOringlellyPosition.x * mMinimapSize.x / mMapSize.x, mPointOringlellyPosition.z * mMinimapSize.y / mMapSize.z);
        mPoint.localPosition = targetPosition;
        mPointOffset = targetPosition;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        mThirdCamera.enabled = true;
        InitPointPosition();
    }

    public void OnPointerDown(PointerEventData eventData)
    {

        if (!mPolygonCollider2D.OverlapPoint(eventData.position)) { return; }

        mThirdCamera.enabled = false;
        mPoint.position = eventData.position;
        SetCameraPosition(mPoint.transform.localPosition);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!mPolygonCollider2D.OverlapPoint(eventData.position)) { return; }

        mThirdCamera.enabled = false;
        mPoint.position = eventData.position;
        SetCameraPosition(mPoint.transform.localPosition);
    }

    private void SetCameraPosition(Vector2 vec)
    {
        vec = vec - mPointOffset;
        Vector3 targetPosition = new Vector3(mMapSize.x * vec.x / mMinimapSize.x, 0, mMapSize.z * vec.y / mMinimapSize.y) + mMainCameraOriginallyOffset;
        Camera.main.transform.position = targetPosition;
    }
}
