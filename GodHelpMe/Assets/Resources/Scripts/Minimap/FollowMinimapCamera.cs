﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMinimapCamera : MonoBehaviour
{

    private Camera mMiniCamera;

    private void Start()
    {
        if (GameObject.Find("MinimapCamera"))
        {
            mMiniCamera = GameObject.Find("MinimapCamera").GetComponent<Camera>();
        }
    }

    private void Update()
    {
        StartCoroutine(lookAtCamera());
    }

    IEnumerator lookAtCamera()
    {
        if (mMiniCamera)
        {
            Vector3 mLocalPosition = transform.position;
            Quaternion mCameraRotation = mMiniCamera.transform.rotation;
            Vector3 mLookAtDirection = (mLocalPosition + (mCameraRotation * Vector3.forward));
            transform.LookAt(mLookAtDirection);
        }

        yield break;
    }
}
