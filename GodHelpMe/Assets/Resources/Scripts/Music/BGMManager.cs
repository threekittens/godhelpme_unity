﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class BGMManager : MonoBehaviour
{
    private static BGMManager instance;
    public static BGMManager Singleton { get { return instance; } }

    [SerializeField]
    private AudioSource audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerBGMManagerVolume))
            {
                audioSource.volume = PlayerPrefsUtility.LoadValue<float>(ProjectKeywordList.kPPrefsForPlayerBGMManagerVolume);
            }
            else
            {
                audioSource.volume = 1.0f;
            }

            if (PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerBGMManagerMute))
            {
                audioSource.mute = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerBGMManagerMute);
            }
            else
            {
                audioSource.mute = false;
            }

            return audioSource;
        }
        set
        {
            //Debug.Log("Update BGMManager");
            audioSource = value;
            PlayerPrefsUtility.SaveValue<float>(ProjectKeywordList.kPPrefsForPlayerBGMManagerVolume, audioSource.volume);
            PlayerPrefsUtility.SaveValue<bool>(ProjectKeywordList.kPPrefsForPlayerBGMManagerMute, audioSource.mute);
        }
    }

    [SerializeField]
    private AudioClip loginSceneMusic;
    [SerializeField]
    private AudioClip lobySceneMusic;
    [SerializeField]
    private AudioClip selectHeroSceneMusic;
    [SerializeField]
    private AudioClip gameSceneMusic;

    private bool playStatus = false;

    private string privousSceneName;

    private void Awake()
    {
        if (instance != null)
        {
            SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
            DestroyImmediate(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        instance = this;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void Start()
    {
        AudioSource a = AudioSource;
        AudioSource = a;
    }

    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("privousScene = scene " + privousSceneName);
        if (privousSceneName == scene.name) { return; }

        switch (scene.name)
        {
            case "Login_Scene":
                audioSource.clip = trimSilence(loginSceneMusic);
                playStatus = false;
                privousSceneName = "";
                break;
            case "Create_New_Player_Scene":
                break;
            case "Lobby_Scene":
                audioSource.clip = trimSilence(lobySceneMusic);
                playStatus = false;
                privousSceneName = scene.name;
                break;
            case "Character_Selection_Scene":
                audioSource.clip = trimSilence(selectHeroSceneMusic);
                playStatus = false;
                privousSceneName = "";
                break;
            case "Game_Scene":
                audioSource.clip = trimSilence(gameSceneMusic);
                playStatus = false;
                privousSceneName = "";
                break;
            default:
                playStatus = true;
                break;
        }

        if (!playStatus)
        {
            audioSource.Play();
            playStatus = true;
        }
    }

    /// <summary>
    /// Trims silence from both ends in an AudioClip.
    /// Makes mp3 files seamlessly loopable.
    /// </summary>
    /// <param name="inputAudio"></param>
    /// <param name="threshold"></param>
    /// <returns></returns>
    AudioClip trimSilence(AudioClip inputAudio, float threshold = 0.05f)
    {
        // Copy samples from input audio to an array. AudioClip uses interleaved format so the length in samples is multiplied by channel count
        float[] samplesOriginal = new float[inputAudio.samples * inputAudio.channels];
        inputAudio.GetData(samplesOriginal, 0);
        // Find first and last sample (from any channel) that exceed the threshold
        int audioStart = Array.FindIndex(samplesOriginal, sample => sample > threshold),
            audioEnd = Array.FindLastIndex(samplesOriginal, sample => sample > threshold);
        // Copy trimmed audio data into another array
        float[] samplesTrimmed = new float[audioEnd - audioStart];
        Array.Copy(samplesOriginal, audioStart, samplesTrimmed, 0, samplesTrimmed.Length);
        // Create new AudioClip for trimmed audio data
        AudioClip trimmedAudio = AudioClip.Create(inputAudio.name, samplesTrimmed.Length / inputAudio.channels, inputAudio.channels, inputAudio.frequency, false);
        trimmedAudio.SetData(samplesTrimmed, 0);
        return trimmedAudio;
    }
}
