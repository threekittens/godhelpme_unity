﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class SEManager : MonoBehaviour
{
    private static SEManager instance;
    public static SEManager Singleton { get { return instance; } }

    [SerializeField]
    private AudioSource audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerSEManagerVolume))
            {
                audioSource.volume = PlayerPrefsUtility.LoadValue<float>(ProjectKeywordList.kPPrefsForPlayerSEManagerVolume);
            }
            else
            {
                audioSource.volume = 1.0f;
            }

            if (PlayerPrefs.HasKey(ProjectKeywordList.kPPrefsForPlayerSEManagerMute))
            {
                audioSource.mute = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerSEManagerMute);
            }
            else
            {
                audioSource.mute = false;
            }
            return audioSource;
        }
        set
        {
            //Debug.Log("Update SEManager");
            audioSource = value;
            PlayerPrefsUtility.SaveValue<float>(ProjectKeywordList.kPPrefsForPlayerSEManagerVolume, audioSource.volume);
            PlayerPrefsUtility.SaveValue<bool>(ProjectKeywordList.kPPrefsForPlayerSEManagerMute, audioSource.mute);
        }
    }

    [SerializeField]
    private AudioClip seDecide;
    [SerializeField]
    private AudioClip seCancel;
    [SerializeField]
    private AudioClip seOpenBook;
    [SerializeField]
    private AudioClip seToolItemInstall;
    [SerializeField]
    private AudioClip seToolItemUnInstall;
    [SerializeField]
    private AudioClip seToolItemUnlock;
    [SerializeField]
    private AudioClip seCoin;
    [SerializeField]
    private AudioClip seRewardAlert;
    [SerializeField]
    private AudioClip seSelectHeroLock;
    [SerializeField]
    private AudioClip seChoose;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    private void Start()
    {
        AudioSource a = AudioSource;
        AudioSource = a;
    }

    public void PlayDecideSE()
    {
        audioSource.PlayOneShot(seDecide);
    }

    public void PlayCancelSE()
    {
        audioSource.PlayOneShot(seCancel);
    }

    public void PlayOpenBookSE()
    {
        audioSource.PlayOneShot(seOpenBook);
    }

    public void PlayToolItemInstallSE()
    {
        audioSource.PlayOneShot(seToolItemInstall);
    }

    public void PlayToolItemUnInstallSE()
    {
        audioSource.PlayOneShot(seToolItemUnInstall);
    }

    public void PlayToolItemUnlockSE()
    {
        audioSource.PlayOneShot(seToolItemUnlock);
    }

    public void PlayCoinSE()
    {
        audioSource.PlayOneShot(seCoin);
    }

    public void PlayRewardAlertSE()
    {
        audioSource.PlayOneShot(seRewardAlert);
    }

    public void PlaySelectHeroLockSE()
    {
        audioSource.PlayOneShot(seSelectHeroLock);
    }

    public void PlayChooseSE()
    {
        audioSource.PlayOneShot(seChoose);
    }

    public void PlayAudioClipOnce(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}
