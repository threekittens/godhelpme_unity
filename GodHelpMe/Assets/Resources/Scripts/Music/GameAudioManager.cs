﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class GameAudioManager : MonoBehaviour
{

    [SerializeField]
    private bool openSeamlessLooping;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = transform.GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefsUtility.LoadValue<float>(ProjectKeywordList.kPPrefsForPlayerSEManagerVolume);
        audioSource.mute = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerSEManagerMute);
        audioSource.Stop();
        audioSource.Play();
    }

    private void Start()
    {
        if (openSeamlessLooping)
        {
            PlaySeamlessLoopingMP3Audio();
        }
    }

    private void Update()
    {
        audioSource.volume = PlayerPrefsUtility.LoadValue<float>(ProjectKeywordList.kPPrefsForPlayerSEManagerVolume);
        audioSource.mute = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerSEManagerMute);
    }

    public void PlaySeamlessLoopingMP3Audio()
    {
        AudioClip currentClip = audioSource.clip;
        AudioClip seamlessLoopingClip = trimSilence(currentClip);
        audioSource.clip = seamlessLoopingClip;
        audioSource.Stop();
        audioSource.Play();
    }

    /// <summary>
    /// Trims silence from both ends in an AudioClip.
    /// Makes mp3 files seamlessly loopable.
    /// </summary>
    /// <param name="inputAudio"></param>
    /// <param name="threshold"></param>
    /// <returns></returns>
    private AudioClip trimSilence(AudioClip inputAudio, float threshold = 0.05f)
    {
        // Copy samples from input audio to an array. AudioClip uses interleaved format so the length in samples is multiplied by channel count
        float[] samplesOriginal = new float[inputAudio.samples * inputAudio.channels];
        inputAudio.GetData(samplesOriginal, 0);
        // Find first and last sample (from any channel) that exceed the threshold
        int audioStart = Array.FindIndex(samplesOriginal, sample => sample > threshold),
            audioEnd = Array.FindLastIndex(samplesOriginal, sample => sample > threshold);
        // Copy trimmed audio data into another array
        float[] samplesTrimmed = new float[audioEnd - audioStart];
        Array.Copy(samplesOriginal, audioStart, samplesTrimmed, 0, samplesTrimmed.Length);
        // Create new AudioClip for trimmed audio data
        AudioClip trimmedAudio = AudioClip.Create(inputAudio.name, samplesTrimmed.Length / inputAudio.channels, inputAudio.channels, inputAudio.frequency, false);
        trimmedAudio.SetData(samplesTrimmed, 0);
        return trimmedAudio;
    }
}
