﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PathManager;

public enum PointsType
{
    Boss = 0,
    Enemy = 1,
    RepaireTower = 2,
    TowerUpLevel = 3
}

public enum PointsPlayer
{
    Player01 = 0,
    Player02 = 1,
    Player03 = 2
}

public class PointsPanel : MonoBehaviour
{

    [SerializeField]
    private Sprite monsterBossSprite;
    [SerializeField]
    private Sprite monsterSmallSprite;
    [SerializeField]
    private Sprite repairSprite;
    [SerializeField]
    private Sprite upgradSprite;

    private Image typeImage;
    private Sprite typeSprite;
    private Text typeName;

    private string text;

    //private Text playerPoint;
    //private int defaultPoint = 0;

    //internal PointsType pointsType;

    //internal PointsPlayer pointsPlayer;    

    internal int playerCount = 1;

    internal Dictionary<int, GradeInfomation> dictionary;

    internal void Set(PointsType pointsType)
    {
        switch (pointsType)
        {
            case PointsType.Boss:
                typeSprite = monsterBossSprite;
                text = "首領";
                break;
            case PointsType.Enemy:
                typeSprite = monsterSmallSprite;
                text = "一般魔物";
                break;
            case PointsType.RepaireTower:
                typeSprite = repairSprite;
                text = "修護守護塔";
                break;
            case PointsType.TowerUpLevel:
                typeSprite = upgradSprite;
                text = "升級守護塔";
                break;
        }

        typeImage = transform.Find("KindPanel/Image").GetComponent<Image>();
        typeImage.sprite = typeSprite;

        typeName = transform.Find("KindPanel/KindName").GetComponent<Text>();
        typeName.text = text;

        for (int i = 0; i < playerCount; i++)
        {
            PointsPlayer pointsPlayer = (PointsPlayer)i;
            string player = pointsPlayer.ToString();

            int point = 0;

            switch (pointsType)
            {
                case PointsType.Boss:
                    point = dictionary[i].killboss * 100;
                    break;
                case PointsType.Enemy:
                    point = dictionary[i].killenemy * 10;
                    break;
                case PointsType.RepaireTower:
                    point = dictionary[i].repairtower * 30;
                    break;
                case PointsType.TowerUpLevel:
                    point = dictionary[i].uptower * 125;
                    break;
            }

            Text playerPoint = transform.Find(string.Format("PointBoard/{0}/Text", player)).GetComponent<Text>();
            playerPoint.text = point.ToString();
        }
    }
}
