﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class Buff : MonoBehaviour
{
    internal delegate void BuffTimeOut();
    internal event BuffTimeOut OnBuffTimeOut;

    internal TextAsset BuffJSON { get { return mBuffJSON; } set { mBuffJSON = value; } }
    private TextAsset mBuffJSON;

    internal Transform Player { get { return mPlayer; } set { mPlayer = value; } }
    private Transform mPlayer;

    internal string ItemID { get { return mItemID; } set { mItemID = value; } }
    private string mItemID;

    internal int KeepSecond { get { return mKeepSecond; } set { mKeepSecond = value; } }
    private int mKeepSecond;

    private Image mImage;
    private Text mCountDownText;
    private PlayerProperty mPlayerProperty;
    private HealthManager mPlayerHealthManager;
    private GameSceneUIManager mGameSceneUIManager;

    [SerializeField]
    private ParticleSystem superMoveEffect;

    [SerializeField]
    private ParticleSystem moveSpeedUpEffect;
    [SerializeField]
    private ParticleSystem attackUpEffect;
    [SerializeField]
    private ParticleSystem attackSpeedUpEffect;
    [SerializeField]
    private ParticleSystem healthFull50PercentEffect;
    [SerializeField]
    private ParticleSystem resourceExpertEffect;
    [SerializeField]
    private ParticleSystem grimReaperProtectEffect;

    private ParticleSystem effectInstance;
    private ParticleSystem superMoveEffectInstance;
}

public partial class Buff : MonoBehaviour
{
    internal void InitializeSolutionComponent()
    {
        TextAsset JSONFile = (ItemID != default(string)) ? Resources.Load<TextAsset>("Data/ToolItems/" + ItemID) : mBuffJSON;
        string JSON = JSONFile.text;
        ItemData itemData = JsonUtility.FromJson<ItemData>(JSON);
        Sprite icon = Resources.Load<Sprite>(itemData.iconPath);
        mImage = transform.Find("Image").GetComponent<Image>();
        mImage.sprite = icon;
        mCountDownText = transform.Find("Text").GetComponent<Text>();
        if (mPlayer != null)
        {
            mPlayerProperty = mPlayer.GetComponent<PlayerProperty>();
            mPlayerHealthManager = mPlayer.GetComponent<HealthManager>();
            Debug.Log(string.Format("mPlayerProperty 01 {0}", mPlayerProperty.MoveSpeed));
        }
        mGameSceneUIManager = GameObject.Find("USERINTERFACEMANAGER").GetComponent<GameSceneUIManager>();
    }
}

public partial class Buff : MonoBehaviour
{
    internal void TriggerBuff()
    {
        switch (ItemID)
        {
            case "5A6130F9-8EEA8B-1DF1FC79":
                effectInstance = Instantiate(moveSpeedUpEffect, Player.transform);
                superMoveEffectInstance = Instantiate(superMoveEffect, Player.transform);
                superMoveEffectInstance.gameObject.transform.localPosition = Vector3.zero;
                superMoveEffectInstance.Play();
                mPlayerProperty.MoveSpeed *= 2;
                Debug.Log(string.Format("mPlayerProperty 02 {0}", mPlayerProperty.MoveSpeed));
                break;
            case "5A61300D-51E18A-3D8CFEB":
                effectInstance = Instantiate(resourceExpertEffect, Player.transform);
                mGameSceneUIManager.IsTriggerResourceBuff = true;
                break;
            case "5A61312F-A342E1-221BA1CE":
                effectInstance = Instantiate(attackUpEffect, Player.transform);
                mPlayerProperty.IsTirrgerStrengthenDamage = true;
                break;
            case "5A613027-6123CA-15BF1C78":
                effectInstance = Instantiate(grimReaperProtectEffect, Player.transform);
                mPlayerHealthManager.IsInvincibillity = true;
                break;
            case "5A613041-70660B-3AC11352":
                effectInstance = Instantiate(healthFull50PercentEffect, Player.transform);
                InvokeRepeating("FullHealth", 0, 1);
                break;
            case "5A613058-7FA84A-C8DFA7":
                effectInstance = Instantiate(attackSpeedUpEffect, Player.transform);
                mPlayerProperty.Skill01Speed *= 2;
                break;
            default:
                break;
        }

        if (ItemID != default(string))
        {
            effectInstance.gameObject.transform.localPosition = Vector3.zero;
            effectInstance.Stop();
            effectInstance.Clear();
            effectInstance.Play();
        }

        InvokeRepeating("CountdownTime", 0, 1);
    }
}

public partial class Buff : MonoBehaviour
{
    private void CountdownTime()
    {
        mCountDownText.text = string.Format("{0}", mKeepSecond);

        if (mKeepSecond == 0)
        {
            if (OnBuffTimeOut != null)
            {
                OnBuffTimeOut();
            }

            switch (mItemID)
            {
                case "5A6130F9-8EEA8B-1DF1FC79":
                    superMoveEffectInstance.Stop();
                    superMoveEffectInstance.Clear();
                    mPlayerProperty.MoveSpeed /= 2;
                    break;
                case "5A61300D-51E18A-3D8CFEB":
                    mGameSceneUIManager.IsTriggerResourceBuff = false;
                    break;
                case "5A61312F-A342E1-221BA1CE":
                    mPlayerProperty.IsTirrgerStrengthenDamage = false;
                    break;
                case "5A613027-6123CA-15BF1C78":
                    mPlayerHealthManager.IsInvincibillity = false;
                    break;
                case "5A613041-70660B-3AC11352":
                    CancelInvoke();
                    break;
                case "5A613058-7FA84A-C8DFA7":
                    mPlayerProperty.Skill01Speed /= 2;
                    break;
                default:
                    break;
            }

            if (ItemID != default(string))
            {
                effectInstance.Stop();
                effectInstance.Clear();
                DestroyImmediate(effectInstance.gameObject);
            }

            CancelInvoke();
            Destroy(gameObject);
            return;
        }

        mKeepSecond--;
    }
}

public partial class Buff : MonoBehaviour
{
    private void FullHealth()
    {
        float fullHealth = (mPlayerHealthManager.MaxHealth * 0.5f) / mKeepSecond;
        if ((mPlayerHealthManager.Health + fullHealth > mPlayerHealthManager.MaxHealth))
        {
            mPlayerHealthManager.Health = mPlayerHealthManager.MaxHealth;
        }
        else
        {
            mPlayerHealthManager.Health += fullHealth;
        }
    }
}

public partial class Buff : MonoBehaviour
{

}
