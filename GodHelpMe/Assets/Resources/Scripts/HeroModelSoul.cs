﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroModelSoul : MonoBehaviour {

    [SerializeField]
    private GameObject HeroModel;
	
	// Update is called once per frame
	void Update () {
        // 播放 idle 动画    
        StartCoroutine(PalyingAnimation());
	}

    IEnumerator PalyingAnimation()
    {

        Animation anim = HeroModel.GetComponent<Animation>();

        if (anim)
        {            
            if (!anim.IsPlaying("move"))
            {
                anim.Play("idle", PlayMode.StopAll);
            }
        }
        yield return null;
    }
}
