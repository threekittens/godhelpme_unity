﻿using UnityEngine;
using System.Collections;

public class ScenesElementManager
{
    internal static string kTools_Shop_Scene = "Tools_Shop_Scene";
    internal static string kLogin_Scene = "Login_Scene";
    internal static string kLobby_Scene = "Lobby_Scene";
    internal static string kHero_Skill_Introduce_Scene = "Hero_Skill_Introduce_Scene";
    internal static string kHero_Introduce_Scene = "Hero_Introduce_Scene";
    internal static string kGame_Scene = "Game_Scene";
    internal static string kDiamond_Shop_Scene = "Diamond_Shop_Scene";
    internal static string kCreate_New_Player_Scene = "Create_New_Player_Scene";
    internal static string kCharacter_Selection_Scene = "Character_Selection_Scene";
}

