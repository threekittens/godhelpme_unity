﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject mStatusBar;

    internal float PowerFillAmount { get { return mPowerBarTexture.fillAmount; } }
    private Image mPowerBarTexture;

    internal float MaxPower { get { return mMaxQuantityPower; } set { mMaxQuantityPower = value; } }
    private float mMaxQuantityPower;

    [SerializeField]
    internal float Power;

    internal float DefaultPower;

    private void Start()
    {
        mPowerBarTexture = mStatusBar.transform.Find("Power").GetComponent<Image>();
        mMaxQuantityPower = Power;
        DefaultPower = Power;
    }

    private void Update()
    {
        mPowerBarTexture.fillAmount = (Power / mMaxQuantityPower);
    }

    internal void ActivityRecoverPower(bool isActivity, float time)
    {
        if (isActivity == false)
        {
            CancelInvoke();
            return;
        }

        InvokeRepeating("RecoverPower", 0.0f, time);
    }

    private void RecoverPower()
    {
        if (Power == mMaxQuantityPower)
        {
            return;
        }

        Power += (mMaxQuantityPower / 10);
    }
}
