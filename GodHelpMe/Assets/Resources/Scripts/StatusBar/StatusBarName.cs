﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusBarName : MonoBehaviour
{
	private Text PN;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	private void Start()
	{
		PN = transform.GetComponent<Text>();
	}
	
	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	private	void Update()
	{
		if (FirebaseManager.Singleton)
		{
			PN.text = FirebaseManager.Singleton.SignedPlayer.playername;
		}
	}
}
