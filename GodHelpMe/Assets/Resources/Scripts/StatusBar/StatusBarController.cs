﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusBarController : MonoBehaviour
{
    [SerializeField]
    private bool EditModel;

    [SerializeField]
    private float FixedUp = 1.0f;

    private GameObject UI;
    private Transform target;
    private Vector2 screenPos;

    private void Start()
    {
        if (EditModel) { return; }

        UI = GameObject.Find("USERINTERFACEMANAGER");
        target = transform.parent;
        transform.SetParent(UI.transform, false);
        transform.SetAsFirstSibling();
        // screenPos = Camera.main.WorldToScreenPoint(target.transform.position + Vector3.up * FixedUp);
        // transform.SetPositionAndRotation(screenPos, Quaternion.identity);
    }

    private void LateUpdate()
    {
        if (EditModel)
        {
            gameObject.SetActive(false);
            return;
        }

        if (target == null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        screenPos = Camera.main.WorldToScreenPoint(target.transform.position + Vector3.up * FixedUp);
        transform.SetPositionAndRotation(screenPos, Quaternion.identity);
    }
}
