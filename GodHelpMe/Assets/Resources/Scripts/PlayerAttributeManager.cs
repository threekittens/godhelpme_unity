﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathManager;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PAMType
{
    CreatedRole = 0,
    SignedInBefore = 1,
    RewardDay = 2,
    OwnDiamonds = 3,
    RewardDayReset = 4,
    IsGainReward = 5,
    PreviousOpenApplicationTime = 6,
    ItemPackage = 7,
    ItemPanelList = 8,
    UnlockToolButtonIndexList = 9,
    GradeInfomation = 10,
    HeroAchievement = 11,
    OwnHeros = 12
}

public partial class PlayerAttributeManager : MonoBehaviour
{
    public bool EditModel = true;

    [SerializeField]
    private int playerOwnDiamonds;
    public int PlayerOwnDiamonds
    {
        get
        {
            if (!EditModel)
            {
                playerOwnDiamonds = PlayerPrefsUtility.LoadValue<int>(ProjectKeywordList.kPPrefsForPlayerOwnDiamonds);
            }
            return playerOwnDiamonds;
        }
        set
        {
            playerOwnDiamonds = value;
            if (!EditModel)
            {
                StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerOwnDiamonds, playerOwnDiamonds));
                TriggerSyncDataToFirebase(PAMType.OwnDiamonds);
            }
        }
    }

    private Dictionary<string, PlayerItem> playerItemPackage;
    public Dictionary<string, PlayerItem> PlayerItemPackage
    {
        get
        {
            playerItemPackage = PlayerPrefsUtility.LoadDict<string, PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPackage);
            return playerItemPackage;
        }
        set
        {
            Debug.Log("Update item package.");
            playerItemPackage = value;
            StartCoroutine(SaveDict(ProjectKeywordList.kPPrefsForPlayerItemPackage, playerItemPackage));
            TriggerSyncDataToFirebase(PAMType.ItemPackage);
        }
    }

    private List<PlayerItem> playerItemPanelList;
    public List<PlayerItem> PlayerItemPanelList
    {
        get
        {
            playerItemPanelList = PlayerPrefsUtility.LoadList<PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPanelList);
            return playerItemPanelList;
        }
        set
        {
            playerItemPanelList = value;
            StartCoroutine(SaveList(ProjectKeywordList.kPPrefsForPlayerItemPanelList, playerItemPanelList));
            TriggerSyncDataToFirebase(PAMType.ItemPanelList);
        }
    }

    private List<int> unLockToolButtonIndexList;
    public List<int> UnLockToolButtonIndexList
    {
        get
        {
            unLockToolButtonIndexList = PlayerPrefsUtility.LoadList<int>(ProjectKeywordList.kPPrefsForPlayerUnlockToolButtonIndexList);
            return unLockToolButtonIndexList;
        }
        set
        {
            unLockToolButtonIndexList = value;
            StartCoroutine(SaveList(ProjectKeywordList.kPPrefsForPlayerUnlockToolButtonIndexList, unLockToolButtonIndexList));
            TriggerSyncDataToFirebase(PAMType.UnlockToolButtonIndexList);
        }
    }

    private bool syncGradeInfomation = false;
    internal bool SyncGradeInfomation
    {
        get { return syncGradeInfomation; }
        set
        {
            syncGradeInfomation = value;
            if (SyncGradeInfomation)
            {
                SyncGradeInfomation = false;
                TriggerSyncDataToFirebase(PAMType.GradeInfomation);
            }
        }
    }

    private GradeInfomation gradeInfomation;
    public GradeInfomation GradeInfomation
    {
        get
        {
            gradeInfomation = PlayerPrefsUtility.LoadValue<GradeInfomation>(ProjectKeywordList.kPPrefsForPlayerGradeInfomation);
            if (gradeInfomation == null)
            {
                gradeInfomation = new GradeInfomation();
            }
            return gradeInfomation;
        }
        set
        {
            gradeInfomation = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerGradeInfomation, gradeInfomation));
        }
    }

    internal string previousSceneName;
    internal string previousSceneToDiamondShopName;

    private int playerIndex;
    internal int PlayerIndex { get { return playerIndex; } set { playerIndex = value; } }

    private string choosedHeroID;
    internal string ChoosedHeroID
    {
        get
        {
            choosedHeroID = PlayerPrefsUtility.LoadValue<HeroAchievement>(ProjectKeywordList.kPPrefsForPlayerHeroAchievement).uid;
            if (choosedHeroID == null || choosedHeroID == default(string))
            {
                ///The default hero is Iron Core.
                choosedHeroID = "5A631C1A-112F6A0-34500C96";
                HeroAchievement h = this.HeroAchievement;
                h.uid = choosedHeroID;
                this.HeroAchievement = h;
            }

            return choosedHeroID;
        }
        set
        {
            /// When Change Hero id meanwhile changes the Data of Hero Achievement.
            choosedHeroID = value;

            /// Check the HeroAchievementDic has the hero id or not.
            if (HeroAchievementDic.ContainsKey(choosedHeroID))
            {
                Debug.Log("HeroAchievementDic.ContainsKey");
                /// If the HeroAchievementDic has the hero id, get data form HeroAchievementDic and assign it to HeroAchievement.
                PlayerPrefs.DeleteKey(ProjectKeywordList.kPPrefsForPlayerHeroAchievement);
                /// When change the Hero id meanwhile change heroAchievement of currently corresponding hero.
                this.HeroAchievement = HeroAchievementDic[choosedHeroID];
            }
            else
            {
                Debug.Log("HeroAchievementDic donesn't ContainsKey");

                PlayerPrefs.DeleteKey(ProjectKeywordList.kPPrefsForPlayerHeroAchievement);

                /// When change the Hero id meanwhile change heroAchievement of currently corresponding hero.
                HeroAchievement h = this.HeroAchievement;
                h.uid = choosedHeroID;
                this.HeroAchievement = h;
            }
        }
    }

    private Dictionary<int, string> selectedHeroIDs = new Dictionary<int, string>();
    internal Dictionary<int, string> SelectedHeroIDs
    {
        get { return selectedHeroIDs; }
        set { selectedHeroIDs = value; }
    }

    private Dictionary<string, string> heroHashtable;
    internal Dictionary<string, string> HeroHashtable
    {
        get
        {
            heroHashtable = PlayerPrefsUtility.LoadDict<string, string>(ProjectKeywordList.kPPrefsForPlayerHeroHashtable);
            return heroHashtable;
        }
    }

    private bool syncHeroAchievement = false;
    internal bool SyncHeroAchievement
    {
        get { return syncHeroAchievement; }
        set
        {
            syncHeroAchievement = value;
            ///When the SyncHeroAchievement is true will to TriggerSyncDataToFirebase;
            if (syncHeroAchievement)
            {
                syncHeroAchievement = false;
                TriggerSyncDataToFirebase(PAMType.HeroAchievement);
            }
        }
    }

    /// <summary>
    /// The parameter is HeroAchievement of currently chosen hero.
    /// </summary>
    private HeroAchievement heroAchievement;
    internal HeroAchievement HeroAchievement
    {
        get
        {
            heroAchievement = PlayerPrefsUtility.LoadValue<HeroAchievement>(ProjectKeywordList.kPPrefsForPlayerHeroAchievement);
            if (heroAchievement == null) { heroAchievement = new HeroAchievement(); }
            return heroAchievement;
        }
        set
        {
            heroAchievement = value;
            if (FirebaseManager.Singleton)
            {
                heroAchievement.userid = FirebaseManager.Singleton.SignedPlayer.userid;
            }
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerHeroAchievement, heroAchievement));
            Debug.LogWarning("Updated HeroAchievement");

            /////When updating the parameter meanwhile to update HeroAchievementDic.
            Dictionary<string, HeroAchievement> h_dic = this.HeroAchievementDic;
            h_dic[ChoosedHeroID] = heroAchievement;
            this.HeroAchievementDic = h_dic;
        }
    }

    /// Doesn't syncs data to Firebase here, because the heroAchievementDic is for local data storage.
    private Dictionary<string, HeroAchievement> heroAchievementDic;
    internal Dictionary<string, HeroAchievement> HeroAchievementDic
    {
        get
        {
            heroAchievementDic = PlayerPrefsUtility.LoadDict<string, HeroAchievement>(ProjectKeywordList.kPPrefsForPlayerHeroAchievementDic);
            if (heroAchievementDic == null) { heroAchievementDic = new Dictionary<string, HeroAchievement>(); }
            return heroAchievementDic;
        }
        set
        {
            heroAchievementDic = value;
            StartCoroutine(SaveDict(ProjectKeywordList.kPPrefsForPlayerHeroAchievementDic, heroAchievementDic));
            Debug.Log("Updated HeroAchievementDic");
        }
    }

    private List<string> playerOwnHeros;
    public List<string> PlayerOwnHeros
    {
        get
        {
            playerOwnHeros = PlayerPrefsUtility.LoadList<string>(ProjectKeywordList.kPPrefsForPlayerOwnHeros);
            if (playerOwnHeros.Contains("5A631C1A-112F6A0-34500C96"))
            {
                playerOwnHeros.Remove("5A631C1A-112F6A0-34500C96");
            }
            playerOwnHeros.Add("5A631C1A-112F6A0-34500C96");
            return playerOwnHeros;
        }
        set
        {
            playerOwnHeros = value;
            StartCoroutine(SaveList(ProjectKeywordList.kPPrefsForPlayerOwnHeros, playerOwnHeros));
            TriggerSyncDataToFirebase(PAMType.OwnHeros);
        }
    }

    private bool installed;
    public bool Installed
    {
        get
        {
            installed = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerInstalledTheGame);
            return installed;
        }
        set
        {
            installed = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerInstalledTheGame, installed));
        }
    }

    private bool firstTiemPlay;
    public bool FirstTiemPlay
    {
        get
        {
            firstTiemPlay = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerFirstTimePlay);
            return firstTiemPlay;
        }
        set
        {
            firstTiemPlay = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerFirstTimePlay, firstTiemPlay));
        }
    }

    private static PlayerAttributeManager instance;
    internal static PlayerAttributeManager Singleton { get { return instance; } }
}

public partial class PlayerAttributeManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    private void Start()
    {
        Debug.Log("Started for Player Attribute Manager");
        //InitializePlayerOwndiamond();
        InitializeRewardStatus();
        GradeInfomation g = new GradeInfomation();
        StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerGradeInfomation, g));

        //InitializePlayerItemPackage();
        //InitializeUnlockToolCellIndexList();
        //InitializePlayerItemPanelList();
    }

    private void Update()
    {
        // PlayerOwnDiamonds = Diamonds;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnApplicationQuit()
    {

    }
}

/// <summary>
/// Part of below is for TriggerSaveDataInLocalPlayerPrefs.
/// </summary>
public partial class PlayerAttributeManager : MonoBehaviour
{
    private void TriggerSyncDataToFirebase(PAMType type)
    {
        if (FirebaseManager.Singleton) { FirebaseManager.Singleton.OnSyncDataToFirebase(this, type); }
    }
}

public partial class PlayerAttributeManager : MonoBehaviour
{
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        switch (scene.name)
        {
            case "Login_Scene":
                return;
            case "Create_New_Player_Scene":
                return;
            case "Game_Scene":
                return;
            default:
                // if not above scene set player top bar.
                SetPlayerTopBar();
                break;
        }
        //do stuff
        //Debug.Log(scene.name);
    }
}

/// <summary>
/// The part of below is to Initialize the player how much has the diamond.
/// </summary>
public partial class PlayerAttributeManager : MonoBehaviour
{
    private void InitializePlayerOwndiamond()
    {
        PlayerOwnDiamonds = PlayerPrefsUtility.LoadValue<int>(ProjectKeywordList.kPPrefsForPlayerOwnDiamonds);
    }
}

[Serializable]
public class PlayerItem
{
    internal string itemId;
    internal int itemCount;

    internal PlayerItem(string itemId, int itemCount)
    {
        this.itemId = itemId;
        this.itemCount = itemCount;
    }
}


/// <summary>
/// The partial is Set player item package.
/// </summary>
public partial class PlayerAttributeManager : MonoBehaviour
{
    private void InitializePlayerItemPackage()
    {
        PlayerItemPackage = PlayerPrefsUtility.LoadDict<string, PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPackage);
    }

    private void InitializeUnlockToolCellIndexList()
    {
        UnLockToolButtonIndexList = PlayerPrefsUtility.LoadList<int>(ProjectKeywordList.kPPrefsForPlayerUnlockToolButtonIndexList);
    }

    private void InitializePlayerItemPanelList()
    {
        PlayerItemPanelList = PlayerPrefsUtility.LoadList<PlayerItem>(ProjectKeywordList.kPPrefsForPlayerItemPanelList);
    }
}


/// <summary>
/// The partial is Set player Top bar.
/// </summary>
public partial class PlayerAttributeManager : MonoBehaviour
{
    private GameObject SystemTopBar;

    private void SetPlayerTopBar()
    {
        SystemTopBar = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterface("SystemTopBar"));
        Instantiate(SystemTopBar, GameObject.Find("USERINTERFACEMANAGER").transform).transform.SetSiblingIndex(1);

    }
}


/// <summary>
/// Part of below for InitializeRewardStatus.
/// </summary>
public partial class PlayerAttributeManager : MonoBehaviour
{

    private int rewardDay;
    public int RewardDay
    {
        get
        {
            rewardDay = PlayerPrefsUtility.LoadValue<int>(ProjectKeywordList.kPPrefsForPlayerRewardDay);
            return rewardDay;
        }
        set
        {
            rewardDay = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerRewardDay, rewardDay));
            TriggerSyncDataToFirebase(PAMType.RewardDay);
        }
    }

    private bool rewardDayReset;
    public bool RewardDayReset
    {
        get
        {
            rewardDayReset = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerRewardDayReset);
            return rewardDayReset;
        }
        set
        {
            rewardDayReset = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerRewardDayReset, rewardDayReset));
            TriggerSyncDataToFirebase(PAMType.RewardDayReset);
        }
    }

    private bool isGainReward;
    public bool IsGainReward
    {
        get
        {
            isGainReward = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerIsGainReward);
            return isGainReward;
        }
        set
        {
            isGainReward = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerIsGainReward, isGainReward));
            TriggerSyncDataToFirebase(PAMType.IsGainReward);
        }
    }

    private DateTime previousOpenApplicationTime;
    public DateTime PreviousOpenApplicationTime
    {
        get
        {
            previousOpenApplicationTime = PlayerPrefsUtility.LoadValue<DateTime>(ProjectKeywordList.kPPrefsForPlayerPreviousOpenApplicationTime);
            return previousOpenApplicationTime;
        }
        set
        {
            previousOpenApplicationTime = value;
            StartCoroutine(SaveValue(ProjectKeywordList.kPPrefsForPlayerPreviousOpenApplicationTime, previousOpenApplicationTime));
            TriggerSyncDataToFirebase(PAMType.PreviousOpenApplicationTime);
        }
    }

    private void InitializeRewardStatus()
    {
        // if first install the rewardDay is 0;
        //RewardDay = PlayerPrefsUtility.LoadValue<int>(ProjectKeywordList.kPPrefsForPlayerRewardDay);
        //RewardDayReset = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerRewardDayReset);
        //IsGainReward = PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerIsGainReward);

        // if is default return here.
        //PreviousOpenApplicationTime = PlayerPrefsUtility.LoadValue<DateTime>(ProjectKeywordList.kPPrefsForPlayerPreviousOpenApplicationTime);

        if (PreviousOpenApplicationTime == default(DateTime)) { return; }

        TimeSpan previous = TimeSpan.FromTicks(PreviousOpenApplicationTime.Ticks);
        TimeSpan current = TimeSpan.FromTicks(DateTime.Now.Ticks);
        TimeSpan diff = current - previous;
        double Seconds = diff.TotalSeconds;

        // if hours over 24 already, player gain reward again.
        if (Seconds > 30)
        {
            if (IsGainReward == true)
            {
                RewardDay += 1;
            }

            if (RewardDayReset == true)
            {
                RewardDay = 0;

                // if reseted before false the RewardDayReset.
                RewardDayReset = false;
            }

            if (RewardDay == 6)
            {
                // if day of reward is 7 days true the RewardDayReset for rest days of reward.
                RewardDayReset = true;
            }

            Debug.Log("RewardDay => " + RewardDay);

            IsGainReward = false;
        }
    }
}

public partial class PlayerAttributeManager : MonoBehaviour
{
    IEnumerator SaveValue<T>(string key, T value)
    {
        PlayerPrefsUtility.SaveValue(key, value);
        PlayerPrefs.Save();
        yield break;
    }

    IEnumerator SaveList<T>(string key, List<T> value)
    {
        PlayerPrefsUtility.SaveList(key, value);
        PlayerPrefs.Save();
        yield break;
    }

    IEnumerator SaveDict<Key, Value>(string key, Dictionary<Key, Value> value)
    {
        PlayerPrefsUtility.SaveDict(key, value);
        PlayerPrefs.Save();
        yield break;
    }
}