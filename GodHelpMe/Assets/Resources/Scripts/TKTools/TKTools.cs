
namespace TKTools
{
    public class Parameter
    {    
        // public static string GETNAME<T>(T myInput) where T : class
        // {
        //     if (myInput == null)
        //         return string.Empty;

        //     return typeof(T).GetProperties()[0].Name;
        // }

        public static string GETNAME<T>(T myInput)
        {
            if (myInput == null)
                return string.Empty;

            return typeof(T).GetProperties()[0].Name;
        }        
    }
}