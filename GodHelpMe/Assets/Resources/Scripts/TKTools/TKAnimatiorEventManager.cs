﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TKTools
{
    [RequireComponent(typeof(Animator))]
    public class TKAnimatiorEventManager : MonoBehaviour
    {
        public delegate void FinishedAnimationEvnet();

        public event FinishedAnimationEvnet OnFinishedAnimationEvnet;

        public void FinishedAnimationEvent()
        {
            if (OnFinishedAnimationEvnet != null) { OnFinishedAnimationEvnet(); }
        }
    }
}
