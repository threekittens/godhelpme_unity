﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace TK_UNITY_File_Splitter
{
    public class JsonFileSplitter
    {
        public static T LoadJsonFile<T>(string path)
        {
            T item = default(T);
            try
            {
                if (File.Exists(path))
                {
                    // Open the stream and read it back.
                    using (StreamReader sr = File.OpenText(path))
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            item = JsonUtility.FromJson<T>(s);
                            return item;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return item;
        }
    }
}
