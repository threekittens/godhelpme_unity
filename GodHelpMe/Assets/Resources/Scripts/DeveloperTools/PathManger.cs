﻿namespace PathManager
{
    public class LoadResourceFilePath
    {
        public static string PrefabHeros()
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.Heros);
        }

        public static string PrefabVFXEffects()
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.VFXEffects);
        }

        public static string PrefabSkill()
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.Skill);
        }

        public static string PrefabEnemys()
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.Enemys);
        }

        public static string PrefabUserInterface(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + fileName;
        }

        public static string PrefabUserInterfaceCharacterSelectionScene(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(SceneType.Character_Selection_Scene) + fileName;
        }

        public static string PrefabUserInterfaceHeroSkillIntroduceScene(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(SceneType.Hero_Skill_Introduce_Scene) + fileName;
        }

        public static string PrefabUserInterfaceGameSceneSkillPanel(string choosedHeroName)
        {             
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(SceneType.Game_Scene) + "SkillPanel/" + choosedHeroName + "/SkillsJoystickPanel";
        }

        public static string PrefabUserInterfaceGameScene(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(SceneType.Game_Scene) + fileName;
        }

        public static string PrefabUserInterfaceRewardView(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(FolderType.Reward_View) + fileName;
        }

        public static string PrefabUserInterfaceToolsShopScene(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(SceneType.Tools_Shop_Scene) + fileName;
        }

        public static string PrefabUserInterfaceHeroIntroduceScene(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(SceneType.Hero_Introduce_Scene) + fileName;
        }

        public static string PrefabUserInterfaceLobyScene(string fileName)
        {
            return GetPath(FileType.Prefab) + GetPath(FolderType.UserInterface) + GetPath(SceneType.Loby_Scene) + fileName;
        }

        public static string TextureUserInterface()
        {
            return GetPath(FileType.Texture) + GetPath(FolderType.UserInterface);
        }

        public static string TextureUserInterface(string fileName)
        {
            return GetPath(FileType.Texture) + GetPath(FolderType.UserInterface) + fileName;
        }

        public static string TextureUserInterfaceTools(string fileName)
        {
            return GetPath(FileType.Texture) + GetPath(FolderType.UserInterface) + GetPath(FolderType.Tools) + fileName;
        }

        public static string TextureUserInterfaceHeros(string fileName)
        {
            return GetPath(FileType.Texture) + GetPath(FolderType.UserInterface) + GetPath(FolderType.Heros) + fileName;
        }

        public enum FileType
        {
            None,
            Prefab,
            Script,
            Texture
        }

        public enum FolderType
        {
            None,
            Heros,
            Peoples,
            UserInterface,
            Skill,
            Enemys,
            Tools,
            Reward_View,
            VFXEffects
        }

        public enum SceneType
        {
            None,
            Login_Scene,
            Loby_Scene,
            Character_Selection_Scene,
            Tools_Shop_Scene,
            Hero_Skill_Introduce_Scene,
            Hero_Introduce_Scene,
            Game_Scene,
            Diamond_Shop_Scene,
        }

        public static string GetPath(FileType fileType)
        {
            string fileTypePath = "";

            switch (fileType)
            {
                case FileType.None:
                    break;
                case FileType.Prefab:
                    fileTypePath = "Prefab" + "/";
                    break;
                case FileType.Script:
                    fileTypePath = "Script" + "/";
                    break;
                case FileType.Texture:
                    fileTypePath = "Texture" + "/";
                    break;
            }

            return fileTypePath;
        }

        public static string GetPath(SceneType sceneType)
        {
            string sceneTypePath = "";

            switch (sceneType)
            {
                case SceneType.None:
                    break;
                case SceneType.Character_Selection_Scene:
                    sceneTypePath = "Character_Selection_Scene" + "/";
                    break;
                case SceneType.Diamond_Shop_Scene:
                    sceneTypePath = "Diamond_Shop_Scene" + "/";
                    break;
                case SceneType.Game_Scene:
                    sceneTypePath = "Game_Scene" + "/";
                    break;
                case SceneType.Hero_Introduce_Scene:
                    sceneTypePath = "Hero_Introduce_Scene" + "/";
                    break;
                case SceneType.Hero_Skill_Introduce_Scene:
                    sceneTypePath = "Hero_Skill_Introduce_Scene" + "/";
                    break;
                case SceneType.Loby_Scene:
                    sceneTypePath = "Loby_Scene" + "/";
                    break;
                case SceneType.Login_Scene:
                    sceneTypePath = "Login_Scene" + "/";
                    break;
                case SceneType.Tools_Shop_Scene:
                    sceneTypePath = "Tools_Shop_Scene" + "/";
                    break;
            }

            return sceneTypePath;
        }

        public static string GetPath(FolderType folderType)
        {
            string folderTypePath = "";

            switch (folderType)
            {
                case FolderType.None:
                    break;
                case FolderType.Heros:
                    folderTypePath = "Heros" + "/";
                    break;
                case FolderType.Peoples:
                    folderTypePath = "Peoples" + "/";
                    break;
                case FolderType.UserInterface:
                    folderTypePath = "UserInterface" + "/";
                    break;
                case FolderType.Skill:
                    folderTypePath = "Skill" + "/";
                    break;
                case FolderType.Enemys:
                    folderTypePath = "Enemys" + "/";
                    break;
                case FolderType.Tools:
                    folderTypePath = "Tools" + "/";
                    break;
                case FolderType.Reward_View:
                    folderTypePath = "Reward_View" + "/";
                    break;
                case FolderType.VFXEffects:
                    folderTypePath = "VFXEffects" + "/";
                    break;
            }

            return folderTypePath;
        }
    }
}
