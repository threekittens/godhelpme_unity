﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarnAlertor : MonoBehaviour
{
    public static event System.EventHandler OnClosed;

    private static GameObject warnAlertor;
    private static GameObject warnAlertorInstance;

    public static void Show(string text, Transform USERINTERFACEMANAGER)
    {
        warnAlertor = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("WarnAlertor"));
        warnAlertorInstance = Instantiate(warnAlertor, USERINTERFACEMANAGER);

        Text tip = warnAlertorInstance.transform.Find("Background/Image/Tip").GetComponent<Text>();
        tip.text = text;

        Button checkButton = warnAlertorInstance.transform.Find("Background/Line/CheckButton").GetComponent<Button>();
        checkButton.onClick.AddListener(() => Close());

        RectTransform rect = warnAlertorInstance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        warnAlertorInstance.SetActive(true);
    }

    public static void Close()
    {
        Debug.Log("Warn Alertor Close");

        if (OnClosed != null)
        {
            OnClosed(null, System.EventArgs.Empty);
        }

        DestroyImmediate(warnAlertorInstance);
    }
}
