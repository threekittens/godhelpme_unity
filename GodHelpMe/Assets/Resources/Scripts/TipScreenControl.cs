﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TipScreenControl : MonoBehaviour, IPointerDownHandler
{

    public event System.EventHandler OnPointEvent;
    public event System.EventHandler OnEnableActive;
    public event System.EventHandler OnDisableActive;
    private Image genderImage;
    private Image finger;
    private bool isPointDown = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("On Pointer Down");
        if (isPointDown == false) { return; }
        if (OnPointEvent != null) { OnPointEvent(this, System.EventArgs.Empty); }
    }

    private void OnEnable()
    {
        genderImage = transform.Find("Chat/Image").GetComponent<Image>();
        finger = transform.Find("Chat/Finger").GetComponent<Image>();
        if (FirebaseManager.Singleton)
        {
            Sprite sprite = (FirebaseManager.Singleton.SignedPlayer.gender == (int)CNPRooleGender.Female) ?
                Resources.Load<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface("elf_woman")) : Resources.Load<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface("elf_man"));
            genderImage.sprite = sprite;
        }
        StartCoroutine(DelayFunction(1.5f));
    }

    IEnumerator DelayFunction(float s)
    {
        yield return new WaitForSeconds(s);

        finger.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        if (!isPointDown)
        {
            isPointDown = true;
        }

        if (OnEnableActive != null) { OnEnableActive(this, System.EventArgs.Empty); }
        yield break;
    }

    private void OnDisable()
    {
        Resources.UnloadUnusedAssets();
        if (OnDisableActive != null) { OnDisableActive(this, System.EventArgs.Empty); }
    }
}
