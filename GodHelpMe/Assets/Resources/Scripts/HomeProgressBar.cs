﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class HomeProgressBar : MonoBehaviour
{

    private GameObject mOutward;
    private Image mInner;
    private float mProgress;
    private float mUnit = 100.0f;

    internal float mLoadingSecond;


    internal delegate void HomeProgressBarDidStarted(HomeProgressBar homeProgressBar, float parogress);
    internal delegate void HomeProgressBarDidStop(HomeProgressBar homeProgressBar, float parogress);
    internal delegate void HomeProgressBarOnDisable(HomeProgressBar homeProgressBar);

    internal HomeProgressBarDidStarted OnHomeProgressBarDidStarted;
    internal HomeProgressBarDidStop OnHomeProgressBarDidStop;
    internal HomeProgressBarOnDisable OnHomeProgressBarOnDisable;

    private void OnEnable()
    {
        mOutward = transform.GetChild(0).gameObject;
        mInner = mOutward.transform.GetChild(0).gameObject.GetComponent<Image>();
        InvokeRepeating("Progressing", 0.0f, (1.0f / mUnit) * mLoadingSecond);
    }

    private void OnDisable()
    {
        if (OnHomeProgressBarOnDisable != null)
        {
            OnHomeProgressBarOnDisable(this);
        }

        CancelInvoke();
    }

    private void Update()
    {

    }

    private void Progressing()
    {
        if (mInner.fillAmount == 1.0f)
        {
            StopBarProgressing();
        }

        float currentFillAmount = (mProgress / mUnit);
        mInner.fillAmount = currentFillAmount;
        if (OnHomeProgressBarDidStarted != null)
        {
            OnHomeProgressBarDidStarted(this, currentFillAmount);
        }
        mProgress++;
    }

    private void StopBarProgressing()
    {
        Debug.Log("StopBarProgressing");
        float currentFillAmount = mInner.fillAmount;
        if (OnHomeProgressBarDidStop != null)
        {
            OnHomeProgressBarDidStop(this, currentFillAmount);
        }
        mProgress = 0.0f;
        gameObject.SetActive(false);
    }
}
