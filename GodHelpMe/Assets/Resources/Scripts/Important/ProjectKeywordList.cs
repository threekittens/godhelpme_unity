﻿

/// <summary>
/// Part of below is for the keyword of scene. always "kSceneFor" be the title.
/// </summary>
public partial class ProjectKeywordList
{
    public static string kSceneForCharacterSelection = "Character_Selection_Scene";
    public static string kSceneForCreateNewPlayer = "Create_New_Player_Scene";
    public static string kSceneForDiamondShop = "Diamond_Shop_Scene";
    public static string kSceneForGame = "Game_Scene";
    public static string kSceneForHeroIntroduce = "Hero_Introduce_Scene";
    public static string kSceneForHeroSkillIntroduce = "Hero_Skill_Introduce_Scene";
    public static string kSceneForLobby = "Lobby_Scene";
    public static string kSceneForLogin = "Login_Scene";
    public static string kSceneForToolsShop = "Tools_Shop_Scene";
}


/// <summary>
/// Part of below is for the keyword of PlayerPrefs. always "kPPrefsFor" be the title.
/// </summary>
public partial class ProjectKeywordList
{
    // For Example
    //public static string kPPrefsForXXXXX = "kPPrefsForXXXXX";        

    public static string kPPrefsForPlayerBGMManagerVolume = "kPPrefsForPlayerBGMManagerVolume";
    public static string kPPrefsForPlayerBGMManagerMute = "kPPrefsForPlayerBGMManagerMute";

    public static string kPPrefsForPlayerSEManagerVolume = "kPPrefsForPlayerSEManagerVolume";
    public static string kPPrefsForPlayerSEManagerMute = "kPPrefsForPlayerSEManagerMute";

    public static string kPPrefsForPlayerSignedInBefore = "kPPrefsForPlayerSignedInBefore";

    public static string kPPrefsForPlayerRewardDay = "kPPrefsForPlayerRewardDay";
    public static string kPPrefsForPlayerOwnDiamonds = "kPPrefsForPlayerOwnDiamonds";

    public static string kPPrefsForPlayerRewardDayReset = "kPPrefsForPlayerRewardDayReset";
    public static string kPPrefsForPlayerIsGainReward = "kPPrefsForPlayerIsGainReward";
    public static string kPPrefsForPlayerPreviousOpenApplicationTime = "kPPrefsForPlayerPreviousOpenApplicationTime";

    public static string kPPrefsForPlayerItemPackage = "kPPrefsForPlayerItemPackage";

    public static string kPPrefsForPlayerItemPanelList = "kPPrefsForPlayerItemPanelList";
    public static string kPPrefsForPlayerUnlockToolButtonIndexList = "kPPrefsForPlayerUnlockToolButtonIndexList";

    public static string kPPrefsForPlayerGradeInfomation = "kPPrefsForPlayerGradeInfomation";

    public static string kPPrefsForPlayerHeroHashtable = "kPPrefsForPlayerHeroHashtable";
    public static string kPPrefsForPlayerHeroAchievement = "kPPrefsForPlayerHeroAchievement";
    public static string kPPrefsForPlayerHeroAchievementDic = "kPPrefsForPlayerHeroAchievementDic";
    public static string kPPrefsForPlayerOwnHeros = "kPPrefsForPlayerOwnHeros";

    public static string kPPrefsForPlayerInstalledTheGame = "kPPrefsForPlayerInstalledTheGame";

    public static string kPPrefsForPlayerFirstTimePlay = "kPPrefsForPlayerFirstTimePlay";

    public static string kPPrefsForPlayerDecisionWarStatus = "kPPrefsForPlayerDecisionWarStatus";
    public static string kPPrefsForPlayerSelectedHeroIndex = "kPPrefsForPlayerSelectedHeroIndex";

    //public static string kPPrefsForPlayerSignInFirst = "kPPrefsForPlayerSignInFirst";
    //public static string kPPrefsForPlayerUserData = "kPPrefsForPlayerUserData";
    //public static string kPPrefsForPlayerUnlockValuesListForSave = "kPPrefsForPlayerUnlockValuesListForSave";
    //public static string kPPrefsForPlayerCanceledIndexList = "kPPrefsForPlayerCanceledIndexList";
    //public static string kPPrefsForPlayerQueuAddIndex = "kPPrefsForPlayerQueuAddIndex";
}


/// <summary>
/// Part of below is for the keyword of Input Axis. always "kAxisFor" be the title.
/// </summary>
public partial class ProjectKeywordList
{
    public static string kAxisForHorizontal = "Horizontal";
}


/// <summary>
/// Part of below is for the name of animation be executed. always "kAnimationFor" be the title.
/// </summary>
public partial class ProjectKeywordList
{
    public static string kAnimationForMove = "move";
    public static string kAnimationForIdle = "idle";
    public static string kAnimationForDie = "die";
    public static string kAnimationForEffect = "effect";
    public static string kAnimationForSkill01 = "skill_01";
    public static string kAnimationForSkill01_1 = "skill_01_1";
    public static string kAnimationForSkill01_2 = "skill_01_2";
    public static string kAnimationForSkill02 = "skill_02";
    public static string kAnimationForSkill03 = "skill_03";
    public static string kAnimationForSkill04 = "skill_04";
    public static string kAnimationForSkill04Bool = "skill_04_bool";
    public static string kAnimationForAttack = "attack";
    public static string kAnimationForAttackSpeed = "attack_speed";
    public static string kAnimationForSkill01Speed = "Skill01Speed";
    public static string kAnimationForSkill02Speed = "Skill02Speed";
    public static string kAnimationForSkill03Speed = "Skill03Speed";
    public static string kAnimationForSkill04Speed = "Skill04Speed";
    public static string kAnimationForMoveSpeed = "MoveSpeed";
}

/// <summary>
/// Part of below is for the name of Layer. always "kLayerFor" be the title.
/// </summary>
public static partial class ProjectKeywordList
{
    public static string kLayerForEnemyAttackable = "ENEMY_ATTACKABLE";
    public static string kLayerForAttackable = "ATTACKABLE";
    public static string kLayerForMiniMap = "MINI_MAP";
}

public static partial class ProjectKeywordList
{
    public static string kUSERINTERFACEMANAGER = "USERINTERFACEMANAGER";
}

/// <summary>
/// Part of below is for the name of Tag. always "kTagFor" be the title.
/// </summary>
public static partial class ProjectKeywordList
{
    public static string kTagForTowerCore = "TowerCore";
    public static string kTagForPlayer = "Player";
    public static string kTagForEnemy = "Enemy";
}
