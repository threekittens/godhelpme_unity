﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class PlayerProperty : MonoBehaviour
{
    //internal bool IsUpAttackSpeed;
    private DieCountdownView dieCountdownView;

    [Range(1.0f, 10.0f)]
    internal float Skill01Speed = 1.0f;
    [Range(1.0f, 10.0f)]
    internal float Skill02Speed = 1.0f;
    [Range(1.0f, 10.0f)]
    internal float Skill03Speed = 1.0f;
    [Range(1.0f, 10.0f)]
    internal float Skill04Speed = 1.0f;

    internal bool IsTirrgerStrengthenDamage;

    internal string HeroID { get { return heroID; } set { heroID = value; } }
    private string heroID;

    private Motor motor;
    internal float MoveSpeed
    {
        get
        {
            return motor.moveSpeed;
        }
        set
        {
            motor.moveSpeed = value;
        }
    }

    [SerializeField]
    private int _currentExp;
    internal int CurrentExp
    {
        get
        {
            if (PlayerAttributeManager.Singleton)
            {
                _currentExp = PlayerAttributeManager.Singleton.HeroAchievement.exp;
            }
            else
            {
                _currentExp = 0;
            }

            return _currentExp;
        }
        set
        {
            _currentExp = value;
            if (PlayerAttributeManager.Singleton)
            {
                HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievement;
                h.exp = _currentExp;
                PlayerAttributeManager.Singleton.HeroAchievement = h;

                if (expInner)
                {
                    float _c = _currentExp;
                    float _e = ExpRequired;
                    float fillAmount = _c / _e;
                    // Debug.Log("fillAmount " + fillAmount);
                    expInner.fillAmount = fillAmount;
                }
            }
        }
    }

    [SerializeField]
    private int _expRequired;
    private int ExpRequired { get { return _expRequired; } set { _expRequired = value; } }

    private int maxLevel = 10;

    private int _level = 1;
    private int Level { get { return _level; } set { _level = value; levelText.text = _level.ToString(); } }

    [SerializeField]
    private Image expInner;

    private Text levelText;
    private SkillsJoystickPanel skillsJoystickPanel;

    private HealthManager healthManager;
    private PowerManager powerManager;

    internal int killboss;
    internal int killenemy;
    internal int repairtower;
    internal int uptower;

    private enum JobType
    {
        Tank = 0,
        Fighter = 1,
        Magician = 2,
        Archer = 3,
    }

    [SerializeField]
    private JobType jobType;

    [SerializeField]
    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }

    private int r;
    private int R { get { return r; } set { r = value; } }

    private struct PlayerPropertyBasic
    {
        public int HP, MP, Damage, WeightHP, WeightMP, WeightDamage, Exp;

        PlayerPropertyBasic(int HP, int MP, int Damage, int WeightHP, int WeightMP, int WeightDamage, int Exp)
        {
            this.HP = HP;
            this.MP = MP;
            this.Damage = Damage;
            this.WeightHP = WeightHP;
            this.WeightMP = WeightMP;
            this.WeightDamage = WeightDamage;
            this.Exp = Exp;
        }
    }

    [SerializeField]
    private PlayerPropertyBasic ppb;

    private PlayerPropertyBasic PPB
    {
        get
        {
            ppb.HP = 1250;
            ppb.MP = 1000;
            ppb.Damage = 10;
            ppb.Exp = 500;
            switch (jobType)
            {
                case JobType.Tank:
                    {
                        ppb.WeightHP = 30;
                        ppb.WeightMP = 15;
                        ppb.WeightDamage = 15;
                        break;
                    }
                case JobType.Fighter:
                    {
                        ppb.WeightHP = 25;
                        ppb.WeightMP = 20;
                        ppb.WeightDamage = 30;
                        break;
                    }
                case JobType.Magician:
                    {
                        ppb.WeightHP = 15;
                        ppb.WeightMP = 35;
                        ppb.WeightDamage = 35;
                        break;
                    }
                case JobType.Archer:
                    {
                        ppb.WeightHP = 20;
                        ppb.WeightMP = 25;
                        ppb.WeightDamage = 25;
                        break;
                    }

            }
            return ppb;
        }
    }
}

public partial class PlayerProperty : MonoBehaviour
{
    private void Start()
    {
        GameObject USERINTERFACEMANAGER = GameObject.Find(ProjectKeywordList.kUSERINTERFACEMANAGER);
        skillsJoystickPanel = USERINTERFACEMANAGER.transform.Find("SkillsJoystickPanel").GetComponent<SkillsJoystickPanel>();
        dieCountdownView = USERINTERFACEMANAGER.transform.Find("DieCountdownView").GetComponent<DieCountdownView>();
        levelText = expInner.transform.Find("ExpOutward/Level").GetComponent<Text>();

        motor = transform.GetComponent<Motor>();

        if (PlayerAttributeManager.Singleton)
        {
            Level = PlayerAttributeManager.Singleton.HeroAchievement.level;

            R = PlayerAttributeManager.Singleton.HeroAchievement.reincarnation + 1;

            Debug.Log("PlayerProperty Start");
            Debug.Log("R = " + R);
            Debug.Log("Level = " + Level);

            healthManager = transform.GetComponent<HealthManager>();
            healthManager.Health = (PPB.HP + (PPB.HP * 1.2f * (Level - 1))) * (100 * (PPB.WeightHP * R)) / 1000;
            Debug.Log("Health = " + healthManager.Health);
            healthManager.DefaultHealth = healthManager.Health;
            healthManager.MaxHealth = healthManager.Health;
            healthManager.OnHealthManagerDieNotice += MHealthManager_OnHealthManagerDieNotice;

            powerManager = transform.GetComponent<PowerManager>();
            powerManager.Power = (PPB.MP + (PPB.MP * 0.4f * (Level - 1))) * (100 * (PPB.WeightMP * R)) / 1000;
            powerManager.DefaultPower = powerManager.Power;
            powerManager.MaxPower = powerManager.Power;
            powerManager.ActivityRecoverPower(true, 10.0f);

            float weightD = PPB.Damage * 0.2f * (Level - 1);
            Damage = (PPB.Damage + Convert.ToInt32(weightD)) * (100 * PPB.WeightDamage) / 1000;

            ExpRequired = (PPB.Exp + (125 * Level));

            float _c = CurrentExp;
            float _e = ExpRequired;
            float fillAmount = _c / _e;
            // Debug.Log("fillAmount " + fillAmount);
            expInner.fillAmount = fillAmount;
        }
    }

    private void MHealthManager_OnHealthManagerDieNotice(GameObject target)
    {
        dieCountdownView.gameObject.SetActive(true);
    }
}

public partial class PlayerProperty : MonoBehaviour
{
    internal void GainExperienceFromEnemey(int exp)
    {
        Debug.Log("The Exp is " + exp);

        CurrentExp += exp;

        if (CurrentExp >= ExpRequired)
        {
            UpLevel();
        }
    }
}

public partial class PlayerProperty : MonoBehaviour
{
    private void UpLevel()
    {
        Level += 1;

        healthManager.Health = healthManager.DefaultHealth;
        healthManager.Health = (PPB.HP + (PPB.HP * 1.2f * (Level - 1))) * (100 * (PPB.WeightHP * R)) / 1000;
        healthManager.DefaultHealth = healthManager.Health;
        healthManager.MaxHealth = healthManager.Health;

        powerManager.Power = powerManager.DefaultPower;
        powerManager.Power = (PPB.MP + (PPB.MP * 0.4f * (Level - 1))) * (100 * (PPB.WeightMP * R)) / 1000;
        powerManager.DefaultPower = powerManager.Power;
        powerManager.MaxPower = powerManager.Power;

        float weightD = PPB.Damage * 0.2f * (Level - 1);
        Damage = (PPB.Damage + Convert.ToInt32(weightD)) * (100 * PPB.WeightDamage) / 1000;

        // Up more Request Up Level need experience.
        ExpRequired = (PPB.Exp + (125 * Level));

        /// Reset the current exp to 0 or
        /// The CurrentExp minues the ExpRequired If the CurrentExp more than ExpRequired.
        if (CurrentExp >= ExpRequired)
        {
            CurrentExp -= ExpRequired;
        }
        else
        {
            CurrentExp = 0;
        }

        if (PlayerAttributeManager.Singleton)
        {
            HeroAchievement h = PlayerAttributeManager.Singleton.HeroAchievement;
            h.level = Level;
            PlayerAttributeManager.Singleton.HeroAchievement = h;
        }

        skillsJoystickPanel.OnPlayerUpdateLevel(Level);
    }
}

public partial class PlayerProperty : MonoBehaviour
{
    internal void OnReceiveTrackingEvent(PointsType pointsType)
    {
        switch (pointsType)
        {
            case PointsType.Boss:
                killboss++;
                break;
            case PointsType.Enemy:
                killenemy++;
                break;
            case PointsType.RepaireTower:
                repairtower++;
                break;
            case PointsType.TowerUpLevel:
                uptower++;
                break;
        }

        if (PlayerAttributeManager.Singleton)
        {
            if (PlayerAttributeManager.Singleton.GradeInfomation == null)
            {
                PlayerAttributeManager.Singleton.GradeInfomation = new GradeInfomation();
            }

            var g = PlayerAttributeManager.Singleton.GradeInfomation;
            g.killboss = killboss;
            g.killenemy = killenemy;
            g.repairtower = repairtower;
            g.uptower = uptower;
            PlayerAttributeManager.Singleton.GradeInfomation = g;
        }
    }
}

public partial class PlayerProperty : MonoBehaviour
{

}

public partial class PlayerProperty : MonoBehaviour
{

}
