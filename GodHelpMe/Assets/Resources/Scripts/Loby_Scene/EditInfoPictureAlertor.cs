﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditInfoPictureAlertor : MonoBehaviour
{

    internal delegate void EditInfoPictureAlertorDelegate(int index);
    internal event EditInfoPictureAlertorDelegate ChoosedIndexOfPicture;

    [SerializeField]
    private GameObject panelItem;
    [SerializeField]
    private Sprite selectSprite;
    [SerializeField]
    private Sprite unSelectSprite;

    private GameObject panel;
    private Sprite[] pictureList;

    private List<Button> buttonList;

    private int pictureIndex = -1;

    private void Awake()
    {
        pictureList = Resources.LoadAll<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface() + "SpellBook");
    }

    private void Start()
    {
        panel = transform.Find("Background/Panel").gameObject;
        buttonList = new List<Button>();

        for (int i = 0; i < pictureList.Length; i++)
        {
            Sprite sp = pictureList[i];

            GameObject instance = Instantiate(panelItem, panel.transform);

            Image image = instance.GetComponent<Image>();
            image.sprite = sp;

            Button button = instance.transform.Find("Button").GetComponent<Button>();
            button.onClick.AddListener(() => ButtonEvent(button));
            buttonList.Add(button);
        }


        if (FirebaseManager.Singleton)
        {
            pictureIndex = FirebaseManager.Singleton.SignedPlayer.pictureindex;
            buttonList[pictureIndex].image.sprite = selectSprite;
        }
    }

    private void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }

    private void ButtonEvent(Button button)
    {
        if (pictureIndex != -1) { buttonList[pictureIndex].image.sprite = unSelectSprite; }

        button.image.sprite = selectSprite;

        int index = buttonList.IndexOf(button);

        pictureIndex = index;

        Debug.Log(pictureIndex);
    }

    public void Apply()
    {
        SEManager.Singleton.PlayDecideSE();
        if (ChoosedIndexOfPicture != null) { ChoosedIndexOfPicture(pictureIndex); }
        Close();
    }

    public void Close()
    {
        SEManager.Singleton.PlayCancelSE();
        DestroyImmediate(gameObject);
        Debug.Log("Close");
    }
}
