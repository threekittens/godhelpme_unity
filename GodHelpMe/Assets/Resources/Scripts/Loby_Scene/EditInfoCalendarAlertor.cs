﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditInfoCalendarAlertor : MonoBehaviour
{
    public delegate void CalendarControllerDelegate(EditInfoAlertorDate editInfoAlertorDate);
    public event CalendarControllerDelegate ChoosedTheDate;

    public GameObject calendarPanel;
    public Text yearNumText;
    public Text monthNumText;
    public GameObject item;

    private List<GameObject> dateItems = new List<GameObject>();
    const int totalDateNum = 42;

    private DateTime dateTime;

    void Start()
    {
        Vector3 startPos = item.transform.localPosition;
        RectTransform startRect = item.transform.GetComponent<RectTransform>();
        dateItems.Clear();
        dateItems.Add(item);

        for (int i = 1; i < totalDateNum; i++)
        {
            GameObject instance = GameObject.Instantiate(item) as GameObject;
            instance.name = "Item" + (i + 1).ToString();
            instance.transform.SetParent(item.transform.parent);
            instance.transform.localScale = Vector3.one;
            instance.transform.localRotation = Quaternion.identity;
            instance.transform.localPosition = new Vector3((i % 7) * (startRect.rect.width + 1) + startPos.x, startPos.y - (i / 7) * (startRect.rect.height + 1), startPos.z);

            dateItems.Add(instance);
        }

        dateTime = DateTime.Now;

        CreateCalendar();
    }

    void CreateCalendar()
    {
        DateTime firstDay = dateTime.AddDays(-(dateTime.Day - 1));
        int index = GetDays(firstDay.DayOfWeek);

        int date = 0;
        for (int i = 0; i < totalDateNum; i++)
        {
            Text label = dateItems[i].GetComponentInChildren<Text>();
            dateItems[i].SetActive(false);

            if (i >= index)
            {
                DateTime thatDay = firstDay.AddDays(date);
                if (thatDay.Month == firstDay.Month)
                {
                    dateItems[i].SetActive(true);

                    label.text = (date + 1).ToString();
                    date++;
                }
            }
        }
        yearNumText.text = dateTime.Year.ToString();
        monthNumText.text = dateTime.Month.ToString();
    }

    int GetDays(DayOfWeek day)
    {
        switch (day)
        {
            case DayOfWeek.Monday: return 1;
            case DayOfWeek.Tuesday: return 2;
            case DayOfWeek.Wednesday: return 3;
            case DayOfWeek.Thursday: return 4;
            case DayOfWeek.Friday: return 5;
            case DayOfWeek.Saturday: return 6;
            case DayOfWeek.Sunday: return 0;
        }

        return 0;
    }
    public void YearPrev()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayOpenBookSE();
        dateTime = dateTime.AddYears(-1);
        CreateCalendar();
    }

    public void YearNext()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayOpenBookSE();
        dateTime = dateTime.AddYears(1);
        CreateCalendar();
    }

    public void MonthPrev()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayOpenBookSE();
        dateTime = dateTime.AddMonths(-1);
        CreateCalendar();
    }

    public void MonthNext()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayOpenBookSE();
        dateTime = dateTime.AddMonths(1);
        CreateCalendar();
    }

    public void OnDateItemClick(string day)
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayDecideSE();

        if (ChoosedTheDate != null)
        {
            EditInfoAlertorDate editInfoAlertorDate = new EditInfoAlertorDate();
            editInfoAlertorDate.Year = yearNumText.text;
            editInfoAlertorDate.Month = monthNumText.text;
            editInfoAlertorDate.Day = day;
            ChoosedTheDate(editInfoAlertorDate);           
        }

        DestroyImmediate(gameObject);
    }

    public void Close()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayCancelSE();
        DestroyImmediate(gameObject);
    }

    public void OnDateItemClick(GameObject item)
    {
        OnDateItemClick(item.GetComponentInChildren<Text>().text);
    }
}
