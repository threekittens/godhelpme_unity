﻿using PathManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public partial class LobySceneUIManager : MonoBehaviour
{
#if UNITY_IOS
    private string gameId = "1670050";
#elif UNITY_ANDROID
    private string gameId = "1486550";
#elif UNITY_EDITOR
    private string gameId = "1670050";
#elif UNITY_STANDALONE_WIN
    private string gameId = "1670050";
#endif

    [SerializeField] private bool EditModel = false;

    [SerializeField] private Button adsButton;
    [SerializeField] private GameObject adsButtonEffect;
    [SerializeField] private Button rewardButton;
    [SerializeField] private GameObject rewardButtonEffect;

    [SerializeField] private Image picture;
    [SerializeField] private Text playerName;

    private GameObject rewardEveryDayAlertor;
    private GameObject editInfoAlertor;
    private GameObject watchingAdsAlertor;
    private GameObject rewardAlertor;

    [SerializeField] private GameObject countdownPanel;

    [SerializeField] private GameObject tipScreens;

    private int tipScreenIndex = 0;

    private string placementId = "rewardedVideo";

    private Sprite[] pictureList;

    private void Awake()
    {
        pictureList = Resources.LoadAll<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface() + "SpellBook");
    }

    private void Start()
    {
        Debug.Log("LobySceneUIManager Start");
        //OnRoomJoined();

        if (Advertisement.isSupported)
        {
            Advertisement.Initialize(gameId, true);
        }

        editInfoAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceLobyScene("EditInfoAlertor"));
        rewardAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceRewardView("RewardAlertor"));
        watchingAdsAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterface("WatchingAdsAlertor"));
        rewardEveryDayAlertor = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceLobyScene("RewardEveryDayAlertor"));

        if (FirebaseManager.Singleton)
        {
            picture.sprite = pictureList[FirebaseManager.Singleton.SignedPlayer.pictureindex];
            playerName.text = FirebaseManager.Singleton.SignedPlayer.playername;
        }

        if (PlayerAttributeManager.Singleton)
        {
            if (PlayerAttributeManager.Singleton.Installed == false)
            {
                TipScreenControl next_control = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
                next_control.OnPointEvent += Control_OnPointEvent;
                next_control.gameObject.SetActive(true);

                TipScreenControl control = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
                control.OnPointEvent += Control_OnPointEvent;
            }
        }
    }


    private void Control_OnPointEvent(object sender, System.EventArgs e)
    {
        TipScreenControl privous_control = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
        privous_control.OnPointEvent -= Control_OnPointEvent;
        privous_control.gameObject.SetActive(false);

        if (tipScreenIndex == (tipScreens.transform.childCount - 1))
        {
            Debug.Log("Finished Tip");
            tipScreenIndex = 0;
            PlayerAttributeManager.Singleton.Installed = true;
            return;
        }

        tipScreenIndex += 1;

        TipScreenControl next_control = tipScreens.transform.GetChild(tipScreenIndex).GetComponent<TipScreenControl>();
        next_control.OnPointEvent += Control_OnPointEvent;
        next_control.gameObject.SetActive(true);
    }

    private void WarnAlertor_OnClosed(object sender, System.EventArgs e)
    {
        WarnAlertor.OnClosed -= WarnAlertor_OnClosed;
    }

    private void Update()
    {
        if (adsButton)
        {
            adsButton.interactable = Advertisement.IsReady(placementId);
            adsButtonEffect.SetActive(adsButton);
        }

        if (rewardButton)
        {
            if (PlayerAttributeManager.Singleton)
            {
                rewardButtonEffect.SetActive(!PlayerAttributeManager.Singleton.IsGainReward);
            }
        }

        //SceneManager.LoadSceneAsync(ProjectKeywordList.kSceneForCharacterSelection);
    }

    private void OnDestroy()
    {
        //Debug.Log("OnDestroy");        
        Resources.UnloadUnusedAssets();
    }

    private void OnDisable()
    {
        //Debug.Log("OnDisable");
        Resources.UnloadUnusedAssets();
    }
}


public partial class LobySceneUIManager : MonoBehaviour
{
    public void ShowAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show(placementId, options);
    }

    private void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");
            GameObject rewardAlertorInstance = Instantiate(rewardAlertor, this.transform);
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}

/// <summary>
/// Open watch Ads alertor.
/// </summary>
public partial class LobySceneUIManager : MonoBehaviour
{
    public void OpenWatchingAdsAlertor()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayToolItemUnlockSE();
        }
        GameObject watchingAdsAlertorInstance = Instantiate(watchingAdsAlertor, this.transform);
        var rect = watchingAdsAlertorInstance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }
}


/// <summary>
/// Open edit info alertor.
/// </summary>
public partial class LobySceneUIManager : MonoBehaviour
{
    public void OpenEditInfoAlertor()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }
        GameObject instance = Instantiate(editInfoAlertor, this.transform);
        EditInfoAlertor infoAlertor = instance.GetComponent<EditInfoAlertor>();
        infoAlertor.UpdatedTheSignedPlayer += InfoAlertor_UpdatedTheSignedPlayer;
        infoAlertor.ClosedTheEditInfoAlertor += InfoAlertor_ClosedTheEditInfoAlertor;
        var rect = instance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }

    private void InfoAlertor_ClosedTheEditInfoAlertor(object sender, System.EventArgs e)
    {
        EditInfoAlertor infoAlertor = sender as EditInfoAlertor;
        infoAlertor.UpdatedTheSignedPlayer -= InfoAlertor_UpdatedTheSignedPlayer;
        infoAlertor.ClosedTheEditInfoAlertor -= InfoAlertor_ClosedTheEditInfoAlertor;
    }

    private void InfoAlertor_UpdatedTheSignedPlayer(SignedPlayer s)
    {
        playerName.text = s.playername;
        picture.sprite = pictureList[s.pictureindex];
    }
}



/// <summary>
/// Open reward every day alertor.
/// </summary>
public partial class LobySceneUIManager : MonoBehaviour
{
    public void OpenRewardEveryDayAlertor()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }
        GameObject instance = Instantiate(rewardEveryDayAlertor, this.transform);
        var rect = instance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        //RewardEveryDayAlertor alertor = instance.GetComponent<RewardEveryDayAlertor>();
        //alertor.GainedReward += Alertor_GainedReward;
    }

    private void Alertor_GainedReward(object sender, System.EventArgs e)
    {

    }

    public void OpenTeachIntroduceScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }
    }
}



/// <summary>
/// Loding to Diamond Shop Scene.
/// </summary>
public partial class LobySceneUIManager : MonoBehaviour
{
    public void StartGame()
    {
        if (PlayerPrefsUtility.LoadValue<bool>(ProjectKeywordList.kPPrefsForPlayerDecisionWarStatus) == false)
        {
            WarnAlertor.OnClosed += WarnAlertor_OnClosed;
            WarnAlertor.Show("請至神社選擇一位您要出戰的守護神", this.transform);
            return;
        }
        StartCoroutine(StartLoading(ProjectKeywordList.kSceneForGame, this.transform));
    }

    private IEnumerator StartLoading(string scene, Transform t)
    {
        GameObject loadingView = Resources.Load<GameObject>(PathManager.LoadResourceFilePath.PrefabUserInterface("LoadingView"));
        GameObject instance = Instantiate(loadingView, t);
        instance.SetActive(true);

        // The loadingBar is child of loadingView.
        Slider loadingBar = instance.transform.Find("LoadingBar").GetComponent<Slider>();

        // The loadingText is child of loadingBar.
        Text loadingText = loadingBar.transform.Find("LoadingText").GetComponent<Text>();

        yield return new WaitUntil(() => instance.activeInHierarchy);

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);
        op.allowSceneActivation = false;
        while (op.progress < 0.9f)
        {
            toProgress = (int)op.progress * 100;
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                loadingBar.value = displayProgress * 0.01f;
                loadingText.text = displayProgress + "%";
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            loadingBar.value = displayProgress * 0.01f;
            loadingText.text = displayProgress + "%";
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;
    }

    // 加入Room后返回
    private void OnRoomJoined()
    {
        if (countdownPanel == null) { return; }
        //Debug.Log(transform);

        Instantiate(countdownPanel, transform);
        Debug.Log("We are joined to Room");
    }
}


/// <summary>
/// Loading to Tools Shop Scene.
/// </summary>
public partial class LobySceneUIManager : MonoBehaviour
{
    public void LoadingToolsShopScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }
        StartCoroutine(StartLoading(ProjectKeywordList.kSceneForToolsShop, this.transform));
    }
}


/// <summary>
/// Loading to Hero Introduce Scene.
/// </summary>
public partial class LobySceneUIManager : MonoBehaviour
{
    public void ToHeroIntroduceScene()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayOpenBookSE();
        }
        StartCoroutine(StartLoading(ProjectKeywordList.kSceneForCharacterSelection, this.transform));
    }
}


public partial class LobySceneUIManager : MonoBehaviour
{
    public void OpenRankingPanel()
    {
        RankingAlertor.OnClickedMenu += RankingAlertor_OnClickedMenu;
        RankingAlertor.OnCancel += RankingAlertor_OnCancel;
        RankingAlertor.OnClose += RankingAlertor_OnClose;
        RankingAlertor.Open();

        TextAsset[] heroDatas = Resources.LoadAll<TextAsset>("Data/Heros");
        string JSON = heroDatas[0].text;
        HeroData heroData = JsonUtility.FromJson<HeroData>(JSON);

        if (FirebaseManager.Singleton)
        {
            FirebaseManager.Singleton.GetRankDataByHeroID(heroData.id, (message, haList, spList) =>
            {
                Debug.Log(message);
                RankingAlertor.SetDataToUpdateRankPanelBy(haList, spList);
            });
        }
    }

    private void RankingAlertor_OnClickedMenu(object sender, System.EventArgs e)
    {
        RankingAlertorEventArgs _e = (RankingAlertorEventArgs)e;
        Debug.Log(_e.Id);
        if (FirebaseManager.Singleton)
        {
            FirebaseManager.Singleton.GetRankDataByHeroID(_e.Id, (message, haList, spList) =>
            {
                Debug.Log(message);
                RankingAlertor.SetDataToUpdateRankPanelBy(haList, spList);
            });
        }
    }

    private void RankingAlertor_OnClose(object sender, System.EventArgs e)
    {
        Resources.UnloadUnusedAssets();
        RankingAlertor.OnClickedMenu -= RankingAlertor_OnClickedMenu;
        RankingAlertor.OnCancel -= RankingAlertor_OnCancel;
        RankingAlertor.OnClose -= RankingAlertor_OnClose;
    }

    private void RankingAlertor_OnCancel(object sender, System.EventArgs e)
    {

    }
}


public partial class LobySceneUIManager : MonoBehaviour
{

}

