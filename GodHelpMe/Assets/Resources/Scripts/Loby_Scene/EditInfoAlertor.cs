﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditInfoAlertorDate
{
    public string Year;
    public string Month;
    public string Day;

    public EditInfoAlertorDate() { }
}

public class EditInfoAlertor : MonoBehaviour
{
    internal delegate void EditInfoAlertorDelegate(SignedPlayer s);
    internal event EditInfoAlertorDelegate UpdatedTheSignedPlayer;

    internal event System.EventHandler ClosedTheEditInfoAlertor;

    [SerializeField]
    private Image picture;

    [SerializeField]
    private InputField nameInputField;
    private string playerName;

    [SerializeField]
    private InputField yearInputField;
    [SerializeField]
    private InputField monthInputField;
    [SerializeField]
    private InputField dayInputField;
    [SerializeField]
    private Button MaleHookStatusButton;
    [SerializeField]
    private Button FemaleHookStatusButton;

    [SerializeField]
    private GameObject editInfoPictureAlertor;

    [SerializeField]
    private GameObject editInfoCalendarAlertor;

    [SerializeField]
    private Sprite hook;
    [SerializeField]
    private Sprite unHook;

    private Sprite[] pictureList;

    private int pictureIndex = -1;
    private int PictureIndex
    {
        get { return pictureIndex; }
        set
        {
            pictureIndex = value;
            if (pictureList.Length > 0) { picture.sprite = pictureList[pictureIndex]; }
        }
    }

    private CNPRooleGender gender;
    private CNPRooleGender Gender
    {
        get { return gender; }
        set
        {
            gender = value;
            switch (gender)
            {
                case CNPRooleGender.Male:
                    MaleHookStatusButton.image.sprite = hook;
                    FemaleHookStatusButton.image.sprite = unHook;
                    break;
                case CNPRooleGender.Female:
                    FemaleHookStatusButton.image.sprite = hook;
                    MaleHookStatusButton.image.sprite = unHook;
                    break;
            }
        }
    }

    private EditInfoAlertorDate editInfoAlertorDate;
    private EditInfoAlertorDate EditInfoAlertorDate
    {
        get { return editInfoAlertorDate; }
        set
        {
            editInfoAlertorDate = value;
            yearInputField.text = editInfoAlertorDate.Year;
            monthInputField.text = editInfoAlertorDate.Month;
            dayInputField.text = editInfoAlertorDate.Day;
        }
    }

    private void Awake()
    {
        pictureList = Resources.LoadAll<Sprite>(PathManager.LoadResourceFilePath.TextureUserInterface() + "SpellBook");
    }

    private void Start()
    {
        if (FirebaseManager.Singleton)
        {
            nameInputField.text = FirebaseManager.Singleton.SignedPlayer.playername;

            PictureIndex = FirebaseManager.Singleton.SignedPlayer.pictureindex;
            Gender = (CNPRooleGender)FirebaseManager.Singleton.SignedPlayer.gender;
            EditInfoAlertorDate = GetBirthDate(FirebaseManager.Singleton.SignedPlayer.birthdate);
        }
    }

    private void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }

    public void OpenEditInfoPictureAlertor()
    {
        GameObject instance = Instantiate(editInfoPictureAlertor, this.transform.parent);
        EditInfoPictureAlertor editInfoPicture = instance.GetComponent<EditInfoPictureAlertor>();
        editInfoPicture.ChoosedIndexOfPicture += EditInfoPicture_ChoosedIndexOfPicture;
        var rect = instance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }

    public void OpenEditInfoCalendarAlertor()
    {
        if (SEManager.Singleton)
            SEManager.Singleton.PlayOpenBookSE();

        GameObject instance = Instantiate(editInfoCalendarAlertor, this.transform.parent);
        EditInfoCalendarAlertor editInfoCalendar = instance.GetComponent<EditInfoCalendarAlertor>();
        editInfoCalendar.ChoosedTheDate += EditInfoCalendar_ChoosedTheDate;
        var rect = instance.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }

    private void EditInfoCalendar_ChoosedTheDate(EditInfoAlertorDate editInfoAlertorDate)
    {
        EditInfoAlertorDate = editInfoAlertorDate;
    }

    private void EditInfoPicture_ChoosedIndexOfPicture(int index)
    {
        PictureIndex = index;
    }

    public void Apply()
    {
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayDecideSE();
        }

        if (FirebaseManager.Singleton)
        {
            FirebaseManager.Singleton.OnSignedPlayerUpdated += Singleton_OnSignedPlayerUpdated;
            SignedPlayer s = FirebaseManager.Singleton.SignedPlayer;
            s.pictureindex = pictureIndex;
            s.playername = playerName;
            s.gender = (int)Gender;
            s.birthdate = EditInfoAlertorDateToBirthDate();
            FirebaseManager.Singleton.SignedPlayer = s;
            if (UpdatedTheSignedPlayer != null) { UpdatedTheSignedPlayer(FirebaseManager.Singleton.SignedPlayer); }
        }

        Close();
    }

    private void Singleton_OnSignedPlayerUpdated(bool finished)
    {
        FirebaseManager.Singleton.OnSignedPlayerUpdated -= Singleton_OnSignedPlayerUpdated;

        if (finished)
        {
            Debug.Log("succeeded for Update player data.");
        }
        else
        {
            Debug.Log("fail for Update player data.");
        }
    }

    public void Close()
    {
        DestroyImmediate(gameObject);
        if (ClosedTheEditInfoAlertor != null) { ClosedTheEditInfoAlertor(this, System.EventArgs.Empty); }
        if (SEManager.Singleton)
        {
            SEManager.Singleton.PlayCancelSE();
        }
    }

    public void NameInputEvent(InputField input)
    {
        if (input.text.Length > 0)
        {
            playerName = input.text.ToString();
            print("Player Name: " + input.text);
        }
        else if (input.text.Length == 0)
        {
            print("Player Name Input is Empty");
        }
    }

    private string EditInfoAlertorDateToBirthDate()
    {
        return string.Format("{0}_{1}_{2}", EditInfoAlertorDate.Year, EditInfoAlertorDate.Month, EditInfoAlertorDate.Day);
    }

    private EditInfoAlertorDate GetBirthDate(string date)
    {
        EditInfoAlertorDate alertorDate = new EditInfoAlertorDate();

        if (date != null)
        {
            if (date.Contains("_"))
            {
                string[] dates = date.Split('_');
                alertorDate.Year = dates[0];
                alertorDate.Month = dates[1];
                alertorDate.Day = dates[2];
            }
        }

        return alertorDate;

    }

    public void YearInputEvent(InputField input)
    {
        if (input.text.Length > 0)
        {
            print("Year Name: " + input.text);
        }
        else if (input.text.Length == 0)
        {
            print("Year Name Input is Empty");
        }
    }

    public void MonthInputEvent(InputField input)
    {
        if (input.text.Length > 0)
        {
            print("Month Name: " + input.text);
        }
        else if (input.text.Length == 0)
        {
            print("Month Name Input is Empty");
        }
    }

    public void DayInputEvent(InputField input)
    {
        if (input.text.Length > 0)
        {
            print("Day Name: " + input.text);
        }
        else if (input.text.Length == 0)
        {
            print("Day Name Input is Empty");
        }
    }

    public void HookStatusButtonEvent(Button button)
    {
        if (button == MaleHookStatusButton)
        {
            Gender = CNPRooleGender.Male;
        }
        else
        {
            Gender = CNPRooleGender.Female;
        }
    }
}
