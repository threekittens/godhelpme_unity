﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PathManager;
using System;
using System.Globalization;

public partial class RewardEveryDayAlertor : MonoBehaviour
{
    internal event System.EventHandler GainedReward;

    private GameObject pointer;
    private RectTransform pointerRect;

    private int day;
    private float rotationDegree;
    private int pointerSpeed = 200;

    private Sprite hookSprite;

    private void Awake()
    {
        hookSprite = Resources.Load<Sprite>(LoadResourceFilePath.TextureUserInterface("hook"));
    }

    private void Start()
    {
        if (PlayerAttributeManager.Singleton)
        {
            // the day of first day is 0           
            day = (PlayerAttributeManager.Singleton.RewardDay);

            if (day > 0)
            {
                for (int i = 0; i <= day; i++)
                {
                    GameObject instance = transform.Find("Background/" + (i + 1) + "Day").gameObject;
                    instance.transform.Find("Hook").GetComponent<Image>().sprite = Resources.Load<Sprite>(LoadResourceFilePath.TextureUserInterface("hook"));
                    StopEffectOnRewardImage(instance);
                }
            }
            else
            {
                // Is First Day
                GameObject instance = transform.Find("Background/" + (day + 1) + "Day").gameObject;
                instance.transform.Find("Hook").GetComponent<Image>().sprite = hookSprite;
                StopEffectOnRewardImage(instance);
            }

            pointer = transform.Find("Background/Pointer").gameObject;
            pointerRect = pointer.GetComponent<RectTransform>();
        }
    }

    private void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }

    private void Update()
    {
        // 360 is a Cycle and minues 180 is image just half a cycle / (7 day minus 1 day) mutiple current day
        if (pointerRect.rotation.eulerAngles.z > 0 && pointerRect.rotation.eulerAngles.z <= (360 - (180 / (7 - 1) * (day))))
        {
            GainRewaedAndRecordDay();
            return;
        }

        rotationDegree = -(pointerSpeed * Time.deltaTime);
        pointerRect.Rotate(new Vector3(0, 0, rotationDegree));
    }

    //IEnumerator test()
    //{
    //    TimeSpan now = TimeSpan.FromTicks(DateTime.Now.Ticks);

    //    yield return new WaitForSeconds(5);

    //    TimeSpan after = TimeSpan.FromTicks(DateTime.Now.Ticks);

    //    TimeSpan diff = after - now;
    //    double seconds = diff.TotalSeconds;

    //    Debug.Log(seconds);
    //}

    private void GainRewaedAndRecordDay()
    {
        if (!PlayerAttributeManager.Singleton.IsGainReward)
        {
            Debug.Log("Gain Rewards.");
            PlayerAttributeManager.Singleton.IsGainReward = true;
            PlayerAttributeManager.Singleton.PreviousOpenApplicationTime = DateTime.Now;
            GameObject instance = Resources.Load<GameObject>(LoadResourceFilePath.PrefabUserInterfaceRewardView("RewardAlertor"));
            Instantiate(instance, transform);
            if (GainedReward != null) { GainedReward(this, EventArgs.Empty); }
        }
    }

    private void StopEffectOnRewardImage(GameObject instance)
    {
        Transform rwardImage = instance.transform.Find("Reward_Image").transform;
        for (int index = 0; index < rwardImage.childCount; index++)
        {
            rwardImage.GetChild(index).gameObject.SetActive(false);
        }
    }
}


public partial class RewardEveryDayAlertor : MonoBehaviour
{
    public void Close()
    {
        DestroyImmediate(gameObject);
    }
}
