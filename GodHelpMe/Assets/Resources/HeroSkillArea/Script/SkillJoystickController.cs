﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum SkillKind
{
    Skill01 = 0,
    Skill02 = 1,
    Skill03 = 2,
    Skill04 = 3,
}

public class SkillJoystickController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{

    [SerializeField]
    private bool debugModel;

    private GameObject joystickObj;
    private SkillJoystick joystick;
    private SkillArea joySkillArea;
    private SkillsJoystickPanel skillsJoystickPanel;
    private Button upLevelButton;
    private Image expInner;
    private SkillCoolDown skillCoolDown;
    private GameObject cancelCastButton;

    private ThirdCamera thirdCamera;
    private Transform player;
    private List<Renderer> rList = new List<Renderer>();

    internal delegate void TriggerUpSkillLevel(SkillKind skillKind, int level);
    internal event TriggerUpSkillLevel OnTriggerUpSkillLevel;

    internal delegate void TriggerSkillCoolDown(int second);
    internal TriggerSkillCoolDown OnTriggerSkillCoolDown;
    private int maxSkillLevel = 25;
    private SkillKind skillKind;

    private int _skillLevel = 0;
    internal int SkillLevel { get { return _skillLevel; } set { _skillLevel = value; } }

    private void Start()
    {
        OnTriggerSkillCoolDown = OnTriggeredSkillCoolDown;

        cancelCastButton = transform.parent.parent.Find("CancelCastButton").gameObject;
        cancelCastButton.SetActive(false);

        skillsJoystickPanel = transform.parent.GetComponent<SkillsJoystickPanel>();
        if (transform.Find("SkillUpLevelButton"))
        {
            upLevelButton = transform.Find("SkillUpLevelButton").GetComponent<Button>();
            expInner = transform.Find("SkillExpBarInner").GetComponent<Image>();
            upLevelButton.onClick.AddListener(() => OnUpLevelButtonClicked(upLevelButton, expInner));
        }
        joystickObj = transform.GetChild(0).gameObject;
        joystick = joystickObj.GetComponent<SkillJoystick>();
        joySkillArea = joystickObj.GetComponent<SkillArea>();
        joySkillArea.InitializesSkillArea(joystick);

        if (transform.Find("SkillCoolDown"))
        {
            skillCoolDown = transform.Find("SkillCoolDown").GetComponent<SkillCoolDown>();
        }

        for (int i = 0; i < transform.parent.childCount; i++)
        {
            if (transform.parent.GetChild(i).name == transform.name)
            {
                switch (i)
                {
                    case 0:
                        skillKind = SkillKind.Skill01;
                        if (PlayerAttributeManager.Singleton)
                        {
                            SkillLevel = PlayerAttributeManager.Singleton.HeroAchievement.skill01;
                        }
                        break;
                    case 1:
                        skillKind = SkillKind.Skill02;
                        if (PlayerAttributeManager.Singleton)
                        {
                            SkillLevel = PlayerAttributeManager.Singleton.HeroAchievement.skill02;
                        }
                        break;
                    case 2:
                        skillKind = SkillKind.Skill03;
                        if (PlayerAttributeManager.Singleton)
                        {
                            SkillLevel = PlayerAttributeManager.Singleton.HeroAchievement.skill03;
                        }
                        break;
                    case 3:
                        skillKind = SkillKind.Skill04;
                        if (PlayerAttributeManager.Singleton)
                        {
                            SkillLevel = PlayerAttributeManager.Singleton.HeroAchievement.skill04;
                        }
                        break;
                }
                break;
            }
        }

        if (SkillLevel == 0)
        {
            Color c = transform.GetComponent<Image>().color;
            c.a = 0.5f;
            transform.GetComponent<Image>().color = c;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!debugModel)
        {
            if (skillKind != SkillKind.Skill01)
            {
                if (SkillLevel == 0) { return; }
            }
        }

        thirdCamera = Camera.main.gameObject.GetComponent<ThirdCamera>();
        player = thirdCamera.target;

        if (IsCoolDownStatus()) { return; }
        cancelCastButton.SetActive(true);

        skillsJoystickPanel.OnPlayerCancelCastSkill(false);

        joystick.OnPointerDown(eventData);
        joystickObj.SetActive(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!debugModel)
        {
            if (skillKind != SkillKind.Skill01)
            {
                if (SkillLevel == 0) { return; }
            }
        }

        if (IsCoolDownStatus()) { return; }

        cancelCastButton.SetActive(false);

        player.Find("SkillArea").GetComponentsInChildren(rList);
        foreach (Renderer r in rList)
        {
            var blue_color = new Color32(58, 133, 255, 255);
            r.material.SetColor("_TintColor", blue_color);
        }

        joystick.OnPointerUp(eventData);
        joystickObj.SetActive(false);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (IsCoolDownStatus()) { return; }
        joystick.OnDrag(eventData);
    }

    public void OnUpLevelButtonClicked(Button button, Image expInner)
    {
        if (SkillLevel > maxSkillLevel) { return; }

        float progress = expInner.fillAmount + (1.0f / maxSkillLevel);
        expInner.fillAmount = progress;

        skillsJoystickPanel.SkillPoint = skillsJoystickPanel.SkillPoint - 1;

        if (skillsJoystickPanel.SkillPoint > 0)
        {
            button.gameObject.SetActive(true);
        }
        else
        {
            button.gameObject.SetActive(false);
        }

        SkillLevel += 1;

        if (SkillLevel > 0)
        {
            Color c = transform.GetComponent<Image>().color;
            c.a = 1.0f;
            transform.GetComponent<Image>().color = c;
        }

        if (OnTriggerUpSkillLevel != null)
        {
            OnTriggerUpSkillLevel(skillKind, SkillLevel);
        }
    }

    private void OnTriggeredSkillCoolDown(int second)
    {
        if (skillCoolDown == null) { return; }
        if (skillsJoystickPanel.IsCancelCast) { return; }
        skillCoolDown.CDSecond = second;
        skillCoolDown.gameObject.SetActive(true);
    }

    private bool IsCoolDownStatus()
    {
        if (skillCoolDown == null) { return false; }
        return skillCoolDown.gameObject.activeSelf;
    }
}
