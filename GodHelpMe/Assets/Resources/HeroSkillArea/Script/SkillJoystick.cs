﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class SkillJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{

    // 控制器按下后，可滑动的范围。
    private float outerCircleRadius = 10;

    public Transform innerCircleTrans;

    private Vector2 outerCircleStartWorldPos = Vector2.zero;

    public Action<Vector2> onJoystickDownEvent;     // 按下事件
    public Action onJoystickUpEvent;     // 抬起事件
    public Action<Vector2> onJoystickMoveEvent;     // 滑动事件

    private void OnEnable()
    {
        innerCircleTrans = transform.GetChild(0);
        outerCircleStartWorldPos = transform.position;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        innerCircleTrans.position = eventData.position;
        if (onJoystickDownEvent != null)
            onJoystickDownEvent(innerCircleTrans.localPosition / outerCircleRadius);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        innerCircleTrans.localPosition = Vector3.zero;
        if (onJoystickUpEvent != null)
            onJoystickUpEvent();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 touchPos = eventData.position - outerCircleStartWorldPos;
        if (Vector3.Distance(touchPos, Vector2.zero) < outerCircleRadius)
            innerCircleTrans.localPosition = touchPos;
        else
            innerCircleTrans.localPosition = touchPos.normalized * outerCircleRadius;

        if (onJoystickMoveEvent != null)
            onJoystickMoveEvent(innerCircleTrans.localPosition / outerCircleRadius);
    }
}
