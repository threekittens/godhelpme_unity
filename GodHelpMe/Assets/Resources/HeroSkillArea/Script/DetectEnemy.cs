﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public enum SkillType
{
    skill01 = 0,
    skill02 = 1,
    skill03 = 2,
    skill04 = 3
}

[System.Serializable]
public class DetectEnenmyProperty
{
    public SkillType skillType;
    public List<GameObject> targets;
    public float range;
    public Vector3 forwardEndPos;
    public DetectEnenmyProperty() { }
}

public partial class DetectEnemy : MonoBehaviour
{
    // Callback signature    
    public delegate void DetectEnemyCallBack(DetectEnenmyProperty detectEnenmyProperty);
    // Event declaration
    public event DetectEnemyCallBack OnDetectedEnemy;

    public bool isDrawGizmos = true;
    public float range;
    public int maxTargets;
    private LayerMask layerMask;
    private Collider[] enemiesInRange = null;

    [SerializeField]
    public SkillType skillType;
    // this is a list that we will populate with enemies in range
    private List<GameObject> enemiesToHit;
    private List<GameObject> lockedEnemiesInRange = new List<GameObject>();
    private Vector3 playerPos;
    private Vector3 forwardEndPos;
    private Vector3 targetPos;
    private float maximumRange;
    private SkillAreaType skillAreaType;
}


public partial class DetectEnemy : MonoBehaviour
{
    public void Cast(Vector3 _playerPos, Vector3 _forwardEndPos, float outerRadius, SkillAreaType type)
    {
        // 每次 重新 null 不占用记忆体；
        enemiesInRange = null;
        maximumRange = outerRadius;
        skillAreaType = type;
        playerPos = _playerPos;
        forwardEndPos = _forwardEndPos;
        layerMask = 1 << 9;

        // Get any colliders in range and only on the ENEMY_ATTACKABLE layermask.
        switch (skillAreaType)
        {
            case SkillAreaType.OuterCircle:
                StartCoroutine(OuterCircle());
                break;
            case SkillAreaType.OuterCircle_InnerCircle:
                StartCoroutine(OuterCircle_InnerCircle());
                break;
            case SkillAreaType.OuterCircle_InnerCube:
                StartCoroutine(OuterCircle_InnerCube());
                break;
            case SkillAreaType.OuterCircle_InnerSector:
                StartCoroutine(OuterCircle_InnerSector());
                break;
        }
    }
}

public partial class DetectEnemy : MonoBehaviour
{
    // 放開技能搖桿後觸發 傳入 player
    public void ActiveDetectEnemty(GameObject player)
    {
        if (player != null)
        {
            if (OnDetectedEnemy != null)
            {
                //Debug.Log("Active Fire");
                DetectEnenmyProperty detectEnenmyProperty = new DetectEnenmyProperty();
                detectEnenmyProperty.skillType = skillType;
                detectEnenmyProperty.targets = enemiesToHit;
                detectEnenmyProperty.range = maximumRange;
                detectEnenmyProperty.forwardEndPos = targetPos;
                OnDetectedEnemy(detectEnenmyProperty);
            }
        }

        if (lockedEnemiesInRange.Count > 0)
        {
            foreach (GameObject enemy in lockedEnemiesInRange)
            {
                MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                marksTarget.ExecutesMarks(false);
            }

            lockedEnemiesInRange.Clear();
        }
    }
}

public partial class DetectEnemy : MonoBehaviour
{
    IEnumerator OuterCircle()
    {
        enemiesInRange = Physics.OverlapSphere(playerPos, range, layerMask);

        targetPos = playerPos;

        // if more than maxTargets were found
        if (enemiesInRange.Length > 0)
        {
            //DebugLog(enemiesInRange.Length);
            enemiesInRange.OrderBy(hit => Vector3.Distance(hit.transform.position, transform.position));

            foreach (Collider c in enemiesInRange)
            {
                //DebugLog(c);
            }
        }

        // 每次 init new List;
        enemiesToHit = new List<GameObject>();

        // populate the "enemiesToHit" list, will only add up to "maxTargets" number of objects
        for (int i = 0; i < maxTargets; i++)
        {
            // make sure "i" doesn't go past the number of enemies in range
            if (i < enemiesInRange.Length)
            {
                Collider enemy = enemiesInRange[i];
                //DebugLog(enemy.name);
                MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                marksTarget.ExecutesMarks(false);

                Vector3 enemyPos = enemy.transform.position;
                List<GameObject> tempArray = new List<GameObject>();

                // 检查 目标 距离 是否 低于 3
                float dis = Vector3.Distance(enemyPos, playerPos);
                if (dis < range)
                {
                    // 建立 暂时 用来找出 排序后 最近 的目标
                    tempArray.Add(enemy.gameObject);
                    tempArray.OrderBy(obj => Vector3.Distance(obj.transform.position, playerPos));

                    // 检查 目标群里数量小于 1 时 可添加 目标 
                    if (enemiesToHit.Count < 1)
                    {
                        var tempEnemy = enemiesInRange[0];
                        enemiesToHit.Add(tempArray[0].gameObject);
                        lockedEnemiesInRange.Add(enemy.gameObject);
                        MarksTarget tempMarksTarget = tempEnemy.GetComponent<MarksTarget>();
                        tempMarksTarget.ExecutesMarks(true);
                    }
                }
            }
            else
            {
                // stop the loop if there are no more hits                
                break;
            }
        }

        if (enemiesToHit.Count > 0)
        {
            IEnumerable<GameObject> onlyInLockedSet = lockedEnemiesInRange.Except(enemiesToHit);
            List<GameObject> asList = onlyInLockedSet.ToList();

            if (asList.Count > 0)
            {
                foreach (GameObject enemy in asList)
                {
                    if (enemy)
                    {
                        MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                        marksTarget.ExecutesMarks(false);
                    }
                }
                lockedEnemiesInRange.Clear();
            }

            foreach (GameObject enemy in enemiesToHit)
            {
                //Debug.Log("enemiesToHit in area enemy = " + enemy);
                //Debug.Log("enemiesToHit in area enemy count = " + enemiesToHit.Count);
                // do damage, send a projectile, whatever else
            }
        }
        else
        {

            if (lockedEnemiesInRange.Count > 0)
            {
                foreach (GameObject enemy in lockedEnemiesInRange)
                {
                    if (enemy != null)
                    {
                        MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                        marksTarget.ExecutesMarks(false);
                    }
                }

                lockedEnemiesInRange.Clear();
            }
        }

        yield break;
    }
}


public partial class DetectEnemy : MonoBehaviour
{
    IEnumerator OuterCircle_InnerCircle()
    {
        enemiesInRange = Physics.OverlapSphere(forwardEndPos, range, layerMask);
        targetPos = forwardEndPos;

        // if more than maxTargets were found
        if (enemiesInRange.Length > 0)
        {
            //Debug.Log(enemiesInRange.Length);
            enemiesInRange.OrderBy(hit => Vector3.Distance(hit.transform.position, transform.position));
        }

        // 每次 init new List;
        enemiesToHit = new List<GameObject>();

        // populate the "enemiesToHit" list, will only add up to "maxTargets" number of objects
        for (int i = 0; i < maxTargets; i++)
        {
            // make sure "i" doesn't go past the number of enemies in range
            if (i < enemiesInRange.Length)
            {
                Collider enemy = enemiesInRange[i];
                MarksTarget marksTarget = enemy.gameObject.GetComponent<MarksTarget>();
                marksTarget.ExecutesMarks(false);

                enemiesToHit.Add(enemy.gameObject);
                marksTarget.ExecutesMarks(true);
                lockedEnemiesInRange.Add(enemy.gameObject);
            }
            else
            {
                break;
            }
        }

        if (enemiesToHit.Count > 0)
        {
            IEnumerable<GameObject> onlyInLockedSet = lockedEnemiesInRange.Except(enemiesToHit);
            List<GameObject> asList = onlyInLockedSet.ToList();

            if (asList.Count > 0)
            {
                foreach (GameObject enemy in asList)
                {
                    MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                    marksTarget.ExecutesMarks(false);
                }
                lockedEnemiesInRange.Clear();
            }

            foreach (GameObject enemy in enemiesToHit)
            {
                Debug.Log("enemiesToHit in area enemy = " + enemy);
                //Debug.Log("enemiesToHit in area enemy count = " + enemiesToHit.Count);
                // do damage, send a projectile, whatever else
            }
        }
        else
        {
            if (lockedEnemiesInRange.Count > 0)
            {
                foreach (GameObject enemy in lockedEnemiesInRange)
                {
                    MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                    marksTarget.ExecutesMarks(false);
                }

                lockedEnemiesInRange.Clear();
            }
        }

        yield break;
    }
}


public partial class DetectEnemy : MonoBehaviour
{
    IEnumerator OuterCircle_InnerCube()
    {
        enemiesInRange = Physics.OverlapCapsule(playerPos, forwardEndPos, range, layerMask);

        targetPos = forwardEndPos;

        // if more than maxTargets were found
        if (enemiesInRange.Length > 0)
        {
            enemiesInRange.OrderBy(hit => Vector3.Distance(hit.transform.position, transform.position));
        }

        // 每次 init new List;
        enemiesToHit = new List<GameObject>();

        // populate the "enemiesToHit" list, will only add up to "maxTargets" number of objects
        for (int i = 0; i < maxTargets; i++)
        {
            // make sure "i" doesn't go past the number of enemies in range
            if (i < enemiesInRange.Length)
            {
                Collider enemy = enemiesInRange[i];

                MarksTarget marksTarget = enemy.gameObject.GetComponent<MarksTarget>();
                marksTarget.ExecutesMarks(false);

                enemiesToHit.Add(enemy.gameObject);
                marksTarget.ExecutesMarks(true);

                lockedEnemiesInRange.Add(enemy.gameObject);

            }
            else
            {
                break;
            }
        }

        if (enemiesToHit.Count > 0)
        {
            IEnumerable<GameObject> onlyInLockedSet = lockedEnemiesInRange.Except(enemiesToHit);
            List<GameObject> asList = onlyInLockedSet.ToList();

            if (asList.Count > 0)
            {
                foreach (GameObject enemy in asList)
                {
                    if (enemy)
                    {
                        MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                        marksTarget.ExecutesMarks(false);
                    }

                }
                lockedEnemiesInRange.Clear();
            }

            foreach (GameObject enemy in enemiesToHit)
            {
                //Debug.Log("enemiesToHit in area enemy = " + enemy);
                // do damage, send a projectile, whatever else
            }
        }
        else
        {
            if (lockedEnemiesInRange.Count > 0)
            {
                foreach (GameObject enemy in lockedEnemiesInRange)
                {
                    if (enemy)
                    {
                        MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                        marksTarget.ExecutesMarks(false);
                    }
                }

                lockedEnemiesInRange.Clear();
            }
        }

        yield break;
    }
}


public partial class DetectEnemy : MonoBehaviour
{
    IEnumerator OuterCircle_InnerSector()
    {
        enemiesInRange = Physics.OverlapSphere(playerPos, range, layerMask);
        targetPos = playerPos;

        // if more than maxTargets were found
        if (enemiesInRange.Length > 0)
        {
            //Debug.Log(enemiesInRange.Length);
            enemiesInRange.OrderBy(hit => Vector3.Distance(hit.transform.position, transform.position));
        }

        // 每次 init new List;
        enemiesToHit = new List<GameObject>();

        // populate the "enemiesToHit" list, will only add up to "maxTargets" number of objects
        for (int i = 0; i < maxTargets; i++)
        {
            // make sure "i" doesn't go past the number of enemies in range
            if (i < enemiesInRange.Length)
            {
                Collider enemy = enemiesInRange[i];
                MarksTarget marksTarget = enemy.gameObject.GetComponent<MarksTarget>();
                marksTarget.ExecutesMarks(false);

                Vector3 enemyPosVec = playerPos - enemy.transform.position;
                Vector3 skillForwardVec = playerPos - forwardEndPos;

                // 计算 敌人 于 技能 之间的角度
                float degree = Vector3.Angle(enemyPosVec, skillForwardVec);

                float minDegree = 60f;
                if (degree <= minDegree)
                {
                    enemiesToHit.Add(enemy.gameObject);
                    marksTarget.ExecutesMarks(true);
                    lockedEnemiesInRange.Add(enemy.gameObject);
                }
            }
            else
            {
                // stop the loop if there are no more hits                
                break;
            }
        }

        if (enemiesToHit.Count > 0)
        {
            IEnumerable<GameObject> onlyInLockedSet = lockedEnemiesInRange.Except(enemiesToHit);
            List<GameObject> asList = onlyInLockedSet.ToList();

            if (asList.Count > 0)
            {
                foreach (GameObject enemy in asList)
                {
                    MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                    marksTarget.ExecutesMarks(false);
                }
                lockedEnemiesInRange.Clear();
            }

            foreach (GameObject enemy in enemiesToHit)
            {
                Debug.Log("enemiesToHit in area enemy = " + enemy);
                //Debug.Log("enemiesToHit in area enemy count = " + enemiesToHit.Count);
                // do damage, send a projectile, whatever else
            }
        }
        else
        {
            if (lockedEnemiesInRange.Count > 0)
            {
                foreach (GameObject enemy in lockedEnemiesInRange)
                {
                    MarksTarget marksTarget = enemy.GetComponent<MarksTarget>();
                    marksTarget.ExecutesMarks(false);
                }

                lockedEnemiesInRange.Clear();
            }
        }

        yield break;
    }
}

public partial class DetectEnemy : MonoBehaviour
{
    //畫出範圍用的
    private void OnDrawGizmos()
    {
        if (isDrawGizmos)
        {
            Color c = Color.yellow;
            c.a = 0.5f;
            Gizmos.color = c;
            Gizmos.DrawSphere(targetPos, range);
        }
    }
}