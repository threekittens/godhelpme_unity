﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    internal System.EventHandler OnTheHealthEmpty;

    internal delegate void HealthManagerDieNotice(GameObject target);
    internal event HealthManagerDieNotice OnHealthManagerDieNotice;

    internal delegate void ListeningAttacker(GameObject attacker);
    internal ListeningAttacker OnListeningAttacker;

    internal bool IsInvincibillity;

    [SerializeField] private float health;
    internal float Health { get { return health; } set { health = value; } }

    private float maxQuantityHealth;
    internal float MaxHealth { get { return maxQuantityHealth; } set { maxQuantityHealth = value; } }

    private GameObject attacker;
    internal GameObject Attacker { get { return attacker; } set { attacker = value; } }

    [SerializeField] internal GameObject mStatusBar;
    [SerializeField] private GameObject dieEffect;
    [SerializeField] private float fixedEffectScale = 1.0f;

    internal float HealthFillAmount { get { return healthBarTexture.fillAmount; } }
    private Image healthBarTexture;

    internal float DefaultHealth;

    private Animator animator;
    private Text healthQuantityText;
    private bool isObjectKilled;

    private void Start()
    {
        healthBarTexture = mStatusBar.transform.Find("Health").GetComponent<Image>();
        if (mStatusBar.transform.Find("HealthQuantity"))
        {
            healthQuantityText = mStatusBar.transform.Find("HealthQuantity").GetComponent<Text>();
        }
        maxQuantityHealth = Health;
        animator = transform.GetComponent<Animator>();
        OnListeningAttacker = OnListeningAttackerEvent;
        DefaultHealth = Health;
    }

    private void Update()
    {
        if (IsInvincibillity) { return; }

        if (healthBarTexture.fillAmount <= 0.0f)
        {
            if (isObjectKilled) { return; }

            isObjectKilled = true;

            if (OnTheHealthEmpty != null) { OnTheHealthEmpty(this, System.EventArgs.Empty); }

            StartCoroutine(ObjectKilled());
        }
        else
        {
            healthBarTexture.fillAmount = (Health / maxQuantityHealth);
            if (healthQuantityText)
            {
                if (Health < 0.0f) { Health = 0.0f; }

                healthQuantityText.text = string.Format("{0}/{1}", Health, maxQuantityHealth);
            }
        }
    }

    private void OnDestroy()
    {
        if (mStatusBar)
        {
            DestroyImmediate(mStatusBar);
        }
    }

    IEnumerator ObjectKilled()
    {
        if (animator)
        {
            animator.SetInteger(ProjectKeywordList.kAnimationForDie, (int)healthBarTexture.fillAmount);
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName(ProjectKeywordList.kAnimationForDie));
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length + animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        }

        if (dieEffect)
        {
            GameObject instance = Instantiate(dieEffect, gameObject.transform);
            ParticleSystem[] ps_array = instance.GetComponentsInChildren<ParticleSystem>(true);
            ps_array[0].Stop();

            CharacterController character = transform.GetComponent<CharacterController>();

            float effectScale = character.height / 5.0f;

            foreach (ParticleSystem ps in ps_array)
            {
                //var main = ps.main;
                //var mainSize = main.startSize;

                //mainSize.constantMin *= effectScale * fixedEffectScale;
                //mainSize.constantMax *= effectScale * fixedEffectScale;
                //main.startSize = mainSize;

                ps.transform.localScale *= fixedEffectScale;
            }

            //Scale Lights' range
            Light[] lights = instance.GetComponentsInChildren<Light>();
            foreach (Light l in lights)
            {
                l.range *= effectScale * fixedEffectScale;
                l.transform.localPosition *= effectScale * fixedEffectScale;
            }

            instance.transform.parent = null;
            ps_array[0].Play();
        }

        if (OnHealthManagerDieNotice != null)
        {
            OnHealthManagerDieNotice(gameObject);
        }

        DestroyImmediate(gameObject);
        yield break;
    }

    private void OnListeningAttackerEvent(GameObject player)
    {
        Attacker = player;
        //Debug.Log(string.Format("The Attakcer who is {0}", attacker.name));
    }
}
