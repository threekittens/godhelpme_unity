﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Bullet : MonoBehaviour
{
    // [SerializeField] [Range(0.0f, 1000.0f)] public float mDamage = 0.0f;
    private int damage;
    internal int Damage { get { return damage; } set { damage = value; } }
    [SerializeField] private float bulletSpeed = 10;
    private Transform target;
    internal Transform Target;

    private void Update()
    {
        if (this.gameObject != null)
        {
            StartCoroutine(Moveing());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Target == null) { return; }

        if (Target.GetComponent<HealthManager>().Health == 0.0) { return; }

        if (other.gameObject.layer != Target.gameObject.layer) { return; }

        FloatingTextControl.Initialize();
        Transform enemy = other.gameObject.transform;
        HealthManager helethManager = enemy.GetComponent<HealthManager>();
        helethManager.Health -= Damage;
        Vector3 contactPoint = enemy.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
        FloatingTextControl.CreateFloatingText(Damage.ToString(), contactPoint);
        HealthManager healthManager = other.gameObject.GetComponent<HealthManager>();
        GameObject player = GameObject.FindGameObjectWithTag(ProjectKeywordList.kTagForPlayer);
        healthManager.OnListeningAttacker(player);
        Destroy(this.gameObject);
    }

    IEnumerator Moveing()
    {
        if (Target == null)
        {
            DestroyImmediate(this.gameObject);
            yield break;
        }
        else
        {
            transform.LookAt(Target.position);

            if (this.gameObject != null && Target != null)
            {
                //fire
                float step = bulletSpeed * Time.deltaTime;
                this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, Target.position + Vector3.up, step);
            }
        }
        yield break;
    }
}