﻿using System.Collections;
using UnityEngine;

public class HealthBarScript : MonoBehaviour {

    private void Update()
    {
        StartCoroutine(lookAtCamera());
    }
    
    IEnumerator lookAtCamera()
    {
        //transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
        Vector3 mLocalPosition = transform.position;
        Quaternion mCameraRotation = Camera.main.transform.rotation;
        Vector3 mLookAtDirection = (mLocalPosition + (mCameraRotation * Vector3.forward));
        transform.LookAt(mLookAtDirection);
        yield break;
    }
}
