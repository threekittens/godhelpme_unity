﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CloseFightingInductor : MonoBehaviour
{
    internal delegate void HitedGameObject(int damage, GameObject gameObject);
    internal event HitedGameObject OnHitedGameObject;
        
    public int Damage = 75;  // 最大损伤

    [SerializeField]
    public FighterType fighterType;

    [SerializeField]
    public int targetLayer;

    public enum FighterType
    {
        Enemy,
        Player,
    }

    private void OnTriggerEnter(Collider other)
    {
        if (OnHitedGameObject != null)
        {
            OnHitedGameObject(Damage, other.gameObject);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(CloseFightingInductor))]
public class CloseFightingInductorEditor : Editor
{
    CloseFightingInductor inductor;

    private void OnEnable()
    {
        inductor = (CloseFightingInductor)target;
    }

    public override void OnInspectorGUI()
    {

        inductor.fighterType = (CloseFightingInductor.FighterType)EditorGUILayout.EnumPopup("FighterType", inductor.fighterType);
        switch (inductor.fighterType)
        {
            case CloseFightingInductor.FighterType.Enemy:
                {
                    inductor.Damage = EditorGUILayout.IntSlider("Damage", inductor.Damage, 0, 1000);
                    inductor.targetLayer = LayerMask.NameToLayer("ATTACKABLE");
                    break;
                }
            case CloseFightingInductor.FighterType.Player:
                {
                    inductor.Damage = EditorGUILayout.IntSlider("Damage", inductor.Damage, 0, 5000);
                    inductor.targetLayer = LayerMask.NameToLayer("ENEMY_ATTACKABLE");
                    break;
                }
        }


        if (GUILayout.Button("Add"))
        {
            Debug.Log("It's alive: " + target.name);
            CapsuleCollider collider = inductor.gameObject.AddComponent<CapsuleCollider>();
            collider.center = Vector3.zero;
            collider.isTrigger = true;
        }

        if (GUILayout.Button("Remove"))
        {
            CapsuleCollider collider = inductor.gameObject.GetComponent<CapsuleCollider>();
            DestroyImmediate(collider);
        }

        if (GUILayout.Button("FitSize"))
        {
            CapsuleCollider collider = inductor.gameObject.GetComponent<CapsuleCollider>();
            collider.center = Vector3.zero;

            float x = inductor.transform.localScale.x;
            float y = inductor.transform.localScale.y;
            float z = inductor.transform.localScale.z;

            float[] floats = new float[] { x, y, z };

            Debug.Log("x => " + x);
            Debug.Log("y => " + y);
            Debug.Log("z => " + z);

            floats.OrderBy(target => 1.0f);

            Debug.Log("first => " + floats.First());

            if (x == 1.0f && y == 1.0f && z == 1.0f)
            {
                collider.direction = 0;
                collider.height = x;
                float m_x = -(x / 2) - (x / 4);
                collider.center = new Vector3(m_x, 0, 0);
                return;
            }

            switch (floats.First().Equals(x))
            {
                case true:
                    collider.direction = 0;
                    collider.height = x;
                    float m_x = -(x / 2) - (x / 4);
                    collider.center = new Vector3(m_x, 0, 0);
                    break;
                case false:
                    break;
            }

            switch (floats.First().Equals(y))
            {
                case true:
                    collider.direction = 1;
                    collider.height = y;
                    float m_y = -(y / 2) - (y / 4);
                    collider.center = new Vector3(m_y, 0, 0);
                    break;
                case false:
                    break;
            }

            switch (floats.First().Equals(z))
            {
                case true:
                    collider.direction = 2;
                    collider.height = z;
                    float m_z = -(z / 2) - (z / 4);
                    collider.center = new Vector3(m_z, 0, 0);
                    break;
                case false:
                    break;
            }


        }

    }
}
#endif
