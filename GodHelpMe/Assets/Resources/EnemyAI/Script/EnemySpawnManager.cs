﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathManager;
using System;

public class EnemySpawnManager : MonoBehaviour
{
    // How long between each spawn.
    [SerializeField]
    private float spawnTime = 3f;

    // Limited spawn enemy count.
    private int bornMaximumCount;
    private int BornMaximumCount { get { return bornMaximumCount; } set { bornMaximumCount = value; } }

    [SerializeField]
    private EnemyTarget firstPriporityEnemyTarget;

    [SerializeField]
    private EnemyTarget secondPriporityEnemyTarget;

    // Count of current the spawn's
    private int bornCount;

    private GameObject[] enemys;

    private int checkpoint;
    public int Checkpoint
    {
        get
        {
            return checkpoint;
        }
        set
        {
            checkpoint = value;

            if ((checkpoint + 1) % 5 == 0)
            {
                BornMaximumCount = 1;
            }
            else
            {
                BornMaximumCount = 5;
            }

            // Debug.Log("checkpoint " + checkpoint);
            // BornMaximumCount = 1;
        }
    }

    private void Start()
    {
        enemys = Resources.LoadAll<GameObject>("Prefab/Enemys");
    }

    public void ActivitySpawen(bool isOpen)
    {
        if (isOpen)
        {
            //Debug.Log("Enable Spawn");

            // Always excute the fuction when enabled spawn.
            resetEnemyBornCount();
            InvokeRepeating("Spawn", spawnTime, spawnTime);

        }
        else
        {
            // Always unload unused assets at enemy spawn before.
            Resources.UnloadUnusedAssets();
            //Debug.Log("Cancel Spawn");
            CancelInvoke("Spawn");
        }
    }

    private void resetEnemyBornCount()
    {
        bornCount = 0;
    }

    private void Spawn()
    {
        //Debug.Log(transform.parent.name + " => " + enemyBornCount);

        if (bornCount == BornMaximumCount)
        {
            CancelInvoke("Spawn");
        }
        else
        {
            StartCoroutine(InitializeEnemyThenAssignTarget());
        }
    }

    IEnumerator InitializeEnemyThenAssignTarget()
    {
        ///The model of enemies is only total are 20 kinds.
        long index = Checkpoint % 20;
        Debug.Log("Checkpoint: " + Checkpoint);

        GameObject enemy = enemys[index];
        GameObject instance = Instantiate(enemy, transform.position, transform.rotation);
        yield return new WaitUntil(() => instance != null);
        Debug.Log("Initialize the enemy");

        int cc = Checkpoint + 1;
        EnemyProperty enemyProperty = instance.GetComponent<EnemyProperty>();
        enemyProperty.CC = cc;
        Debug.Log("Asign the CC to EnemyProperty");

        instance.SetActive(true);
        yield return new WaitUntil(() => instance.activeSelf == true);
        Debug.Log("Set the enemy status of active to true");

        bornCount++;
        yield break;
    }

    private void NextLeve(long cp, System.Action<bool> task)
    {
        if (cp >= 20)
        {
            task(true);
            return;
        }
        task(false);
    }
}
