﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(EnemyNav))]
public class EnemyRemoteAttack : MonoBehaviour
{

    [SerializeField]
    private int delayBetweenEachShooting = 1;

    [SerializeField]
    private int _damage;
    internal int Damage { get { return _damage; } set { _damage = value; } }

    [SerializeField]
    private SimpleBulletLauncher simpleBulletLauncher;

    private Animator animator;
    private EnemyNav enemyNav;
    private bool didLaunch = false;

    private void Start()
    {
        Debug.Log("Started Initialize the EnemyRemoteAttack...");

        animator = transform.GetComponent<Animator>();
        enemyNav = transform.GetComponent<EnemyNav>();
        enemyNav.OnNavStoppedAtTarget += OnNavStoppedAtTarget;

        simpleBulletLauncher.OnSimpleBulletHitedTarget += SimpleBulletLauncher_OnSimpleBulletHitedTarget;
    }

    private void OnNavStoppedAtTarget(bool status)
    {
        //Debug.Log("didLaunch");
        StartCoroutine(Shooting(status));
    }

    private void OnDestroy()
    {
        enemyNav.OnNavStoppedAtTarget -= OnNavStoppedAtTarget;
        simpleBulletLauncher.OnSimpleBulletHitedTarget -= SimpleBulletLauncher_OnSimpleBulletHitedTarget;
    }

    IEnumerator Shooting(bool status)
    {
        switch (status)
        {
            case false:
                CancelInvoke();
                animator.SetBool(ProjectKeywordList.kAnimationForAttack, status);
                break;
            case true:
                InvokeRepeating("ContinuousShooting", 0, delayBetweenEachShooting);
                break;
        }

        yield break;
    }

    private void ContinuousShooting()
    {
        animator.SetBool(ProjectKeywordList.kAnimationForAttack, true);

        if (!didLaunch)
        {
            didLaunch = true;
            simpleBulletLauncher.target = enemyNav.Target;
            if (transform.tag == ProjectKeywordList.kTagForEnemy) { Damage = GetComponent<EnemyProperty>().Damage; }
            simpleBulletLauncher.Damage = Damage;
            simpleBulletLauncher.fixedBulletSpeed = 1.5f;
            simpleBulletLauncher.maximumRange = enemyNav.DefaultStopDis;
            simpleBulletLauncher.LauncinghBullet();
        }
    }

    private void SimpleBulletLauncher_OnSimpleBulletHitedTarget(Collider other)
    {
        didLaunch = false;
    }
}
