﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingTextControl : MonoBehaviour
{
    private static FloatingText popupText;
    private static GameObject USERINTERFACEMANAGER;

    public static void Initialize()
    {
        USERINTERFACEMANAGER = GameObject.Find("USERINTERFACEMANAGER");
        if (!popupText)
            popupText = Resources.Load<FloatingText>("Prefab/UserInterface/PopupTextParent");            
    }

    public static void CreateFloatingText(string text, Vector3 contactPoint, bool explosionEffect = true)
    {
        FloatingText instance = Instantiate(popupText);
        instance.gameObject.transform.Find("Background").GetComponent<Image>().enabled = explosionEffect;
        Vector2 screenPos = Camera.main.WorldToScreenPoint(contactPoint);
        //Vector2 screenPos = Camera.main.WorldToScreenPoint(new Vector2(location.position.x, Random.Range(-.5f, .5f)));
        instance.transform.SetParent(USERINTERFACEMANAGER.transform, false);
        instance.transform.position = screenPos;
        instance.SetText(text);
    }
}
