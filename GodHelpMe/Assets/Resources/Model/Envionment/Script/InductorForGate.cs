﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InductorForGate : MonoBehaviour
{

    [SerializeField]
    private GameObject devilGate;

    [SerializeField]
    private GameObject devilHandHurtManager;

    [SerializeField]
    private bool gateStatus;

    private bool played = true;

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(playAnimation(gateStatus, other));
    }

    IEnumerator playAnimation(bool status, Collider other)
    {

        if (other.gameObject.tag == ("Enemy"))
        {
            GameObject enemy = other.gameObject;
            EnemyNav nav = enemy.GetComponent<EnemyNav>();
            played = !nav.spawned;

            Animation devilGateAnim = devilGate.GetComponent<Animation>();
            if (played == false)
            {
                if (status)
                {
                    Debug.Log("open");
                    devilGateAnim["open_and_close"].speed = 0.5f;
                    played = true;
                }
                else
                {
                    devilGateAnim["open_and_close"].time = devilGateAnim["open_and_close"].length;
                    devilGateAnim["open_and_close"].speed = -0.5f;
                    Debug.Log("close");
                    played = true;
                }

                devilGateAnim.Play("open_and_close", PlayMode.StopAll);
            }
        }

        if (other.gameObject.tag == ("Player"))
        {
            RangeHurtManager rangeHurtManager = devilHandHurtManager.GetComponent<RangeHurtManager>();

            rangeHurtManager.ExecuteRangeAttack();
        }

        yield return null;
    }
}
