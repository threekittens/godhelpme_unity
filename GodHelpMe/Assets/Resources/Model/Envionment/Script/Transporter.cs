﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transporter : MonoBehaviour
{

    [SerializeField]
    private Transform spawnPoint;

    [SerializeField]
    private GameObject Transmit_Up;

    [SerializeField]
    private GameObject Transmit_Down;

    [SerializeField]
    private EnemyTarget targetTag;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != tagByEnemyTargets(targetTag))
        {
            return;
        }
        StartCoroutine(PlayParticle(other));
    }

    string tagByEnemyTargets(EnemyTarget target)
    {
        var rtn = "";
        switch (target)
        {
            case EnemyTarget.Player:
                rtn = "Player";
                break;
            case EnemyTarget.TowerCore:
                rtn = "TowerCore";
                break;
            case EnemyTarget.Tower01:
                rtn = "Tower01";
                break;
            case EnemyTarget.Tower02:
                rtn = "Tower02";
                break;
            case EnemyTarget.Tower03:
                rtn = "Tower03";
                break;
            case EnemyTarget.Tower04:
                rtn = "Tower04";
                break;
            case EnemyTarget.Tower05:
                rtn = "Tower05";
                break;
        }

        return rtn;
    }

    IEnumerator PlayParticle(Collider other)
    {        
        //==========================================================================
        //获取玩家物件
        GameObject player = other.gameObject;

        //关闭玩家移动脚本
        player.GetComponent<Motor>().enabled = false;



        //==========================================================================

        //起动物件
        Transmit_Up.SetActive(true);

        //获取传送特效Up物件
        ParticleSystem transmitUpEffect = Transmit_Up.GetComponent<ParticleSystem>();
        
        //改变特效位置至玩家位置
        Transmit_Up.transform.SetPositionAndRotation(player.transform.position, Quaternion.identity);

        //播放特效
        transmitUpEffect.Play();

        //暂停1秒
        yield return new WaitForSeconds(1f);
        
        //关闭物件
        Transmit_Up.gameObject.SetActive(false);

        //传送玩家至新的指定位置
        player.transform.SetPositionAndRotation(spawnPoint.position, Quaternion.identity);

        //==========================================================================
        //起动物件
        Transmit_Down.SetActive(true);

        //获取传送特效Down物件
        ParticleSystem transmitDownEffect = Transmit_Down.GetComponent<ParticleSystem>();
        
        //改变特效位置至玩家位置
        Transmit_Down.transform.SetPositionAndRotation(player.transform.position, Quaternion.identity);
        
        //播放特效
        transmitDownEffect.Play();

        //暂停1秒
        yield return new WaitForSeconds(1f);

        //关闭物件
        Transmit_Down.gameObject.SetActive(false);

        //==========================================================================
        //起动玩家移动脚本
        player.GetComponent<Motor>().enabled = true;

    }
}
